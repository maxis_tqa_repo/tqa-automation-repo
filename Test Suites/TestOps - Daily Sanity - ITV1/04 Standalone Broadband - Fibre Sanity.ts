<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>04 Standalone Broadband - Fibre Sanity</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>b81a29fa-55ea-4fb0-a177-5b1e48dd4df4</testSuiteGuid>
   <testCaseLink>
      <guid>de1c9c61-b831-40a4-b241-7850fcd9f487</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/00 Daily Sanity/04 - Standalone Fibre Broadband/04.01 Provide Order</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>274f7887-0efa-4bec-b895-3bccd3c34964</id>
         <iterationEntity>
            <iterationType>RANGE</iterationType>
            <value>4-4</value>
         </iterationEntity>
         <testDataId>Data Files/Test Data and Input Files/Daily Sanity - Airtable POC/Daily Sanity - Airtable POC</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>274f7887-0efa-4bec-b895-3bccd3c34964</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>idValue</value>
         <variableId>927df28c-74c2-4cf7-b30f-0f5239695225</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>274f7887-0efa-4bec-b895-3bccd3c34964</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>planId</value>
         <variableId>8f0f7784-538f-4847-947d-467d6c68eaa3</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>274f7887-0efa-4bec-b895-3bccd3c34964</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>customerName</value>
         <variableId>06787115-900b-4e5a-b450-2a18710d557c</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>274f7887-0efa-4bec-b895-3bccd3c34964</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>row</value>
         <variableId>202eadf4-5a18-4d54-bd7f-da55423ccc4b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>SCRIPT_VARIABLE</type>
         <value>&quot;FOREIGN ID CARD&quot;</value>
         <variableId>5af50f13-032a-446f-933c-f627e97c0662</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>ede9b75c-ae2a-4ae5-8203-a440407ca242</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/00 Daily Sanity/04 - Standalone Fibre Broadband/04.02 Order Completion</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>d7546888-d41a-4975-b6b0-9bcb824ad768</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/00 Daily Sanity/04 - Standalone Fibre Broadband/04.03 Search Subscription</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>SCRIPT_VARIABLE</type>
         <value>&quot;A30506&quot;</value>
         <variableId>2f4ed476-39b2-4d71-824b-d6b04e9fb2c2</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>SCRIPT_VARIABLE</type>
         <value>&quot;Maxis Fibre - 100Mbps&quot;</value>
         <variableId>b28744ac-97ff-43f6-a469-36fefdca532c</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
