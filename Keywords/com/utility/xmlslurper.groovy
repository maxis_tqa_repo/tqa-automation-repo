package com.utility

import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration

/**
 * Returns a string containing references to all Test Cases found in the
 * specified Test Suite.
 * <p>
 * Supports the <b>TestCase Lister</b> tool used for Test Case/Test Suite analysis.
 *
 * @param suiteName (String) The Test Suite to be examined.
 * @param before (String) Typically used to pass HTML to the output.
 * @param after (String) Typically used to pass HTML to the output.
 * @return String.
 */


static String getSuiteTests(String suiteName, String before = "", String after = "") {
	int count = 0
	String projDir = RunConfiguration.getProjectDir()
	String fname = projDir + "/Test Suites/" + suiteName + ".ts"
	String xmlText = new File(fname).getText()
	def testList = new XmlSlurper().parseText(xmlText)
	String output = ""

	testList.testCaseLink.testCaseId.each {
		count++
		output += before + count.toString() + ": " + it.toString() + after + "\n\n"
	}
	return output
}