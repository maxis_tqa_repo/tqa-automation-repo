package com.database

//import com.microsoft.sqlserver.jdbc.SQLServerDriver

import java.sql.DriverManager

import java.sql.ResultSet

import java.sql.Statement

import com.kms.katalon.core.annotation.Keyword

import java.sql.Connection

public class connectSql {

	private static Connection connection = null;

	/**
	 * Open and return a connection to database
	 * @param dataFile absolute file path
	 * @return an instance of java.sql.Connection
	 */

	//Establishing a connection to the DataBase

	@Keyword

	//	def connectDB(String url, String dbname, String port, String username, String password){

	def connectDB(String url, String dbname, String port, String username, String password){

		//Load driver class for your specific database type
		//String conn = "jdbc:sqlserver://" + url + ":" + port + "/" + dbname


		String conn = "jdbc:sqlserver://" + url + ":" + port

		//Class.forName("org.sqlite.JDBC")

		//String connectionString = "jdbc:sqlite:" + dataFile

		if(connection != null && !connection.isClosed()){

			connection.close()

		}

		connection = DriverManager.getConnection(conn, username, password)

		return connection

	}

	/**
	 * execute a SQL query on database
	 * @param queryString SQL query string
	 * @return a reference to returned data collection, an instance of java.sql.ResultSet
	 */

	//Executing the constructed Query and Saving results in result set

	@Keyword

	def executeQuery(String queryString) {

		Statement stm = connection.createStatement()

		ResultSet rs = stm.executeQuery(queryString)

		return rs

	}

	//Closing the connection

	@Keyword

	def closeDatabaseConnection() {

		if(connection != null && !connection.isClosed()){

			connection.close()

		}

		connection = null

	}

	/**
	 * Execute non-query (usually INSERT/UPDATE/DELETE/COUNT/SUM...) on database
	 * @param queryString a SQL statement
	 * @return single value result of SQL statement
	 */

	@Keyword

	def execute(String queryString) {

		Statement stm = connection.createStatement()

		boolean result = stm.execute(queryString)

		return result

	}

	/**
	 * Execute insert query into AMR with variable IC value
	 */

	@Keyword

	def insertCustomerDetailAMR(String idValue, String customerName) {

		String amrQuery = "insert into MyKadWS_Info(fullname,new_ic,old_ic,finger_match,finger_thumb,date_of_birth,gender,race,nationality,religion,address1,address2,address3,postcode,city,state,photo,date_created,created_by,last_updated,modified_by,finger_image,reader_model,reader_serial,application_id)values('" + customerName + "','" + idValue + "',NULL,'Y','1','1991-01-01 00:00:00.000','LELAKI','MELAYU','WARGANEGARA','ISLAM','A6-03A','GREEN SURIA APARTMENT','JALAN GREEN SURIA','40000','Kuala Lumpur','WP KUALA LUMPUR',NULL,getdate(),'CNTRL.00030',getdate(),'CNTRL.00030',NULL,'ZF1','10031188','6');"

		Statement stm = connection.createStatement()

		boolean result = stm.execute(amrQuery)

		return result

	}

}
