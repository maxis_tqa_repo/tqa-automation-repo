1
00:00:01,293 --> 00:00:02,386
1. callTestCase(findTestCase("01_Retailer Portal/RP-TMPL-01 NEW PLAN REGISTRATION NO DEVICE"), ["idType":idType, "idValue":idValue, "planId":planId, "customerName":customerName, "row":row, "msisdn":"", "accountNo":"", ... ], STOP_ON_FAILURE)

2
00:00:02,417 --> 00:00:03,767
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), [:], STOP_ON_FAILURE)

3
00:00:03,771 --> 00:00:15,589
1. openBrowser("")

4
00:00:15,598 --> 00:00:41,690
7. URL is set as a Global Variable controlled in Profiles

5
00:00:41,696 --> 00:00:43,925
13. Username is set as local variable

6
00:00:43,932 --> 00:00:45,831
19. Password is set as local variable

7
00:00:45,839 --> 00:00:46,924
23. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

8
00:00:46,930 --> 00:00:46,957
27. comment("ASSERTION")

9
00:00:46,965 --> 00:00:48,889
33. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

10
00:00:48,899 --> 00:00:48,906
5. runId = toString().substring(toString().lastIndexOf("\") + 1)

11
00:00:48,915 --> 00:00:49,989
9. callTestCase(findTestCase("01_Retailer Portal/RP02_Search Customer_Page/RP02_1 Search Customer"), ["idType":idType, "idValue":idValue, "accountNo":accountNo], STOP_ON_FAILURE)

12
00:00:49,998 --> 00:00:51,026
1. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/i_keyboard_arrow_down"))

13
00:00:51,033 --> 00:00:51,040
5. comment("IF statement to check if Account No is populated in the input sheet. If TRUE idType will be forced to ACCOUNT and search done using the accountNo")

14
00:00:51,053 --> 00:00:51,056
9. if (accountNo.isEmpty() == false)

15
00:00:51,063 --> 00:00:52,214
13. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/Dropdown ID Type", ["idType":idType]))

16
00:00:52,223 --> 00:00:54,177
17. setText(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), idValue)

17
00:00:54,181 --> 00:00:55,282
21. sendKeys(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), Keys.chord(ENTER))

18
00:00:55,287 --> 00:00:55,293
25. comment("Checking for text "Customer Profile does not exist", if text exists the Global Variable newCustomer_Flag is set to TRUE and flow handling will be for New Customer ")

19
00:00:55,299 --> 00:00:56,430
29. newCustomer_Flag = verifyTextPresent("Customer Profile does not exist", false, OPTIONAL)

20
00:00:56,437 --> 00:00:58,021
13. callTestCase(findTestCase("01_Retailer Portal/RP03_CIC Dashboard_Page/RP03_1 New Purchase"), ["accountNo":accountNo], STOP_ON_FAILURE)

21
00:00:58,029 --> 00:00:58,034
1. comment("Different Call To Action elements will be loaded depending on whether the customer is new or an existing one")

22
00:00:58,045 --> 00:00:58,182
5. comment("1. New Purchase button for New Customer, 2. New Purchase button for Existing Customer Existing Account, 3. New Purchase button for Existing Customer New Account")

23
00:00:58,198 --> 00:00:58,346
9. if (newCustomer_Flag == true)

24
00:00:58,373 --> 00:01:01,712
1. click(findTestObject("01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/div_New Purchase"))

25
00:01:01,719 --> 00:01:01,725
13. comment("ASSERTION")

26
00:01:01,732 --> 00:01:14,776
17. verifyTextPresent("Catalog", false)

27
00:01:14,783 --> 00:01:15,740
17. callTestCase(findTestCase("01_Retailer Portal/RP04_Catalog_Page/RP04_1 Purchase plan only, no device"), ["planId":planId], STOP_ON_FAILURE)

28
00:01:15,748 --> 00:01:16,987
3. Click mobile plan tab

29
00:01:16,994 --> 00:01:18,295
7. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.button.planProduct", ["planProduct":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3)]))

30
00:01:18,302 --> 00:01:50,092
11. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.label.planType", ["planType":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 2)]), OPTIONAL)

31
00:01:50,098 --> 00:01:50,146
15. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

32
00:01:50,157 --> 00:01:50,158
19. if (isFibreStandAlone == true)

33
00:01:50,170 --> 00:01:50,172
23. if (isFibreStandAlone == false)

34
00:01:50,180 --> 00:01:51,232
21. callTestCase(findTestCase("01_Retailer Portal/RP05_Product Configuration_Page/RP05_1 Skip Product Configuration"), ["planId":planId], STOP_ON_FAILURE)

35
00:01:51,240 --> 00:01:51,256
1. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

36
00:01:51,266 --> 00:01:51,267
5. if (isFibreStandAlone == true)

37
00:01:51,275 --> 00:01:51,282
9. if (isFibreStandAlone == false)

38
00:01:51,290 --> 00:01:52,302
25. callTestCase(findTestCase("01_Retailer Portal/RP06_Cart_Page/RP06_1 Cart Page"), ["msisdn":msisdn, "broadbandPlan":broadbandPlan, "planId":planId], STOP_ON_FAILURE)

39
00:01:52,312 --> 00:01:52,329
1. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

40
00:01:52,340 --> 00:01:52,342
5. if (isFibreStandAlone == true)

41
00:01:52,347 --> 00:01:53,585
1. callTestCase(findTestCase("01_Retailer Portal/RP06A_Link_to_Broadband/RP06A_2 Add Broadband - Fibre"), ["broadbandPlan":planId], STOP_ON_FAILURE)

42
00:01:53,593 --> 00:01:56,078
1. click(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_bundle-list/button_I want this plan", ["broadbandPlan":broadbandPlan]))

43
00:01:56,091 --> 00:01:57,211
5. click(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/input_search_searchAddresInput"))

44
00:01:57,230 --> 00:01:58,241
9. click(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/div_search"))

45
00:01:58,251 --> 00:02:00,293
13. setText(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/input_search_searchAddresInput"), "no 1, Jalan Medan Masria, Taman Masria, 43200 Cheras, Selangor")

46
00:02:00,301 --> 00:02:01,391
17. sendKeys(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/input_search_searchAddresInput"), Keys.chord(ENTER))

47
00:02:01,412 --> 00:02:05,379
21. click(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/i_navigate_next"))

48
00:02:05,393 --> 00:02:07,511
25. setText(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/input_access_time_providerId"), "1-12345678901")

49
00:02:07,523 --> 00:02:10,019
29. setText(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/input_Sample Format_serviceId"), "BU1234567890")

50
00:02:10,025 --> 00:02:11,232
33. click(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/button_Submit and Continue"))

51
00:02:11,244 --> 00:02:17,793
37. click(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_bundle-configuration/button_Next"))

52
00:02:17,799 --> 00:02:20,553
41. enhancedClick(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_bundle-configuration/button_Continue_to_Cart"))

53
00:02:20,560 --> 00:02:22,610
45. delay(2)

54
00:02:22,616 --> 00:02:23,704
5. callTestCase(findTestCase("01_Retailer Portal/RP06A_Link_to_Broadband/RP06A_3 Assign VoIP number"), [:], STOP_ON_FAILURE)

55
00:02:23,711 --> 00:02:26,005
1. click(findTestObject("01_Retailer Portal_Objects/RP06_Cart_Page_Objects/a_Assign Number"))

56
00:02:26,013 --> 00:02:27,590
5. click(findTestObject("01_Retailer Portal_Objects/RP06B VoIP Number Assignment/p_60382103247"))

57
00:02:27,596 --> 00:02:28,837
9. click(findTestObject("01_Retailer Portal_Objects/RP06B VoIP Number Assignment/button_Continue"))

58
00:02:28,843 --> 00:02:32,292
9. click(findTestObject("01_Retailer Portal_Objects/RP06_Cart_Page_Objects/button_Check Out"))

59
00:02:32,305 --> 00:02:32,311
9. if (isFibreStandAlone == false)

60
00:02:32,324 --> 00:02:33,360
29. callTestCase(findTestCase("01_Retailer Portal/RP07_Create Customer_Page/RP07_1 Create Customer_Page"), ["idType":idType, "customerName":customerName, "accountNo":accountNo, "idValue":idValue], STOP_ON_FAILURE)

61
00:02:33,366 --> 00:02:33,379
1. isMalaysianCitizen = verifyMatch(idType, "MALAYSIAN ID CARD", false, OPTIONAL)

62
00:02:33,394 --> 00:02:33,395
5. if (accountNo.isEmpty() == false)

63
00:02:33,405 --> 00:02:33,406
9. if (accountNo.isEmpty() == true)

64
00:02:33,417 --> 00:02:56,090
1. verifyTextPresent("Customer Details", false)

65
00:02:56,095 --> 00:02:56,097
5. if (newCustomer_Flag == true)

66
00:02:56,107 --> 00:02:56,108
1. if (isMalaysianCitizen == false)

67
00:02:56,116 --> 00:02:57,108
1. callTestCase(findTestCase("01_Retailer Portal/RP07_Create Customer_Page/RP07_1_1 Fill Contact Address"), ["customerName":customerName], STOP_ON_FAILURE)

68
00:02:57,121 --> 00:02:58,202
1. click(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/button_Manual Entry"), OPTIONAL)

69
00:02:58,231 --> 00:03:00,396
5. setText(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/input_Non-Malaysian_nameAsPerId"), customerName, OPTIONAL)

70
00:03:00,408 --> 00:03:01,439
9. click(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/button_month"), OPTIONAL)

71
00:03:01,450 --> 00:03:02,550
13. click(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/a_Aug"), OPTIONAL)

72
00:03:02,558 --> 00:03:04,595
17. setText(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/input_Dec_day"), "31", OPTIONAL)

73
00:03:04,605 --> 00:03:06,564
21. setText(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/input_Dec_year"), "1957", OPTIONAL)

74
00:03:06,581 --> 00:03:07,670
25. click(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/label_Male"), OPTIONAL)

75
00:03:07,678 --> 00:03:08,849
29. click(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/button_Validate Passport Details"), OPTIONAL)

76
00:03:08,867 --> 00:03:11,238
33. setText(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/input_Address_contactAddreslineOne"), "addressLine1")

77
00:03:11,246 --> 00:03:13,383
37. setText(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/input_Address_contactAddreslineTwo"), "addressLine2")

78
00:03:13,399 --> 00:03:15,446
41. setText(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/input_Address_contactAddreslineThree"), "addressLine3")

79
00:03:15,454 --> 00:03:17,520
45. setText(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/input_Address_contactPostalCode"), "43200")

80
00:03:17,527 --> 00:03:18,779
49. click(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/div_(function(loadWidget)        var domPla_a0cbbf_1"))

81
00:03:18,801 --> 00:03:19,814
5. callTestCase(findTestCase("01_Retailer Portal/RP07_Create Customer_Page/RP07_1_4 Fill Contact Details Section"), [:], STOP_ON_FAILURE)

82
00:03:19,824 --> 00:03:21,836
1. setText(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/input_Contact Details_email"), Email)

83
00:03:21,841 --> 00:03:23,975
5. setText(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/input_Invalid Email_contactNumber"), "601234567890")

84
00:03:23,983 --> 00:03:25,470
9. callTestCase(findTestCase("01_Retailer Portal/RP07_Create Customer_Page/RP07_1_2 Use Contact Address as Billing Address"), [:], STOP_ON_FAILURE)

85
00:03:25,484 --> 00:03:26,660
1. click(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/label_Use Contact Address as Billing Address"))

86
00:03:26,669 --> 00:03:28,845
5. setText(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/input_Receive a monthly email notification _bc6444"), Email)

87
00:03:28,857 --> 00:03:29,933
9. click(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/div_Regular Paper Bill_CustomCheckbox"))

88
00:03:29,940 --> 00:03:31,126
13. click(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/button_Continue"))

89
00:03:31,137 --> 00:03:31,139
5. if (isMalaysianCitizen == true)

90
00:03:31,164 --> 00:03:31,170
9. if (newCustomer_Flag == false)

91
00:03:31,184 --> 00:03:32,679
33. callTestCase(findTestCase("01_Retailer Portal/RP08_Order Summary_Page/RP08_1 Document Upload and Submit Order"), ["idType":idType], STOP_ON_FAILURE)

92
00:03:32,694 --> 00:03:38,723
1. verifyTextPresent("Order Summary", false, CONTINUE_ON_FAILURE)

93
00:03:38,730 --> 00:03:39,823
5. click(findTestObject("01_Retailer Portal_Objects/RP08_Order Summary_Page_Objects/i_keyboard_arrow_down"))

94
00:03:39,833 --> 00:03:40,909
9. click(findTestObject("01_Retailer Portal_Objects/RP08_Order Summary_Page_Objects/DropDownID_Type", ["idType":idType]))

95
00:03:40,916 --> 00:03:40,919
13. dirName = getProjectDir()

96
00:03:40,928 --> 00:03:40,929
17. upload = "" + dirName + "\Data Files\Test Data and Input Files\Uploads\maxis.jpg"

97
00:03:40,937 --> 00:03:40,939
21. uploadPath = upload.replace("/", "\")

98
00:03:40,953 --> 00:03:48,160
25. com.uploadfile.UploadFile.uploadFileUsingRobot(findTestObject("01_Retailer Portal_Objects/RP08_Order Summary_Page_Objects/div_Browse"), uploadPath)

99
00:03:48,172 --> 00:03:48,182
29. delay(0)

100
00:03:48,198 --> 00:04:20,538
33. enhancedClick(findTestObject("01_Retailer Portal_Objects/RP08_Order Summary_Page_Objects/checkbox_Terms and Condition"))

101
00:04:20,548 --> 00:04:21,653
37. click(findTestObject("01_Retailer Portal_Objects/RP08_Order Summary_Page_Objects/canvas_refresh_signature-pad-1"))

102
00:04:21,676 --> 00:04:22,779
41. click(findTestObject("01_Retailer Portal_Objects/RP08_Order Summary_Page_Objects/canvas_refresh_signature-pad-1"))

103
00:04:22,787 --> 00:04:24,937
45. setText(findTestObject("01_Retailer Portal_Objects/RP08_Order Summary_Page_Objects/textarea_Apply Waiver_remarks_super"), "Approved")

104
00:04:24,945 --> 00:04:26,086
49. click(findTestObject("01_Retailer Portal_Objects/RP08_Order Summary_Page_Objects/button_Approve"))

105
00:04:26,093 --> 00:04:27,377
37. callTestCase(findTestCase("01_Retailer Portal/RP09_Order Confirmation_Page/RP09_1 Verify order ID is given"), [:], STOP_ON_FAILURE)

106
00:04:27,385 --> 00:04:28,734
1. verifyTextPresent("Order Confirmed", false, CONTINUE_ON_FAILURE)

107
00:04:28,742 --> 00:04:42,674
5. click(findTestObject("01_Retailer Portal_Objects/RP09_Order Confirmation_Page_Objects/Order ID"))

