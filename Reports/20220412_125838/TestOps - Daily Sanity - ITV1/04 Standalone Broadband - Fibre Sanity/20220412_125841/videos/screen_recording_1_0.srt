1
00:00:01,308 --> 00:00:02,321
1. callTestCase(findTestCase("01_Retailer Portal/RP-TMPL-01 NEW PLAN REGISTRATION NO DEVICE"), ["idType":"MALAYSIAN ID CARD", "idValue":idValue, "planId":planId, "customerName":customerName, "row":row, "msisdn":"", "accountNo":"", ... ], STOP_ON_FAILURE)

2
00:00:02,350 --> 00:00:03,434
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), [:], STOP_ON_FAILURE)

3
00:00:03,439 --> 00:00:16,105
1. openBrowser("")

4
00:00:16,110 --> 00:00:40,329
7. URL is set as a Global Variable controlled in Profiles

5
00:00:40,336 --> 00:00:42,412
13. Username is set as local variable

6
00:00:42,418 --> 00:00:44,373
19. Password is set as local variable

7
00:00:44,380 --> 00:00:45,471
23. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

8
00:00:45,479 --> 00:00:45,512
27. comment("ASSERTION")

9
00:00:45,523 --> 00:00:47,623
33. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

10
00:00:47,634 --> 00:00:47,638
5. runId = toString().substring(toString().lastIndexOf("\") + 1)

11
00:00:47,646 --> 00:00:48,681
9. callTestCase(findTestCase("01_Retailer Portal/RP02_Search Customer_Page/RP02_1 Search Customer"), ["idType":idType, "idValue":idValue, "accountNo":accountNo], STOP_ON_FAILURE)

12
00:00:48,687 --> 00:00:49,681
1. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/i_keyboard_arrow_down"))

13
00:00:49,701 --> 00:00:49,707
5. comment("IF statement to check if Account No is populated in the input sheet. If TRUE idType will be forced to ACCOUNT and search done using the accountNo")

14
00:00:49,720 --> 00:00:49,722
9. if (accountNo.isEmpty() == false)

15
00:00:49,732 --> 00:00:50,887
13. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/Dropdown ID Type", ["idType":idType]))

16
00:00:50,898 --> 00:00:52,804
17. setText(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), idValue)

17
00:00:52,808 --> 00:00:53,874
21. sendKeys(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), Keys.chord(ENTER))

18
00:00:53,881 --> 00:00:53,888
25. comment("Checking for text "Customer Profile does not exist", if text exists the Global Variable newCustomer_Flag is set to TRUE and flow handling will be for New Customer ")

19
00:00:53,897 --> 00:00:54,824
29. newCustomer_Flag = verifyTextPresent("Customer Profile does not exist", false, OPTIONAL)

20
00:00:54,830 --> 00:00:56,222
13. callTestCase(findTestCase("01_Retailer Portal/RP03_CIC Dashboard_Page/RP03_1 New Purchase"), ["accountNo":accountNo], STOP_ON_FAILURE)

21
00:00:56,229 --> 00:00:56,236
1. comment("Different Call To Action elements will be loaded depending on whether the customer is new or an existing one")

22
00:00:56,251 --> 00:00:56,257
5. comment("1. New Purchase button for New Customer, 2. New Purchase button for Existing Customer Existing Account, 3. New Purchase button for Existing Customer New Account")

23
00:00:56,270 --> 00:00:56,273
9. if (newCustomer_Flag == true)

24
00:00:56,282 --> 00:00:59,138
1. click(findTestObject("01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/div_New Purchase"))

25
00:00:59,146 --> 00:00:59,157
13. comment("ASSERTION")

26
00:00:59,179 --> 00:01:12,883
17. verifyTextPresent("Catalog", false)

27
00:01:12,890 --> 00:01:13,902
17. callTestCase(findTestCase("01_Retailer Portal/RP04_Catalog_Page/RP04_1 Purchase plan only, no device"), ["planId":planId], STOP_ON_FAILURE)

28
00:01:13,908 --> 00:01:15,324
3. Click mobile plan tab

29
00:01:15,328 --> 00:01:16,647
7. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.button.planProduct", ["planProduct":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3)]))

30
00:01:16,654 --> 00:01:48,590
11. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.label.planType", ["planType":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 2)]), OPTIONAL)

31
00:01:48,597 --> 00:01:48,667
15. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

32
00:01:48,679 --> 00:01:48,681
19. if (isFibreStandAlone == true)

33
00:01:48,689 --> 00:01:48,693
23. if (isFibreStandAlone == false)

34
00:01:48,701 --> 00:01:49,697
21. callTestCase(findTestCase("01_Retailer Portal/RP05_Product Configuration_Page/RP05_1 Skip Product Configuration"), ["planId":planId], STOP_ON_FAILURE)

35
00:01:49,704 --> 00:01:49,722
1. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

36
00:01:49,730 --> 00:01:49,732
5. if (isFibreStandAlone == true)

37
00:01:49,740 --> 00:01:49,744
9. if (isFibreStandAlone == false)

38
00:01:49,752 --> 00:01:50,804
25. callTestCase(findTestCase("01_Retailer Portal/RP06_Cart_Page/RP06_1 Cart Page"), ["msisdn":msisdn, "broadbandPlan":broadbandPlan, "planId":planId], STOP_ON_FAILURE)

39
00:01:50,808 --> 00:01:50,823
1. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

40
00:01:50,833 --> 00:01:50,834
5. if (isFibreStandAlone == true)

41
00:01:50,843 --> 00:01:51,792
1. callTestCase(findTestCase("01_Retailer Portal/RP06A_Link_to_Broadband/RP06A_2 Add Broadband - Fibre"), ["broadbandPlan":planId], STOP_ON_FAILURE)

42
00:01:51,796 --> 00:01:54,414
1. click(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_bundle-list/button_I want this plan", ["broadbandPlan":broadbandPlan]))

43
00:01:54,422 --> 00:01:55,620
5. click(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/input_search_searchAddresInput"))

44
00:01:55,629 --> 00:01:56,675
9. click(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/div_search"))

45
00:01:56,681 --> 00:01:58,691
13. setText(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/input_search_searchAddresInput"), "no 1, Jalan Medan Masria, Taman Masria, 43200 Cheras, Selangor")

46
00:01:58,697 --> 00:01:59,750
17. sendKeys(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/input_search_searchAddresInput"), Keys.chord(ENTER))

47
00:01:59,758 --> 00:02:03,740
21. click(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/i_navigate_next"))

48
00:02:03,748 --> 00:02:05,809
25. setText(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/input_access_time_providerId"), "1-12345678901")

49
00:02:05,819 --> 00:02:07,764
29. setText(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/input_Sample Format_serviceId"), "BU1234567890")

50
00:02:07,777 --> 00:02:09,003
33. click(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/button_Submit and Continue"))

51
00:02:09,016 --> 00:02:15,648
37. click(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_bundle-configuration/button_Next"))

52
00:02:15,659 --> 00:02:18,172
41. enhancedClick(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_bundle-configuration/button_Continue_to_Cart"))

53
00:02:18,177 --> 00:02:20,217
45. delay(2)

54
00:02:20,226 --> 00:02:21,312
5. callTestCase(findTestCase("01_Retailer Portal/RP06A_Link_to_Broadband/RP06A_3 Assign VoIP number"), [:], STOP_ON_FAILURE)

55
00:02:21,325 --> 00:02:23,474
1. click(findTestObject("01_Retailer Portal_Objects/RP06_Cart_Page_Objects/a_Assign Number"))

56
00:02:23,484 --> 00:02:25,083
5. click(findTestObject("01_Retailer Portal_Objects/RP06B VoIP Number Assignment/p_60382103247"))

57
00:02:25,094 --> 00:02:26,249
9. click(findTestObject("01_Retailer Portal_Objects/RP06B VoIP Number Assignment/button_Continue"))

58
00:02:26,263 --> 00:02:29,768
9. click(findTestObject("01_Retailer Portal_Objects/RP06_Cart_Page_Objects/button_Check Out"))

59
00:02:29,775 --> 00:02:29,781
9. if (isFibreStandAlone == false)

60
00:02:29,797 --> 00:02:31,370
29. callTestCase(findTestCase("01_Retailer Portal/RP07_Create Customer_Page/RP07_1 Create Customer_Page"), ["idType":idType, "customerName":customerName, "accountNo":accountNo, "idValue":idValue], STOP_ON_FAILURE)

61
00:02:31,376 --> 00:02:31,382
1. isMalaysianCitizen = verifyMatch(idType, "MALAYSIAN ID CARD", false, OPTIONAL)

62
00:02:31,396 --> 00:02:31,398
5. if (accountNo.isEmpty() == false)

63
00:02:31,411 --> 00:02:31,414
9. if (accountNo.isEmpty() == true)

64
00:02:31,426 --> 00:02:32,932
1. verifyTextPresent("Customer Details", false)

65
00:02:32,940 --> 00:02:32,941
5. if (newCustomer_Flag == true)

66
00:02:32,952 --> 00:02:32,953
1. if (isMalaysianCitizen == false)

67
00:02:32,963 --> 00:02:32,964
5. if (isMalaysianCitizen == true)

68
00:02:32,980 --> 00:02:33,923
1. callTestCase(findTestCase("01_Retailer Portal/RP07_Create Customer_Page/RP07_1_6 Insert Data into AMR"), ["idValue":idValue, "customerName":customerName], STOP_ON_FAILURE)

69
00:02:33,935 --> 00:02:34,875
1. com.database.connectSql.connectDB("10.200.50.177", "AMRDB", "1433", "amr_user", "amr_user2015")

70
00:02:34,887 --> 00:02:35,093
5. com.database.connectSql.insertCustomerDetailAMR(idValue, customerName)

71
00:02:35,102 --> 00:02:35,108
9. com.database.connectSql.closeDatabaseConnection()

72
00:02:35,112 --> 00:02:36,190
5. callTestCase(findTestCase("01_Retailer Portal/RP07_Create Customer_Page/RP07_1_5 Validate Contact Address"), [:], STOP_ON_FAILURE)

73
00:02:36,196 --> 00:02:37,389
1. click(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/button_Identity Verification"))

74
00:02:37,397 --> 00:02:38,374
9. callTestCase(findTestCase("01_Retailer Portal/RP07_Create Customer_Page/RP07_1_4 Fill Contact Details Section"), [:], STOP_ON_FAILURE)

75
00:02:38,381 --> 00:02:40,303
1. setText(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/input_Contact Details_email"), Email)

76
00:02:40,309 --> 00:02:42,355
5. setText(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/input_Invalid Email_contactNumber"), "601234567890")

77
00:02:42,368 --> 00:02:43,509
13. callTestCase(findTestCase("01_Retailer Portal/RP07_Create Customer_Page/RP07_1_2 Use Contact Address as Billing Address"), [:], STOP_ON_FAILURE)

78
00:02:43,515 --> 00:02:44,586
1. click(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/label_Use Contact Address as Billing Address"))

79
00:02:44,594 --> 00:02:46,602
5. setText(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/input_Receive a monthly email notification _bc6444"), Email)

80
00:02:46,607 --> 00:02:47,752
9. click(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/div_Regular Paper Bill_CustomCheckbox"))

81
00:02:47,760 --> 00:02:48,856
13. click(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/button_Continue"))

82
00:02:48,867 --> 00:02:48,873
9. if (newCustomer_Flag == false)

83
00:02:48,887 --> 00:02:49,964
33. callTestCase(findTestCase("01_Retailer Portal/RP08_Order Summary_Page/RP08_1 Document Upload and Submit Order"), ["idType":idType], STOP_ON_FAILURE)

84
00:02:49,971 --> 00:02:50,590
1. verifyTextPresent("Order Summary", false, CONTINUE_ON_FAILURE)

85
00:02:50,608 --> 00:03:24,341
5. click(findTestObject("01_Retailer Portal_Objects/RP08_Order Summary_Page_Objects/i_keyboard_arrow_down"))

