1
00:00:01,273 --> 00:00:03,099
1. callTestCase(findTestCase("01_Retailer Portal/RP-TMPL-04 PREPAID STARTER PACK REGISTRATION - POC"), ["idValue":idValue, "customerName":customerName, "msisdn":msisdn, "row":row], STOP_ON_FAILURE)

2
00:00:03,133 --> 00:00:04,108
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), ["username":"mcSup", "password":"123"], STOP_ON_FAILURE)

3
00:00:04,112 --> 00:00:14,148
1. openBrowser("")

4
00:00:14,156 --> 00:00:38,843
7. URL is set as a Global Variable controlled in Profiles

5
00:00:38,850 --> 00:00:40,934
13. Username is set as local variable

6
00:00:40,944 --> 00:00:42,799
19. Password is set as local variable

7
00:00:42,805 --> 00:00:43,867
23. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

8
00:00:43,875 --> 00:00:43,895
27. comment("ASSERTION")

9
00:00:43,902 --> 00:00:45,594
33. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

10
00:00:45,601 --> 00:00:46,541
5. callTestCase(findTestCase("01_Retailer Portal/RP07_Create Customer_Page/RP07_1_6 Insert Data into AMR"), ["idValue":idValue, "customerName":customerName], STOP_ON_FAILURE)

11
00:00:46,546 --> 00:00:47,494
1. com.database.connectSql.connectDB("10.200.50.177", "AMRDB", "1433", "amr_user", "amr_user2015")

12
00:00:47,502 --> 00:00:47,678
5. com.database.connectSql.insertCustomerDetailAMR(idValue, customerName)

13
00:00:47,686 --> 00:00:47,691
9. com.database.connectSql.closeDatabaseConnection()

14
00:00:47,698 --> 00:00:48,560
9. callTestCase(findTestCase("01_Retailer Portal/RP03_CIC Dashboard_Page/RP03_4 Starter Pack Registration"), [:], STOP_ON_FAILURE)

15
00:00:48,569 --> 00:00:50,219
1. click(findTestObject("01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/div_Starter Pack Registration"))

16
00:00:50,228 --> 00:00:51,094
13. callTestCase(findTestCase("98 Webservices/WS01_Airtable REST APIs/Get Inventory and marked as used"), ["inventoryType":"inventoryHotlinkStarterPack", "recordId":""], STOP_ON_FAILURE)

17
00:00:51,105 --> 00:00:51,660
1. callTestCase(findTestCase("98 Webservices/WS01_Airtable REST APIs/WS01_01 GET Available Inventory"), ["inventoryType":inventoryType], STOP_ON_FAILURE)

18
00:00:51,671 --> 00:00:54,271
1. response = sendRequest(findTestObject("98 Webservices Requests/WS1_Airtable_REST/GET Available Inventory", ["inventoryType":inventoryType]))

19
00:00:54,277 --> 00:00:54,293
5. slurper = new groovy.json.JsonSlurper()

20
00:00:54,301 --> 00:00:54,339
9. parsedJson = slurper.parseText(response.getResponseText())

21
00:00:54,346 --> 00:00:54,363
13. availableSim = simSerial

22
00:00:54,371 --> 00:00:54,373
17. simSerial = replace("[", "").replace("]", "")

23
00:00:54,383 --> 00:00:54,390
21. println(simSerial)

24
00:00:54,399 --> 00:00:54,401
25. simSerial = simSerial

25
00:00:54,409 --> 00:00:54,410
29. inventoryId = id

26
00:00:54,423 --> 00:00:54,425
33. recordId = replace("[", "").replace("]", "")

27
00:00:54,437 --> 00:00:54,439
37. println(recordId)

28
00:00:54,450 --> 00:00:54,451
41. id = recordId

29
00:00:54,456 --> 00:00:54,457
45. starterPackMsisdn = msisdn

30
00:00:54,463 --> 00:00:54,465
49. msisdn = replace("[", "").replace("]", "")

31
00:00:54,471 --> 00:00:54,473
53. println(msisdn)

32
00:00:54,480 --> 00:00:54,485
57. msisdn = msisdn

33
00:00:54,489 --> 00:00:55,408
5. callTestCase(findTestCase("98 Webservices/WS01_Airtable REST APIs/WS01_02 PATCH Used Inventory"), ["recordId":recordId], STOP_ON_FAILURE)

34
00:00:55,413 --> 00:00:55,415
1. recordId = id

35
00:00:55,420 --> 00:00:56,844
5. response = sendRequest(findTestObject("98 Webservices Requests/WS1_Airtable_REST/PATCH Used Inventory", ["recordId":recordId]))

36
00:00:56,849 --> 00:00:56,961
11. Verify if the response from "REST_Status Codes/POST_201" object returns the 201 status code

