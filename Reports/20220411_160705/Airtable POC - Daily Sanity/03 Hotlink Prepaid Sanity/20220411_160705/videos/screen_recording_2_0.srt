1
00:00:00,002 --> 00:00:00,144
1. callTestCase(findTestCase("01_Retailer Portal/RP-TMPL-04 PREPAID STARTER PACK REGISTRATION - POC"), ["idValue":idValue, "customerName":customerName, "msisdn":msisdn, "row":row], STOP_ON_FAILURE)

2
00:00:00,152 --> 00:00:00,232
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), ["username":"mcSup", "password":"123"], STOP_ON_FAILURE)

3
00:00:00,236 --> 00:00:05,886
1. openBrowser("")

4
00:00:05,900 --> 00:00:28,792
7. URL is set as a Global Variable controlled in Profiles

5
00:00:28,798 --> 00:00:30,765
13. Username is set as local variable

6
00:00:30,769 --> 00:00:32,598
19. Password is set as local variable

7
00:00:32,605 --> 00:00:33,637
23. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

8
00:00:33,649 --> 00:00:33,651
27. comment("ASSERTION")

9
00:00:33,661 --> 00:00:35,571
33. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

10
00:00:35,579 --> 00:00:35,672
5. callTestCase(findTestCase("01_Retailer Portal/RP07_Create Customer_Page/RP07_1_6 Insert Data into AMR"), ["idValue":idValue, "customerName":customerName], STOP_ON_FAILURE)

11
00:00:35,684 --> 00:00:36,035
1. com.database.connectSql.connectDB("10.200.50.177", "AMRDB", "1433", "amr_user", "amr_user2015")

12
00:00:36,042 --> 00:00:36,185
5. com.database.connectSql.insertCustomerDetailAMR(idValue, customerName)

13
00:00:36,194 --> 00:00:36,198
9. com.database.connectSql.closeDatabaseConnection()

14
00:00:36,206 --> 00:00:36,295
9. callTestCase(findTestCase("01_Retailer Portal/RP03_CIC Dashboard_Page/RP03_4 Starter Pack Registration"), [:], STOP_ON_FAILURE)

15
00:00:36,306 --> 00:00:37,807
1. click(findTestObject("01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/div_Starter Pack Registration"))

16
00:00:37,815 --> 00:00:37,906
13. callTestCase(findTestCase("98 Webservices/WS01_Airtable REST APIs/Get Inventory and marked as used"), ["inventoryType":"inventoryHotlinkStarterPack", "recordId":""], STOP_ON_FAILURE)

17
00:00:37,910 --> 00:00:38,004
1. callTestCase(findTestCase("98 Webservices/WS01_Airtable REST APIs/WS01_01 GET Available Inventory"), ["inventoryType":inventoryType], STOP_ON_FAILURE)

18
00:00:38,008 --> 00:00:39,279
1. response = sendRequest(findTestObject("98 Webservices Requests/WS1_Airtable_REST/GET Available Inventory", ["inventoryType":inventoryType]))

19
00:00:39,283 --> 00:00:39,284
5. slurper = new groovy.json.JsonSlurper()

20
00:00:39,287 --> 00:00:39,288
9. parsedJson = slurper.parseText(response.getResponseText())

21
00:00:39,291 --> 00:00:39,292
13. availableSim = simSerial

22
00:00:39,298 --> 00:00:39,299
17. simSerial = replace("[", "").replace("]", "")

23
00:00:39,303 --> 00:00:39,304
21. println(simSerial)

24
00:00:39,307 --> 00:00:39,308
25. simSerial = simSerial

25
00:00:39,313 --> 00:00:39,314
29. inventoryId = id

26
00:00:39,319 --> 00:00:39,320
33. recordId = replace("[", "").replace("]", "")

27
00:00:39,324 --> 00:00:39,326
37. println(recordId)

28
00:00:39,330 --> 00:00:39,331
41. id = recordId

29
00:00:39,336 --> 00:00:39,338
45. starterPackMsisdn = msisdn

30
00:00:39,344 --> 00:00:39,346
49. msisdn = replace("[", "").replace("]", "")

31
00:00:39,352 --> 00:00:39,354
53. println(msisdn)

32
00:00:39,360 --> 00:00:39,364
57. msisdn = msisdn

33
00:00:39,369 --> 00:00:39,447
5. callTestCase(findTestCase("98 Webservices/WS01_Airtable REST APIs/WS01_02 PATCH Used Inventory"), ["recordId":recordId], STOP_ON_FAILURE)

34
00:00:39,450 --> 00:00:39,451
1. recordId = id

35
00:00:39,455 --> 00:00:40,826
5. response = sendRequest(findTestObject("98 Webservices Requests/WS1_Airtable_REST/PATCH Used Inventory", ["recordId":recordId]))

36
00:00:40,831 --> 00:00:40,882
11. Verify if the response from "REST_Status Codes/POST_201" object returns the 201 status code

