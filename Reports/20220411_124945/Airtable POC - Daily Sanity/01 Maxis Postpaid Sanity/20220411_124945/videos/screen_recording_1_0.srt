1
00:00:01,663 --> 00:00:02,986
1. callTestCase(findTestCase("01_Retailer Portal/RP-TMPL-01 NEW PLAN REGISTRATION NO DEVICE"), ["idType":idType, "idValue":idValue, "planId":planId, "customerName":customerName, "row":row, "msisdn":"", "accountNo":"", ... ], STOP_ON_FAILURE)

2
00:00:03,025 --> 00:00:04,637
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), [:], STOP_ON_FAILURE)

3
00:00:04,641 --> 00:00:18,936
1. openBrowser("")

4
00:00:18,944 --> 00:00:43,408
7. URL is set as a Global Variable controlled in Profiles

5
00:00:43,415 --> 00:00:45,751
13. Username is set as local variable

6
00:00:45,757 --> 00:00:47,680
19. Password is set as local variable

7
00:00:47,686 --> 00:00:48,732
23. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

8
00:00:48,738 --> 00:00:48,760
27. comment("ASSERTION")

9
00:00:48,767 --> 00:00:50,809
33. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

10
00:00:50,815 --> 00:00:50,826
5. runId = toString().substring(toString().lastIndexOf("\") + 1)

11
00:00:50,836 --> 00:00:52,139
9. callTestCase(findTestCase("01_Retailer Portal/RP02_Search Customer_Page/RP02_1 Search Customer"), ["idType":idType, "idValue":idValue, "accountNo":accountNo], STOP_ON_FAILURE)

12
00:00:52,148 --> 00:00:53,349
1. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/i_keyboard_arrow_down"))

13
00:00:53,358 --> 00:00:53,367
5. comment("IF statement to check if Account No is populated in the input sheet. If TRUE idType will be forced to ACCOUNT and search done using the accountNo")

14
00:00:53,379 --> 00:00:53,382
9. if (accountNo.isEmpty() == false)

15
00:00:53,396 --> 00:00:54,634
13. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/Dropdown ID Type", ["idType":idType]))

16
00:00:54,641 --> 00:00:56,624
17. setText(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), idValue)

17
00:00:56,631 --> 00:00:57,711
21. sendKeys(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), Keys.chord(ENTER))

18
00:00:57,723 --> 00:00:57,731
25. comment("Checking for text "Customer Profile does not exist", if text exists the Global Variable newCustomer_Flag is set to TRUE and flow handling will be for New Customer ")

19
00:00:57,741 --> 00:01:37,689
29. newCustomer_Flag = verifyTextPresent("Customer Profile does not exist", false, OPTIONAL)

20
00:01:37,695 --> 00:01:38,963
13. callTestCase(findTestCase("01_Retailer Portal/RP03_CIC Dashboard_Page/RP03_1 New Purchase"), ["accountNo":accountNo], STOP_ON_FAILURE)

21
00:01:38,974 --> 00:01:38,978
1. comment("Different Call To Action elements will be loaded depending on whether the customer is new or an existing one")

22
00:01:38,985 --> 00:01:39,100
5. comment("1. New Purchase button for New Customer, 2. New Purchase button for Existing Customer Existing Account, 3. New Purchase button for Existing Customer New Account")

23
00:01:39,151 --> 00:01:39,157
9. if (newCustomer_Flag == true)

24
00:01:39,188 --> 00:01:41,445
3. click(findTestObject("01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/button_New Purchase"))

25
00:01:41,457 --> 00:01:41,463
13. comment("ASSERTION")

26
00:01:41,474 --> 00:01:57,865
17. verifyTextPresent("Catalog", false)

27
00:01:57,872 --> 00:01:58,732
17. callTestCase(findTestCase("01_Retailer Portal/RP04_Catalog_Page/RP04_1 Purchase plan only, no device"), ["planId":planId], STOP_ON_FAILURE)

28
00:01:58,738 --> 00:01:59,826
3. Click mobile plan tab

29
00:01:59,829 --> 00:02:01,071
7. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.button.planProduct", ["planProduct":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3)]))

30
00:02:01,075 --> 00:02:02,694
11. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.label.planType", ["planType":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 2)]), OPTIONAL)

31
00:02:02,701 --> 00:02:02,749
15. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

32
00:02:02,762 --> 00:02:02,764
19. if (isFibreStandAlone == true)

33
00:02:02,775 --> 00:02:02,776
23. if (isFibreStandAlone == false)

34
00:02:02,784 --> 00:02:04,864
1. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/Button Select This Plan", ["planId":planId]))

35
00:02:04,870 --> 00:02:13,060
7. Verfiy that the Plan Name is the same as the one purchased in previous page

36
00:02:13,067 --> 00:02:14,208
11. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/button_Continue without Device"))

37
00:02:14,214 --> 00:02:15,484
21. callTestCase(findTestCase("01_Retailer Portal/RP05_Product Configuration_Page/RP05_1 Skip Product Configuration"), ["planId":planId], STOP_ON_FAILURE)

38
00:02:15,497 --> 00:02:15,530
1. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

39
00:02:15,544 --> 00:02:15,546
5. if (isFibreStandAlone == true)

40
00:02:15,573 --> 00:02:15,575
9. if (isFibreStandAlone == false)

41
00:02:15,605 --> 00:02:47,322
1. click(findTestObject("01_Retailer Portal_Objects/RP05_Product Configuration_Page_Objects/button_Next"))

