1
00:00:01,309 --> 00:00:02,793
1. callTestCase(findTestCase("01_Retailer Portal/RP-TMPL-04 PREPAID STARTER PACK REGISTRATION - POC"), ["idValue":idValue, "customerName":customerName, "msisdn":msisdn, "row":row], STOP_ON_FAILURE)

2
00:00:02,814 --> 00:00:03,674
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), ["username":"mcSup", "password":"123"], STOP_ON_FAILURE)

3
00:00:03,678 --> 00:00:13,886
1. openBrowser("")

4
00:00:13,893 --> 00:00:39,504
7. URL is set as a Global Variable controlled in Profiles

5
00:00:39,510 --> 00:00:41,965
13. Username is set as local variable

6
00:00:41,972 --> 00:00:43,860
19. Password is set as local variable

7
00:00:43,867 --> 00:00:44,976
23. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

8
00:00:44,988 --> 00:00:45,009
27. comment("ASSERTION")

9
00:00:45,016 --> 00:00:46,921
33. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

10
00:00:46,930 --> 00:00:47,777
5. callTestCase(findTestCase("01_Retailer Portal/RP07_Create Customer_Page/RP07_1_6 Insert Data into AMR"), ["idValue":idValue, "customerName":customerName], STOP_ON_FAILURE)

11
00:00:47,783 --> 00:00:48,855
1. com.database.connectSql.connectDB("10.200.50.177", "AMRDB", "1433", "amr_user", "amr_user2015")

12
00:00:48,865 --> 00:00:49,104
5. com.database.connectSql.insertCustomerDetailAMR(idValue, customerName)

13
00:00:49,111 --> 00:00:49,121
9. com.database.connectSql.closeDatabaseConnection()

14
00:00:49,129 --> 00:00:50,039
9. callTestCase(findTestCase("01_Retailer Portal/RP03_CIC Dashboard_Page/RP03_4 Starter Pack Registration"), [:], STOP_ON_FAILURE)

15
00:00:50,046 --> 00:00:51,787
1. click(findTestObject("01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/div_Starter Pack Registration"))

16
00:00:51,793 --> 00:00:52,813
13. callTestCase(findTestCase("01_Retailer Portal/RP13_Starter Pack Registration_Page/RP13_1 Register Prepaid Starter Pack via NRIC - POC"), ["simSerial":"", "msisdn":"", "idValue":""], STOP_ON_FAILURE)

17
00:00:52,820 --> 00:00:52,822
1. simSerial = simSerial

18
00:00:52,833 --> 00:00:54,807
5. setText(findTestObject("01_Retailer Portal_Objects/RP13_Starter Pack Registration Page_Objects/input_idForSimNum"), simSerial)

19
00:00:54,818 --> 00:00:54,819
9. msisdn = msisdn

20
00:00:54,828 --> 00:00:56,755
13. setText(findTestObject("01_Retailer Portal_Objects/RP13_Starter Pack Registration Page_Objects/input_idForMsisdnNum"), msisdn)

21
00:00:56,761 --> 00:00:57,829
17. click(findTestObject("01_Retailer Portal_Objects/RP13_Starter Pack Registration Page_Objects/button_Verify"))

22
00:00:57,836 --> 00:01:29,430
21. click(findTestObject("01_Retailer Portal_Objects/RP13_Starter Pack Registration Page_Objects/button_NRIC"))

