1
00:00:00,000 --> 00:00:00,000
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), ["username":"SUPERRR", "password":"test"], STOP_ON_FAILURE)

2
00:00:00,000 --> 00:00:00,000
1. openBrowser("")

3
00:00:00,000 --> 00:00:00,000
7. URL is set as a Global Variable controlled in Profiles

4
00:00:00,000 --> 00:00:00,000
13. Username is set as local variable

5
00:00:00,000 --> 00:00:00,000
19. Password is set as local variable

6
00:00:00,000 --> 00:00:00,000
23. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

7
00:00:00,000 --> 00:00:00,000
27. comment("ASSERTION")

8
00:00:00,000 --> 00:00:00,000
33. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

9
00:00:00,000 --> 00:00:00,000
5. callTestCase(findTestCase("01_Retailer Portal/RP02_Search Customer_Page/RP02_1 Search Customer"), ["idType":"ServiceID", "idValue":"A56648", "accountNo":""], STOP_ON_FAILURE)

10
00:00:00,000 --> 00:00:00,000
1. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/i_keyboard_arrow_down"))

11
00:00:00,000 --> 00:00:00,000
5. comment("IF statement to check if Account No is populated in the input sheet. If TRUE idType will be forced to ACCOUNT and search done using the accountNo")

12
00:00:00,000 --> 00:00:00,000
9. if (accountNo.isEmpty() == false)

13
00:00:00,000 --> 00:00:00,000
13. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/Dropdown ID Type", ["idType":idType]))

14
00:00:00,000 --> 00:00:00,000
17. setText(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), idValue)

15
00:00:00,000 --> 00:00:00,000
21. sendKeys(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), Keys.chord(ENTER))

16
00:00:00,000 --> 00:00:00,000
25. comment("Checking for text "Customer Profile does not exist", if text exists the Global Variable newCustomer_Flag is set to TRUE and flow handling will be for New Customer ")

17
00:00:00,000 --> 00:00:00,000
29. newCustomer_Flag = verifyTextPresent("Customer Profile does not exist", false, OPTIONAL)

18
00:00:00,000 --> 00:00:00,000
9. verifyTextPresent("Searched By ServiceID A56648", false)

19
00:00:00,000 --> 00:00:00,000
13. verifyTextPresent("Maxis Fibre - 100Mbps", false)

