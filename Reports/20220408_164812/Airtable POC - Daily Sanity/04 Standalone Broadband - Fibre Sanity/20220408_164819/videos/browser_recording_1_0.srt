1
00:00:00,000 --> 00:00:00,000
1. callTestCase(findTestCase("01_Retailer Portal/RP-TMPL-01 NEW PLAN REGISTRATION NO DEVICE"), ["idType":"MALAYSIAN ID CARD", "idValue":idValue, "planId":planId, "customerName":customerName, "row":row, "msisdn":"", "accountNo":"", ... ], STOP_ON_FAILURE)

2
00:00:00,000 --> 00:00:00,000
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), [:], STOP_ON_FAILURE)

3
00:00:00,000 --> 00:00:00,000
1. openBrowser("")

4
00:00:00,000 --> 00:00:00,000
7. URL is set as a Global Variable controlled in Profiles

5
00:00:00,000 --> 00:00:00,000
13. Username is set as local variable

6
00:00:00,000 --> 00:00:00,000
19. Password is set as local variable

7
00:00:00,000 --> 00:00:00,000
23. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

8
00:00:00,000 --> 00:00:00,000
27. comment("ASSERTION")

9
00:00:00,000 --> 00:00:00,000
33. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

10
00:00:00,000 --> 00:00:00,000
5. runId = toString().substring(toString().lastIndexOf("\") + 1)

11
00:00:00,000 --> 00:00:00,000
9. callTestCase(findTestCase("01_Retailer Portal/RP02_Search Customer_Page/RP02_1 Search Customer"), ["idType":idType, "idValue":idValue, "accountNo":accountNo], STOP_ON_FAILURE)

12
00:00:00,000 --> 00:00:00,000
1. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/i_keyboard_arrow_down"))

13
00:00:00,000 --> 00:00:00,000
5. comment("IF statement to check if Account No is populated in the input sheet. If TRUE idType will be forced to ACCOUNT and search done using the accountNo")

14
00:00:00,000 --> 00:00:00,000
9. if (accountNo.isEmpty() == false)

15
00:00:00,000 --> 00:00:00,000
13. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/Dropdown ID Type", ["idType":idType]))

16
00:00:00,000 --> 00:00:00,000
17. setText(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), idValue)

17
00:00:00,000 --> 00:00:00,000
21. sendKeys(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), Keys.chord(ENTER))

18
00:00:00,000 --> 00:00:00,000
25. comment("Checking for text "Customer Profile does not exist", if text exists the Global Variable newCustomer_Flag is set to TRUE and flow handling will be for New Customer ")

19
00:00:00,000 --> 00:00:00,000
29. newCustomer_Flag = verifyTextPresent("Customer Profile does not exist", false, OPTIONAL)

20
00:00:00,000 --> 00:00:00,000
13. callTestCase(findTestCase("01_Retailer Portal/RP03_CIC Dashboard_Page/RP03_1 New Purchase"), ["accountNo":accountNo], STOP_ON_FAILURE)

21
00:00:00,000 --> 00:00:00,000
1. comment("Different Call To Action elements will be loaded depending on whether the customer is new or an existing one")

22
00:00:00,000 --> 00:00:00,000
5. comment("1. New Purchase button for New Customer, 2. New Purchase button for Existing Customer Existing Account, 3. New Purchase button for Existing Customer New Account")

23
00:00:00,000 --> 00:00:00,000
9. if (newCustomer_Flag == true)

24
00:00:00,000 --> 00:00:00,000
3. click(findTestObject("01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/button_New Purchase"))

25
00:00:00,000 --> 00:00:00,000
13. comment("ASSERTION")

26
00:00:00,000 --> 00:00:00,000
17. verifyTextPresent("Catalog", false)

27
00:00:00,000 --> 00:00:00,000
17. callTestCase(findTestCase("01_Retailer Portal/RP04_Catalog_Page/RP04_1 Purchase plan only, no device"), ["planId":planId], STOP_ON_FAILURE)

28
00:00:00,000 --> 00:00:00,000
3. Click mobile plan tab

29
00:00:00,000 --> 00:00:00,000
7. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.button.planProduct", ["planProduct":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3)]))

30
00:00:00,000 --> 00:00:00,000
11. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.label.planType", ["planType":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 2)]), OPTIONAL)

31
00:00:00,000 --> 00:00:00,000
15. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

32
00:00:00,000 --> 00:00:00,000
19. if (isFibreStandAlone == true)

33
00:00:00,000 --> 00:00:00,000
23. if (isFibreStandAlone == false)

34
00:00:00,000 --> 00:00:00,000
21. callTestCase(findTestCase("01_Retailer Portal/RP05_Product Configuration_Page/RP05_1 Skip Product Configuration"), ["planId":planId], STOP_ON_FAILURE)

35
00:00:00,000 --> 00:00:00,000
1. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

36
00:00:00,000 --> 00:00:00,000
5. if (isFibreStandAlone == true)

37
00:00:00,000 --> 00:00:00,000
9. if (isFibreStandAlone == false)

38
00:00:00,000 --> 00:00:00,000
25. callTestCase(findTestCase("01_Retailer Portal/RP06_Cart_Page/RP06_1 Cart Page"), ["msisdn":msisdn, "broadbandPlan":broadbandPlan, "planId":planId], STOP_ON_FAILURE)

39
00:00:00,000 --> 00:00:00,000
1. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

40
00:00:00,000 --> 00:00:00,000
5. if (isFibreStandAlone == true)

41
00:00:00,000 --> 00:00:00,000
1. callTestCase(findTestCase("01_Retailer Portal/RP06A_Link_to_Broadband/RP06A_2 Add Broadband - Fibre"), ["broadbandPlan":planId], STOP_ON_FAILURE)

42
00:00:00,000 --> 00:00:00,000
1. click(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_bundle-list/button_I want this plan", ["broadbandPlan":broadbandPlan]))

43
00:00:00,000 --> 00:00:00,000
5. click(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/input_search_searchAddresInput"))

44
00:00:00,000 --> 00:00:00,000
9. click(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/div_search"))

45
00:00:00,000 --> 00:00:00,000
13. setText(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/input_search_searchAddresInput"), "no 1, Jalan Medan Masria, Taman Masria, 43200 Cheras, Selangor")

46
00:00:00,000 --> 00:00:00,000
17. sendKeys(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/input_search_searchAddresInput"), Keys.chord(ENTER))

47
00:00:00,000 --> 00:00:00,000
21. click(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/i_navigate_next"))

48
00:00:00,000 --> 00:00:00,000
25. setText(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/input_access_time_providerId"), "1-12345678901")

49
00:00:00,000 --> 00:00:00,000
29. setText(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/input_Sample Format_serviceId"), "BU1234567890")

50
00:00:00,000 --> 00:00:00,000
33. click(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/button_Submit and Continue"))

51
00:00:00,000 --> 00:00:00,000
37. click(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_bundle-configuration/button_Next"))

52
00:00:00,000 --> 00:00:00,000
41. enhancedClick(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_bundle-configuration/button_Continue_to_Cart"))

53
00:00:00,000 --> 00:00:00,000
45. delay(2)

54
00:00:00,000 --> 00:00:00,000
5. callTestCase(findTestCase("01_Retailer Portal/RP06A_Link_to_Broadband/RP06A_3 Assign VoIP number"), [:], STOP_ON_FAILURE)

55
00:00:00,000 --> 00:00:00,000
1. click(findTestObject("01_Retailer Portal_Objects/RP06_Cart_Page_Objects/a_Assign Number"))

56
00:00:00,000 --> 00:00:00,000
5. click(findTestObject("01_Retailer Portal_Objects/RP06B VoIP Number Assignment/p_60382103247"))

57
00:00:00,000 --> 00:00:00,000
9. click(findTestObject("01_Retailer Portal_Objects/RP06B VoIP Number Assignment/button_Continue"))

58
00:00:00,000 --> 00:00:00,000
9. click(findTestObject("01_Retailer Portal_Objects/RP06_Cart_Page_Objects/button_Check Out"))

59
00:00:00,000 --> 00:00:00,000
9. if (isFibreStandAlone == false)

60
00:00:00,000 --> 00:00:00,000
29. callTestCase(findTestCase("01_Retailer Portal/RP07_Create Customer_Page/RP07_1 Create Customer_Page"), ["idType":idType, "customerName":customerName, "accountNo":accountNo, "idValue":idValue], STOP_ON_FAILURE)

61
00:00:00,000 --> 00:00:00,000
1. isMalaysianCitizen = verifyMatch(idType, "MALAYSIAN ID CARD", false, OPTIONAL)

62
00:00:00,000 --> 00:00:00,000
5. if (accountNo.isEmpty() == false)

63
00:00:00,000 --> 00:00:00,000
9. if (accountNo.isEmpty() == true)

64
00:00:00,000 --> 00:00:00,000
1. verifyTextPresent("Customer Details", false)

65
00:00:00,000 --> 00:00:00,000
5. if (newCustomer_Flag == true)

66
00:00:00,000 --> 00:00:00,000
9. if (newCustomer_Flag == false)

67
00:00:00,000 --> 00:00:00,000
1. callTestCase(findTestCase("01_Retailer Portal/RP07_Create Customer_Page/RP07_1_2 Use Contact Address as Billing Address"), [:], STOP_ON_FAILURE)

68
00:00:00,000 --> 00:00:00,000
1. click(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/label_Use Contact Address as Billing Address"))

69
00:00:00,000 --> 00:00:00,000
5. setText(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/input_Receive a monthly email notification _bc6444"), Email)

70
00:00:00,000 --> 00:00:00,000
9. click(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/div_Regular Paper Bill_CustomCheckbox"))

71
00:00:00,000 --> 00:00:00,000
13. click(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/button_Continue"))

72
00:00:00,000 --> 00:00:00,000
33. callTestCase(findTestCase("01_Retailer Portal/RP08_Order Summary_Page/RP08_1 Document Upload and Submit Order"), ["idType":idType], STOP_ON_FAILURE)

73
00:00:00,000 --> 00:00:00,000
1. verifyTextPresent("Order Summary", false, CONTINUE_ON_FAILURE)

74
00:00:00,000 --> 00:00:00,000
5. click(findTestObject("01_Retailer Portal_Objects/RP08_Order Summary_Page_Objects/i_keyboard_arrow_down"))

75
00:00:00,000 --> 00:00:00,000
9. click(findTestObject("01_Retailer Portal_Objects/RP08_Order Summary_Page_Objects/DropDownID_Type", ["idType":idType]))

76
00:00:00,000 --> 00:00:00,000
13. dirName = getProjectDir()

77
00:00:00,000 --> 00:00:00,000
17. upload = "" + dirName + "\Data Files\Test Data and Input Files\Uploads\maxis.jpg"

78
00:00:00,000 --> 00:00:00,000
21. uploadPath = upload.replace("/", "\")

79
00:00:00,000 --> 00:00:00,000
25. com.uploadfile.UploadFile.uploadFileUsingRobot(findTestObject("01_Retailer Portal_Objects/RP08_Order Summary_Page_Objects/div_Browse"), uploadPath)

80
00:00:00,000 --> 00:00:00,000
29. delay(0)

81
00:00:00,000 --> 00:00:00,000
33. enhancedClick(findTestObject("01_Retailer Portal_Objects/RP08_Order Summary_Page_Objects/checkbox_Terms and Condition"))

82
00:00:00,000 --> 00:00:00,000
37. click(findTestObject("01_Retailer Portal_Objects/RP08_Order Summary_Page_Objects/canvas_refresh_signature-pad-1"))

83
00:00:00,000 --> 00:00:00,000
41. click(findTestObject("01_Retailer Portal_Objects/RP08_Order Summary_Page_Objects/canvas_refresh_signature-pad-1"))

84
00:00:00,000 --> 00:00:00,000
45. setText(findTestObject("01_Retailer Portal_Objects/RP08_Order Summary_Page_Objects/textarea_Apply Waiver_remarks_super"), "Approved")

85
00:00:00,000 --> 00:00:00,000
49. click(findTestObject("01_Retailer Portal_Objects/RP08_Order Summary_Page_Objects/button_Approve"))

86
00:00:00,000 --> 00:00:00,000
37. callTestCase(findTestCase("01_Retailer Portal/RP09_Order Confirmation_Page/RP09_1 Verify order ID is given"), [:], STOP_ON_FAILURE)

87
00:00:00,000 --> 00:00:00,000
1. verifyTextPresent("Order Confirmed", false, CONTINUE_ON_FAILURE)

88
00:00:00,000 --> 00:00:00,000
5. click(findTestObject("01_Retailer Portal_Objects/RP09_Order Confirmation_Page_Objects/Order ID"))

89
00:00:00,000 --> 00:00:00,000
41. callTestCase(findTestCase("01_Retailer Portal/RP10_Order Details_Page/RP10_2 Write Order Details to Excel - Modular"), ["row":row, "broadbandPlan":broadbandPlan, "planId":planId, "filePath":"Data Files\Test Data and Input Files\Daily Sanity\Daily Sanity Test Data.xlsx"], STOP_ON_FAILURE)

90
00:00:00,000 --> 00:00:00,000
1. accountNo = getText(findTestObject("01_Retailer Portal_Objects/RP10_Order Details_Page_Objects/AccountNo"))

91
00:00:00,000 --> 00:00:00,000
5. serviceMsisdn = getText(findTestObject("01_Retailer Portal_Objects/RP10_Order Details_Page_Objects/Order Details - MSISDN"))

92
00:00:00,000 --> 00:00:00,000
9. submissionTime = getText(findTestObject("01_Retailer Portal_Objects/RP10_Order Details_Page_Objects/p_06 Sep 2021 125522 PM"))

93
00:00:00,000 --> 00:00:00,000
13. orderId = getText(findTestObject("01_Retailer Portal_Objects/RP10_Order Details_Page_Objects/p_orderId"))

94
00:00:00,000 --> 00:00:00,000
17. customerName = getText(findTestObject("01_Retailer Portal_Objects/RP10_Order Details_Page_Objects/p_customerName"))

95
00:00:00,000 --> 00:00:00,000
21. servicePlan = getText(findTestObject("01_Retailer Portal_Objects/RP10_Order Details_Page_Objects/p_MaxisServicePlan"), OPTIONAL)

96
00:00:00,000 --> 00:00:00,000
25. testName = toString().substring(toString().lastIndexOf("\") + 1)

97
00:00:00,000 --> 00:00:00,000
29. runId = toString().substring(toString().lastIndexOf("\") + 1)

98
00:00:00,000 --> 00:00:00,000
33. resultFilename = "Data Files\Output\" + runId + " " + testName + ".xlsx"

99
00:00:00,000 --> 00:00:00,000
37. myLog = new java.io.File(resultFilename)

100
00:00:00,000 --> 00:00:00,000
41. if (myLog.exists() == false)

101
00:00:00,000 --> 00:00:00,000
1. println("Creating file")

102
00:00:00,000 --> 00:00:00,000
5. originalWb = new java.io.File(filePath)

103
00:00:00,000 --> 00:00:00,000
9. clonedWb = new java.io.File(resultFilename)

104
00:00:00,000 --> 00:00:00,000
13. Files.copy(originalWb.toPath(), clonedWb.toPath())

105
00:00:00,000 --> 00:00:00,000
45. file = new java.io.FileInputStream(new java.io.File(resultFilename))

106
00:00:00,000 --> 00:00:00,000
49. i = row

107
00:00:00,000 --> 00:00:00,000
53. workbook = new org.apache.poi.xssf.usermodel.XSSFWorkbook(file)

108
00:00:00,000 --> 00:00:00,000
57. sheet = workbook.getSheetAt(0)

109
00:00:00,000 --> 00:00:00,000
61. example = serviceMsisdn

110
00:00:00,000 --> 00:00:00,000
65. msisdn = example.replaceAll(" ", "")

111
00:00:00,000 --> 00:00:00,000
69. msisdn = msisdn

112
00:00:00,000 --> 00:00:00,000
73. orderID = orderId

113
00:00:00,000 --> 00:00:00,000
77. createCell(3).setCellValue(customerName)

114
00:00:00,000 --> 00:00:00,000
81. createCell(4).setCellValue(accountNo)

115
00:00:00,000 --> 00:00:00,000
85. createCell(5).setCellValue(orderId)

116
00:00:00,000 --> 00:00:00,000
89. createCell(6).setCellValue(submissionTime)

117
00:00:00,000 --> 00:00:00,000
93. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

118
00:00:00,000 --> 00:00:00,000
97. if (isFibreStandAlone == false)

119
00:00:00,000 --> 00:00:00,000
101. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

120
00:00:00,000 --> 00:00:00,000
105. if (isFibreStandAlone == true)

121
00:00:00,000 --> 00:00:00,000
1. modemId = getText(findTestObject("01_Retailer Portal_Objects/RP10_Order Details_Page_Objects/p_fibreStandAloneModemIP"), OPTIONAL)

122
00:00:00,000 --> 00:00:00,000
5. voipNo = getText(findTestObject("01_Retailer Portal_Objects/RP10_Order Details_Page_Objects/p_fibreStandAloneVoIp"), OPTIONAL)

123
00:00:00,000 --> 00:00:00,000
9. createCell(16).setCellValue(modemId)

124
00:00:00,000 --> 00:00:00,000
13. createCell(17).setCellValue(voipNo)

125
00:00:00,000 --> 00:00:00,000
109. if (broadbandPlan.isEmpty() | isFibreStandAlone == false)

126
00:00:00,000 --> 00:00:00,000
113. file.close()

127
00:00:00,000 --> 00:00:00,000
117. outFile = new java.io.FileOutputStream(new java.io.File(resultFilename))

128
00:00:00,000 --> 00:00:00,000
121. workbook.write(outFile)

129
00:00:00,000 --> 00:00:00,000
125. outFile.close()

130
00:00:00,000 --> 00:00:00,000
129. delay(5)

