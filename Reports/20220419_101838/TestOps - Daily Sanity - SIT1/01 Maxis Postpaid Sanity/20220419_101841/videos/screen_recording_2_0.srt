1
00:00:00,547 --> 00:00:01,214
1. callTestCase(findTestCase("02_Web POS/WP-TMPL-01 AMDOCS DASHBOARD ASSIGN INVENTORY FROM POOL AND PAYMENT - POC"), ["msisdn":"", "simSerial":"", "inventoryType":inventoryType], STOP_ON_FAILURE)

2
00:00:01,229 --> 00:00:01,230
1. msisdn = msisdn

3
00:00:01,231 --> 00:00:01,838
5. callTestCase(findTestCase("02_Web POS/WP01_Login_Page/WP01_01 Login to WebPOS"), [:], STOP_ON_FAILURE)

4
00:00:01,838 --> 00:00:05,286
1. openBrowser("")

5
00:00:05,293 --> 00:00:08,416
5. navigateToUrl(WebPOS_URL)

6
00:00:08,423 --> 00:00:10,473
9. setText(findTestObject("02_WebPOS_Objects/WP01_Login_Page_Objects/input_Online ID_txtUser"), WebPOS_Username)

7
00:00:10,478 --> 00:00:12,535
13. setText(findTestObject("02_WebPOS_Objects/WP01_Login_Page_Objects/input_Password_txtPassword"), WebPOS_Password)

8
00:00:12,540 --> 00:01:14,348
17. click(findTestObject("02_WebPOS_Objects/WP01_Login_Page_Objects/input_Password_btnLogin"))

9
00:01:14,355 --> 00:01:45,759
21. checkAnotherSessionExists = verifyElementPresent(findTestObject("02_WebPOS_Objects/WP01_Login_Page_Objects/errorMessage"), 0, OPTIONAL)

10
00:01:45,762 --> 00:01:45,766
25. if (checkAnotherSessionExists == true)

11
00:01:45,769 --> 00:01:46,554
9. callTestCase(findTestCase("02_Web POS/WP02_Maxis_WebPOS_Page/WP02_01a Inventory Assignment From Test Inventory Pool - POC"), ["msisdn":msisdn, "fwbbMsisdn":"", "simSerial":"", "simSerial2":"", "inventoryType":inventoryType], STOP_ON_FAILURE)

12
00:01:46,558 --> 00:01:46,721
1. maximizeWindow()

13
00:01:46,724 --> 00:01:47,844
5. click(findTestObject("02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/a_Amdocs Inventory Dashboard"))

14
00:01:47,847 --> 00:01:50,437
9. click(findTestObject("02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/td_Ready for Inventory_QueueNo"))

15
00:01:50,441 --> 00:01:51,981
13. click(findTestObject("02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/Global Variable MSISDN (Assign Inventory)", ["msisdn":msisdn]))

16
00:01:51,987 --> 00:01:53,355
17. click(findTestObject("02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/button_Process"))

17
00:01:53,358 --> 00:01:54,220
21. callTestCase(findTestCase("98 Webservices/WS01_Airtable REST APIs/Get Inventory and marked as used"), ["inventoryType":inventoryType, "recordId":""], STOP_ON_FAILURE)

18
00:01:54,225 --> 00:01:54,527
1. callTestCase(findTestCase("98 Webservices/WS01_Airtable REST APIs/WS01_01 GET Available Inventory"), ["inventoryType":inventoryType], STOP_ON_FAILURE)

19
00:01:54,531 --> 00:01:59,803
1. response = sendRequest(findTestObject("98 Webservices Requests/WS1_Airtable_REST/GET Available Inventory", ["inventoryType":inventoryType]))

20
00:01:59,804 --> 00:01:59,808
5. slurper = new groovy.json.JsonSlurper()

21
00:01:59,808 --> 00:01:59,839
9. parsedJson = slurper.parseText(response.getResponseText())

22
00:01:59,855 --> 00:01:59,860
13. availableSim = simSerial

23
00:01:59,860 --> 00:01:59,860
17. simSerial = replace("[", "").replace("]", "")

24
00:01:59,860 --> 00:01:59,876
21. println(simSerial)

25
00:01:59,876 --> 00:01:59,876
25. simSerial = simSerial

26
00:01:59,876 --> 00:01:59,876
29. inventoryId = id

27
00:01:59,891 --> 00:01:59,891
33. recordId = replace("[", "").replace("]", "")

28
00:01:59,891 --> 00:01:59,891
37. println(recordId)

29
00:01:59,891 --> 00:01:59,891
41. id = recordId

30
00:01:59,891 --> 00:01:59,891
45. starterPackMsisdn = msisdn

31
00:01:59,907 --> 00:01:59,907
49. msisdn = replace("[", "").replace("]", "")

32
00:01:59,907 --> 00:01:59,907
53. println(msisdn)

33
00:01:59,907 --> 00:01:59,907
57. msisdn = msisdn

34
00:01:59,923 --> 00:02:00,706
5. callTestCase(findTestCase("98 Webservices/WS01_Airtable REST APIs/WS01_02 PATCH Used Inventory"), ["inventoryType":inventoryType], STOP_ON_FAILURE)

35
00:02:00,706 --> 00:02:00,716
1. recordId = id

36
00:02:00,718 --> 00:02:02,080
5. response = sendRequest(findTestObject("98 Webservices Requests/WS1_Airtable_REST/PATCH Used Inventory", ["recordId":recordId, "inventoryType":inventoryType]))

37
00:02:02,080 --> 00:02:02,096
11. Verify if the response from "REST_Status Codes/POST_201" object returns the 201 status code

38
00:02:02,111 --> 00:02:02,111
25. simSerial = simSerial

39
00:02:02,111 --> 00:02:02,111
29. println(simSerial)

40
00:02:02,111 --> 00:02:04,096
33. setText(findTestObject("02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/input_SIM_IMEINo"), simSerial)

41
00:02:04,101 --> 00:02:04,102
37. if (fwbbMsisdn.isEmpty() == false)

42
00:02:04,102 --> 00:02:05,501
41. click(findTestObject("02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/button_Submit"))

43
00:02:05,504 --> 00:02:06,595
45. click(findTestObject("02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/button_inventory_close"))

44
00:02:06,598 --> 00:02:07,415
13. callTestCase(findTestCase("02_Web POS/WP02_Maxis_WebPOS_Page/WP02_02 Payment"), ["msisdn":msisdn], STOP_ON_FAILURE)

45
00:02:07,420 --> 00:02:08,497
1. click(findTestObject("02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/a_Amdocs Dashboard"))

46
00:02:08,502 --> 00:02:49,768
5. click(findTestObject("02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/Global Variable MSISDN (Payment)", ["msisdn":msisdn]))

