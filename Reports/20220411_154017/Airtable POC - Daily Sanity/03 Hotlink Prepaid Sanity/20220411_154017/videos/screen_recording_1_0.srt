1
00:00:01,037 --> 00:00:02,592
1. callTestCase(findTestCase("01_Retailer Portal/RP-TMPL-04 PREPAID STARTER PACK REGISTRATION - POC"), ["idValue":idValue, "customerName":customerName, "msisdn":msisdn, "row":row], STOP_ON_FAILURE)

2
00:00:02,619 --> 00:00:03,692
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), ["username":"mcSup", "password":"123"], STOP_ON_FAILURE)

3
00:00:03,696 --> 00:00:13,573
1. openBrowser("")

4
00:00:13,581 --> 00:00:38,057
7. URL is set as a Global Variable controlled in Profiles

5
00:00:38,063 --> 00:00:40,057
13. Username is set as local variable

6
00:00:40,064 --> 00:00:41,894
19. Password is set as local variable

7
00:00:41,899 --> 00:00:42,918
23. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

8
00:00:42,924 --> 00:00:42,953
27. comment("ASSERTION")

9
00:00:42,958 --> 00:00:44,717
33. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

10
00:00:44,722 --> 00:00:45,488
5. callTestCase(findTestCase("01_Retailer Portal/RP07_Create Customer_Page/RP07_1_6 Insert Data into AMR"), ["idValue":idValue, "customerName":customerName], STOP_ON_FAILURE)

11
00:00:45,492 --> 00:00:46,516
1. com.database.connectSql.connectDB("10.200.50.177", "AMRDB", "1433", "amr_user", "amr_user2015")

12
00:00:46,519 --> 00:00:46,833
5. com.database.connectSql.insertCustomerDetailAMR(idValue, customerName)

13
00:00:46,836 --> 00:00:46,842
9. com.database.connectSql.closeDatabaseConnection()

14
00:00:46,846 --> 00:00:47,672
9. callTestCase(findTestCase("01_Retailer Portal/RP03_CIC Dashboard_Page/RP03_4 Starter Pack Registration"), [:], STOP_ON_FAILURE)

15
00:00:47,676 --> 00:00:49,249
1. click(findTestObject("01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/div_Starter Pack Registration"))

16
00:00:49,256 --> 00:00:50,119
13. callTestCase(findTestCase("01_Retailer Portal/RP13_Starter Pack Registration_Page/RP13_1 Register Prepaid Starter Pack via NRIC"), ["simSerial":simSerial, "msisdn":msisdn, "idValue":idValue], STOP_ON_FAILURE)

17
00:00:50,123 --> 00:00:50,125
1. simSerial = simSerial

18
00:00:50,129 --> 00:00:51,988
5. setText(findTestObject("01_Retailer Portal_Objects/RP13_Starter Pack Registration Page_Objects/input_idForSimNum"), simSerial)

19
00:00:51,992 --> 00:00:51,993
9. msisdn = msisdn

20
00:00:51,998 --> 00:00:53,865
13. setText(findTestObject("01_Retailer Portal_Objects/RP13_Starter Pack Registration Page_Objects/input_idForMsisdnNum"), msisdn)

21
00:00:53,871 --> 00:00:54,879
17. click(findTestObject("01_Retailer Portal_Objects/RP13_Starter Pack Registration Page_Objects/button_Verify"))

22
00:00:54,883 --> 00:01:26,632
21. click(findTestObject("01_Retailer Portal_Objects/RP13_Starter Pack Registration Page_Objects/button_NRIC"))

