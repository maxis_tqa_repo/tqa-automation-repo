1
00:00:01,510 --> 00:00:03,022
1. callTestCase(findTestCase("01_Retailer Portal/RP-TMPL-04 PREPAID STARTER PACK REGISTRATION - POC"), ["idValue":idValue, "customerName":customerName, "row":row, "inventoryType":inventoryType], STOP_ON_FAILURE)

2
00:00:03,046 --> 00:00:04,257
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), ["username":"mcSup", "password":"123"], STOP_ON_FAILURE)

3
00:00:04,264 --> 00:00:17,074
1. openBrowser("")

4
00:00:17,082 --> 00:00:42,310
7. URL is set as a Global Variable controlled in Profiles

5
00:00:42,316 --> 00:00:44,543
13. Username is set as local variable

6
00:00:44,550 --> 00:00:46,417
19. Password is set as local variable

7
00:00:46,422 --> 00:00:47,604
23. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

8
00:00:47,614 --> 00:00:47,635
27. comment("ASSERTION")

9
00:00:47,642 --> 00:00:49,617
33. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

10
00:00:49,641 --> 00:00:50,893
5. callTestCase(findTestCase("01_Retailer Portal/RP07_Create Customer_Page/RP07_1_6 Insert Data into AMR"), ["idValue":idValue, "customerName":customerName], STOP_ON_FAILURE)

11
00:00:50,902 --> 00:00:51,909
1. com.database.connectSql.connectDB("10.200.50.177", "AMRDB", "1433", "amr_user", "amr_user2015")

12
00:00:51,919 --> 00:00:52,092
5. com.database.connectSql.insertCustomerDetailAMR(idValue, customerName)

13
00:00:52,100 --> 00:00:52,108
9. com.database.connectSql.closeDatabaseConnection()

14
00:00:52,117 --> 00:00:53,356
9. callTestCase(findTestCase("01_Retailer Portal/RP03_CIC Dashboard_Page/RP03_4 Starter Pack Registration"), [:], STOP_ON_FAILURE)

15
00:00:53,371 --> 00:00:55,223
1. click(findTestObject("01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/div_Starter Pack Registration"))

16
00:00:55,230 --> 00:00:56,160
13. callTestCase(findTestCase("98 Webservices/WS01_Airtable REST APIs/Get Inventory and marked as used"), ["inventoryType":inventoryType], STOP_ON_FAILURE)

17
00:00:56,168 --> 00:00:56,564
1. callTestCase(findTestCase("98 Webservices/WS01_Airtable REST APIs/WS01_01 GET Available Inventory"), ["inventoryType":inventoryType], STOP_ON_FAILURE)

18
00:00:56,575 --> 00:01:01,250
1. response = sendRequest(findTestObject("98 Webservices Requests/WS1_Airtable_REST/GET Available Inventory", ["inventoryType":inventoryType]))

19
00:01:01,259 --> 00:01:01,277
5. slurper = new groovy.json.JsonSlurper()

20
00:01:01,290 --> 00:01:01,334
9. parsedJson = slurper.parseText(response.getResponseText())

21
00:01:01,346 --> 00:01:01,370
13. availableSim = simSerial

22
00:01:01,380 --> 00:01:01,383
17. simSerial = replace("[", "").replace("]", "")

23
00:01:01,393 --> 00:01:01,403
21. println(simSerial)

24
00:01:01,411 --> 00:01:01,413
25. simSerial = simSerial

25
00:01:01,421 --> 00:01:01,422
29. inventoryId = id

26
00:01:01,427 --> 00:01:01,428
33. recordId = replace("[", "").replace("]", "")

27
00:01:01,433 --> 00:01:01,435
37. println(recordId)

28
00:01:01,440 --> 00:01:01,442
41. id = recordId

29
00:01:01,447 --> 00:01:01,448
45. starterPackMsisdn = msisdn

30
00:01:01,453 --> 00:01:01,454
49. msisdn = replace("[", "").replace("]", "")

31
00:01:01,458 --> 00:01:01,461
53. println(msisdn)

32
00:01:01,465 --> 00:01:01,470
57. msisdn = msisdn

33
00:01:01,477 --> 00:01:02,593
5. callTestCase(findTestCase("98 Webservices/WS01_Airtable REST APIs/WS01_02 PATCH Used Inventory"), ["inventoryType":inventoryType], STOP_ON_FAILURE)

34
00:01:02,601 --> 00:01:02,602
1. recordId = id

35
00:01:02,609 --> 00:01:04,149
5. response = sendRequest(findTestObject("98 Webservices Requests/WS1_Airtable_REST/PATCH Used Inventory", ["recordId":recordId, "inventoryType":"inventoryHotlinkStarterPack"]))

36
00:01:04,156 --> 00:01:04,199
11. Verify if the response from "REST_Status Codes/POST_201" object returns the 201 status code

37
00:01:04,205 --> 00:01:05,518
17. callTestCase(findTestCase("01_Retailer Portal/RP13_Starter Pack Registration_Page/RP13_1 Register Prepaid Starter Pack via NRIC - POC"), ["idValue":idValue], STOP_ON_FAILURE)

