1
00:00:02,764 --> 00:00:04,281
1. callTestCase(findTestCase("01_Retailer Portal/RP-TMPL-01 NEW PLAN REGISTRATION NO DEVICE"), ["idType":idType, "idValue":idValue, "planId":planId, "customerName":customerName, "row":row, "msisdn":"", "accountNo":"", ... ], STOP_ON_FAILURE)

2
00:00:04,317 --> 00:00:05,615
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), [:], STOP_ON_FAILURE)

3
00:00:05,622 --> 00:00:23,863
1. openBrowser("")

4
00:00:23,869 --> 00:00:51,934
7. URL is set as a Global Variable controlled in Profiles

5
00:00:51,953 --> 00:00:54,939
13. Username is set as local variable

6
00:00:54,962 --> 00:00:57,658
19. Password is set as local variable

7
00:00:57,676 --> 00:00:59,226
23. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

8
00:00:59,241 --> 00:00:59,281
27. comment("ASSERTION")

9
00:00:59,299 --> 00:01:02,886
33. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

10
00:01:02,898 --> 00:01:02,914
5. runId = toString().substring(toString().lastIndexOf("\") + 1)

11
00:01:02,930 --> 00:01:04,990
9. callTestCase(findTestCase("01_Retailer Portal/RP02_Search Customer_Page/RP02_1 Search Customer"), ["idType":idType, "idValue":idValue, "accountNo":accountNo], STOP_ON_FAILURE)

12
00:01:04,999 --> 00:01:06,307
1. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/i_keyboard_arrow_down"))

13
00:01:06,334 --> 00:01:06,344
5. comment("IF statement to check if Account No is populated in the input sheet. If TRUE idType will be forced to ACCOUNT and search done using the accountNo")

14
00:01:06,363 --> 00:01:06,368
9. if (accountNo.isEmpty() == false)

15
00:01:06,382 --> 00:01:07,820
13. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/Dropdown ID Type", ["idType":idType]))

16
00:01:07,828 --> 00:01:10,098
17. setText(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), idValue)

17
00:01:10,107 --> 00:01:11,274
21. sendKeys(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), Keys.chord(ENTER))

18
00:01:11,281 --> 00:01:11,288
25. comment("Checking for text "Customer Profile does not exist", if text exists the Global Variable newCustomer_Flag is set to TRUE and flow handling will be for New Customer ")

19
00:01:11,295 --> 00:01:38,321
29. newCustomer_Flag = verifyTextPresent("Customer Profile does not exist", false, OPTIONAL)

20
00:01:38,328 --> 00:01:39,942
13. callTestCase(findTestCase("01_Retailer Portal/RP03_CIC Dashboard_Page/RP03_1 New Purchase"), ["accountNo":accountNo], STOP_ON_FAILURE)

21
00:01:39,954 --> 00:01:39,960
1. comment("Different Call To Action elements will be loaded depending on whether the customer is new or an existing one")

22
00:01:39,974 --> 00:01:39,979
5. comment("1. New Purchase button for New Customer, 2. New Purchase button for Existing Customer Existing Account, 3. New Purchase button for Existing Customer New Account")

23
00:01:39,994 --> 00:01:40,000
9. if (newCustomer_Flag == true)

24
00:01:40,006 --> 00:01:42,588
3. click(findTestObject("01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/button_New Purchase"))

25
00:01:42,596 --> 00:01:42,602
13. comment("ASSERTION")

26
00:01:42,610 --> 00:02:07,917
17. verifyTextPresent("Catalog", false)

27
00:02:07,924 --> 00:02:09,360
17. callTestCase(findTestCase("01_Retailer Portal/RP04_Catalog_Page/RP04_1 Purchase plan only, no device"), ["planId":planId], STOP_ON_FAILURE)

28
00:02:09,382 --> 00:02:10,810
3. Click mobile plan tab

29
00:02:10,816 --> 00:02:12,550
7. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.button.planProduct", ["planProduct":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3)]))

30
00:02:12,560 --> 00:02:15,859
11. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.label.planType", ["planType":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 2)]), OPTIONAL)

31
00:02:15,869 --> 00:02:16,036
15. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

32
00:02:16,057 --> 00:02:16,065
19. if (isFibreStandAlone == true)

33
00:02:16,079 --> 00:02:16,082
23. if (isFibreStandAlone == false)

34
00:02:16,099 --> 00:02:19,830
1. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/Button Select This Plan", ["planId":planId]))

35
00:02:19,839 --> 00:02:50,300
7. Verfiy that the Plan Name is the same as the one purchased in previous page

36
00:02:50,309 --> 00:02:55,662
11. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/button_Continue without Device"))

37
00:02:55,670 --> 00:02:56,931
21. callTestCase(findTestCase("01_Retailer Portal/RP05_Product Configuration_Page/RP05_1 Skip Product Configuration"), ["planId":planId], STOP_ON_FAILURE)

38
00:02:56,943 --> 00:02:56,963
1. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

39
00:02:56,974 --> 00:02:56,976
5. if (isFibreStandAlone == true)

40
00:02:56,985 --> 00:02:56,987
9. if (isFibreStandAlone == false)

41
00:02:56,995 --> 00:03:07,097
1. click(findTestObject("01_Retailer Portal_Objects/RP05_Product Configuration_Page_Objects/button_Next"))

42
00:03:07,104 --> 00:03:23,303
5. verifyTextPresent("Selected Plan", false)

43
00:03:23,312 --> 00:03:26,452
9. click(findTestObject("01_Retailer Portal_Objects/RP05_Product Configuration_Page_Objects/button_Continue to Cart"))

44
00:03:26,462 --> 00:03:28,254
25. callTestCase(findTestCase("01_Retailer Portal/RP06_Cart_Page/RP06_1 Cart Page"), ["msisdn":msisdn, "broadbandPlan":broadbandPlan, "planId":planId], STOP_ON_FAILURE)

45
00:03:28,267 --> 00:03:28,294
1. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

46
00:03:28,312 --> 00:03:28,315
5. if (isFibreStandAlone == true)

47
00:03:28,357 --> 00:03:28,360
9. if (isFibreStandAlone == false)

48
00:03:28,368 --> 00:03:28,370
3. If msisdn in the input files is empty, then the order will just use the assigned msisdn

49
00:03:28,377 --> 00:03:28,383
1. callTestCase(findTestCase("01_Retailer Portal/RP06_Cart_Page/RP06_5 Link Broadband and Check Out"), ["broadbandPlan":broadbandPlan], STOP_ON_FAILURE)

50
00:03:28,396 --> 00:03:29,957
5. callTestCase(findTestCase("01_Retailer Portal/RP06_Cart_Page/RP06_5 Link Broadband and Check Out - Copy"), ["broadbandPlan":broadbandPlan, "planId":planId], STOP_ON_FAILURE)

51
00:03:29,963 --> 00:03:29,994
1. isplanTypeUnlimited = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 2), "Unlimited", false, OPTIONAL)

52
00:03:30,001 --> 00:03:30,029
5. isplanTypeFamilyPlan = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 2), "Family Plan", false, OPTIONAL)

53
00:03:30,037 --> 00:03:30,061
9. isplanTypePrime = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 2), "Prime", false, OPTIONAL)

54
00:03:30,069 --> 00:03:30,072
13. if (isplanTypeUnlimited == true)

55
00:03:30,081 --> 00:03:30,083
17. if (isplanTypeFamilyPlan == true)

56
00:03:30,091 --> 00:03:30,093
21. if (isplanTypePrime == true)

57
00:03:30,101 --> 00:03:31,556
25. click(findTestObject("01_Retailer Portal_Objects/RP06_Cart_Page_Objects/button_Check Out"))

58
00:03:31,565 --> 00:03:33,502
29. callTestCase(findTestCase("01_Retailer Portal/RP07_Create Customer_Page/RP07_1 Create Customer_Page"), ["idType":idType, "customerName":customerName, "accountNo":accountNo, "idValue":idValue], STOP_ON_FAILURE)

59
00:03:33,514 --> 00:03:33,525
1. isMalaysianCitizen = verifyMatch(idType, "MALAYSIAN ID CARD", false, OPTIONAL)

60
00:03:33,544 --> 00:03:33,546
5. if (accountNo.isEmpty() == false)

61
00:03:33,564 --> 00:03:33,567
9. if (accountNo.isEmpty() == true)

62
00:03:33,586 --> 00:03:35,646
1. verifyTextPresent("Customer Details", false)

63
00:03:35,654 --> 00:03:35,656
5. if (newCustomer_Flag == true)

64
00:03:35,666 --> 00:03:35,671
9. if (newCustomer_Flag == false)

65
00:03:35,677 --> 00:03:36,972
1. callTestCase(findTestCase("01_Retailer Portal/RP07_Create Customer_Page/RP07_1_2 Use Contact Address as Billing Address"), [:], STOP_ON_FAILURE)

66
00:03:36,979 --> 00:03:38,203
1. click(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/label_Use Contact Address as Billing Address"))

67
00:03:38,212 --> 00:03:40,549
5. setText(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/input_Receive a monthly email notification _bc6444"), Email)

68
00:03:40,557 --> 00:03:41,685
9. click(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/div_Regular Paper Bill_CustomCheckbox"))

69
00:03:41,708 --> 00:03:42,993
13. click(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/button_Continue"))

70
00:03:43,001 --> 00:03:44,421
33. callTestCase(findTestCase("01_Retailer Portal/RP08_Order Summary_Page/RP08_1 Document Upload and Submit Order"), ["idType":idType], STOP_ON_FAILURE)

71
00:03:44,437 --> 00:03:54,536
1. verifyTextPresent("Order Summary", false, CONTINUE_ON_FAILURE)

72
00:03:54,544 --> 00:03:55,645
5. click(findTestObject("01_Retailer Portal_Objects/RP08_Order Summary_Page_Objects/i_keyboard_arrow_down"))

73
00:03:55,661 --> 00:03:56,784
9. click(findTestObject("01_Retailer Portal_Objects/RP08_Order Summary_Page_Objects/DropDownID_Type", ["idType":idType]))

74
00:03:56,797 --> 00:03:56,801
13. dirName = getProjectDir()

75
00:03:56,815 --> 00:03:56,816
17. upload = "" + dirName + "\Data Files\Test Data and Input Files\Uploads\maxis.jpg"

76
00:03:56,827 --> 00:03:56,830
21. uploadPath = upload.replace("/", "\")

77
00:03:56,836 --> 00:04:04,220
25. com.uploadfile.UploadFile.uploadFileUsingRobot(findTestObject("01_Retailer Portal_Objects/RP08_Order Summary_Page_Objects/div_Browse"), uploadPath)

78
00:04:04,233 --> 00:04:04,281
29. delay(0)

79
00:04:04,295 --> 00:04:36,220
33. enhancedClick(findTestObject("01_Retailer Portal_Objects/RP08_Order Summary_Page_Objects/checkbox_Terms and Condition"))

80
00:04:36,229 --> 00:04:37,410
37. click(findTestObject("01_Retailer Portal_Objects/RP08_Order Summary_Page_Objects/canvas_refresh_signature-pad-1"))

81
00:04:37,424 --> 00:04:38,501
41. click(findTestObject("01_Retailer Portal_Objects/RP08_Order Summary_Page_Objects/canvas_refresh_signature-pad-1"))

82
00:04:38,510 --> 00:04:40,556
45. setText(findTestObject("01_Retailer Portal_Objects/RP08_Order Summary_Page_Objects/textarea_Apply Waiver_remarks_super"), "Approved")

83
00:04:40,562 --> 00:04:41,633
49. click(findTestObject("01_Retailer Portal_Objects/RP08_Order Summary_Page_Objects/button_Approve"))

84
00:04:41,642 --> 00:04:42,866
37. callTestCase(findTestCase("01_Retailer Portal/RP09_Order Confirmation_Page/RP09_1 Verify order ID is given"), [:], STOP_ON_FAILURE)

85
00:04:42,873 --> 00:04:44,011
1. verifyTextPresent("Order Confirmed", false, CONTINUE_ON_FAILURE)

86
00:04:44,020 --> 00:05:16,036
5. click(findTestObject("01_Retailer Portal_Objects/RP09_Order Confirmation_Page_Objects/Order ID"))

