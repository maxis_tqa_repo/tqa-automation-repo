1
00:00:00,972 --> 00:00:02,309
1. callTestCase(findTestCase("02_Web POS/WP01_Login_Page/WP01_01 Login to WebPOS"), [:], STOP_ON_FAILURE)

2
00:00:02,338 --> 00:00:14,223
1. openBrowser("")

3
00:00:14,235 --> 00:00:18,249
5. navigateToUrl(WebPOS_URL)

4
00:00:18,256 --> 00:00:20,832
9. setText(findTestObject("02_WebPOS_Objects/WP01_Login_Page_Objects/input_Online ID_txtUser"), WebPOS_Username)

5
00:00:20,839 --> 00:00:23,115
13. setText(findTestObject("02_WebPOS_Objects/WP01_Login_Page_Objects/input_Password_txtPassword"), WebPOS_Password)

6
00:00:23,121 --> 00:00:25,198
17. click(findTestObject("02_WebPOS_Objects/WP01_Login_Page_Objects/input_Password_btnLogin"))

7
00:00:25,205 --> 00:00:25,785
21. checkAnotherSessionExists = verifyElementPresent(findTestObject("02_WebPOS_Objects/WP01_Login_Page_Objects/errorMessage"), 0, OPTIONAL)

8
00:00:25,792 --> 00:00:25,796
25. if (checkAnotherSessionExists == true)

9
00:00:25,803 --> 00:00:27,383
1. click(findTestObject("02_WebPOS_Objects/WP01_Login_Page_Objects/a_remove"))

10
00:00:27,390 --> 00:00:32,948
5. click(findTestObject("02_WebPOS_Objects/WP01_Login_Page_Objects/input_Password_btnLogin"))

11
00:00:32,953 --> 00:00:33,855
5. callTestCase(findTestCase("02_Web POS/WP99_Teardown/WP99_02 WebPOS Operator Declaration"), ["expectedValue":""], STOP_ON_FAILURE)

12
00:00:33,859 --> 00:01:05,354
1. ErrorShown = verifyElementPresent(findTestObject("02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/div_Error Business Date invalid"), 0, OPTIONAL)

13
00:01:05,358 --> 00:01:05,360
5. if (ErrorShown == true)

14
00:01:05,365 --> 00:01:37,280
9. click(findTestObject("02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/a_Tab 2"), OPTIONAL)

15
00:01:37,286 --> 00:02:16,623
13. click(findTestObject("02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/a_Operator Declaration"), OPTIONAL)

16
00:02:16,632 --> 00:02:48,463
17. expectedValue = getText(findTestObject("02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/span_20306.88"), OPTIONAL)

17
00:02:48,468 --> 00:02:49,087
21. setText(findTestObject("02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/input_Cash_DeclaredAmount"), expectedValue, OPTIONAL)

18
00:02:49,093 --> 00:03:20,873
25. click(findTestObject("02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/button_Confirm Operator Declaration"))

