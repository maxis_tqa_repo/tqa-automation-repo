1
00:00:01,311 --> 00:00:02,085
1. callTestCase(findTestCase("01_Retailer Portal/RP-TMPL-01 NEW PLAN REGISTRATION NO DEVICE"), ["idType":idType, "idValue":idValue, "planId":planId, "customerName":customerName, "row":row, "msisdn":"", "accountNo":"", ... ], STOP_ON_FAILURE)

2
00:00:02,117 --> 00:00:02,872
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), [:], STOP_ON_FAILURE)

3
00:00:02,878 --> 00:00:11,750
1. openBrowser("")

4
00:00:11,755 --> 00:00:38,771
5. navigateToUrl(RP_URL)

5
00:00:38,777 --> 00:00:41,019
11. Username is set as local variable

6
00:00:41,025 --> 00:00:43,078
17. Password is set as local variable

7
00:00:43,082 --> 00:00:44,205
21. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

8
00:00:44,211 --> 00:00:44,233
25. comment("ASSERTION")

9
00:00:44,238 --> 00:00:46,402
31. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

10
00:00:46,408 --> 00:00:46,414
5. runId = toString().substring(toString().lastIndexOf("\") + 1)

11
00:00:46,420 --> 00:00:47,468
9. callTestCase(findTestCase("01_Retailer Portal/RP02_Search Customer_Page/RP02_1 Search Customer"), ["idType":idType, "idValue":idValue, "accountNo":accountNo], STOP_ON_FAILURE)

12
00:00:47,473 --> 00:00:48,567
1. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/i_keyboard_arrow_down"))

13
00:00:48,574 --> 00:00:48,582
5. comment("IF statement to check if Account No is populated in the input sheet. If TRUE idType will be forced to ACCOUNT and search done using the accountNo")

14
00:00:48,590 --> 00:00:48,593
9. if (accountNo.isEmpty() == false)

15
00:00:48,600 --> 00:00:49,791
13. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/Dropdown ID Type", ["idType":idType]))

16
00:00:49,795 --> 00:00:51,978
17. setText(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), idValue)

17
00:00:51,983 --> 00:00:53,225
21. sendKeys(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), Keys.chord(ENTER))

18
00:00:53,229 --> 00:00:53,234
25. comment("Checking for text "Customer Profile does not exist", if text exists the Global Variable newCustomer_Flag is set to TRUE and flow handling will be for New Customer ")

19
00:00:53,238 --> 00:01:12,205
29. newCustomer_Flag = verifyTextPresent("Customer Profile does not exist", false, OPTIONAL)

20
00:01:12,210 --> 00:01:13,227
13. callTestCase(findTestCase("01_Retailer Portal/RP03_CIC Dashboard_Page/RP03_1 New Purchase"), ["accountNo":accountNo], STOP_ON_FAILURE)

21
00:01:13,234 --> 00:01:13,238
1. comment("Different Call To Action elements will be loaded depending on whether the customer is new or an existing one")

22
00:01:13,244 --> 00:01:13,248
5. comment("1. New Purchase button for New Customer, 2. New Purchase button for Existing Customer Existing Account, 3. New Purchase button for Existing Customer New Account")

23
00:01:13,253 --> 00:01:13,258
9. if (newCustomer_Flag == true)

24
00:01:13,264 --> 00:01:15,943
3. click(findTestObject("01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/button_New Purchase"))

25
00:01:15,951 --> 00:01:15,959
13. comment("ASSERTION")

26
00:01:15,966 --> 00:01:35,036
17. verifyTextPresent("Catalog", false)

27
00:01:35,040 --> 00:01:35,898
17. callTestCase(findTestCase("01_Retailer Portal/RP04_Catalog_Page/RP04_1 Purchase plan only, no device"), ["planId":planId], STOP_ON_FAILURE)

28
00:01:35,905 --> 00:01:37,115
3. Click mobile plan tab

29
00:01:37,120 --> 00:01:38,483
7. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.button.planProduct", ["planProduct":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3)]))

30
00:01:38,489 --> 00:01:39,996
11. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.label.planType", ["planType":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 2)]), OPTIONAL)

31
00:01:40,001 --> 00:01:40,048
15. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

32
00:01:40,054 --> 00:01:40,055
19. if (isFibreStandAlone == true)

33
00:01:40,065 --> 00:01:40,066
23. if (isFibreStandAlone == false)

34
00:01:40,071 --> 00:01:42,306
1. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/Button Select This Plan", ["planId":planId]))

35
00:01:42,314 --> 00:02:05,340
7. Verfiy that the Plan Name is the same as the one purchased in previous page

36
00:02:05,344 --> 00:02:06,811
11. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/button_Continue without Device"))

37
00:02:06,820 --> 00:02:07,640
21. callTestCase(findTestCase("01_Retailer Portal/RP05_Product Configuration_Page/RP05_1 Skip Product Configuration"), ["planId":planId], STOP_ON_FAILURE)

38
00:02:07,645 --> 00:02:07,658
1. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

39
00:02:07,663 --> 00:02:07,666
5. if (isFibreStandAlone == true)

40
00:02:07,671 --> 00:02:07,672
9. if (isFibreStandAlone == false)

41
00:02:07,677 --> 00:02:39,121
1. click(findTestObject("01_Retailer Portal_Objects/RP05_Product Configuration_Page_Objects/button_Next"))

