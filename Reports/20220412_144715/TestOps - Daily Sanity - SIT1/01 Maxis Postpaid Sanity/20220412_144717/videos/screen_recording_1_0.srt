1
00:00:01,878 --> 00:00:03,557
1. callTestCase(findTestCase("01_Retailer Portal/RP-TMPL-01 NEW PLAN REGISTRATION NO DEVICE"), ["idType":idType, "idValue":idValue, "planId":planId, "customerName":customerName, "row":row, "msisdn":"", "accountNo":"", ... ], STOP_ON_FAILURE)

2
00:00:03,754 --> 00:00:06,065
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), [:], STOP_ON_FAILURE)

3
00:00:06,073 --> 00:00:23,193
1. openBrowser("")

4
00:00:23,200 --> 00:00:48,930
7. URL is set as a Global Variable controlled in Profiles

5
00:00:48,937 --> 00:00:51,087
13. Username is set as local variable

6
00:00:51,093 --> 00:00:52,967
19. Password is set as local variable

7
00:00:52,974 --> 00:00:54,065
23. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

8
00:00:54,077 --> 00:00:54,105
27. comment("ASSERTION")

9
00:00:54,112 --> 00:00:56,013
33. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

10
00:00:56,019 --> 00:00:56,027
5. runId = toString().substring(toString().lastIndexOf("\") + 1)

11
00:00:56,036 --> 00:00:57,108
9. callTestCase(findTestCase("01_Retailer Portal/RP02_Search Customer_Page/RP02_1 Search Customer"), ["idType":idType, "idValue":idValue, "accountNo":accountNo], STOP_ON_FAILURE)

12
00:00:57,117 --> 00:00:58,262
1. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/i_keyboard_arrow_down"))

13
00:00:58,275 --> 00:00:58,280
5. comment("IF statement to check if Account No is populated in the input sheet. If TRUE idType will be forced to ACCOUNT and search done using the accountNo")

14
00:00:58,291 --> 00:00:58,295
9. if (accountNo.isEmpty() == false)

15
00:00:58,306 --> 00:00:59,472
13. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/Dropdown ID Type", ["idType":idType]))

16
00:00:59,481 --> 00:01:01,426
17. setText(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), idValue)

17
00:01:01,433 --> 00:01:02,551
21. sendKeys(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), Keys.chord(ENTER))

18
00:01:02,561 --> 00:01:02,567
25. comment("Checking for text "Customer Profile does not exist", if text exists the Global Variable newCustomer_Flag is set to TRUE and flow handling will be for New Customer ")

19
00:01:02,576 --> 00:01:26,533
29. newCustomer_Flag = verifyTextPresent("Customer Profile does not exist", false, OPTIONAL)

20
00:01:26,540 --> 00:01:28,083
13. callTestCase(findTestCase("01_Retailer Portal/RP03_CIC Dashboard_Page/RP03_1 New Purchase"), ["accountNo":accountNo], STOP_ON_FAILURE)

21
00:01:28,094 --> 00:01:28,102
1. comment("Different Call To Action elements will be loaded depending on whether the customer is new or an existing one")

22
00:01:28,112 --> 00:01:28,119
5. comment("1. New Purchase button for New Customer, 2. New Purchase button for Existing Customer Existing Account, 3. New Purchase button for Existing Customer New Account")

23
00:01:28,132 --> 00:01:28,139
9. if (newCustomer_Flag == true)

24
00:01:28,150 --> 00:01:30,532
3. click(findTestObject("01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/button_New Purchase"))

25
00:01:30,545 --> 00:01:30,551
13. comment("ASSERTION")

26
00:01:30,565 --> 00:01:39,255
17. verifyTextPresent("Catalog", false)

27
00:01:39,270 --> 00:01:40,898
17. callTestCase(findTestCase("01_Retailer Portal/RP04_Catalog_Page/RP04_1 Purchase plan only, no device"), ["planId":planId], STOP_ON_FAILURE)

28
00:01:40,909 --> 00:01:42,388
3. Click mobile plan tab

29
00:01:42,402 --> 00:01:43,930
7. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.button.planProduct", ["planProduct":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3)]))

30
00:01:43,938 --> 00:01:45,686
11. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.label.planType", ["planType":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 2)]), OPTIONAL)

31
00:01:45,693 --> 00:01:45,771
15. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

32
00:01:45,778 --> 00:01:45,780
19. if (isFibreStandAlone == true)

33
00:01:45,787 --> 00:01:45,789
23. if (isFibreStandAlone == false)

34
00:01:45,797 --> 00:01:48,847
1. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/Button Select This Plan", ["planId":planId]))

35
00:01:48,855 --> 00:01:57,512
7. Verfiy that the Plan Name is the same as the one purchased in previous page

36
00:01:57,520 --> 00:01:58,958
11. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/button_Continue without Device"))

37
00:01:58,966 --> 00:02:00,734
21. callTestCase(findTestCase("01_Retailer Portal/RP05_Product Configuration_Page/RP05_1 Skip Product Configuration"), ["planId":planId], STOP_ON_FAILURE)

38
00:02:00,742 --> 00:02:00,768
1. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

39
00:02:00,775 --> 00:02:00,777
5. if (isFibreStandAlone == true)

40
00:02:00,784 --> 00:02:00,786
9. if (isFibreStandAlone == false)

41
00:02:00,794 --> 00:02:08,388
1. click(findTestObject("01_Retailer Portal_Objects/RP05_Product Configuration_Page_Objects/button_Next"))

42
00:02:08,395 --> 00:02:14,377
5. verifyTextPresent("Selected Plan", false)

43
00:02:14,386 --> 00:02:17,670
9. click(findTestObject("01_Retailer Portal_Objects/RP05_Product Configuration_Page_Objects/button_Continue to Cart"))

44
00:02:17,676 --> 00:02:19,227
25. callTestCase(findTestCase("01_Retailer Portal/RP06_Cart_Page/RP06_1 Cart Page"), ["msisdn":msisdn, "broadbandPlan":broadbandPlan, "planId":planId], STOP_ON_FAILURE)

45
00:02:19,244 --> 00:02:19,273
1. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

46
00:02:19,316 --> 00:02:19,318
5. if (isFibreStandAlone == true)

47
00:02:19,345 --> 00:02:19,347
9. if (isFibreStandAlone == false)

48
00:02:19,363 --> 00:02:19,365
3. If msisdn in the input files is empty, then the order will just use the assigned msisdn

49
00:02:19,380 --> 00:02:19,384
1. callTestCase(findTestCase("01_Retailer Portal/RP06_Cart_Page/RP06_5 Link Broadband and Check Out"), ["broadbandPlan":broadbandPlan], STOP_ON_FAILURE)

50
00:02:19,399 --> 00:02:21,218
5. callTestCase(findTestCase("01_Retailer Portal/RP06_Cart_Page/RP06_5 Link Broadband and Check Out - Copy"), ["broadbandPlan":broadbandPlan, "planId":planId], STOP_ON_FAILURE)

51
00:02:21,233 --> 00:02:21,258
1. isplanTypeUnlimited = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 2), "Unlimited", false, OPTIONAL)

52
00:02:21,271 --> 00:02:21,297
5. isplanTypeFamilyPlan = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 2), "Family Plan", false, OPTIONAL)

53
00:02:21,309 --> 00:02:21,334
9. isplanTypePrime = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 2), "Prime", false, OPTIONAL)

54
00:02:21,341 --> 00:02:21,343
13. if (isplanTypeUnlimited == true)

55
00:02:21,350 --> 00:02:21,352
17. if (isplanTypeFamilyPlan == true)

56
00:02:21,358 --> 00:02:21,359
21. if (isplanTypePrime == true)

57
00:02:21,367 --> 00:02:22,847
25. click(findTestObject("01_Retailer Portal_Objects/RP06_Cart_Page_Objects/button_Check Out"))

58
00:02:22,854 --> 00:02:24,700
29. callTestCase(findTestCase("01_Retailer Portal/RP07_Create Customer_Page/RP07_1 Create Customer_Page"), ["idType":idType, "customerName":customerName, "accountNo":accountNo, "idValue":idValue], STOP_ON_FAILURE)

59
00:02:24,708 --> 00:02:24,722
1. isMalaysianCitizen = verifyMatch(idType, "MALAYSIAN ID CARD", false, OPTIONAL)

60
00:02:24,745 --> 00:02:24,747
5. if (accountNo.isEmpty() == false)

61
00:02:24,755 --> 00:02:24,757
9. if (accountNo.isEmpty() == true)

62
00:02:24,766 --> 00:02:27,338
1. verifyTextPresent("Customer Details", false)

63
00:02:27,345 --> 00:02:27,348
5. if (newCustomer_Flag == true)

64
00:02:27,355 --> 00:02:27,357
9. if (newCustomer_Flag == false)

65
00:02:27,365 --> 00:02:28,421
1. callTestCase(findTestCase("01_Retailer Portal/RP07_Create Customer_Page/RP07_1_2 Use Contact Address as Billing Address"), [:], STOP_ON_FAILURE)

66
00:02:28,427 --> 00:02:29,617
1. click(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/label_Use Contact Address as Billing Address"))

67
00:02:29,622 --> 00:02:31,739
5. setText(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/input_Receive a monthly email notification _bc6444"), Email)

68
00:02:31,746 --> 00:02:32,859
9. click(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/div_Regular Paper Bill_CustomCheckbox"))

69
00:02:32,871 --> 00:02:34,088
13. click(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/button_Continue"))

70
00:02:34,103 --> 00:02:35,493
33. callTestCase(findTestCase("01_Retailer Portal/RP08_Order Summary_Page/RP08_1 Document Upload and Submit Order"), ["idType":idType], STOP_ON_FAILURE)

71
00:02:35,504 --> 00:02:46,400
1. verifyTextPresent("Order Summary", false, CONTINUE_ON_FAILURE)

72
00:02:46,416 --> 00:02:47,522
5. click(findTestObject("01_Retailer Portal_Objects/RP08_Order Summary_Page_Objects/i_keyboard_arrow_down"))

73
00:02:47,532 --> 00:02:48,632
9. click(findTestObject("01_Retailer Portal_Objects/RP08_Order Summary_Page_Objects/DropDownID_Type", ["idType":idType]))

74
00:02:48,649 --> 00:02:48,653
13. dirName = getProjectDir()

75
00:02:48,668 --> 00:02:48,669
17. upload = "" + dirName + "\Data Files\Test Data and Input Files\Uploads\maxis.jpg"

76
00:02:48,688 --> 00:02:48,691
21. uploadPath = upload.replace("/", "\")

77
00:02:48,711 --> 00:02:56,025
25. com.uploadfile.UploadFile.uploadFileUsingRobot(findTestObject("01_Retailer Portal_Objects/RP08_Order Summary_Page_Objects/div_Browse"), uploadPath)

78
00:02:56,033 --> 00:02:56,082
29. delay(0)

