1
00:00:01,046 --> 00:00:01,816
1. callTestCase(findTestCase("01_Retailer Portal/RP-TMPL-01 NEW PLAN REGISTRATION NO DEVICE"), ["idType":idType, "idValue":idValue, "planId":planId, "customerName":customerName, "row":row, "msisdn":"", "accountNo":"", ... ], STOP_ON_FAILURE)

2
00:00:01,840 --> 00:00:02,575
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), [:], STOP_ON_FAILURE)

3
00:00:02,580 --> 00:00:11,136
1. openBrowser("")

4
00:00:11,141 --> 00:00:34,944
5. navigateToUrl(RP_URL)

5
00:00:34,950 --> 00:00:37,197
11. Username is set as local variable

6
00:00:37,201 --> 00:00:39,227
17. Password is set as local variable

7
00:00:39,231 --> 00:00:40,374
21. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

8
00:00:40,380 --> 00:00:40,401
25. comment("ASSERTION")

9
00:00:40,407 --> 00:00:42,138
31. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

10
00:00:42,143 --> 00:00:42,149
5. runId = toString().substring(toString().lastIndexOf("\") + 1)

11
00:00:42,154 --> 00:00:42,902
9. callTestCase(findTestCase("01_Retailer Portal/RP02_Search Customer_Page/RP02_1 Search Customer"), ["idType":idType, "idValue":idValue, "accountNo":accountNo], STOP_ON_FAILURE)

12
00:00:42,909 --> 00:00:43,989
1. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/i_keyboard_arrow_down"))

13
00:00:43,995 --> 00:00:44,001
5. comment("IF statement to check if Account No is populated in the input sheet. If TRUE idType will be forced to ACCOUNT and search done using the accountNo")

14
00:00:44,011 --> 00:00:44,013
9. if (accountNo.isEmpty() == false)

15
00:00:44,020 --> 00:00:45,191
13. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/Dropdown ID Type", ["idType":idType]))

16
00:00:45,196 --> 00:00:47,289
17. setText(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), idValue)

17
00:00:47,293 --> 00:00:48,396
21. sendKeys(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), Keys.chord(ENTER))

18
00:00:48,402 --> 00:00:48,406
25. comment("Checking for text "Customer Profile does not exist", if text exists the Global Variable newCustomer_Flag is set to TRUE and flow handling will be for New Customer ")

19
00:00:48,414 --> 00:01:09,852
29. newCustomer_Flag = verifyTextPresent("Customer Profile does not exist", false, OPTIONAL)

20
00:01:09,858 --> 00:01:10,880
13. callTestCase(findTestCase("01_Retailer Portal/RP03_CIC Dashboard_Page/RP03_1 New Purchase"), ["accountNo":accountNo], STOP_ON_FAILURE)

21
00:01:10,887 --> 00:01:10,892
1. comment("Different Call To Action elements will be loaded depending on whether the customer is new or an existing one")

22
00:01:10,899 --> 00:01:10,904
5. comment("1. New Purchase button for New Customer, 2. New Purchase button for Existing Customer Existing Account, 3. New Purchase button for Existing Customer New Account")

23
00:01:10,912 --> 00:01:10,916
9. if (newCustomer_Flag == true)

24
00:01:10,923 --> 00:01:12,666
3. click(findTestObject("01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/button_New Purchase"))

25
00:01:12,670 --> 00:01:12,675
13. comment("ASSERTION")

26
00:01:12,681 --> 00:01:25,550
17. verifyTextPresent("Catalog", false)

27
00:01:25,554 --> 00:01:26,303
17. callTestCase(findTestCase("01_Retailer Portal/RP04_Catalog_Page/RP04_1 Purchase plan only, no device"), ["planId":planId], STOP_ON_FAILURE)

28
00:01:26,309 --> 00:01:27,489
3. Click mobile plan tab

29
00:01:27,495 --> 00:01:28,810
7. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.button.planProduct", ["planProduct":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3)]))

30
00:01:28,815 --> 00:01:30,242
11. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.label.planType", ["planType":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 2)]), OPTIONAL)

31
00:01:30,245 --> 00:01:30,281
15. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

32
00:01:30,291 --> 00:01:30,293
19. if (isFibreStandAlone == true)

33
00:01:30,299 --> 00:01:30,301
23. if (isFibreStandAlone == false)

34
00:01:30,308 --> 00:01:32,499
1. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/Button Select This Plan", ["planId":planId]))

35
00:01:32,508 --> 00:01:37,140
7. Verfiy that the Plan Name is the same as the one purchased in previous page

36
00:01:37,145 --> 00:01:38,375
11. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/button_Continue without Device"))

37
00:01:38,386 --> 00:01:39,163
21. callTestCase(findTestCase("01_Retailer Portal/RP05_Product Configuration_Page/RP05_1 Skip Product Configuration"), ["planId":planId], STOP_ON_FAILURE)

38
00:01:39,171 --> 00:01:39,186
1. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

39
00:01:39,192 --> 00:01:39,194
5. if (isFibreStandAlone == true)

40
00:01:39,201 --> 00:01:39,203
9. if (isFibreStandAlone == false)

41
00:01:39,211 --> 00:02:10,619
1. click(findTestObject("01_Retailer Portal_Objects/RP05_Product Configuration_Page_Objects/button_Next"))

