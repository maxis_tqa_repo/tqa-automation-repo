1
00:00:00,783 --> 00:00:00,870
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), [:], STOP_ON_FAILURE)

2
00:00:00,878 --> 00:00:04,634
1. openBrowser("")

3
00:00:04,638 --> 00:00:27,796
5. navigateToUrl(RP_URL)

4
00:00:27,802 --> 00:00:29,905
11. Username is set as local variable

5
00:00:29,915 --> 00:00:32,015
17. Password is set as local variable

6
00:00:32,026 --> 00:00:33,104
21. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

7
00:00:33,114 --> 00:00:33,117
25. comment("ASSERTION")

8
00:00:33,125 --> 00:00:35,021
31. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

9
00:00:35,028 --> 00:00:35,113
5. callTestCase(findTestCase("01_Retailer Portal/RP02_Search Customer_Page/RP02_1 Search Customer"), ["idType":"ServiceID", "idValue":msisdn, "accountNo":""], STOP_ON_FAILURE)

10
00:00:35,118 --> 00:00:36,181
1. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/i_keyboard_arrow_down"))

11
00:00:36,194 --> 00:00:36,198
5. comment("IF statement to check if Account No is populated in the input sheet. If TRUE idType will be forced to ACCOUNT and search done using the accountNo")

12
00:00:36,207 --> 00:00:36,209
9. if (accountNo.isEmpty() == false)

13
00:00:36,218 --> 00:00:37,315
13. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/Dropdown ID Type", ["idType":idType]))

14
00:00:37,321 --> 00:00:39,486
17. setText(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), idValue)

15
00:00:39,490 --> 00:00:40,629
21. sendKeys(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), Keys.chord(ENTER))

16
00:00:40,635 --> 00:00:40,639
25. comment("Checking for text "Customer Profile does not exist", if text exists the Global Variable newCustomer_Flag is set to TRUE and flow handling will be for New Customer ")

17
00:00:40,650 --> 00:01:01,305
29. newCustomer_Flag = verifyTextPresent("Customer Profile does not exist", false, OPTIONAL)

18
00:01:01,310 --> 00:01:01,777
9. verifyTextPresent("Searched By", false)

19
00:01:01,786 --> 00:01:02,508
13. verifyTextPresent(planName, false)

