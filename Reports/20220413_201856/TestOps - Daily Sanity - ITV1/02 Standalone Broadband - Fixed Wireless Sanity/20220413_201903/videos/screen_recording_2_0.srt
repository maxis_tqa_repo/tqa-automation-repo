1
00:00:00,562 --> 00:00:00,649
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), [:], STOP_ON_FAILURE)

2
00:00:00,656 --> 00:00:04,266
1. openBrowser("")

3
00:00:04,270 --> 00:00:30,204
5. navigateToUrl(RP_URL)

4
00:00:30,209 --> 00:00:32,261
11. Username is set as local variable

5
00:00:32,267 --> 00:00:34,381
17. Password is set as local variable

6
00:00:34,385 --> 00:00:35,558
21. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

7
00:00:35,562 --> 00:00:35,565
25. comment("ASSERTION")

8
00:00:35,568 --> 00:00:37,354
31. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

9
00:00:37,358 --> 00:00:37,442
5. callTestCase(findTestCase("01_Retailer Portal/RP02_Search Customer_Page/RP02_1 Search Customer"), ["idType":"ServiceID", "idValue":msisdn, "accountNo":""], STOP_ON_FAILURE)

10
00:00:37,447 --> 00:00:38,510
1. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/i_keyboard_arrow_down"))

11
00:00:38,515 --> 00:00:38,519
5. comment("IF statement to check if Account No is populated in the input sheet. If TRUE idType will be forced to ACCOUNT and search done using the accountNo")

12
00:00:38,524 --> 00:00:38,525
9. if (accountNo.isEmpty() == false)

13
00:00:38,531 --> 00:00:39,658
13. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/Dropdown ID Type", ["idType":idType]))

14
00:00:39,662 --> 00:00:41,777
17. setText(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), idValue)

15
00:00:41,781 --> 00:00:42,799
21. sendKeys(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), Keys.chord(ENTER))

16
00:00:42,802 --> 00:00:42,803
25. comment("Checking for text "Customer Profile does not exist", if text exists the Global Variable newCustomer_Flag is set to TRUE and flow handling will be for New Customer ")

17
00:00:42,806 --> 00:01:04,312
29. newCustomer_Flag = verifyTextPresent("Customer Profile does not exist", false, OPTIONAL)

18
00:01:04,316 --> 00:01:04,820
9. verifyTextPresent("Searched By", false)

19
00:01:04,824 --> 00:01:05,494
13. verifyTextPresent(planName, false)

