1
00:00:00,618 --> 00:00:00,691
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), [:], STOP_ON_FAILURE)

2
00:00:00,698 --> 00:00:04,184
1. openBrowser("")

3
00:00:04,189 --> 00:00:28,433
5. navigateToUrl(RP_URL)

4
00:00:28,439 --> 00:00:30,465
11. Username is set as local variable

5
00:00:30,470 --> 00:00:32,587
17. Password is set as local variable

6
00:00:32,593 --> 00:00:33,780
21. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

7
00:00:33,787 --> 00:00:33,790
25. comment("ASSERTION")

8
00:00:33,796 --> 00:00:35,503
31. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

9
00:00:35,508 --> 00:00:35,585
5. callTestCase(findTestCase("01_Retailer Portal/RP02_Search Customer_Page/RP02_1 Search Customer"), ["idType":"ServiceID", "idValue":idValue, "accountNo":""], STOP_ON_FAILURE)

10
00:00:35,589 --> 00:00:36,626
1. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/i_keyboard_arrow_down"))

11
00:00:36,631 --> 00:00:36,634
5. comment("IF statement to check if Account No is populated in the input sheet. If TRUE idType will be forced to ACCOUNT and search done using the accountNo")

12
00:00:36,639 --> 00:00:36,640
9. if (accountNo.isEmpty() == false)

13
00:00:36,644 --> 00:00:37,722
13. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/Dropdown ID Type", ["idType":idType]))

14
00:00:37,726 --> 00:00:39,869
17. setText(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), idValue)

15
00:00:39,874 --> 00:00:40,911
21. sendKeys(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), Keys.chord(ENTER))

16
00:00:40,915 --> 00:00:40,917
25. comment("Checking for text "Customer Profile does not exist", if text exists the Global Variable newCustomer_Flag is set to TRUE and flow handling will be for New Customer ")

17
00:00:40,922 --> 00:01:02,121
29. newCustomer_Flag = verifyTextPresent("Customer Profile does not exist", false, OPTIONAL)

18
00:01:02,125 --> 00:01:02,557
9. verifyTextPresent("Searched By", false)

19
00:01:02,560 --> 00:01:03,214
13. verifyTextPresent(planName, false)

