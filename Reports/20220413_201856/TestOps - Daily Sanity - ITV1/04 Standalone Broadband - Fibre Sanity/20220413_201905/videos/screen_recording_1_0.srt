1
00:00:01,002 --> 00:00:01,706
1. callTestCase(findTestCase("01_Retailer Portal/RP-TMPL-01 NEW PLAN REGISTRATION NO DEVICE"), ["idType":idType, "idValue":idValue, "planId":planId, "customerName":customerName, "row":row, "msisdn":"", "accountNo":"", ... ], STOP_ON_FAILURE)

2
00:00:01,725 --> 00:00:02,446
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), [:], STOP_ON_FAILURE)

3
00:00:02,451 --> 00:00:09,914
1. openBrowser("")

4
00:00:09,926 --> 00:00:34,235
5. navigateToUrl(RP_URL)

5
00:00:34,241 --> 00:00:36,363
11. Username is set as local variable

6
00:00:36,368 --> 00:00:38,401
17. Password is set as local variable

7
00:00:38,405 --> 00:00:39,499
21. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

8
00:00:39,504 --> 00:00:39,523
25. comment("ASSERTION")

9
00:00:39,527 --> 00:00:41,334
31. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

10
00:00:41,337 --> 00:00:41,341
5. runId = toString().substring(toString().lastIndexOf("\") + 1)

11
00:00:41,345 --> 00:00:42,009
9. callTestCase(findTestCase("01_Retailer Portal/RP02_Search Customer_Page/RP02_1 Search Customer"), ["idType":idType, "idValue":idValue, "accountNo":accountNo], STOP_ON_FAILURE)

12
00:00:42,013 --> 00:00:43,061
1. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/i_keyboard_arrow_down"))

13
00:00:43,067 --> 00:00:43,074
5. comment("IF statement to check if Account No is populated in the input sheet. If TRUE idType will be forced to ACCOUNT and search done using the accountNo")

14
00:00:43,078 --> 00:00:43,081
9. if (accountNo.isEmpty() == false)

15
00:00:43,087 --> 00:00:44,248
13. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/Dropdown ID Type", ["idType":idType]))

16
00:00:44,251 --> 00:00:46,367
17. setText(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), idValue)

17
00:00:46,371 --> 00:00:47,458
21. sendKeys(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), Keys.chord(ENTER))

18
00:00:47,462 --> 00:00:47,465
25. comment("Checking for text "Customer Profile does not exist", if text exists the Global Variable newCustomer_Flag is set to TRUE and flow handling will be for New Customer ")

19
00:00:47,469 --> 00:01:08,063
29. newCustomer_Flag = verifyTextPresent("Customer Profile does not exist", false, OPTIONAL)

20
00:01:08,068 --> 00:01:08,881
13. callTestCase(findTestCase("01_Retailer Portal/RP03_CIC Dashboard_Page/RP03_1 New Purchase"), ["accountNo":accountNo], STOP_ON_FAILURE)

21
00:01:08,886 --> 00:01:08,890
1. comment("Different Call To Action elements will be loaded depending on whether the customer is new or an existing one")

22
00:01:08,895 --> 00:01:08,898
5. comment("1. New Purchase button for New Customer, 2. New Purchase button for Existing Customer Existing Account, 3. New Purchase button for Existing Customer New Account")

23
00:01:08,902 --> 00:01:08,906
9. if (newCustomer_Flag == true)

24
00:01:08,910 --> 00:01:11,075
3. click(findTestObject("01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/button_New Purchase"))

25
00:01:11,079 --> 00:01:11,081
13. comment("ASSERTION")

26
00:01:11,085 --> 00:01:23,853
17. verifyTextPresent("Catalog", false)

27
00:01:23,860 --> 00:01:24,727
17. callTestCase(findTestCase("01_Retailer Portal/RP04_Catalog_Page/RP04_1 Purchase plan only, no device"), ["planId":planId], STOP_ON_FAILURE)

28
00:01:24,733 --> 00:01:25,888
3. Click mobile plan tab

29
00:01:25,892 --> 00:01:27,116
7. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.button.planProduct", ["planProduct":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3)]))

30
00:01:27,120 --> 00:02:00,075
11. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.label.planType", ["planType":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 2)]), OPTIONAL)

31
00:02:00,081 --> 00:02:00,135
15. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

32
00:02:00,141 --> 00:02:00,142
19. if (isFibreStandAlone == true)

33
00:02:00,146 --> 00:02:00,149
23. if (isFibreStandAlone == false)

34
00:02:00,153 --> 00:02:00,800
21. callTestCase(findTestCase("01_Retailer Portal/RP05_Product Configuration_Page/RP05_1 Skip Product Configuration"), ["planId":planId], STOP_ON_FAILURE)

35
00:02:00,803 --> 00:02:00,812
1. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

36
00:02:00,816 --> 00:02:00,817
5. if (isFibreStandAlone == true)

37
00:02:00,821 --> 00:02:00,823
9. if (isFibreStandAlone == false)

38
00:02:00,827 --> 00:02:01,536
25. callTestCase(findTestCase("01_Retailer Portal/RP06_Cart_Page/RP06_1 Cart Page"), ["msisdn":msisdn, "broadbandPlan":broadbandPlan, "planId":planId], STOP_ON_FAILURE)

39
00:02:01,539 --> 00:02:01,552
1. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

40
00:02:01,556 --> 00:02:01,557
5. if (isFibreStandAlone == true)

41
00:02:01,562 --> 00:02:02,208
1. callTestCase(findTestCase("01_Retailer Portal/RP06A_Link_to_Broadband/RP06A_2 Add Broadband - Fibre"), ["broadbandPlan":planId], STOP_ON_FAILURE)

42
00:02:02,214 --> 00:02:33,748
1. click(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_bundle-list/button_I want this plan", ["broadbandPlan":broadbandPlan]))

