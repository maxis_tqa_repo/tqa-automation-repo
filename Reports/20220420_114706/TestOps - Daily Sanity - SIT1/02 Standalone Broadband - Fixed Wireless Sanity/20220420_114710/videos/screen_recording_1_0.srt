1
00:00:01,775 --> 00:00:02,948
1. callTestCase(findTestCase("01_Retailer Portal/RP-01 New Plan, New Registration, No Device"), ["idType":idType, "idValue":idValue, "planId":planId, "customerName":customerName, "row":row, "msisdn":"", "accountNo":"", ... ], STOP_ON_FAILURE)

2
00:00:02,975 --> 00:00:03,946
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), [:], STOP_ON_FAILURE)

3
00:00:03,950 --> 00:00:17,963
1. openBrowser("")

4
00:00:17,971 --> 00:00:44,327
5. navigateToUrl(RP_URL)

5
00:00:44,337 --> 00:00:44,714
9. maximizeWindow()

6
00:00:44,726 --> 00:00:47,474
15. Username is set as local variable

7
00:00:47,481 --> 00:00:49,988
21. Password is set as local variable

8
00:00:50,000 --> 00:00:51,352
25. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

9
00:00:51,359 --> 00:00:54,600
31. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

10
00:00:54,611 --> 00:00:54,685
35. comment("ASSERTION : [ OK ] ")

11
00:00:54,696 --> 00:00:54,744
39. comment("		Successfully logged in as : " + RP_Username)

12
00:00:54,752 --> 00:00:54,760
5. runId = toString().substring(toString().lastIndexOf("\") + 1)

13
00:00:54,768 --> 00:00:58,461
9. callTestCase(findTestCase("01_Retailer Portal/RP02_Search Customer_Page/RP02_1 Search Customer"), ["idType":idType, "idValue":idValue, "accountNo":accountNo], STOP_ON_FAILURE)

14
00:00:58,738 --> 00:01:02,234
1. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/i_keyboard_arrow_down"))

15
00:01:02,260 --> 00:01:02,270
7. IF statement to check if Account No is populated in the input sheet. If TRUE idType will be forced to ACCOUNT and search done using the accountNo

16
00:01:02,329 --> 00:01:04,896
11. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/Dropdown ID Type", ["idType":idType]))

17
00:01:04,943 --> 00:01:08,039
15. setText(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), idValue)

18
00:01:08,064 --> 00:01:10,427
19. sendKeys(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), Keys.chord(ENTER))

19
00:01:10,436 --> 00:01:53,014
25. Checking for text "Customer Profile does not exist", if text exists the Global Variable newCustomer_Flag is set to TRUE and flow handling will be for New Customer 

20
00:01:53,026 --> 00:01:53,054
29. comment("WORKFLOW HANDLING : ")

21
00:01:53,067 --> 00:01:53,076
33. comment("	ID TYPE : " + idType)

22
00:01:53,087 --> 00:01:53,097
37. comment("	ID VALUE : " + idValue)

23
00:01:53,112 --> 00:01:53,124
41. "	NEW CUSTOMER : " + newCustomer_Flag

24
00:01:53,134 --> 00:01:55,298
13. callTestCase(findTestCase("01_Retailer Portal/RP03_CIC Dashboard_Page/RP03_1 New Purchase"), ["accountNo":accountNo], STOP_ON_FAILURE)

25
00:01:55,309 --> 00:01:55,315
1. comment("")

26
00:01:55,326 --> 00:01:55,334
5. comment("")

27
00:01:55,366 --> 00:01:55,369
11. Different Call To Action elements will be loaded depending on whether the customer is new or an existing one

1. New Purchase button for New Customer,
2. New Purchase button for Existing Customer Existing Account,
3. New Purchase button for Existing Customer New Account

28
00:01:55,378 --> 00:01:57,739
3. click(findTestObject("01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/button_New Purchase"))

29
00:01:57,752 --> 00:02:16,682
15. verifyTextPresent("Catalog", false)

30
00:02:16,690 --> 00:02:16,697
19. comment("ASSERTION : [ OK ] ")

31
00:02:16,710 --> 00:02:16,715
23. comment("	Product Catalogue Page loaded")

32
00:02:16,725 --> 00:02:17,801
27. comment("	Text Catalog present? " + println(verifyTextPresent("Catalog", false)))

33
00:02:17,825 --> 00:02:19,908
17. callTestCase(findTestCase("01_Retailer Portal/RP04_Catalog_Page/RP04_1 Purchase plan only, no device"), ["planId":planId], STOP_ON_FAILURE)

34
00:02:19,934 --> 00:02:21,650
3. Click mobile plan tab

35
00:02:21,672 --> 00:02:23,778
7. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.button.planProduct", ["planProduct":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3)]))

36
00:02:23,869 --> 00:02:25,629
11. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.label.planType", ["planType":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 2)]), OPTIONAL)

37
00:02:25,643 --> 00:02:25,742
15. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

38
00:02:25,767 --> 00:02:25,773
19. comment("")

39
00:02:25,788 --> 00:02:25,791
23. if (isFibreStandAlone == true)

40
00:02:25,797 --> 00:02:25,799
27. if (isFibreStandAlone == false)

41
00:02:25,806 --> 00:02:29,249
1. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/Button Select This Plan", ["planId":planId]))

42
00:02:29,256 --> 00:02:30,323
7. Verfiy that the Plan Name is the same as the one purchased in previous page

43
00:02:30,337 --> 00:02:40,371
11. delay(10)

44
00:02:40,378 --> 00:02:41,744
15. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/button_Continue without Device"))

45
00:02:41,757 --> 00:02:41,761
19. comment("Note that this has been identified as a common failure point. - aabangf")

46
00:02:41,770 --> 00:02:54,473
25. Note that this has been identified as a common failure point. - aabangf

Verify that once Continue WIthout Device button is clicked the page has moved on to the next page by checking that the title of the new page is shown

