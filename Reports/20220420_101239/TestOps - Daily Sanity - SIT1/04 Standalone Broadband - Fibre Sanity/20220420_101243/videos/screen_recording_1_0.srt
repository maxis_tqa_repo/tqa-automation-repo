1
00:00:00,730 --> 00:00:01,446
1. callTestCase(findTestCase("01_Retailer Portal/RP-01 New Plan, New Registration, No Device"), ["idType":idType, "idValue":idValue, "planId":planId, "customerName":customerName, "row":row, "msisdn":"", "accountNo":"", ... ], STOP_ON_FAILURE)

2
00:00:01,475 --> 00:00:02,243
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), [:], STOP_ON_FAILURE)

3
00:00:02,246 --> 00:00:09,790
1. openBrowser("")

4
00:00:09,803 --> 00:00:35,072
5. navigateToUrl(RP_URL)

5
00:00:35,079 --> 00:00:35,396
9. maximizeWindow()

6
00:00:35,407 --> 00:00:37,602
15. Username is set as local variable

7
00:00:37,605 --> 00:00:39,705
21. Password is set as local variable

8
00:00:39,709 --> 00:00:40,839
25. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

9
00:00:40,844 --> 00:00:42,867
31. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

10
00:00:42,874 --> 00:00:42,912
35. comment("ASSERTION : [ OK ] ")

11
00:00:42,924 --> 00:00:42,971
39. comment("		Successfully logged in as : " + RP_Username)

12
00:00:42,978 --> 00:00:42,989
5. runId = toString().substring(toString().lastIndexOf("\") + 1)

13
00:00:43,000 --> 00:00:44,037
9. callTestCase(findTestCase("01_Retailer Portal/RP02_Search Customer_Page/RP02_1 Search Customer"), ["idType":idType, "idValue":idValue, "accountNo":accountNo], STOP_ON_FAILURE)

14
00:00:44,043 --> 00:00:45,125
1. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/i_keyboard_arrow_down"))

15
00:00:45,188 --> 00:00:45,193
7. IF statement to check if Account No is populated in the input sheet. If TRUE idType will be forced to ACCOUNT and search done using the accountNo

16
00:00:45,235 --> 00:00:46,402
11. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/Dropdown ID Type", ["idType":idType]))

17
00:00:46,408 --> 00:00:48,603
15. setText(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), idValue)

18
00:00:48,609 --> 00:00:49,833
19. sendKeys(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), Keys.chord(ENTER))

19
00:00:49,839 --> 00:01:07,644
25. Checking for text "Customer Profile does not exist", if text exists the Global Variable newCustomer_Flag is set to TRUE and flow handling will be for New Customer 

20
00:01:07,648 --> 00:01:07,655
29. comment("WORKFLOW HANDLING : ")

21
00:01:07,666 --> 00:01:07,671
33. comment("	ID TYPE : " + idType)

22
00:01:07,680 --> 00:01:07,685
37. comment("	ID VALUE : " + idValue)

23
00:01:07,696 --> 00:01:07,699
41. "	NEW CUSTOMER : " + newCustomer_Flag

24
00:01:07,705 --> 00:01:08,878
13. callTestCase(findTestCase("01_Retailer Portal/RP03_CIC Dashboard_Page/RP03_1 New Purchase"), ["accountNo":accountNo], STOP_ON_FAILURE)

25
00:01:08,887 --> 00:01:08,891
1. comment("")

26
00:01:08,901 --> 00:01:08,906
5. comment("")

27
00:01:08,921 --> 00:01:08,927
11. Different Call To Action elements will be loaded depending on whether the customer is new or an existing one

1. New Purchase button for New Customer,
2. New Purchase button for Existing Customer Existing Account,
3. New Purchase button for Existing Customer New Account

28
00:01:08,936 --> 00:01:11,100
3. click(findTestObject("01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/button_New Purchase"))

29
00:01:11,112 --> 00:01:28,937
15. verifyTextPresent("Catalog", false)

30
00:01:28,943 --> 00:01:28,948
19. comment("ASSERTION : [ OK ] ")

31
00:01:28,962 --> 00:01:28,967
23. comment("	Product Catalogue Page loaded")

32
00:01:28,978 --> 00:01:29,653
27. comment("	Text Catalog present? " + println(verifyTextPresent("Catalog", false)))

33
00:01:29,663 --> 00:01:30,774
17. callTestCase(findTestCase("01_Retailer Portal/RP04_Catalog_Page/RP04_1 Purchase plan only, no device"), ["planId":planId], STOP_ON_FAILURE)

34
00:01:30,783 --> 00:01:32,017
3. Click mobile plan tab

35
00:01:32,025 --> 00:01:33,402
7. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.button.planProduct", ["planProduct":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3)]))

36
00:01:33,434 --> 00:02:05,154
11. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.label.planType", ["planType":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 2)]), OPTIONAL)

37
00:02:05,158 --> 00:02:05,186
15. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

38
00:02:05,192 --> 00:02:05,195
19. comment("")

39
00:02:05,201 --> 00:02:05,203
23. if (isFibreStandAlone == true)

40
00:02:05,208 --> 00:02:05,211
1. comment("	Step skipped because order flow type is for stand alone Fibre")

41
00:02:05,215 --> 00:02:05,218
27. if (isFibreStandAlone == false)

42
00:02:05,223 --> 00:02:05,938
21. callTestCase(findTestCase("01_Retailer Portal/RP05_Product Configuration_Page/RP05_1 Skip Product Configuration"), ["planId":planId], STOP_ON_FAILURE)

43
00:02:05,947 --> 00:02:05,960
1. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

44
00:02:05,968 --> 00:02:05,969
5. if (isFibreStandAlone == true)

45
00:02:05,976 --> 00:02:05,980
1. comment("	Step skipped because order flow type is for stand alone Fibre")

46
00:02:05,986 --> 00:02:05,990
9. if (isFibreStandAlone == false)

47
00:02:05,993 --> 00:02:07,052
25. callTestCase(findTestCase("01_Retailer Portal/RP06_Cart_Page/RP06_1 Cart Page"), ["msisdn":msisdn, "broadbandPlan":broadbandPlan, "planId":planId], STOP_ON_FAILURE)

48
00:02:07,056 --> 00:02:07,070
1. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

49
00:02:07,074 --> 00:02:07,075
5. if (isFibreStandAlone == true)

50
00:02:07,078 --> 00:02:07,808
1. callTestCase(findTestCase("01_Retailer Portal/RP06A_Link_to_Broadband/RP06A_2 Add Broadband - Fibre"), ["broadbandPlan":planId], STOP_ON_FAILURE)

51
00:02:07,812 --> 00:02:09,875
1. click(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_bundle-list/button_I want this plan", ["broadbandPlan":broadbandPlan]))

52
00:02:09,881 --> 00:02:11,070
5. click(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/input_search_searchAddresInput"))

53
00:02:11,075 --> 00:02:12,129
9. click(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/div_search"))

54
00:02:12,133 --> 00:02:14,193
13. setText(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/input_search_searchAddresInput"), "no 1, Jalan Medan Masria, Taman Masria, 43200 Cheras, Selangor")

55
00:02:14,200 --> 00:02:15,305
17. sendKeys(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/input_search_searchAddresInput"), Keys.chord(ENTER))

56
00:02:15,310 --> 00:02:19,455
21. click(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/i_navigate_next"))

57
00:02:19,459 --> 00:02:21,757
25. setText(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/input_access_time_providerId"), "1-12345678901")

58
00:02:21,760 --> 00:02:23,843
29. setText(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/input_Sample Format_serviceId"), "BU1234567890")

59
00:02:23,847 --> 00:02:24,976
33. click(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/button_Submit and Continue"))

60
00:02:24,980 --> 00:02:36,128
37. click(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_bundle-configuration/button_Next"))

61
00:02:36,134 --> 00:02:37,261
41. enhancedClick(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_bundle-configuration/button_Continue_to_Cart"))

62
00:02:37,266 --> 00:02:39,290
45. delay(2)

63
00:02:39,293 --> 00:02:39,982
5. callTestCase(findTestCase("01_Retailer Portal/RP06A_Link_to_Broadband/RP06A_3 Assign VoIP number"), [:], STOP_ON_FAILURE)

64
00:02:39,985 --> 00:03:11,703
1. click(findTestObject("01_Retailer Portal_Objects/RP06_Cart_Page_Objects/a_Assign Number"))

