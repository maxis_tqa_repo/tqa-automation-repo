1
00:00:01,456 --> 00:00:02,479
1. callTestCase(findTestCase("01_Retailer Portal/RP-TMPL-01 NEW PLAN REGISTRATION NO DEVICE"), ["idType":idType, "idValue":idValue, "planId":planId, "customerName":customerName, "row":row, "msisdn":"", "accountNo":"", ... ], STOP_ON_FAILURE)

2
00:00:02,510 --> 00:00:03,793
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), [:], STOP_ON_FAILURE)

3
00:00:03,798 --> 00:00:16,208
1. openBrowser("")

4
00:00:16,213 --> 00:00:40,331
7. URL is set as a Global Variable controlled in Profiles

5
00:00:40,338 --> 00:00:42,398
13. Username is set as local variable

6
00:00:42,409 --> 00:00:44,377
19. Password is set as local variable

7
00:00:44,384 --> 00:00:45,454
23. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

8
00:00:45,460 --> 00:00:45,482
27. comment("ASSERTION")

9
00:00:45,486 --> 00:00:47,404
33. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

10
00:00:47,411 --> 00:00:47,415
5. runId = toString().substring(toString().lastIndexOf("\") + 1)

11
00:00:47,420 --> 00:00:48,400
9. callTestCase(findTestCase("01_Retailer Portal/RP02_Search Customer_Page/RP02_1 Search Customer"), ["idType":idType, "idValue":idValue, "accountNo":accountNo], STOP_ON_FAILURE)

12
00:00:48,405 --> 00:00:49,548
1. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/i_keyboard_arrow_down"))

13
00:00:49,554 --> 00:00:49,561
5. comment("IF statement to check if Account No is populated in the input sheet. If TRUE idType will be forced to ACCOUNT and search done using the accountNo")

14
00:00:49,568 --> 00:00:49,571
9. if (accountNo.isEmpty() == false)

15
00:00:49,578 --> 00:00:50,770
13. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/Dropdown ID Type", ["idType":idType]))

16
00:00:50,776 --> 00:00:52,759
17. setText(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), idValue)

17
00:00:52,764 --> 00:00:53,835
21. sendKeys(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), Keys.chord(ENTER))

18
00:00:53,839 --> 00:00:53,845
25. comment("Checking for text "Customer Profile does not exist", if text exists the Global Variable newCustomer_Flag is set to TRUE and flow handling will be for New Customer ")

19
00:00:53,849 --> 00:00:54,798
29. newCustomer_Flag = verifyTextPresent("Customer Profile does not exist", false, OPTIONAL)

20
00:00:54,805 --> 00:00:56,199
13. callTestCase(findTestCase("01_Retailer Portal/RP03_CIC Dashboard_Page/RP03_1 New Purchase"), ["accountNo":accountNo], STOP_ON_FAILURE)

21
00:00:56,205 --> 00:00:56,211
1. comment("Different Call To Action elements will be loaded depending on whether the customer is new or an existing one")

22
00:00:56,218 --> 00:00:56,224
5. comment("1. New Purchase button for New Customer, 2. New Purchase button for Existing Customer Existing Account, 3. New Purchase button for Existing Customer New Account")

23
00:00:56,230 --> 00:00:56,233
9. if (newCustomer_Flag == true)

24
00:00:56,241 --> 00:00:58,403
1. click(findTestObject("01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/div_New Purchase"))

25
00:00:58,409 --> 00:00:58,419
13. comment("ASSERTION")

26
00:00:58,426 --> 00:01:11,216
17. verifyTextPresent("Catalog", false)

27
00:01:11,222 --> 00:01:12,106
17. callTestCase(findTestCase("01_Retailer Portal/RP04_Catalog_Page/RP04_1 Purchase plan only, no device"), ["planId":planId], STOP_ON_FAILURE)

28
00:01:12,112 --> 00:01:13,311
3. Click mobile plan tab

29
00:01:13,315 --> 00:01:14,688
7. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.button.planProduct", ["planProduct":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3)]))

30
00:01:14,694 --> 00:01:16,448
11. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.label.planType", ["planType":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 2)]), OPTIONAL)

31
00:01:16,453 --> 00:01:16,525
15. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

32
00:01:16,531 --> 00:01:16,532
19. if (isFibreStandAlone == true)

33
00:01:16,538 --> 00:01:16,540
23. if (isFibreStandAlone == false)

34
00:01:16,544 --> 00:01:48,145
1. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/Button Select This Plan", ["planId":planId]))

