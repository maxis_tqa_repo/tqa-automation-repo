1
00:00:01,558 --> 00:00:02,789
1. callTestCase(findTestCase("01_Retailer Portal/RP-TMPL-01 NEW PLAN REGISTRATION NO DEVICE"), ["idType":"MALAYSIAN ID CARD", "idValue":idValue, "planId":planId, "customerName":customerName, "row":row, "msisdn":"", "accountNo":"", ... ], STOP_ON_FAILURE)

2
00:00:02,815 --> 00:00:03,895
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), [:], STOP_ON_FAILURE)

3
00:00:03,906 --> 00:00:15,936
1. openBrowser("")

4
00:00:15,941 --> 00:00:40,586
7. URL is set as a Global Variable controlled in Profiles

5
00:00:40,592 --> 00:00:42,686
13. Username is set as local variable

6
00:00:42,691 --> 00:00:44,601
19. Password is set as local variable

7
00:00:44,608 --> 00:00:45,652
23. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

8
00:00:45,657 --> 00:00:45,682
27. comment("ASSERTION")

9
00:00:45,688 --> 00:00:47,798
33. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

10
00:00:47,804 --> 00:00:47,811
5. runId = toString().substring(toString().lastIndexOf("\") + 1)

11
00:00:47,819 --> 00:00:48,900
9. callTestCase(findTestCase("01_Retailer Portal/RP02_Search Customer_Page/RP02_1 Search Customer"), ["idType":idType, "idValue":idValue, "accountNo":accountNo], STOP_ON_FAILURE)

12
00:00:48,904 --> 00:00:49,930
1. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/i_keyboard_arrow_down"))

13
00:00:49,936 --> 00:00:49,942
5. comment("IF statement to check if Account No is populated in the input sheet. If TRUE idType will be forced to ACCOUNT and search done using the accountNo")

14
00:00:49,949 --> 00:00:49,952
9. if (accountNo.isEmpty() == false)

15
00:00:49,958 --> 00:00:51,093
13. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/Dropdown ID Type", ["idType":idType]))

16
00:00:51,098 --> 00:00:53,041
17. setText(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), idValue)

17
00:00:53,047 --> 00:00:54,076
21. sendKeys(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), Keys.chord(ENTER))

18
00:00:54,081 --> 00:00:54,085
25. comment("Checking for text "Customer Profile does not exist", if text exists the Global Variable newCustomer_Flag is set to TRUE and flow handling will be for New Customer ")

19
00:00:54,089 --> 00:00:55,126
29. newCustomer_Flag = verifyTextPresent("Customer Profile does not exist", false, OPTIONAL)

20
00:00:55,133 --> 00:00:56,602
13. callTestCase(findTestCase("01_Retailer Portal/RP03_CIC Dashboard_Page/RP03_1 New Purchase"), ["accountNo":accountNo], STOP_ON_FAILURE)

21
00:00:56,609 --> 00:00:56,615
1. comment("Different Call To Action elements will be loaded depending on whether the customer is new or an existing one")

22
00:00:56,623 --> 00:00:56,660
5. comment("1. New Purchase button for New Customer, 2. New Purchase button for Existing Customer Existing Account, 3. New Purchase button for Existing Customer New Account")

23
00:00:56,667 --> 00:00:56,669
9. if (newCustomer_Flag == true)

24
00:00:56,677 --> 00:01:00,131
1. click(findTestObject("01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/div_New Purchase"))

25
00:01:00,138 --> 00:01:00,145
13. comment("ASSERTION")

26
00:01:00,151 --> 00:01:13,065
17. verifyTextPresent("Catalog", false)

27
00:01:13,072 --> 00:01:14,520
17. callTestCase(findTestCase("01_Retailer Portal/RP04_Catalog_Page/RP04_1 Purchase plan only, no device"), ["planId":planId], STOP_ON_FAILURE)

28
00:01:14,527 --> 00:01:15,773
3. Click mobile plan tab

29
00:01:15,778 --> 00:01:17,046
7. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.button.planProduct", ["planProduct":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3)]))

30
00:01:17,053 --> 00:01:48,772
11. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.label.planType", ["planType":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 2)]), OPTIONAL)

31
00:01:48,779 --> 00:01:48,823
15. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

32
00:01:48,828 --> 00:01:48,830
19. if (isFibreStandAlone == true)

33
00:01:48,835 --> 00:01:48,838
23. if (isFibreStandAlone == false)

34
00:01:48,843 --> 00:01:49,809
21. callTestCase(findTestCase("01_Retailer Portal/RP05_Product Configuration_Page/RP05_1 Skip Product Configuration"), ["planId":planId], STOP_ON_FAILURE)

35
00:01:49,813 --> 00:01:49,826
1. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

36
00:01:49,831 --> 00:01:49,832
5. if (isFibreStandAlone == true)

37
00:01:49,838 --> 00:01:49,842
9. if (isFibreStandAlone == false)

38
00:01:49,848 --> 00:01:50,686
25. callTestCase(findTestCase("01_Retailer Portal/RP06_Cart_Page/RP06_1 Cart Page"), ["msisdn":msisdn, "broadbandPlan":broadbandPlan, "planId":planId], STOP_ON_FAILURE)

39
00:01:50,693 --> 00:01:50,710
1. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

40
00:01:50,715 --> 00:01:50,717
5. if (isFibreStandAlone == true)

41
00:01:50,723 --> 00:01:51,670
1. callTestCase(findTestCase("01_Retailer Portal/RP06A_Link_to_Broadband/RP06A_2 Add Broadband - Fibre"), ["broadbandPlan":planId], STOP_ON_FAILURE)

42
00:01:51,676 --> 00:01:54,411
1. click(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_bundle-list/button_I want this plan", ["broadbandPlan":broadbandPlan]))

43
00:01:54,417 --> 00:01:55,553
5. click(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/input_search_searchAddresInput"))

44
00:01:55,560 --> 00:01:56,572
9. click(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/div_search"))

45
00:01:56,578 --> 00:01:58,708
13. setText(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/input_search_searchAddresInput"), "no 1, Jalan Medan Masria, Taman Masria, 43200 Cheras, Selangor")

46
00:01:58,718 --> 00:01:59,935
17. sendKeys(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/input_search_searchAddresInput"), Keys.chord(ENTER))

47
00:01:59,943 --> 00:02:03,891
21. click(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/i_navigate_next"))

48
00:02:03,897 --> 00:02:06,005
25. setText(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/input_access_time_providerId"), "1-12345678901")

49
00:02:06,013 --> 00:02:07,939
29. setText(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/input_Sample Format_serviceId"), "BU1234567890")

50
00:02:07,945 --> 00:02:09,038
33. click(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/button_Submit and Continue"))

51
00:02:09,044 --> 00:02:15,394
37. click(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_bundle-configuration/button_Next"))

52
00:02:15,401 --> 00:02:18,055
41. enhancedClick(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_bundle-configuration/button_Continue_to_Cart"))

53
00:02:18,060 --> 00:02:20,109
45. delay(2)

54
00:02:20,113 --> 00:02:21,030
5. callTestCase(findTestCase("01_Retailer Portal/RP06A_Link_to_Broadband/RP06A_3 Assign VoIP number"), [:], STOP_ON_FAILURE)

55
00:02:21,035 --> 00:02:23,086
1. click(findTestObject("01_Retailer Portal_Objects/RP06_Cart_Page_Objects/a_Assign Number"))

56
00:02:23,094 --> 00:02:24,688
5. click(findTestObject("01_Retailer Portal_Objects/RP06B VoIP Number Assignment/p_60382103247"))

57
00:02:24,696 --> 00:02:25,779
9. click(findTestObject("01_Retailer Portal_Objects/RP06B VoIP Number Assignment/button_Continue"))

58
00:02:25,785 --> 00:02:29,847
9. click(findTestObject("01_Retailer Portal_Objects/RP06_Cart_Page_Objects/button_Check Out"))

59
00:02:29,854 --> 00:02:29,859
9. if (isFibreStandAlone == false)

60
00:02:29,865 --> 00:02:30,812
29. callTestCase(findTestCase("01_Retailer Portal/RP07_Create Customer_Page/RP07_1 Create Customer_Page"), ["idType":idType, "customerName":customerName, "accountNo":accountNo, "idValue":idValue], STOP_ON_FAILURE)

61
00:02:30,818 --> 00:02:30,822
1. isMalaysianCitizen = verifyMatch(idType, "MALAYSIAN ID CARD", false, OPTIONAL)

62
00:02:30,827 --> 00:02:30,830
5. if (accountNo.isEmpty() == false)

63
00:02:30,836 --> 00:02:30,837
9. if (accountNo.isEmpty() == true)

64
00:02:30,843 --> 00:02:32,809
1. verifyTextPresent("Customer Details", false)

65
00:02:32,814 --> 00:02:32,816
5. if (newCustomer_Flag == true)

66
00:02:32,823 --> 00:02:32,824
1. if (isMalaysianCitizen == false)

67
00:02:32,829 --> 00:02:32,831
5. if (isMalaysianCitizen == true)

68
00:02:32,838 --> 00:02:33,873
1. callTestCase(findTestCase("01_Retailer Portal/RP07_Create Customer_Page/RP07_1_6 Insert Data into AMR"), ["idValue":idValue, "customerName":customerName], STOP_ON_FAILURE)

69
00:02:33,878 --> 00:02:34,728
1. com.database.connectSql.connectDB("10.200.50.177", "AMRDB", "1433", "amr_user", "amr_user2015")

70
00:02:34,741 --> 00:02:35,019
5. com.database.connectSql.insertCustomerDetailAMR(idValue, customerName)

71
00:02:35,022 --> 00:02:35,026
9. com.database.connectSql.closeDatabaseConnection()

72
00:02:35,034 --> 00:02:35,981
5. callTestCase(findTestCase("01_Retailer Portal/RP07_Create Customer_Page/RP07_1_5 Validate Contact Address"), [:], STOP_ON_FAILURE)

73
00:02:35,988 --> 00:02:37,129
1. click(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/button_Identity Verification"))

74
00:02:37,139 --> 00:02:38,160
9. callTestCase(findTestCase("01_Retailer Portal/RP07_Create Customer_Page/RP07_1_4 Fill Contact Details Section"), [:], STOP_ON_FAILURE)

75
00:02:38,165 --> 00:02:40,119
1. setText(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/input_Contact Details_email"), Email)

76
00:02:40,124 --> 00:02:42,068
5. setText(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/input_Invalid Email_contactNumber"), "601234567890")

77
00:02:42,074 --> 00:02:43,121
13. callTestCase(findTestCase("01_Retailer Portal/RP07_Create Customer_Page/RP07_1_2 Use Contact Address as Billing Address"), [:], STOP_ON_FAILURE)

78
00:02:43,125 --> 00:02:44,127
1. click(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/label_Use Contact Address as Billing Address"))

79
00:02:44,135 --> 00:02:46,218
5. setText(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/input_Receive a monthly email notification _bc6444"), Email)

80
00:02:46,223 --> 00:02:47,225
9. click(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/div_Regular Paper Bill_CustomCheckbox"))

81
00:02:47,231 --> 00:02:48,391
13. click(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/button_Continue"))

82
00:02:48,399 --> 00:02:48,404
9. if (newCustomer_Flag == false)

83
00:02:48,413 --> 00:02:49,405
33. callTestCase(findTestCase("01_Retailer Portal/RP08_Order Summary_Page/RP08_1 Document Upload and Submit Order"), ["idType":idType], STOP_ON_FAILURE)

84
00:02:49,411 --> 00:02:50,058
1. verifyTextPresent("Order Summary", false, CONTINUE_ON_FAILURE)

85
00:02:50,063 --> 00:03:23,747
5. click(findTestObject("01_Retailer Portal_Objects/RP08_Order Summary_Page_Objects/i_keyboard_arrow_down"))

