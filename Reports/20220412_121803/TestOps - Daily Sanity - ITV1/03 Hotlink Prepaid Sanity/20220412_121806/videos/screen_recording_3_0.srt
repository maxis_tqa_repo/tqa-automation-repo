1
00:00:01,455 --> 00:00:03,115
1. callTestCase(findTestCase("01_Retailer Portal/RP-TMPL-06 HOTLINK PASS PURCHASE"), ["msisdn":msisdn], STOP_ON_FAILURE)

2
00:00:03,129 --> 00:00:03,238
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), [:], STOP_ON_FAILURE)

3
00:00:03,245 --> 00:00:10,000
1. openBrowser("")

4
00:00:10,006 --> 00:00:32,989
7. URL is set as a Global Variable controlled in Profiles

5
00:00:32,996 --> 00:00:34,962
13. Username is set as local variable

6
00:00:34,969 --> 00:00:36,907
19. Password is set as local variable

7
00:00:36,911 --> 00:00:37,928
23. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

8
00:00:37,934 --> 00:00:37,940
27. comment("ASSERTION")

9
00:00:37,951 --> 00:00:39,956
33. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

10
00:00:39,965 --> 00:00:40,057
5. callTestCase(findTestCase("01_Retailer Portal/RP02_Search Customer_Page/RP02_1 Search Customer"), ["idType":"MSISDN", "idValue":msisdn, "accountNo":""], STOP_ON_FAILURE)

11
00:00:40,067 --> 00:00:41,108
1. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/i_keyboard_arrow_down"))

12
00:00:41,121 --> 00:00:41,127
5. comment("IF statement to check if Account No is populated in the input sheet. If TRUE idType will be forced to ACCOUNT and search done using the accountNo")

13
00:00:41,137 --> 00:00:41,138
9. if (accountNo.isEmpty() == false)

14
00:00:41,147 --> 00:00:42,223
13. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/Dropdown ID Type", ["idType":idType]))

15
00:00:42,230 --> 00:00:44,115
17. setText(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), idValue)

16
00:00:44,125 --> 00:00:45,113
21. sendKeys(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), Keys.chord(ENTER))

17
00:00:45,118 --> 00:00:45,123
25. comment("Checking for text "Customer Profile does not exist", if text exists the Global Variable newCustomer_Flag is set to TRUE and flow handling will be for New Customer ")

18
00:00:45,131 --> 00:00:51,047
29. newCustomer_Flag = verifyTextPresent("Customer Profile does not exist", false, OPTIONAL)

19
00:00:51,057 --> 00:00:52,089
9. enhancedClick(findTestObject("01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/i_expand_more_PrepaidAccount"))

20
00:00:52,100 --> 00:01:24,045
13. if (verifyElementClickable(findTestObject("01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/a_prepaid_keyboard_arrow_right", ["msisdn":msisdn])) == false)

