1
00:00:00,923 --> 00:00:01,020
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), [:], STOP_ON_FAILURE)

2
00:00:01,030 --> 00:00:07,294
1. openBrowser("")

3
00:00:07,301 --> 00:00:30,578
7. URL is set as a Global Variable controlled in Profiles

4
00:00:30,591 --> 00:00:32,646
13. Username is set as local variable

5
00:00:32,653 --> 00:00:34,558
19. Password is set as local variable

6
00:00:34,563 --> 00:00:35,754
23. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

7
00:00:35,761 --> 00:00:35,766
27. comment("ASSERTION")

8
00:00:35,784 --> 00:00:38,972
33. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

9
00:00:38,988 --> 00:00:40,038
5. callTestCase(findTestCase("01_Retailer Portal/RP02_Search Customer_Page/RP02_1 Search Customer"), ["idType":"MSISDN", "idValue":msisdn, "accountNo":""], STOP_ON_FAILURE)

10
00:00:40,047 --> 00:00:41,071
1. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/i_keyboard_arrow_down"))

11
00:00:41,088 --> 00:00:41,093
5. comment("IF statement to check if Account No is populated in the input sheet. If TRUE idType will be forced to ACCOUNT and search done using the accountNo")

12
00:00:41,104 --> 00:00:41,108
9. if (accountNo.isEmpty() == false)

13
00:00:41,120 --> 00:00:42,282
13. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/Dropdown ID Type", ["idType":idType]))

14
00:00:42,292 --> 00:00:44,278
17. setText(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), idValue)

15
00:00:44,283 --> 00:00:45,312
21. sendKeys(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), Keys.chord(ENTER))

16
00:00:45,317 --> 00:00:45,322
25. comment("Checking for text "Customer Profile does not exist", if text exists the Global Variable newCustomer_Flag is set to TRUE and flow handling will be for New Customer ")

17
00:00:45,330 --> 00:00:51,798
29. newCustomer_Flag = verifyTextPresent("Customer Profile does not exist", false, OPTIONAL)

18
00:00:51,804 --> 00:00:52,903
9. enhancedClick(findTestObject("01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/i_expand_more_PrepaidAccount"))

19
00:00:52,916 --> 00:01:24,935
13. if (verifyElementClickable(findTestObject("01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/a_prepaid_keyboard_arrow_right", ["msisdn":msisdn])) == false)

