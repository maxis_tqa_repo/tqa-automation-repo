1
00:00:01,008 --> 00:00:01,117
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), [:], STOP_ON_FAILURE)

2
00:00:01,129 --> 00:00:07,016
1. openBrowser("")

3
00:00:07,023 --> 00:00:31,071
7. URL is set as a Global Variable controlled in Profiles

4
00:00:31,077 --> 00:00:33,042
13. Username is set as local variable

5
00:00:33,049 --> 00:00:34,994
19. Password is set as local variable

6
00:00:35,001 --> 00:00:36,035
23. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

7
00:00:36,041 --> 00:00:36,048
27. comment("ASSERTION")

8
00:00:36,058 --> 00:00:37,981
33. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

9
00:00:37,990 --> 00:00:39,176
5. callTestCase(findTestCase("01_Retailer Portal/RP02_Search Customer_Page/RP02_1 Search Customer"), ["idType":"MSISDN", "idValue":msisdn, "accountNo":""], STOP_ON_FAILURE)

10
00:00:39,188 --> 00:00:40,338
1. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/i_keyboard_arrow_down"))

11
00:00:40,345 --> 00:00:40,351
5. comment("IF statement to check if Account No is populated in the input sheet. If TRUE idType will be forced to ACCOUNT and search done using the accountNo")

12
00:00:40,366 --> 00:00:40,369
9. if (accountNo.isEmpty() == false)

13
00:00:40,382 --> 00:00:41,624
13. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/Dropdown ID Type", ["idType":idType]))

14
00:00:41,636 --> 00:00:43,577
17. setText(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), idValue)

15
00:00:43,586 --> 00:00:44,627
21. sendKeys(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), Keys.chord(ENTER))

16
00:00:44,632 --> 00:00:44,637
25. comment("Checking for text "Customer Profile does not exist", if text exists the Global Variable newCustomer_Flag is set to TRUE and flow handling will be for New Customer ")

17
00:00:44,645 --> 00:00:52,689
29. newCustomer_Flag = verifyTextPresent("Customer Profile does not exist", false, OPTIONAL)

18
00:00:52,696 --> 00:00:53,928
9. enhancedClick(findTestObject("01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/i_expand_more_PrepaidAccount"))

19
00:00:53,940 --> 00:01:26,107
13. if (verifyElementClickable(findTestObject("01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/a_prepaid_keyboard_arrow_right", ["msisdn":msisdn])) == false)

