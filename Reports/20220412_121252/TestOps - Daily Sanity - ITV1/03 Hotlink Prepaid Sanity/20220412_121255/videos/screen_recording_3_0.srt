1
00:00:00,856 --> 00:00:01,750
1. callTestCase(findTestCase("01_Retailer Portal/RP-TMPL-06 HOTLINK PASS PURCHASE"), ["msisdn":msisdn], STOP_ON_FAILURE)

2
00:00:01,759 --> 00:00:01,850
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), [:], STOP_ON_FAILURE)

3
00:00:01,856 --> 00:00:08,056
1. openBrowser("")

4
00:00:08,063 --> 00:00:31,396
7. URL is set as a Global Variable controlled in Profiles

5
00:00:31,403 --> 00:00:33,398
13. Username is set as local variable

6
00:00:33,410 --> 00:00:35,438
19. Password is set as local variable

7
00:00:35,444 --> 00:00:36,550
23. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

8
00:00:36,564 --> 00:00:36,570
27. comment("ASSERTION")

9
00:00:36,581 --> 00:00:38,449
33. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

10
00:00:38,461 --> 00:00:38,554
5. callTestCase(findTestCase("01_Retailer Portal/RP02_Search Customer_Page/RP02_1 Search Customer"), ["idType":"MSISDN", "idValue":msisdn, "accountNo":""], STOP_ON_FAILURE)

11
00:00:38,560 --> 00:00:39,598
1. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/i_keyboard_arrow_down"))

12
00:00:39,611 --> 00:00:39,616
5. comment("IF statement to check if Account No is populated in the input sheet. If TRUE idType will be forced to ACCOUNT and search done using the accountNo")

13
00:00:39,627 --> 00:00:39,628
9. if (accountNo.isEmpty() == false)

14
00:00:39,637 --> 00:00:40,715
13. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/Dropdown ID Type", ["idType":idType]))

15
00:00:40,722 --> 00:00:42,592
17. setText(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), idValue)

16
00:00:42,598 --> 00:00:43,587
21. sendKeys(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), Keys.chord(ENTER))

17
00:00:43,598 --> 00:00:43,603
25. comment("Checking for text "Customer Profile does not exist", if text exists the Global Variable newCustomer_Flag is set to TRUE and flow handling will be for New Customer ")

18
00:00:43,612 --> 00:00:44,136
29. newCustomer_Flag = verifyTextPresent("Customer Profile does not exist", false, OPTIONAL)

