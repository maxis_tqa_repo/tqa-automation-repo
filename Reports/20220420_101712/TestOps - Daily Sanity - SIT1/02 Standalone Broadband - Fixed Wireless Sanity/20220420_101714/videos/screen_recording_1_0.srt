1
00:00:00,825 --> 00:00:01,437
1. callTestCase(findTestCase("01_Retailer Portal/RP-01 New Plan, New Registration, No Device"), ["idType":idType, "idValue":idValue, "planId":planId, "customerName":customerName, "row":row, "msisdn":"", "accountNo":"", ... ], STOP_ON_FAILURE)

2
00:00:01,469 --> 00:00:02,441
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), [:], STOP_ON_FAILURE)

3
00:00:02,446 --> 00:00:09,133
1. openBrowser("")

4
00:00:09,138 --> 00:00:33,460
5. navigateToUrl(RP_URL)

5
00:00:33,467 --> 00:00:33,758
9. maximizeWindow()

6
00:00:33,767 --> 00:00:36,139
15. Username is set as local variable

7
00:00:36,152 --> 00:00:38,220
21. Password is set as local variable

8
00:00:38,223 --> 00:00:39,449
25. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

9
00:00:39,456 --> 00:00:41,536
31. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

10
00:00:41,545 --> 00:00:41,571
35. comment("ASSERTION : [ OK ] ")

11
00:00:41,577 --> 00:00:41,610
39. comment("		Successfully logged in as : " + RP_Username)

12
00:00:41,621 --> 00:00:41,626
5. runId = toString().substring(toString().lastIndexOf("\") + 1)

13
00:00:41,637 --> 00:00:43,197
9. callTestCase(findTestCase("01_Retailer Portal/RP02_Search Customer_Page/RP02_1 Search Customer"), ["idType":idType, "idValue":idValue, "accountNo":accountNo], STOP_ON_FAILURE)

14
00:00:43,326 --> 00:00:44,623
1. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/i_keyboard_arrow_down"))

15
00:00:44,646 --> 00:00:44,654
7. IF statement to check if Account No is populated in the input sheet. If TRUE idType will be forced to ACCOUNT and search done using the accountNo

16
00:00:44,666 --> 00:00:46,128
11. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/Dropdown ID Type", ["idType":idType]))

17
00:00:46,140 --> 00:00:48,294
15. setText(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), idValue)

18
00:00:48,302 --> 00:00:49,519
19. sendKeys(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), Keys.chord(ENTER))

19
00:00:49,522 --> 00:01:11,774
25. Checking for text "Customer Profile does not exist", if text exists the Global Variable newCustomer_Flag is set to TRUE and flow handling will be for New Customer 

20
00:01:11,780 --> 00:01:11,790
29. comment("WORKFLOW HANDLING : ")

21
00:01:11,801 --> 00:01:11,807
33. comment("	ID TYPE : " + idType)

22
00:01:11,819 --> 00:01:11,826
37. comment("	ID VALUE : " + idValue)

23
00:01:11,839 --> 00:01:11,846
41. "	NEW CUSTOMER : " + newCustomer_Flag

24
00:01:11,854 --> 00:01:13,130
13. callTestCase(findTestCase("01_Retailer Portal/RP03_CIC Dashboard_Page/RP03_1 New Purchase"), ["accountNo":accountNo], STOP_ON_FAILURE)

25
00:01:13,139 --> 00:01:13,143
1. comment("")

26
00:01:13,153 --> 00:01:13,157
5. comment("")

27
00:01:13,170 --> 00:01:13,175
11. Different Call To Action elements will be loaded depending on whether the customer is new or an existing one

1. New Purchase button for New Customer,
2. New Purchase button for Existing Customer Existing Account,
3. New Purchase button for Existing Customer New Account

28
00:01:13,187 --> 00:01:17,063
3. click(findTestObject("01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/button_New Purchase"))

29
00:01:17,077 --> 00:01:35,290
15. verifyTextPresent("Catalog", false)

30
00:01:35,297 --> 00:01:35,303
19. comment("ASSERTION : [ OK ] ")

31
00:01:35,314 --> 00:01:35,318
23. comment("	Product Catalogue Page loaded")

32
00:01:35,327 --> 00:01:36,328
27. comment("	Text Catalog present? " + println(verifyTextPresent("Catalog", false)))

33
00:01:36,338 --> 00:01:37,565
17. callTestCase(findTestCase("01_Retailer Portal/RP04_Catalog_Page/RP04_1 Purchase plan only, no device"), ["planId":planId], STOP_ON_FAILURE)

34
00:01:37,572 --> 00:01:39,085
3. Click mobile plan tab

35
00:01:39,092 --> 00:01:40,386
7. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.button.planProduct", ["planProduct":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3)]))

36
00:01:40,392 --> 00:01:42,029
11. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.label.planType", ["planType":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 2)]), OPTIONAL)

37
00:01:42,037 --> 00:01:42,102
15. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

38
00:01:42,114 --> 00:01:42,135
19. comment("")

39
00:01:42,145 --> 00:01:42,147
23. if (isFibreStandAlone == true)

40
00:01:42,160 --> 00:01:42,161
27. if (isFibreStandAlone == false)

41
00:01:42,179 --> 00:01:44,821
1. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/Button Select This Plan", ["planId":planId]))

42
00:01:44,837 --> 00:02:00,407
7. Verfiy that the Plan Name is the same as the one purchased in previous page

43
00:02:00,410 --> 00:02:10,434
11. delay(10)

44
00:02:10,438 --> 00:02:11,596
15. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/button_Continue without Device"))

45
00:02:11,610 --> 00:02:11,613
19. comment("Note that this has been identified as a common failure point. - aabangf")

46
00:02:11,622 --> 00:02:23,450
25. Note that this has been identified as a common failure point. - aabangf

Verify that once Continue WIthout Device button is clicked the page has moved on to the next page by checking that the title of the new page is shown

