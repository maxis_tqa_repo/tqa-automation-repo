1
00:00:00,938 --> 00:00:01,029
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), ["username":"SUPERRR", "password":"test"], STOP_ON_FAILURE)

2
00:00:01,038 --> 00:00:08,981
1. openBrowser("")

3
00:00:08,991 --> 00:00:32,590
7. URL is set as a Global Variable controlled in Profiles

4
00:00:32,596 --> 00:00:34,682
13. Username is set as local variable

5
00:00:34,695 --> 00:00:36,660
19. Password is set as local variable

6
00:00:36,665 --> 00:00:37,698
23. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

7
00:00:37,705 --> 00:00:37,711
27. comment("ASSERTION")

8
00:00:37,718 --> 00:00:39,716
33. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

9
00:00:39,729 --> 00:00:39,822
5. callTestCase(findTestCase("01_Retailer Portal/RP02_Search Customer_Page/RP02_1 Search Customer"), ["idType":"ServiceID", "idValue":"60178498360", "accountNo":""], STOP_ON_FAILURE)

10
00:00:39,836 --> 00:00:40,872
1. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/i_keyboard_arrow_down"))

11
00:00:40,886 --> 00:00:40,891
5. comment("IF statement to check if Account No is populated in the input sheet. If TRUE idType will be forced to ACCOUNT and search done using the accountNo")

12
00:00:40,901 --> 00:00:40,902
9. if (accountNo.isEmpty() == false)

13
00:00:40,914 --> 00:00:42,044
13. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/Dropdown ID Type", ["idType":idType]))

14
00:00:42,057 --> 00:00:43,993
17. setText(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), idValue)

15
00:00:43,998 --> 00:00:45,006
21. sendKeys(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), Keys.chord(ENTER))

16
00:00:45,021 --> 00:00:45,026
25. comment("Checking for text "Customer Profile does not exist", if text exists the Global Variable newCustomer_Flag is set to TRUE and flow handling will be for New Customer ")

17
00:00:45,040 --> 00:00:46,116
29. newCustomer_Flag = verifyTextPresent("Customer Profile does not exist", false, OPTIONAL)

18
00:00:46,158 --> 00:00:47,097
9. verifyTextPresent("Searched By ServiceID 60178498360", false)

