1
00:00:01,622 --> 00:00:02,714
1. callTestCase(findTestCase("01_Retailer Portal/RP-TMPL-01 NEW PLAN REGISTRATION NO DEVICE"), ["idType":idType, "idValue":idValue, "planId":planId, "customerName":customerName, "row":row, "msisdn":"", "accountNo":"", ... ], STOP_ON_FAILURE)

2
00:00:02,746 --> 00:00:03,891
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), [:], STOP_ON_FAILURE)

3
00:00:03,895 --> 00:00:17,653
1. openBrowser("")

4
00:00:17,659 --> 00:00:42,500
7. URL is set as a Global Variable controlled in Profiles

5
00:00:42,506 --> 00:00:44,661
13. Username is set as local variable

6
00:00:44,672 --> 00:00:46,609
19. Password is set as local variable

7
00:00:46,619 --> 00:00:47,732
23. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

8
00:00:47,740 --> 00:00:47,767
27. comment("ASSERTION")

9
00:00:47,775 --> 00:00:50,004
33. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

10
00:00:50,015 --> 00:00:50,021
5. runId = toString().substring(toString().lastIndexOf("\") + 1)

11
00:00:50,034 --> 00:00:51,311
9. callTestCase(findTestCase("01_Retailer Portal/RP02_Search Customer_Page/RP02_1 Search Customer"), ["idType":idType, "idValue":idValue, "accountNo":accountNo], STOP_ON_FAILURE)

12
00:00:51,320 --> 00:00:52,381
1. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/i_keyboard_arrow_down"))

13
00:00:52,395 --> 00:00:52,402
5. comment("IF statement to check if Account No is populated in the input sheet. If TRUE idType will be forced to ACCOUNT and search done using the accountNo")

14
00:00:52,415 --> 00:00:52,418
9. if (accountNo.isEmpty() == false)

15
00:00:52,429 --> 00:00:53,733
13. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/Dropdown ID Type", ["idType":idType]))

16
00:00:53,744 --> 00:00:55,712
17. setText(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), idValue)

17
00:00:55,716 --> 00:00:56,799
21. sendKeys(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), Keys.chord(ENTER))

18
00:00:56,809 --> 00:00:56,813
25. comment("Checking for text "Customer Profile does not exist", if text exists the Global Variable newCustomer_Flag is set to TRUE and flow handling will be for New Customer ")

19
00:00:56,820 --> 00:00:57,942
29. newCustomer_Flag = verifyTextPresent("Customer Profile does not exist", false, OPTIONAL)

20
00:00:57,949 --> 00:00:59,620
13. callTestCase(findTestCase("01_Retailer Portal/RP03_CIC Dashboard_Page/RP03_1 New Purchase"), ["accountNo":accountNo], STOP_ON_FAILURE)

21
00:00:59,637 --> 00:00:59,666
1. comment("Different Call To Action elements will be loaded depending on whether the customer is new or an existing one")

22
00:00:59,683 --> 00:00:59,691
5. comment("1. New Purchase button for New Customer, 2. New Purchase button for Existing Customer Existing Account, 3. New Purchase button for Existing Customer New Account")

23
00:00:59,704 --> 00:00:59,707
9. if (newCustomer_Flag == true)

24
00:00:59,733 --> 00:01:01,984
1. click(findTestObject("01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/div_New Purchase"))

25
00:01:01,990 --> 00:01:01,998
13. comment("ASSERTION")

26
00:01:02,007 --> 00:01:18,796
17. verifyTextPresent("Catalog", false)

27
00:01:18,802 --> 00:01:20,048
17. callTestCase(findTestCase("01_Retailer Portal/RP04_Catalog_Page/RP04_1 Purchase plan only, no device"), ["planId":planId], STOP_ON_FAILURE)

28
00:01:20,058 --> 00:01:21,374
3. Click mobile plan tab

29
00:01:21,383 --> 00:01:22,740
7. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.button.planProduct", ["planProduct":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3)]))

30
00:01:22,748 --> 00:01:24,950
11. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.label.planType", ["planType":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 2)]), OPTIONAL)

31
00:01:24,957 --> 00:01:25,042
15. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

32
00:01:25,050 --> 00:01:25,053
19. if (isFibreStandAlone == true)

33
00:01:25,064 --> 00:01:25,065
23. if (isFibreStandAlone == false)

34
00:01:25,074 --> 00:01:27,960
1. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/Button Select This Plan", ["planId":planId]))

35
00:01:27,972 --> 00:01:48,826
7. Verfiy that the Plan Name is the same as the one purchased in previous page

36
00:01:48,834 --> 00:01:50,088
11. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/button_Continue without Device"))

37
00:01:50,112 --> 00:01:51,387
21. callTestCase(findTestCase("01_Retailer Portal/RP05_Product Configuration_Page/RP05_1 Skip Product Configuration"), ["planId":planId], STOP_ON_FAILURE)

38
00:01:51,399 --> 00:01:51,423
1. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

39
00:01:51,436 --> 00:01:51,438
5. if (isFibreStandAlone == true)

40
00:01:51,457 --> 00:01:51,460
9. if (isFibreStandAlone == false)

41
00:01:51,473 --> 00:02:10,164
1. click(findTestObject("01_Retailer Portal_Objects/RP05_Product Configuration_Page_Objects/button_Next"))

42
00:02:10,174 --> 00:02:18,655
5. verifyTextPresent("Selected Plan", false)

43
00:02:18,662 --> 00:02:21,293
9. click(findTestObject("01_Retailer Portal_Objects/RP05_Product Configuration_Page_Objects/button_Continue to Cart"))

44
00:02:21,309 --> 00:02:22,774
25. callTestCase(findTestCase("01_Retailer Portal/RP06_Cart_Page/RP06_1 Cart Page"), ["msisdn":msisdn, "broadbandPlan":broadbandPlan, "planId":planId], STOP_ON_FAILURE)

45
00:02:22,781 --> 00:02:22,801
1. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

46
00:02:22,820 --> 00:02:22,822
5. if (isFibreStandAlone == true)

47
00:02:22,840 --> 00:02:22,842
9. if (isFibreStandAlone == false)

48
00:02:22,857 --> 00:02:22,860
3. If msisdn in the input files is empty, then the order will just use the assigned msisdn

49
00:02:22,877 --> 00:02:22,880
1. callTestCase(findTestCase("01_Retailer Portal/RP06_Cart_Page/RP06_5 Link Broadband and Check Out"), ["broadbandPlan":broadbandPlan], STOP_ON_FAILURE)

50
00:02:22,893 --> 00:02:24,018
5. callTestCase(findTestCase("01_Retailer Portal/RP06_Cart_Page/RP06_5 Link Broadband and Check Out - Copy"), ["broadbandPlan":broadbandPlan, "planId":planId], STOP_ON_FAILURE)

51
00:02:24,026 --> 00:02:24,043
1. isplanTypeUnlimited = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 2), "Unlimited", false, OPTIONAL)

52
00:02:24,053 --> 00:02:24,072
5. isplanTypeFamilyPlan = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 2), "Family Plan", false, OPTIONAL)

53
00:02:24,083 --> 00:02:24,103
9. isplanTypePrime = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 2), "Prime", false, OPTIONAL)

54
00:02:24,118 --> 00:02:24,120
13. if (isplanTypeUnlimited == true)

55
00:02:24,126 --> 00:02:24,127
17. if (isplanTypeFamilyPlan == true)

56
00:02:24,134 --> 00:02:24,135
21. if (isplanTypePrime == true)

57
00:02:24,144 --> 00:02:25,458
25. click(findTestObject("01_Retailer Portal_Objects/RP06_Cart_Page_Objects/button_Check Out"))

58
00:02:25,467 --> 00:02:26,873
29. callTestCase(findTestCase("01_Retailer Portal/RP07_Create Customer_Page/RP07_1 Create Customer_Page"), ["idType":idType, "customerName":customerName, "accountNo":accountNo, "idValue":idValue], STOP_ON_FAILURE)

59
00:02:26,886 --> 00:02:26,896
1. isMalaysianCitizen = verifyMatch(idType, "MALAYSIAN ID CARD", false, OPTIONAL)

60
00:02:26,906 --> 00:02:26,908
5. if (accountNo.isEmpty() == false)

61
00:02:26,918 --> 00:02:26,919
9. if (accountNo.isEmpty() == true)

62
00:02:26,926 --> 00:02:29,661
1. verifyTextPresent("Customer Details", false)

63
00:02:29,678 --> 00:02:29,680
5. if (newCustomer_Flag == true)

64
00:02:29,690 --> 00:02:29,691
1. if (isMalaysianCitizen == false)

65
00:02:29,704 --> 00:02:30,902
1. callTestCase(findTestCase("01_Retailer Portal/RP07_Create Customer_Page/RP07_1_1 Fill Contact Address"), ["customerName":customerName], STOP_ON_FAILURE)

66
00:02:30,914 --> 00:02:32,040
1. click(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/button_Manual Entry"), OPTIONAL)

67
00:02:32,048 --> 00:02:34,189
5. setText(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/input_Non-Malaysian_nameAsPerId"), customerName, OPTIONAL)

68
00:02:34,205 --> 00:02:35,284
9. click(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/button_month"), OPTIONAL)

69
00:02:35,300 --> 00:02:36,422
13. click(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/a_Aug"), OPTIONAL)

70
00:02:36,438 --> 00:02:38,463
17. setText(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/input_Dec_day"), "31", OPTIONAL)

71
00:02:38,471 --> 00:02:40,417
21. setText(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/input_Dec_year"), "1957", OPTIONAL)

72
00:02:40,428 --> 00:02:41,489
25. click(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/label_Male"), OPTIONAL)

73
00:02:41,498 --> 00:02:42,589
29. click(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/button_Validate Passport Details"), OPTIONAL)

74
00:02:42,603 --> 00:02:44,723
33. setText(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/input_Address_contactAddreslineOne"), "addressLine1")

75
00:02:44,732 --> 00:02:46,737
37. setText(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/input_Address_contactAddreslineTwo"), "addressLine2")

76
00:02:46,746 --> 00:02:48,804
41. setText(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/input_Address_contactAddreslineThree"), "addressLine3")

77
00:02:48,821 --> 00:02:50,903
45. setText(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/input_Address_contactPostalCode"), "43200")

78
00:02:50,916 --> 00:02:52,095
49. click(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/div_(function(loadWidget)        var domPla_a0cbbf_1"))

79
00:02:52,118 --> 00:02:53,193
5. callTestCase(findTestCase("01_Retailer Portal/RP07_Create Customer_Page/RP07_1_4 Fill Contact Details Section"), [:], STOP_ON_FAILURE)

80
00:02:53,199 --> 00:02:55,148
1. setText(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/input_Contact Details_email"), Email)

81
00:02:55,158 --> 00:02:57,332
5. setText(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/input_Invalid Email_contactNumber"), "601234567890")

82
00:02:57,339 --> 00:02:58,422
9. callTestCase(findTestCase("01_Retailer Portal/RP07_Create Customer_Page/RP07_1_2 Use Contact Address as Billing Address"), [:], STOP_ON_FAILURE)

83
00:02:58,433 --> 00:02:59,502
1. click(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/label_Use Contact Address as Billing Address"))

84
00:02:59,514 --> 00:03:01,483
5. setText(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/input_Receive a monthly email notification _bc6444"), Email)

85
00:03:01,497 --> 00:03:02,608
9. click(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/div_Regular Paper Bill_CustomCheckbox"))

86
00:03:02,615 --> 00:03:03,761
13. click(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/button_Continue"))

87
00:03:03,794 --> 00:03:03,796
5. if (isMalaysianCitizen == true)

88
00:03:03,812 --> 00:03:03,818
9. if (newCustomer_Flag == false)

89
00:03:03,829 --> 00:03:05,181
33. callTestCase(findTestCase("01_Retailer Portal/RP08_Order Summary_Page/RP08_1 Document Upload and Submit Order"), ["idType":idType], STOP_ON_FAILURE)

90
00:03:05,186 --> 00:03:11,319
1. verifyTextPresent("Order Summary", false, CONTINUE_ON_FAILURE)

91
00:03:11,324 --> 00:03:12,381
5. click(findTestObject("01_Retailer Portal_Objects/RP08_Order Summary_Page_Objects/i_keyboard_arrow_down"))

92
00:03:12,402 --> 00:03:13,483
9. click(findTestObject("01_Retailer Portal_Objects/RP08_Order Summary_Page_Objects/DropDownID_Type", ["idType":idType]))

93
00:03:13,488 --> 00:03:13,492
13. dirName = getProjectDir()

94
00:03:13,501 --> 00:03:13,502
17. upload = "" + dirName + "\Data Files\Test Data and Input Files\Uploads\maxis.jpg"

95
00:03:13,516 --> 00:03:13,518
21. uploadPath = upload.replace("/", "\")

96
00:03:13,528 --> 00:04:46,234
25. com.uploadfile.UploadFile.uploadFileUsingRobot(findTestObject("01_Retailer Portal_Objects/RP08_Order Summary_Page_Objects/div_Browse"), uploadPath)

