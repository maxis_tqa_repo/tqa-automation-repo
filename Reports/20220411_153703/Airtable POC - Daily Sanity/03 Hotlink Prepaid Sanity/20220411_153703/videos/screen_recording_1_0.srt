1
00:00:01,400 --> 00:00:03,009
1. callTestCase(findTestCase("01_Retailer Portal/RP-TMPL-04 PREPAID STARTER PACK REGISTRATION - POC"), ["idValue":idValue, "customerName":customerName, "msisdn":msisdn, "row":row], STOP_ON_FAILURE)

2
00:00:03,041 --> 00:00:04,035
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), ["username":"mcSup", "password":"123"], STOP_ON_FAILURE)

3
00:00:04,040 --> 00:00:15,668
1. openBrowser("")

4
00:00:15,675 --> 00:00:31,464
7. URL is set as a Global Variable controlled in Profiles

5
00:00:31,469 --> 00:00:34,296
13. Username is set as local variable

6
00:00:34,304 --> 00:00:36,124
19. Password is set as local variable

7
00:00:36,130 --> 00:00:37,212
23. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

8
00:00:37,240 --> 00:00:37,271
27. comment("ASSERTION")

9
00:00:37,281 --> 00:00:39,344
33. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

10
00:00:39,353 --> 00:00:40,324
5. callTestCase(findTestCase("01_Retailer Portal/RP07_Create Customer_Page/RP07_1_6 Insert Data into AMR"), ["idValue":idValue, "customerName":customerName], STOP_ON_FAILURE)

11
00:00:40,334 --> 00:00:41,260
1. com.database.connectSql.connectDB("10.200.50.177", "AMRDB", "1433", "amr_user", "amr_user2015")

12
00:00:41,268 --> 00:00:41,432
5. com.database.connectSql.insertCustomerDetailAMR(idValue, customerName)

13
00:00:41,443 --> 00:00:41,449
9. com.database.connectSql.closeDatabaseConnection()

14
00:00:41,459 --> 00:00:42,530
9. callTestCase(findTestCase("01_Retailer Portal/RP03_CIC Dashboard_Page/RP03_4 Starter Pack Registration"), [:], STOP_ON_FAILURE)

15
00:00:42,538 --> 00:00:44,148
1. click(findTestObject("01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/div_Starter Pack Registration"))

16
00:00:44,158 --> 00:00:45,116
13. callTestCase(findTestCase("01_Retailer Portal/RP13_Starter Pack Registration_Page/RP13_1 Register Prepaid Starter Pack via NRIC"), ["simSerial":simSerial, "msisdn":msisdn, "idValue":idValue], STOP_ON_FAILURE)

17
00:00:45,121 --> 00:00:47,021
1. setText(findTestObject("01_Retailer Portal_Objects/RP13_Starter Pack Registration Page_Objects/input_idForSimNum"), simSerial)

18
00:00:47,031 --> 00:00:49,006
5. setText(findTestObject("01_Retailer Portal_Objects/RP13_Starter Pack Registration Page_Objects/input_idForMsisdnNum"), msisdn)

19
00:00:49,011 --> 00:00:50,036
9. click(findTestObject("01_Retailer Portal_Objects/RP13_Starter Pack Registration Page_Objects/button_Verify"))

20
00:00:50,052 --> 00:01:21,760
13. click(findTestObject("01_Retailer Portal_Objects/RP13_Starter Pack Registration Page_Objects/button_NRIC"))

