1
00:00:01,065 --> 00:00:01,870
1. callTestCase(findTestCase("01_Retailer Portal/RP-TMPL-01 NEW PLAN REGISTRATION NO DEVICE"), ["idType":idType, "idValue":idValue, "planId":planId, "customerName":customerName, "row":row, "msisdn":"", "accountNo":"", ... ], STOP_ON_FAILURE)

2
00:00:01,892 --> 00:00:02,763
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), [:], STOP_ON_FAILURE)

3
00:00:02,768 --> 00:00:11,640
1. openBrowser("")

4
00:00:11,646 --> 00:00:35,248
7. URL is set as a Global Variable controlled in Profiles

5
00:00:35,253 --> 00:00:37,239
13. Username is set as local variable

6
00:00:37,244 --> 00:00:39,099
19. Password is set as local variable

7
00:00:39,104 --> 00:00:40,213
23. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

8
00:00:40,219 --> 00:00:40,244
27. comment("ASSERTION")

9
00:00:40,249 --> 00:00:42,711
33. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

10
00:00:42,715 --> 00:00:42,719
5. runId = toString().substring(toString().lastIndexOf("\") + 1)

11
00:00:42,725 --> 00:00:43,636
9. callTestCase(findTestCase("01_Retailer Portal/RP02_Search Customer_Page/RP02_1 Search Customer"), ["idType":idType, "idValue":idValue, "accountNo":accountNo], STOP_ON_FAILURE)

12
00:00:43,642 --> 00:00:44,656
1. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/i_keyboard_arrow_down"))

13
00:00:44,667 --> 00:00:44,673
5. comment("IF statement to check if Account No is populated in the input sheet. If TRUE idType will be forced to ACCOUNT and search done using the accountNo")

14
00:00:44,682 --> 00:00:44,684
9. if (accountNo.isEmpty() == false)

15
00:00:44,692 --> 00:00:45,765
13. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/Dropdown ID Type", ["idType":idType]))

16
00:00:45,771 --> 00:00:47,684
17. setText(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), idValue)

17
00:00:47,692 --> 00:00:48,723
21. sendKeys(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), Keys.chord(ENTER))

18
00:00:48,729 --> 00:00:48,732
25. comment("Checking for text "Customer Profile does not exist", if text exists the Global Variable newCustomer_Flag is set to TRUE and flow handling will be for New Customer ")

19
00:00:48,740 --> 00:01:07,037
29. newCustomer_Flag = verifyTextPresent("Customer Profile does not exist", false, OPTIONAL)

20
00:01:07,041 --> 00:01:08,163
13. callTestCase(findTestCase("01_Retailer Portal/RP03_CIC Dashboard_Page/RP03_1 New Purchase"), ["accountNo":accountNo], STOP_ON_FAILURE)

21
00:01:08,169 --> 00:01:08,173
1. comment("Different Call To Action elements will be loaded depending on whether the customer is new or an existing one")

22
00:01:08,179 --> 00:01:08,183
5. comment("1. New Purchase button for New Customer, 2. New Purchase button for Existing Customer Existing Account, 3. New Purchase button for Existing Customer New Account")

23
00:01:08,190 --> 00:01:08,194
9. if (newCustomer_Flag == true)

24
00:01:08,201 --> 00:01:09,923
3. click(findTestObject("01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/button_New Purchase"))

25
00:01:09,930 --> 00:01:09,938
13. comment("ASSERTION")

26
00:01:09,945 --> 00:01:38,252
17. verifyTextPresent("Catalog", false)

27
00:01:38,259 --> 00:01:39,125
17. callTestCase(findTestCase("01_Retailer Portal/RP04_Catalog_Page/RP04_1 Purchase plan only, no device"), ["planId":planId], STOP_ON_FAILURE)

28
00:01:39,131 --> 00:01:40,212
3. Click mobile plan tab

29
00:01:40,216 --> 00:01:41,528
7. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.button.planProduct", ["planProduct":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3)]))

30
00:01:41,536 --> 00:01:42,916
11. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.label.planType", ["planType":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 2)]), OPTIONAL)

31
00:01:42,919 --> 00:01:42,953
15. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

32
00:01:42,959 --> 00:01:42,961
19. if (isFibreStandAlone == true)

33
00:01:42,970 --> 00:01:42,972
23. if (isFibreStandAlone == false)

34
00:01:42,978 --> 00:01:45,361
1. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/Button Select This Plan", ["planId":planId]))

35
00:01:45,366 --> 00:01:56,149
7. Verfiy that the Plan Name is the same as the one purchased in previous page

36
00:01:56,155 --> 00:01:57,299
11. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/button_Continue without Device"))

37
00:01:57,314 --> 00:01:58,206
21. callTestCase(findTestCase("01_Retailer Portal/RP05_Product Configuration_Page/RP05_1 Skip Product Configuration"), ["planId":planId], STOP_ON_FAILURE)

38
00:01:58,211 --> 00:01:58,228
1. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

39
00:01:58,237 --> 00:01:58,238
5. if (isFibreStandAlone == true)

40
00:01:58,247 --> 00:01:58,249
9. if (isFibreStandAlone == false)

