1
00:00:01,539 --> 00:00:02,661
1. callTestCase(findTestCase("01_Retailer Portal/RP-TMPL-01 NEW PLAN REGISTRATION NO DEVICE"), ["idType":idType, "idValue":idValue, "planId":planId, "customerName":customerName, "row":row, "msisdn":"", "accountNo":"", ... ], STOP_ON_FAILURE)

2
00:00:02,694 --> 00:00:04,034
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), [:], STOP_ON_FAILURE)

3
00:00:04,039 --> 00:00:16,672
1. openBrowser("")

4
00:00:16,679 --> 00:00:41,816
7. URL is set as a Global Variable controlled in Profiles

5
00:00:41,821 --> 00:00:44,656
13. Username is set as local variable

6
00:00:44,661 --> 00:00:46,603
19. Password is set as local variable

7
00:00:46,608 --> 00:00:47,711
23. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

8
00:00:47,720 --> 00:00:47,747
27. comment("ASSERTION")

9
00:00:47,754 --> 00:00:49,844
33. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

10
00:00:49,851 --> 00:00:49,857
5. runId = toString().substring(toString().lastIndexOf("\") + 1)

11
00:00:49,865 --> 00:00:51,035
9. callTestCase(findTestCase("01_Retailer Portal/RP02_Search Customer_Page/RP02_1 Search Customer"), ["idType":idType, "idValue":idValue, "accountNo":accountNo], STOP_ON_FAILURE)

12
00:00:51,042 --> 00:00:52,557
1. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/i_keyboard_arrow_down"))

13
00:00:52,567 --> 00:00:52,593
5. comment("IF statement to check if Account No is populated in the input sheet. If TRUE idType will be forced to ACCOUNT and search done using the accountNo")

14
00:00:52,604 --> 00:00:52,623
9. if (accountNo.isEmpty() == false)

15
00:00:52,634 --> 00:00:54,912
13. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/Dropdown ID Type", ["idType":idType]))

16
00:00:54,917 --> 00:00:56,857
17. setText(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), idValue)

17
00:00:56,863 --> 00:00:57,921
21. sendKeys(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), Keys.chord(ENTER))

18
00:00:57,927 --> 00:00:57,933
25. comment("Checking for text "Customer Profile does not exist", if text exists the Global Variable newCustomer_Flag is set to TRUE and flow handling will be for New Customer ")

19
00:00:57,947 --> 00:00:59,144
29. newCustomer_Flag = verifyTextPresent("Customer Profile does not exist", false, OPTIONAL)

20
00:00:59,175 --> 00:01:00,730
13. callTestCase(findTestCase("01_Retailer Portal/RP03_CIC Dashboard_Page/RP03_1 New Purchase"), ["accountNo":accountNo], STOP_ON_FAILURE)

21
00:01:00,743 --> 00:01:00,750
1. comment("Different Call To Action elements will be loaded depending on whether the customer is new or an existing one")

22
00:01:00,764 --> 00:01:00,769
5. comment("1. New Purchase button for New Customer, 2. New Purchase button for Existing Customer Existing Account, 3. New Purchase button for Existing Customer New Account")

23
00:01:00,780 --> 00:01:00,782
9. if (newCustomer_Flag == true)

24
00:01:00,792 --> 00:01:02,950
1. click(findTestObject("01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/div_New Purchase"))

25
00:01:02,956 --> 00:01:02,963
13. comment("ASSERTION")

26
00:01:02,976 --> 00:01:16,563
17. verifyTextPresent("Catalog", false)

27
00:01:16,569 --> 00:01:17,532
17. callTestCase(findTestCase("01_Retailer Portal/RP04_Catalog_Page/RP04_1 Purchase plan only, no device"), ["planId":planId], STOP_ON_FAILURE)

28
00:01:17,538 --> 00:01:18,750
3. Click mobile plan tab

29
00:01:18,756 --> 00:01:20,187
7. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.button.planProduct", ["planProduct":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3)]))

30
00:01:20,199 --> 00:01:21,779
11. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.label.planType", ["planType":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 2)]), OPTIONAL)

31
00:01:21,786 --> 00:01:21,881
15. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

32
00:01:21,896 --> 00:01:21,899
19. if (isFibreStandAlone == true)

33
00:01:21,911 --> 00:01:21,913
23. if (isFibreStandAlone == false)

34
00:01:21,923 --> 00:01:53,481
1. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/Button Select This Plan", ["planId":planId]))

