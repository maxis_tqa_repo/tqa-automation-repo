1
00:00:01,585 --> 00:00:02,684
1. callTestCase(findTestCase("01_Retailer Portal/RP-TMPL-01 NEW PLAN REGISTRATION NO DEVICE"), ["idType":idType, "idValue":idValue, "planId":planId, "customerName":customerName, "row":row, "msisdn":"", "accountNo":"", ... ], STOP_ON_FAILURE)

2
00:00:02,713 --> 00:00:03,735
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), [:], STOP_ON_FAILURE)

3
00:00:03,741 --> 00:00:17,730
1. openBrowser("")

4
00:00:17,737 --> 00:00:43,042
7. URL is set as a Global Variable controlled in Profiles

5
00:00:43,050 --> 00:00:45,312
13. Username is set as local variable

6
00:00:45,319 --> 00:00:47,242
19. Password is set as local variable

7
00:00:47,248 --> 00:00:48,355
23. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

8
00:00:48,359 --> 00:00:48,380
27. comment("ASSERTION")

9
00:00:48,385 --> 00:00:50,707
33. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

10
00:00:50,713 --> 00:00:50,722
5. runId = toString().substring(toString().lastIndexOf("\") + 1)

11
00:00:50,729 --> 00:00:51,762
9. callTestCase(findTestCase("01_Retailer Portal/RP02_Search Customer_Page/RP02_1 Search Customer"), ["idType":idType, "idValue":idValue, "accountNo":accountNo], STOP_ON_FAILURE)

12
00:00:51,767 --> 00:00:52,788
1. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/i_keyboard_arrow_down"))

13
00:00:52,794 --> 00:00:52,800
5. comment("IF statement to check if Account No is populated in the input sheet. If TRUE idType will be forced to ACCOUNT and search done using the accountNo")

14
00:00:52,805 --> 00:00:52,808
9. if (accountNo.isEmpty() == false)

15
00:00:52,814 --> 00:00:53,965
13. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/Dropdown ID Type", ["idType":idType]))

16
00:00:53,973 --> 00:00:55,931
17. setText(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), idValue)

17
00:00:55,936 --> 00:00:56,952
21. sendKeys(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), Keys.chord(ENTER))

18
00:00:56,958 --> 00:00:56,964
25. comment("Checking for text "Customer Profile does not exist", if text exists the Global Variable newCustomer_Flag is set to TRUE and flow handling will be for New Customer ")

19
00:00:56,970 --> 00:01:18,958
29. newCustomer_Flag = verifyTextPresent("Customer Profile does not exist", false, OPTIONAL)

20
00:01:18,966 --> 00:01:20,450
13. callTestCase(findTestCase("01_Retailer Portal/RP03_CIC Dashboard_Page/RP03_1 New Purchase"), ["accountNo":accountNo], STOP_ON_FAILURE)

21
00:01:20,460 --> 00:01:20,465
1. comment("Different Call To Action elements will be loaded depending on whether the customer is new or an existing one")

22
00:01:20,472 --> 00:01:20,478
5. comment("1. New Purchase button for New Customer, 2. New Purchase button for Existing Customer Existing Account, 3. New Purchase button for Existing Customer New Account")

23
00:01:20,485 --> 00:01:20,490
9. if (newCustomer_Flag == true)

24
00:01:20,503 --> 00:01:21,401
3. click(findTestObject("01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/button_New Purchase"))

