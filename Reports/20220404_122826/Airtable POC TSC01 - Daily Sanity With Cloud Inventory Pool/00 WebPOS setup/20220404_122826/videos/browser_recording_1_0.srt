1
00:00:00,000 --> 00:00:00,000
1. callTestCase(findTestCase("02_Web POS/WP01_Login_Page/WP01_01 Login to WebPOS"), [:], STOP_ON_FAILURE)

2
00:00:00,000 --> 00:00:00,000
1. openBrowser("")

3
00:00:00,000 --> 00:00:00,000
5. navigateToUrl(WebPOS_URL)

4
00:00:00,000 --> 00:00:00,000
9. setText(findTestObject("02_WebPOS_Objects/WP01_Login_Page_Objects/input_Online ID_txtUser"), WebPOS_Username)

5
00:00:00,000 --> 00:00:00,000
13. setText(findTestObject("02_WebPOS_Objects/WP01_Login_Page_Objects/input_Password_txtPassword"), WebPOS_Password)

6
00:00:00,000 --> 00:00:00,000
17. click(findTestObject("02_WebPOS_Objects/WP01_Login_Page_Objects/input_Password_btnLogin"))

7
00:00:00,000 --> 00:00:00,000
21. checkAnotherSessionExists = verifyElementPresent(findTestObject("02_WebPOS_Objects/WP01_Login_Page_Objects/errorMessage"), 0, OPTIONAL)

8
00:00:00,000 --> 00:00:00,000
25. if (checkAnotherSessionExists == true)

9
00:00:00,000 --> 00:00:00,000
1. click(findTestObject("02_WebPOS_Objects/WP01_Login_Page_Objects/a_remove"))

10
00:00:00,000 --> 00:00:00,000
5. click(findTestObject("02_WebPOS_Objects/WP01_Login_Page_Objects/input_Password_btnLogin"))

11
00:00:00,000 --> 00:00:00,000
5. callTestCase(findTestCase("02_Web POS/WP99_Teardown/WP99_02 WebPOS Operator Declaration"), ["expectedValue":""], STOP_ON_FAILURE)

12
00:00:00,000 --> 00:00:00,000
1. click(findTestObject("02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/a_Tab 2"), OPTIONAL)

13
00:00:00,000 --> 00:00:00,000
5. click(findTestObject("02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/a_Operator Declaration"), OPTIONAL)

14
00:00:00,000 --> 00:00:00,000
9. expectedValue = getText(findTestObject("02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/span_20306.88"), OPTIONAL)

15
00:00:00,000 --> 00:00:00,000
13. setText(findTestObject("02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/input_Cash_DeclaredAmount"), expectedValue, OPTIONAL)

16
00:00:00,000 --> 00:00:00,000
17. click(findTestObject("02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/button_Confirm Operator Declaration"))

