1
00:00:01,604 --> 00:00:04,283
1. callTestCase(findTestCase("02_Web POS/WP01_Login_Page/WP01_01 Login to WebPOS"), [:], STOP_ON_FAILURE)

2
00:00:04,317 --> 00:00:22,965
1. openBrowser("")

3
00:00:22,974 --> 00:00:26,382
5. navigateToUrl(WebPOS_URL)

4
00:00:26,389 --> 00:00:28,675
9. setText(findTestObject("02_WebPOS_Objects/WP01_Login_Page_Objects/input_Online ID_txtUser"), WebPOS_Username)

5
00:00:28,682 --> 00:00:30,859
13. setText(findTestObject("02_WebPOS_Objects/WP01_Login_Page_Objects/input_Password_txtPassword"), WebPOS_Password)

6
00:00:30,866 --> 00:00:50,075
17. click(findTestObject("02_WebPOS_Objects/WP01_Login_Page_Objects/input_Password_btnLogin"))

7
00:00:50,081 --> 00:00:51,084
21. checkAnotherSessionExists = verifyElementPresent(findTestObject("02_WebPOS_Objects/WP01_Login_Page_Objects/errorMessage"), 0, OPTIONAL)

8
00:00:51,101 --> 00:00:51,165
25. if (checkAnotherSessionExists == true)

9
00:00:51,171 --> 00:00:52,891
5. callTestCase(findTestCase("02_Web POS/WP99_Teardown/WP99_02 WebPOS Operator Declaration"), ["expectedValue":""], STOP_ON_FAILURE)

10
00:00:52,897 --> 00:00:53,296
1. click(findTestObject("02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/a_Tab 2"), OPTIONAL)

11
00:00:53,309 --> 00:00:53,584
5. click(findTestObject("02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/a_Operator Declaration"), OPTIONAL)

12
00:00:53,593 --> 00:00:54,204
9. expectedValue = getText(findTestObject("02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/span_20306.88"), OPTIONAL)

13
00:00:54,210 --> 00:00:54,890
13. setText(findTestObject("02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/input_Cash_DeclaredAmount"), expectedValue, OPTIONAL)

