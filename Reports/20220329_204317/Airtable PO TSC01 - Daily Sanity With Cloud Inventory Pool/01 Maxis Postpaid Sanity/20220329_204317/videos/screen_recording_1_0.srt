1
00:00:00,850 --> 00:00:01,685
1. callTestCase(findTestCase("01_Retailer Portal/RP-TMPL-01 NEW PLAN REGISTRATION NO DEVICE"), ["idType":idType, "idValue":idValue, "planId":planId, "customerName":customerName, "row":row, "msisdn":"", "accountNo":"", ... ], STOP_ON_FAILURE)

2
00:00:01,713 --> 00:00:02,580
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), [:], STOP_ON_FAILURE)

3
00:00:02,584 --> 00:00:11,967
1. openBrowser("")

4
00:00:11,972 --> 00:00:36,604
7. URL is set as a Global Variable controlled in Profiles

5
00:00:36,609 --> 00:00:38,694
13. Username is set as local variable

6
00:00:38,717 --> 00:00:40,601
19. Password is set as local variable

7
00:00:40,605 --> 00:00:41,731
23. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

8
00:00:41,736 --> 00:00:41,764
27. comment("ASSERTION")

9
00:00:41,772 --> 00:00:43,608
33. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

10
00:00:43,614 --> 00:00:43,622
5. runId = toString().substring(toString().lastIndexOf("\") + 1)

11
00:00:43,636 --> 00:00:44,566
9. callTestCase(findTestCase("01_Retailer Portal/RP02_Search Customer_Page/RP02_1 Search Customer"), ["idType":idType, "idValue":idValue, "accountNo":accountNo], STOP_ON_FAILURE)

12
00:00:44,570 --> 00:00:45,555
1. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/i_keyboard_arrow_down"))

13
00:00:45,562 --> 00:00:45,568
5. comment("IF statement to check if Account No is populated in the input sheet. If TRUE idType will be forced to ACCOUNT and search done using the accountNo")

14
00:00:45,575 --> 00:00:45,578
9. if (accountNo.isEmpty() == false)

15
00:00:45,586 --> 00:00:46,703
13. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/Dropdown ID Type", ["idType":idType]))

16
00:00:46,707 --> 00:00:48,686
17. setText(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), idValue)

17
00:00:48,690 --> 00:00:49,690
21. sendKeys(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), Keys.chord(ENTER))

18
00:00:49,694 --> 00:00:49,697
25. comment("Checking for text "Customer Profile does not exist", if text exists the Global Variable newCustomer_Flag is set to TRUE and flow handling will be for New Customer ")

19
00:00:49,703 --> 00:01:22,972
29. newCustomer_Flag = verifyTextPresent("Customer Profile does not exist", false, OPTIONAL)

20
00:01:22,983 --> 00:01:23,965
13. callTestCase(findTestCase("01_Retailer Portal/RP03_CIC Dashboard_Page/RP03_1 New Purchase"), ["accountNo":accountNo], STOP_ON_FAILURE)

21
00:01:23,970 --> 00:01:23,974
1. comment("Different Call To Action elements will be loaded depending on whether the customer is new or an existing one")

22
00:01:23,981 --> 00:01:23,987
5. comment("1. New Purchase button for New Customer, 2. New Purchase button for Existing Customer Existing Account, 3. New Purchase button for Existing Customer New Account")

23
00:01:23,998 --> 00:01:24,003
9. if (newCustomer_Flag == true)

24
00:01:24,012 --> 00:01:25,867
3. click(findTestObject("01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/button_New Purchase"))

25
00:01:25,872 --> 00:01:25,881
13. comment("ASSERTION")

26
00:01:25,891 --> 00:01:56,946
17. verifyTextPresent("Catalog", false)

27
00:01:56,952 --> 00:01:57,854
17. callTestCase(findTestCase("01_Retailer Portal/RP04_Catalog_Page/RP04_1 Purchase plan only, no device"), ["planId":planId], STOP_ON_FAILURE)

28
00:01:57,861 --> 00:01:59,008
3. Click mobile plan tab

29
00:01:59,012 --> 00:02:00,216
7. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.button.planProduct", ["planProduct":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3)]))

30
00:02:00,222 --> 00:02:18,952
11. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.label.planType", ["planType":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 2)]), OPTIONAL)

31
00:02:18,955 --> 00:02:18,995
15. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

32
00:02:19,004 --> 00:02:19,006
19. if (isFibreStandAlone == true)

33
00:02:19,015 --> 00:02:19,016
23. if (isFibreStandAlone == false)

34
00:02:19,023 --> 00:02:21,244
1. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/Button Select This Plan", ["planId":planId]))

35
00:02:21,248 --> 00:02:51,540
7. Verfiy that the Plan Name is the same as the one purchased in previous page

36
00:02:51,545 --> 00:02:57,786
11. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/button_Continue without Device"))

37
00:02:57,792 --> 00:02:58,622
21. callTestCase(findTestCase("01_Retailer Portal/RP05_Product Configuration_Page/RP05_1 Skip Product Configuration"), ["planId":planId], STOP_ON_FAILURE)

38
00:02:58,629 --> 00:02:58,644
1. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

39
00:02:58,655 --> 00:02:58,658
5. if (isFibreStandAlone == true)

40
00:02:58,672 --> 00:02:58,675
9. if (isFibreStandAlone == false)

