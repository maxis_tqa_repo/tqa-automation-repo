1
00:00:01,070 --> 00:00:02,232
1. callTestCase(findTestCase("01_Retailer Portal/RP-TMPL-04 PREPAID STARTER PACK REGISTRATION - POC"), ["idValue":idValue, "customerName":customerName, "row":row, "inventoryType":inventoryType], STOP_ON_FAILURE)

2
00:00:02,251 --> 00:00:03,037
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), ["username":"mcSup", "password":"123"], STOP_ON_FAILURE)

3
00:00:03,041 --> 00:00:13,414
1. openBrowser("")

4
00:00:13,422 --> 00:00:37,352
7. URL is set as a Global Variable controlled in Profiles

5
00:00:37,358 --> 00:00:39,410
13. Username is set as local variable

6
00:00:39,414 --> 00:00:41,355
19. Password is set as local variable

7
00:00:41,359 --> 00:00:42,382
23. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

8
00:00:42,391 --> 00:00:42,414
27. comment("ASSERTION")

9
00:00:42,421 --> 00:00:44,259
33. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

10
00:00:44,267 --> 00:00:45,395
5. callTestCase(findTestCase("01_Retailer Portal/RP07_Create Customer_Page/RP07_1_6 Insert Data into AMR"), ["idValue":idValue, "customerName":customerName], STOP_ON_FAILURE)

11
00:00:45,404 --> 00:00:46,352
1. com.database.connectSql.connectDB("10.200.50.177", "AMRDB", "1433", "amr_user", "amr_user2015")

12
00:00:46,360 --> 00:00:46,596
5. com.database.connectSql.insertCustomerDetailAMR(idValue, customerName)

13
00:00:46,602 --> 00:00:46,610
9. com.database.connectSql.closeDatabaseConnection()

14
00:00:46,618 --> 00:00:47,532
9. callTestCase(findTestCase("01_Retailer Portal/RP03_CIC Dashboard_Page/RP03_4 Starter Pack Registration"), [:], STOP_ON_FAILURE)

15
00:00:47,538 --> 00:00:49,329
1. click(findTestObject("01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/div_Starter Pack Registration"))

16
00:00:49,336 --> 00:00:50,123
13. callTestCase(findTestCase("98 Webservices/WS01_Airtable REST APIs/Get Inventory and marked as used"), ["inventoryType":inventoryType], STOP_ON_FAILURE)

17
00:00:50,129 --> 00:00:50,464
1. callTestCase(findTestCase("98 Webservices/WS01_Airtable REST APIs/WS01_01 GET Available Inventory"), ["inventoryType":inventoryType], STOP_ON_FAILURE)

18
00:00:50,472 --> 00:00:53,904
1. response = sendRequest(findTestObject("98 Webservices Requests/WS1_Airtable_REST/GET Available Inventory", ["inventoryType":inventoryType]))

19
00:00:53,913 --> 00:00:53,941
5. slurper = new groovy.json.JsonSlurper()

20
00:00:53,953 --> 00:00:53,994
9. parsedJson = slurper.parseText(response.getResponseText())

21
00:00:54,002 --> 00:00:54,025
13. availableSim = simSerial

22
00:00:54,034 --> 00:00:54,036
17. simSerial = replace("[", "").replace("]", "")

23
00:00:54,046 --> 00:00:54,055
21. println(simSerial)

24
00:00:54,063 --> 00:00:54,066
25. simSerial = simSerial

25
00:00:54,079 --> 00:00:54,081
29. inventoryId = id

26
00:00:54,093 --> 00:00:54,095
33. recordId = replace("[", "").replace("]", "")

27
00:00:54,108 --> 00:00:54,110
37. println(recordId)

28
00:00:54,117 --> 00:00:54,119
41. id = recordId

29
00:00:54,123 --> 00:00:54,125
45. starterPackMsisdn = msisdn

30
00:00:54,130 --> 00:00:54,132
49. msisdn = replace("[", "").replace("]", "")

31
00:00:54,137 --> 00:00:54,141
53. println(msisdn)

32
00:00:54,148 --> 00:00:54,153
57. msisdn = msisdn

33
00:00:54,157 --> 00:00:55,187
5. callTestCase(findTestCase("98 Webservices/WS01_Airtable REST APIs/WS01_02 PATCH Used Inventory"), ["inventoryType":inventoryType], STOP_ON_FAILURE)

34
00:00:55,193 --> 00:00:55,195
1. recordId = id

35
00:00:55,213 --> 00:00:56,586
5. response = sendRequest(findTestObject("98 Webservices Requests/WS1_Airtable_REST/PATCH Used Inventory", ["recordId":recordId, "inventoryType":"inventoryHotlinkStarterPack"]))

36
00:00:56,591 --> 00:00:56,704
11. Verify if the response from "REST_Status Codes/POST_201" object returns the 201 status code

