1
00:00:00,000 --> 00:00:00,000
1. callTestCase(findTestCase("02_Web POS/WP-TMPL-01 AMDOCS DASHBOARD ASSIGN INVENTORY AND PAYMENT"), ["msisdn":msisdn, "simSerial":simSerial], STOP_ON_FAILURE)

2
00:00:00,000 --> 00:00:00,000
1. msisdn = msisdn

3
00:00:00,000 --> 00:00:00,000
5. callTestCase(findTestCase("02_Web POS/WP01_Login_Page/WP01_01 Login to WebPOS"), [:], STOP_ON_FAILURE)

4
00:00:00,000 --> 00:00:00,000
1. openBrowser("")

5
00:00:00,000 --> 00:00:00,000
5. navigateToUrl(WebPOS_URL)

6
00:00:00,000 --> 00:00:00,000
9. setText(findTestObject("02_WebPOS_Objects/WP01_Login_Page_Objects/input_Online ID_txtUser"), WebPOS_Username)

7
00:00:00,000 --> 00:00:00,000
13. setText(findTestObject("02_WebPOS_Objects/WP01_Login_Page_Objects/input_Password_txtPassword"), WebPOS_Password)

8
00:00:00,000 --> 00:00:00,000
17. click(findTestObject("02_WebPOS_Objects/WP01_Login_Page_Objects/input_Password_btnLogin"))

9
00:00:00,000 --> 00:00:00,000
21. checkAnotherSessionExists = verifyElementPresent(findTestObject("02_WebPOS_Objects/WP01_Login_Page_Objects/errorMessage"), 0, OPTIONAL)

10
00:00:00,000 --> 00:00:00,000
25. if (checkAnotherSessionExists == true)

11
00:00:00,000 --> 00:00:00,000
9. callTestCase(findTestCase("02_Web POS/WP02_Maxis_WebPOS_Page/WP02_01 Inventory Assignment"), ["msisdn":msisdn, "fwbbMsisdn":"", "simSerial":simSerial, "simSerial2":""], STOP_ON_FAILURE)

12
00:00:00,000 --> 00:00:00,000
1. click(findTestObject("02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/a_Amdocs Inventory Dashboard"))

13
00:00:00,000 --> 00:00:00,000
5. click(findTestObject("02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/td_Ready for Inventory_QueueNo"))

14
00:00:00,000 --> 00:00:00,000
9. click(findTestObject("02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/Global Variable MSISDN (Assign Inventory)", ["msisdn":msisdn]))

15
00:00:00,000 --> 00:00:00,000
13. click(findTestObject("02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/button_Process"))

16
00:00:00,000 --> 00:00:00,000
17. callTestCase(findTestCase("98 Webservices/WS01_Airtable REST APIs/Get Inventory and marked as used"), ["inventoryType":"inventoryMaxisPostpaidSIM", "recordId":""], STOP_ON_FAILURE)

17
00:00:00,000 --> 00:00:00,000
1. callTestCase(findTestCase("98 Webservices/WS01_Airtable REST APIs/WS01_01 GET Available Inventory"), ["inventoryType":inventoryType], STOP_ON_FAILURE)

18
00:00:00,000 --> 00:00:00,000
1. response = sendRequest(findTestObject("98 Webservices Requests/WS1_Airtable_REST/GET Available Inventory", ["inventoryType":inventoryType]))

19
00:00:00,000 --> 00:00:00,000
5. slurper = new groovy.json.JsonSlurper()

20
00:00:00,000 --> 00:00:00,000
9. parsedJson = slurper.parseText(response.getResponseText())

21
00:00:00,000 --> 00:00:00,000
13. availableSim = simSerial

22
00:00:00,000 --> 00:00:00,000
17. simSerial = replace("[", "").replace("]", "")

23
00:00:00,000 --> 00:00:00,000
21. println(simSerial)

24
00:00:00,000 --> 00:00:00,000
25. simSerial = simSerial

25
00:00:00,000 --> 00:00:00,000
29. inventoryId = id

26
00:00:00,000 --> 00:00:00,000
33. recordId = replace("[", "").replace("]", "")

27
00:00:00,000 --> 00:00:00,000
37. println(recordId)

28
00:00:00,000 --> 00:00:00,000
41. id = recordId

29
00:00:00,000 --> 00:00:00,000
5. callTestCase(findTestCase("98 Webservices/WS01_Airtable REST APIs/WS01_02 PATCH Used Inventory"), ["recordId":recordId], STOP_ON_FAILURE)

30
00:00:00,000 --> 00:00:00,000
1. recordId = id

31
00:00:00,000 --> 00:00:00,000
5. response = sendRequest(findTestObject("98 Webservices Requests/WS1_Airtable_REST/PATCH Used Inventory", ["recordId":recordId]))

32
00:00:00,000 --> 00:00:00,000
11. Verify if the response from "REST_Status Codes/POST_201" object returns the 201 status code

33
00:00:00,000 --> 00:00:00,000
21. simSerial = simSerial

34
00:00:00,000 --> 00:00:00,000
25. setText(findTestObject("02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/input_SIM_IMEINo"), simSerial)

35
00:00:00,000 --> 00:00:00,000
29. if (fwbbMsisdn.isEmpty() == false)

36
00:00:00,000 --> 00:00:00,000
33. click(findTestObject("02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/button_Submit"))

37
00:00:00,000 --> 00:00:00,000
37. click(findTestObject("02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/button_inventory_close"))

38
00:00:00,000 --> 00:00:00,000
13. callTestCase(findTestCase("02_Web POS/WP02_Maxis_WebPOS_Page/WP02_02 Payment"), ["msisdn":msisdn], STOP_ON_FAILURE)

39
00:00:00,000 --> 00:00:00,000
1. click(findTestObject("02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/a_Amdocs Dashboard"))

40
00:00:00,000 --> 00:00:00,000
5. click(findTestObject("02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/Global Variable MSISDN (Payment)", ["msisdn":msisdn]))

