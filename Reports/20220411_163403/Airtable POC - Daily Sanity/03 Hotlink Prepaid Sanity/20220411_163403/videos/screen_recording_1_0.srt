1
00:00:01,232 --> 00:00:02,641
1. callTestCase(findTestCase("01_Retailer Portal/RP-TMPL-04 PREPAID STARTER PACK REGISTRATION - POC"), ["idValue":idValue, "customerName":customerName, "row":row, "inventoryType":inventoryType], STOP_ON_FAILURE)

2
00:00:02,660 --> 00:00:03,735
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), ["username":"mcSup", "password":"123"], STOP_ON_FAILURE)

3
00:00:03,740 --> 00:00:13,649
1. openBrowser("")

4
00:00:13,653 --> 00:00:37,616
7. URL is set as a Global Variable controlled in Profiles

5
00:00:37,624 --> 00:00:39,731
13. Username is set as local variable

6
00:00:39,739 --> 00:00:41,894
19. Password is set as local variable

7
00:00:41,902 --> 00:00:43,261
23. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

8
00:00:43,273 --> 00:00:43,321
27. comment("ASSERTION")

9
00:00:43,328 --> 00:00:45,735
33. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

10
00:00:45,740 --> 00:00:46,901
5. callTestCase(findTestCase("01_Retailer Portal/RP07_Create Customer_Page/RP07_1_6 Insert Data into AMR"), ["idValue":idValue, "customerName":customerName], STOP_ON_FAILURE)

11
00:00:46,905 --> 00:00:47,796
1. com.database.connectSql.connectDB("10.200.50.177", "AMRDB", "1433", "amr_user", "amr_user2015")

12
00:00:47,802 --> 00:00:48,002
5. com.database.connectSql.insertCustomerDetailAMR(idValue, customerName)

13
00:00:48,006 --> 00:00:48,015
9. com.database.connectSql.closeDatabaseConnection()

14
00:00:48,021 --> 00:00:48,909
9. callTestCase(findTestCase("01_Retailer Portal/RP03_CIC Dashboard_Page/RP03_4 Starter Pack Registration"), [:], STOP_ON_FAILURE)

15
00:00:48,914 --> 00:00:50,492
1. click(findTestObject("01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/div_Starter Pack Registration"))

16
00:00:50,498 --> 00:00:51,335
13. callTestCase(findTestCase("98 Webservices/WS01_Airtable REST APIs/Get Inventory and marked as used"), ["inventoryType":inventoryType], STOP_ON_FAILURE)

17
00:00:51,339 --> 00:00:51,716
1. callTestCase(findTestCase("98 Webservices/WS01_Airtable REST APIs/WS01_01 GET Available Inventory"), ["inventoryType":inventoryType], STOP_ON_FAILURE)

18
00:00:51,723 --> 00:00:54,410
1. response = sendRequest(findTestObject("98 Webservices Requests/WS1_Airtable_REST/GET Available Inventory", ["inventoryType":inventoryType]))

19
00:00:54,417 --> 00:00:54,441
5. slurper = new groovy.json.JsonSlurper()

20
00:00:54,447 --> 00:00:54,491
9. parsedJson = slurper.parseText(response.getResponseText())

21
00:00:54,496 --> 00:00:54,523
13. availableSim = simSerial

22
00:00:54,527 --> 00:00:54,529
17. simSerial = replace("[", "").replace("]", "")

23
00:00:54,534 --> 00:00:54,540
21. println(simSerial)

24
00:00:54,545 --> 00:00:54,546
25. simSerial = simSerial

25
00:00:54,554 --> 00:00:54,556
29. inventoryId = id

26
00:00:54,560 --> 00:00:54,562
33. recordId = replace("[", "").replace("]", "")

27
00:00:54,570 --> 00:00:54,572
37. println(recordId)

28
00:00:54,578 --> 00:00:54,579
41. id = recordId

29
00:00:54,584 --> 00:00:54,585
45. starterPackMsisdn = msisdn

30
00:00:54,589 --> 00:00:54,591
49. msisdn = replace("[", "").replace("]", "")

31
00:00:54,595 --> 00:00:54,596
53. println(msisdn)

32
00:00:54,601 --> 00:00:54,605
57. msisdn = msisdn

33
00:00:54,609 --> 00:00:55,866
5. callTestCase(findTestCase("98 Webservices/WS01_Airtable REST APIs/WS01_02 PATCH Used Inventory"), ["inventoryType":inventoryType], STOP_ON_FAILURE)

34
00:00:55,874 --> 00:00:55,876
1. recordId = id

35
00:00:55,884 --> 00:00:57,744
5. response = sendRequest(findTestObject("98 Webservices Requests/WS1_Airtable_REST/PATCH Used Inventory", ["recordId":recordId, "inventoryType":"inventoryHotlinkStarterPack"]))

36
00:00:57,750 --> 00:00:57,790
11. Verify if the response from "REST_Status Codes/POST_201" object returns the 201 status code

37
00:00:57,796 --> 00:00:58,926
17. callTestCase(findTestCase("01_Retailer Portal/RP13_Starter Pack Registration_Page/RP13_1 Register Prepaid Starter Pack via NRIC - POC"), ["idValue":idValue], STOP_ON_FAILURE)

38
00:00:58,930 --> 00:01:00,869
1. setText(findTestObject("01_Retailer Portal_Objects/RP13_Starter Pack Registration Page_Objects/input_idForSimNum"), simSerial)

39
00:01:00,874 --> 00:01:02,816
5. setText(findTestObject("01_Retailer Portal_Objects/RP13_Starter Pack Registration Page_Objects/input_idForMsisdnNum"), msisdn)

40
00:01:02,823 --> 00:01:03,855
9. click(findTestObject("01_Retailer Portal_Objects/RP13_Starter Pack Registration Page_Objects/button_Verify"))

