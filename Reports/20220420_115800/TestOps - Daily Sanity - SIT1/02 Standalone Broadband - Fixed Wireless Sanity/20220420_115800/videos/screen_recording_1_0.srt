1
00:00:01,501 --> 00:00:02,732
1. callTestCase(findTestCase("01_Retailer Portal/RP-01 New Plan, New Registration, No Device"), ["idType":idType, "idValue":idValue, "planId":planId, "customerName":customerName, "row":row, "msisdn":"", "accountNo":"", ... ], STOP_ON_FAILURE)

2
00:00:02,768 --> 00:00:05,169
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), [:], STOP_ON_FAILURE)

3
00:00:05,185 --> 00:00:16,520
1. openBrowser("")

4
00:00:16,525 --> 00:00:42,099
5. navigateToUrl(RP_URL)

5
00:00:42,111 --> 00:00:42,542
9. maximizeWindow()

6
00:00:42,566 --> 00:00:45,540
15. Username is set as local variable

7
00:00:45,547 --> 00:00:47,764
21. Password is set as local variable

8
00:00:47,771 --> 00:00:49,722
25. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

9
00:00:49,737 --> 00:00:52,377
31. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

10
00:00:52,385 --> 00:00:52,463
35. comment("ASSERTION : [ OK ] ")

11
00:00:52,473 --> 00:00:52,535
39. comment("		Successfully logged in as : " + RP_Username)

12
00:00:52,547 --> 00:00:52,560
5. runId = toString().substring(toString().lastIndexOf("\") + 1)

13
00:00:52,572 --> 00:00:54,375
9. callTestCase(findTestCase("01_Retailer Portal/RP02_Search Customer_Page/RP02_1 Search Customer"), ["idType":idType, "idValue":idValue, "accountNo":accountNo], STOP_ON_FAILURE)

14
00:00:54,380 --> 00:00:55,794
1. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/i_keyboard_arrow_down"))

15
00:00:55,806 --> 00:00:55,811
7. IF statement to check if Account No is populated in the input sheet. If TRUE idType will be forced to ACCOUNT and search done using the accountNo

16
00:00:55,819 --> 00:00:57,095
11. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/Dropdown ID Type", ["idType":idType]))

17
00:00:57,099 --> 00:00:59,281
15. setText(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), idValue)

18
00:00:59,284 --> 00:01:00,386
19. sendKeys(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), Keys.chord(ENTER))

19
00:01:00,399 --> 00:01:20,735
25. Checking for text "Customer Profile does not exist", if text exists the Global Variable newCustomer_Flag is set to TRUE and flow handling will be for New Customer 

20
00:01:20,742 --> 00:01:20,748
29. comment("WORKFLOW HANDLING : ")

21
00:01:20,757 --> 00:01:20,764
33. comment("	ID TYPE : " + idType)

22
00:01:20,770 --> 00:01:20,774
37. comment("	ID VALUE : " + idValue)

23
00:01:20,782 --> 00:01:20,787
41. "	NEW CUSTOMER : " + newCustomer_Flag

24
00:01:20,794 --> 00:01:21,826
13. callTestCase(findTestCase("01_Retailer Portal/RP03_CIC Dashboard_Page/RP03_1 New Purchase"), ["accountNo":accountNo], STOP_ON_FAILURE)

25
00:01:21,835 --> 00:01:21,843
1. comment("")

26
00:01:21,853 --> 00:01:21,860
5. comment("")

27
00:01:21,871 --> 00:01:21,877
11. Different Call To Action elements will be loaded depending on whether the customer is new or an existing one

1. New Purchase button for New Customer,
2. New Purchase button for Existing Customer Existing Account,
3. New Purchase button for Existing Customer New Account

28
00:01:21,884 --> 00:01:24,201
3. click(findTestObject("01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/button_New Purchase"))

29
00:01:24,215 --> 00:01:43,281
15. verifyTextPresent("Catalog", false)

30
00:01:43,286 --> 00:01:43,289
19. comment("ASSERTION : [ OK ] ")

31
00:01:43,297 --> 00:01:43,301
23. comment("	Product Catalogue Page loaded")

32
00:01:43,309 --> 00:01:43,923
27. comment("	Text Catalog present? " + println(verifyTextPresent("Catalog", false)))

33
00:01:43,930 --> 00:01:45,035
17. callTestCase(findTestCase("01_Retailer Portal/RP04_Catalog_Page/RP04_1 Purchase plan only, no device"), ["planId":planId], STOP_ON_FAILURE)

34
00:01:45,044 --> 00:01:46,355
3. Click mobile plan tab

35
00:01:46,362 --> 00:01:48,087
7. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.button.planProduct", ["planProduct":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3)]))

36
00:01:48,094 --> 00:01:49,660
11. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.label.planType", ["planType":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 2)]), OPTIONAL)

37
00:01:49,666 --> 00:01:49,715
15. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

38
00:01:49,726 --> 00:01:49,729
19. comment("")

39
00:01:49,736 --> 00:01:49,738
23. if (isFibreStandAlone == true)

40
00:01:49,746 --> 00:01:49,749
27. if (isFibreStandAlone == false)

41
00:01:49,761 --> 00:01:52,102
1. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/Button Select This Plan", ["planId":planId]))

42
00:01:52,121 --> 00:01:52,872
7. Verfiy that the Plan Name is the same as the one purchased in previous page

43
00:01:52,875 --> 00:02:02,928
11. delay(10)

44
00:02:02,961 --> 00:02:05,143
15. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/button_Continue without Device"))

45
00:02:05,178 --> 00:02:05,187
19. comment("Note that this has been identified as a common failure point. - aabangf")

46
00:02:05,247 --> 00:02:19,685
25. Note that this has been identified as a common failure point. - aabangf

Verify that once Continue WIthout Device button is clicked the page has moved on to the next page by checking that the title of the new page is shown

