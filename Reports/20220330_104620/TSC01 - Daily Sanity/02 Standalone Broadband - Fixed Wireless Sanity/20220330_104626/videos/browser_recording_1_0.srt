1
00:00:00,000 --> 00:00:00,000
1. callTestCase(findTestCase("01_Retailer Portal/RP-TMPL-01 NEW PLAN REGISTRATION NO DEVICE"), ["idType":idType, "idValue":idValue, "planId":planId, "customerName":customerName, "row":row, "msisdn":"", "accountNo":"", ... ], STOP_ON_FAILURE)

2
00:00:00,000 --> 00:00:00,000
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), [:], STOP_ON_FAILURE)

3
00:00:00,000 --> 00:00:00,000
1. openBrowser("")

4
00:00:00,000 --> 00:00:00,000
7. URL is set as a Global Variable controlled in Profiles

5
00:00:00,000 --> 00:00:00,000
13. Username is set as local variable

6
00:00:00,000 --> 00:00:00,000
19. Password is set as local variable

7
00:00:00,000 --> 00:00:00,000
23. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

8
00:00:00,000 --> 00:00:00,000
27. comment("ASSERTION")

9
00:00:00,000 --> 00:00:00,000
33. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

10
00:00:00,000 --> 00:00:00,000
5. runId = toString().substring(toString().lastIndexOf("\") + 1)

11
00:00:00,000 --> 00:00:00,000
9. callTestCase(findTestCase("01_Retailer Portal/RP02_Search Customer_Page/RP02_1 Search Customer"), ["idType":idType, "idValue":idValue, "accountNo":accountNo], STOP_ON_FAILURE)

12
00:00:00,000 --> 00:00:00,000
1. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/i_keyboard_arrow_down"))

13
00:00:00,000 --> 00:00:00,000
5. comment("IF statement to check if Account No is populated in the input sheet. If TRUE idType will be forced to ACCOUNT and search done using the accountNo")

14
00:00:00,000 --> 00:00:00,000
9. if (accountNo.isEmpty() == false)

15
00:00:00,000 --> 00:00:00,000
13. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/Dropdown ID Type", ["idType":idType]))

16
00:00:00,000 --> 00:00:00,000
17. setText(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), idValue)

17
00:00:00,000 --> 00:00:00,000
21. sendKeys(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), Keys.chord(ENTER))

18
00:00:00,000 --> 00:00:00,000
25. comment("Checking for text "Customer Profile does not exist", if text exists the Global Variable newCustomer_Flag is set to TRUE and flow handling will be for New Customer ")

19
00:00:00,000 --> 00:00:00,000
29. newCustomer_Flag = verifyTextPresent("Customer Profile does not exist", false, OPTIONAL)

20
00:00:00,000 --> 00:00:00,000
13. callTestCase(findTestCase("01_Retailer Portal/RP03_CIC Dashboard_Page/RP03_1 New Purchase"), ["accountNo":accountNo], STOP_ON_FAILURE)

21
00:00:00,000 --> 00:00:00,000
1. comment("Different Call To Action elements will be loaded depending on whether the customer is new or an existing one")

22
00:00:00,000 --> 00:00:00,000
5. comment("1. New Purchase button for New Customer, 2. New Purchase button for Existing Customer Existing Account, 3. New Purchase button for Existing Customer New Account")

23
00:00:00,000 --> 00:00:00,000
9. if (newCustomer_Flag == true)

24
00:00:00,000 --> 00:00:00,000
3. click(findTestObject("01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/button_New Purchase"))

25
00:00:00,000 --> 00:00:00,000
13. comment("ASSERTION")

26
00:00:00,000 --> 00:00:00,000
17. verifyTextPresent("Catalog", false)

27
00:00:00,000 --> 00:00:00,000
17. callTestCase(findTestCase("01_Retailer Portal/RP04_Catalog_Page/RP04_1 Purchase plan only, no device"), ["planId":planId], STOP_ON_FAILURE)

28
00:00:00,000 --> 00:00:00,000
3. Click mobile plan tab

29
00:00:00,000 --> 00:00:00,000
7. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.button.planProduct", ["planProduct":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3)]))

30
00:00:00,000 --> 00:00:00,000
11. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.label.planType", ["planType":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 2)]), OPTIONAL)

31
00:00:00,000 --> 00:00:00,000
15. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

32
00:00:00,000 --> 00:00:00,000
19. if (isFibreStandAlone == true)

33
00:00:00,000 --> 00:00:00,000
23. if (isFibreStandAlone == false)

34
00:00:00,000 --> 00:00:00,000
1. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/Button Select This Plan", ["planId":planId]))

35
00:00:00,000 --> 00:00:00,000
7. Verfiy that the Plan Name is the same as the one purchased in previous page

36
00:00:00,000 --> 00:00:00,000
11. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/button_Continue without Device"))

37
00:00:00,000 --> 00:00:00,000
21. callTestCase(findTestCase("01_Retailer Portal/RP05_Product Configuration_Page/RP05_1 Skip Product Configuration"), ["planId":planId], STOP_ON_FAILURE)

38
00:00:00,000 --> 00:00:00,000
1. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

39
00:00:00,000 --> 00:00:00,000
5. if (isFibreStandAlone == true)

40
00:00:00,000 --> 00:00:00,000
9. if (isFibreStandAlone == false)

41
00:00:00,000 --> 00:00:00,000
1. click(findTestObject("01_Retailer Portal_Objects/RP05_Product Configuration_Page_Objects/button_Next"))

