1
00:00:01,219 --> 00:00:02,079
1. callTestCase(findTestCase("01_Retailer Portal/RP-TMPL-01 NEW PLAN REGISTRATION NO DEVICE"), ["idType":idType, "idValue":idValue, "planId":planId, "customerName":customerName, "row":row, "msisdn":"", "accountNo":"", ... ], STOP_ON_FAILURE)

2
00:00:02,099 --> 00:00:02,998
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), [:], STOP_ON_FAILURE)

3
00:00:03,003 --> 00:00:11,355
1. openBrowser("")

4
00:00:11,359 --> 00:00:35,976
5. navigateToUrl(RP_URL)

5
00:00:35,982 --> 00:00:38,637
11. Username is set as local variable

6
00:00:38,643 --> 00:00:41,304
17. Password is set as local variable

7
00:00:41,312 --> 00:00:42,854
21. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

8
00:00:42,862 --> 00:00:42,897
25. comment("ASSERTION")

9
00:00:42,904 --> 00:00:45,222
31. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

10
00:00:45,227 --> 00:00:45,236
5. runId = toString().substring(toString().lastIndexOf("\") + 1)

11
00:00:45,244 --> 00:00:46,481
9. callTestCase(findTestCase("01_Retailer Portal/RP02_Search Customer_Page/RP02_1 Search Customer"), ["idType":idType, "idValue":idValue, "accountNo":accountNo], STOP_ON_FAILURE)

12
00:00:46,485 --> 00:00:47,648
1. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/i_keyboard_arrow_down"))

13
00:00:47,655 --> 00:00:47,661
5. comment("IF statement to check if Account No is populated in the input sheet. If TRUE idType will be forced to ACCOUNT and search done using the accountNo")

14
00:00:47,669 --> 00:00:47,672
9. if (accountNo.isEmpty() == false)

15
00:00:47,678 --> 00:00:49,426
13. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/Dropdown ID Type", ["idType":idType]))

16
00:00:49,434 --> 00:00:51,634
17. setText(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), idValue)

17
00:00:51,639 --> 00:00:52,849
21. sendKeys(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), Keys.chord(ENTER))

18
00:00:52,854 --> 00:00:52,861
25. comment("Checking for text "Customer Profile does not exist", if text exists the Global Variable newCustomer_Flag is set to TRUE and flow handling will be for New Customer ")

19
00:00:52,867 --> 00:01:13,851
29. newCustomer_Flag = verifyTextPresent("Customer Profile does not exist", false, OPTIONAL)

20
00:01:13,858 --> 00:01:15,261
13. callTestCase(findTestCase("01_Retailer Portal/RP03_CIC Dashboard_Page/RP03_1 New Purchase"), ["accountNo":accountNo], STOP_ON_FAILURE)

21
00:01:15,267 --> 00:01:15,271
1. comment("Different Call To Action elements will be loaded depending on whether the customer is new or an existing one")

22
00:01:15,278 --> 00:01:15,284
5. comment("1. New Purchase button for New Customer, 2. New Purchase button for Existing Customer Existing Account, 3. New Purchase button for Existing Customer New Account")

23
00:01:15,292 --> 00:01:15,299
9. if (newCustomer_Flag == true)

24
00:01:15,305 --> 00:01:17,614
3. click(findTestObject("01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/button_New Purchase"))

25
00:01:17,619 --> 00:01:17,625
13. comment("ASSERTION")

26
00:01:17,632 --> 00:01:31,312
17. verifyTextPresent("Catalog", false)

27
00:01:31,318 --> 00:01:32,657
17. callTestCase(findTestCase("01_Retailer Portal/RP04_Catalog_Page/RP04_1 Purchase plan only, no device"), ["planId":planId], STOP_ON_FAILURE)

28
00:01:32,664 --> 00:01:34,357
3. Click mobile plan tab

29
00:01:34,363 --> 00:01:35,937
7. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.button.planProduct", ["planProduct":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3)]))

30
00:01:35,947 --> 00:01:37,832
11. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.label.planType", ["planType":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 2)]), OPTIONAL)

31
00:01:37,840 --> 00:01:37,910
15. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

32
00:01:37,918 --> 00:01:37,923
19. if (isFibreStandAlone == true)

33
00:01:37,931 --> 00:01:37,933
23. if (isFibreStandAlone == false)

34
00:01:37,945 --> 00:01:41,214
1. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/Button Select This Plan", ["planId":planId]))

35
00:01:41,222 --> 00:02:06,782
7. Verfiy that the Plan Name is the same as the one purchased in previous page

36
00:02:06,788 --> 00:02:08,029
11. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/button_Continue without Device"))

37
00:02:08,036 --> 00:02:08,923
21. callTestCase(findTestCase("01_Retailer Portal/RP05_Product Configuration_Page/RP05_1 Skip Product Configuration"), ["planId":planId], STOP_ON_FAILURE)

38
00:02:08,928 --> 00:02:08,944
1. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

39
00:02:08,949 --> 00:02:08,952
5. if (isFibreStandAlone == true)

40
00:02:08,958 --> 00:02:08,960
9. if (isFibreStandAlone == false)

41
00:02:08,980 --> 00:02:15,941
1. click(findTestObject("01_Retailer Portal_Objects/RP05_Product Configuration_Page_Objects/button_Next"))

42
00:02:15,948 --> 00:02:24,188
5. verifyTextPresent("Selected Plan", false)

43
00:02:24,194 --> 00:02:27,058
9. click(findTestObject("01_Retailer Portal_Objects/RP05_Product Configuration_Page_Objects/button_Continue to Cart"))

44
00:02:27,063 --> 00:02:28,069
25. callTestCase(findTestCase("01_Retailer Portal/RP06_Cart_Page/RP06_1 Cart Page"), ["msisdn":msisdn, "broadbandPlan":broadbandPlan, "planId":planId], STOP_ON_FAILURE)

45
00:02:28,074 --> 00:02:28,092
1. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

46
00:02:28,099 --> 00:02:28,103
5. if (isFibreStandAlone == true)

47
00:02:28,110 --> 00:02:28,112
9. if (isFibreStandAlone == false)

48
00:02:28,118 --> 00:02:28,120
3. If msisdn in the input files is empty, then the order will just use the assigned msisdn

49
00:02:28,127 --> 00:02:28,130
1. callTestCase(findTestCase("01_Retailer Portal/RP06_Cart_Page/RP06_5 Link Broadband and Check Out"), ["broadbandPlan":broadbandPlan], STOP_ON_FAILURE)

50
00:02:28,137 --> 00:02:28,921
5. callTestCase(findTestCase("01_Retailer Portal/RP06_Cart_Page/RP06_5 Link Broadband and Check Out - Copy"), ["broadbandPlan":broadbandPlan, "planId":planId], STOP_ON_FAILURE)

51
00:02:28,926 --> 00:02:28,943
1. isplanTypeUnlimited = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 2), "Unlimited", false, OPTIONAL)

52
00:02:28,950 --> 00:02:28,968
5. isplanTypeFamilyPlan = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 2), "Family Plan", false, OPTIONAL)

53
00:02:28,974 --> 00:02:28,991
9. isplanTypePrime = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 2), "Prime", false, OPTIONAL)

54
00:02:28,996 --> 00:02:28,997
13. if (isplanTypeUnlimited == true)

55
00:02:29,004 --> 00:02:29,006
17. if (isplanTypeFamilyPlan == true)

56
00:02:29,013 --> 00:02:29,015
21. if (isplanTypePrime == true)

57
00:02:29,022 --> 00:02:30,317
25. click(findTestObject("01_Retailer Portal_Objects/RP06_Cart_Page_Objects/button_Check Out"))

58
00:02:30,324 --> 00:02:31,363
29. callTestCase(findTestCase("01_Retailer Portal/RP07_Create Customer_Page/RP07_1 Create Customer_Page"), ["idType":idType, "customerName":customerName, "accountNo":accountNo, "idValue":idValue], STOP_ON_FAILURE)

59
00:02:31,370 --> 00:02:31,377
1. isMalaysianCitizen = verifyMatch(idType, "MALAYSIAN ID CARD", false, OPTIONAL)

60
00:02:31,383 --> 00:02:31,384
5. if (accountNo.isEmpty() == false)

61
00:02:31,388 --> 00:02:31,390
9. if (accountNo.isEmpty() == true)

62
00:02:31,394 --> 00:02:35,474
1. verifyTextPresent("Customer Details", false)

63
00:02:35,478 --> 00:02:35,480
5. if (newCustomer_Flag == true)

64
00:02:35,485 --> 00:02:35,486
9. if (newCustomer_Flag == false)

65
00:02:35,490 --> 00:02:36,363
1. callTestCase(findTestCase("01_Retailer Portal/RP07_Create Customer_Page/RP07_1_2 Use Contact Address as Billing Address"), [:], STOP_ON_FAILURE)

66
00:02:36,369 --> 00:02:37,523
1. click(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/label_Use Contact Address as Billing Address"))

67
00:02:37,531 --> 00:02:39,804
5. setText(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/input_Receive a monthly email notification _bc6444"), Email)

68
00:02:39,809 --> 00:02:41,011
9. click(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/div_Regular Paper Bill_CustomCheckbox"))

69
00:02:41,018 --> 00:02:42,382
13. click(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/button_Continue"))

70
00:02:42,389 --> 00:02:44,020
33. callTestCase(findTestCase("01_Retailer Portal/RP08_Order Summary_Page/RP08_1 Document Upload and Submit Order"), ["idType":idType], STOP_ON_FAILURE)

71
00:02:44,027 --> 00:02:55,617
1. verifyTextPresent("Order Summary", false, CONTINUE_ON_FAILURE)

72
00:02:55,624 --> 00:02:56,895
5. click(findTestObject("01_Retailer Portal_Objects/RP08_Order Summary_Page_Objects/i_keyboard_arrow_down"))

73
00:02:56,903 --> 00:02:58,116
9. click(findTestObject("01_Retailer Portal_Objects/RP08_Order Summary_Page_Objects/DropDownID_Type", ["idType":idType]))

74
00:02:58,123 --> 00:02:58,127
13. dirName = getProjectDir()

75
00:02:58,133 --> 00:02:58,135
17. upload = "" + dirName + "\Data Files\Test Data and Input Files\Uploads\maxis.jpg"

76
00:02:58,140 --> 00:02:58,143
21. uploadPath = upload.replace("/", "\")

77
00:02:58,149 --> 00:03:05,588
25. com.uploadfile.UploadFile.uploadFileUsingRobot(findTestObject("01_Retailer Portal_Objects/RP08_Order Summary_Page_Objects/div_Browse"), uploadPath)

78
00:03:05,608 --> 00:03:05,687
29. delay(0)

79
00:03:05,693 --> 00:03:38,494
33. enhancedClick(findTestObject("01_Retailer Portal_Objects/RP08_Order Summary_Page_Objects/checkbox_Terms and Condition"))

80
00:03:38,501 --> 00:03:39,694
37. click(findTestObject("01_Retailer Portal_Objects/RP08_Order Summary_Page_Objects/canvas_refresh_signature-pad-1"))

81
00:03:39,701 --> 00:03:40,868
41. click(findTestObject("01_Retailer Portal_Objects/RP08_Order Summary_Page_Objects/canvas_refresh_signature-pad-1"))

82
00:03:40,874 --> 00:03:43,169
45. setText(findTestObject("01_Retailer Portal_Objects/RP08_Order Summary_Page_Objects/textarea_Apply Waiver_remarks_super"), "Approved")

83
00:03:43,175 --> 00:03:44,308
49. click(findTestObject("01_Retailer Portal_Objects/RP08_Order Summary_Page_Objects/button_Approve"))

84
00:03:44,313 --> 00:03:45,157
37. callTestCase(findTestCase("01_Retailer Portal/RP09_Order Confirmation_Page/RP09_1 Verify order ID is given"), [:], STOP_ON_FAILURE)

85
00:03:45,161 --> 00:03:45,873
1. verifyTextPresent("Order Confirmed", false, CONTINUE_ON_FAILURE)

86
00:03:45,881 --> 00:04:17,681
5. click(findTestObject("01_Retailer Portal_Objects/RP09_Order Confirmation_Page_Objects/Order ID"))

