1
00:00:00,751 --> 00:00:01,616
1. callTestCase(findTestCase("02_Web POS/WP-TMPL-01 AMDOCS DASHBOARD ASSIGN INVENTORY FROM POOL AND PAYMENT - POC"), ["msisdn":"", "simSerial":"", "inventoryType":inventoryType], STOP_ON_FAILURE)

2
00:00:01,625 --> 00:00:01,626
1. msisdn = msisdn

3
00:00:01,631 --> 00:00:02,672
5. callTestCase(findTestCase("02_Web POS/WP01_Login_Page/WP01_01 Login to WebPOS"), [:], STOP_ON_FAILURE)

4
00:00:02,678 --> 00:00:10,565
1. openBrowser("")

5
00:00:10,571 --> 00:00:11,780
5. navigateToUrl(WebPOS_URL)

6
00:00:11,787 --> 00:00:13,692
9. setText(findTestObject("02_WebPOS_Objects/WP01_Login_Page_Objects/input_Online ID_txtUser"), WebPOS_Username)

7
00:00:13,698 --> 00:00:15,647
13. setText(findTestObject("02_WebPOS_Objects/WP01_Login_Page_Objects/input_Password_txtPassword"), WebPOS_Password)

8
00:00:15,654 --> 00:00:34,172
17. click(findTestObject("02_WebPOS_Objects/WP01_Login_Page_Objects/input_Password_btnLogin"))

9
00:00:34,179 --> 00:01:05,746
21. checkAnotherSessionExists = verifyElementPresent(findTestObject("02_WebPOS_Objects/WP01_Login_Page_Objects/errorMessage"), 0, OPTIONAL)

10
00:01:05,750 --> 00:01:05,755
25. if (checkAnotherSessionExists == true)

11
00:01:05,762 --> 00:01:06,745
9. callTestCase(findTestCase("02_Web POS/WP02_Maxis_WebPOS_Page/WP02_01a Inventory Assignment From Test Inventory Pool - POC"), ["msisdn":msisdn, "fwbbMsisdn":"", "simSerial":"", "simSerial2":"", "inventoryType":inventoryType], STOP_ON_FAILURE)

12
00:01:06,755 --> 00:01:06,997
1. maximizeWindow()

13
00:01:07,016 --> 00:01:08,307
5. click(findTestObject("02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/a_Amdocs Inventory Dashboard"))

14
00:01:08,315 --> 00:01:10,412
9. click(findTestObject("02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/td_Ready for Inventory_QueueNo"))

15
00:01:10,416 --> 00:01:11,989
13. click(findTestObject("02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/Global Variable MSISDN (Assign Inventory)", ["msisdn":msisdn]))

16
00:01:11,995 --> 00:01:13,445
17. click(findTestObject("02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/button_Process"))

17
00:01:13,453 --> 00:01:14,646
21. callTestCase(findTestCase("98 Webservices/WS01_Airtable REST APIs/Get Inventory and marked as used"), ["inventoryType":inventoryType, "recordId":""], STOP_ON_FAILURE)

18
00:01:14,655 --> 00:01:15,067
1. callTestCase(findTestCase("98 Webservices/WS01_Airtable REST APIs/WS01_01 GET Available Inventory"), ["inventoryType":inventoryType], STOP_ON_FAILURE)

19
00:01:15,076 --> 00:01:20,557
1. response = sendRequest(findTestObject("98 Webservices Requests/WS1_Airtable_REST/GET Available Inventory", ["inventoryType":inventoryType]))

20
00:01:20,565 --> 00:01:20,593
5. slurper = new groovy.json.JsonSlurper()

21
00:01:20,605 --> 00:01:20,665
9. parsedJson = slurper.parseText(response.getResponseText())

22
00:01:20,675 --> 00:01:20,706
13. availableSim = simSerial

23
00:01:20,719 --> 00:01:20,721
17. simSerial = replace("[", "").replace("]", "")

24
00:01:20,731 --> 00:01:20,739
21. println(simSerial)

25
00:01:20,750 --> 00:01:20,751
25. simSerial = simSerial

26
00:01:20,766 --> 00:01:20,767
29. inventoryId = id

27
00:01:20,779 --> 00:01:20,781
33. recordId = replace("[", "").replace("]", "")

28
00:01:20,791 --> 00:01:20,792
37. println(recordId)

29
00:01:20,801 --> 00:01:20,802
41. id = recordId

30
00:01:20,808 --> 00:01:20,810
45. starterPackMsisdn = msisdn

31
00:01:20,815 --> 00:01:20,816
49. msisdn = replace("[", "").replace("]", "")

32
00:01:20,821 --> 00:01:20,822
53. println(msisdn)

33
00:01:20,827 --> 00:01:20,831
57. msisdn = msisdn

34
00:01:20,835 --> 00:01:21,828
5. callTestCase(findTestCase("98 Webservices/WS01_Airtable REST APIs/WS01_02 PATCH Used Inventory"), ["inventoryType":inventoryType], STOP_ON_FAILURE)

35
00:01:21,833 --> 00:01:21,834
1. recordId = id

36
00:01:21,838 --> 00:01:23,358
5. response = sendRequest(findTestObject("98 Webservices Requests/WS1_Airtable_REST/PATCH Used Inventory", ["recordId":recordId, "inventoryType":"inventoryHotlinkStarterPack"]))

37
00:01:23,365 --> 00:01:23,486
11. Verify if the response from "REST_Status Codes/POST_201" object returns the 201 status code

