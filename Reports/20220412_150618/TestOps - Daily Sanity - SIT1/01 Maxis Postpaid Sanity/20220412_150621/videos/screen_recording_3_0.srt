1
00:00:00,886 --> 00:00:00,965
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), ["username":"SUPERRR", "password":"test"], STOP_ON_FAILURE)

2
00:00:00,974 --> 00:00:08,554
1. openBrowser("")

3
00:00:08,559 --> 00:00:32,294
7. URL is set as a Global Variable controlled in Profiles

4
00:00:32,300 --> 00:00:34,261
13. Username is set as local variable

5
00:00:34,269 --> 00:00:36,130
19. Password is set as local variable

6
00:00:36,138 --> 00:00:37,171
23. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

7
00:00:37,181 --> 00:00:37,185
27. comment("ASSERTION")

8
00:00:37,194 --> 00:00:39,183
33. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

9
00:00:39,196 --> 00:00:39,295
5. callTestCase(findTestCase("01_Retailer Portal/RP02_Search Customer_Page/RP02_1 Search Customer"), ["idType":"ServiceID", "idValue":idValue, "accountNo":""], STOP_ON_FAILURE)

10
00:00:39,308 --> 00:00:40,478
1. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/i_keyboard_arrow_down"))

11
00:00:40,498 --> 00:00:40,506
5. comment("IF statement to check if Account No is populated in the input sheet. If TRUE idType will be forced to ACCOUNT and search done using the accountNo")

12
00:00:40,547 --> 00:00:40,549
9. if (accountNo.isEmpty() == false)

13
00:00:40,572 --> 00:00:41,762
13. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/Dropdown ID Type", ["idType":idType]))

14
00:00:41,773 --> 00:00:43,648
17. setText(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), idValue)

15
00:00:43,654 --> 00:00:44,646
21. sendKeys(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), Keys.chord(ENTER))

16
00:00:44,652 --> 00:00:44,656
25. comment("Checking for text "Customer Profile does not exist", if text exists the Global Variable newCustomer_Flag is set to TRUE and flow handling will be for New Customer ")

17
00:00:44,666 --> 00:00:45,064
29. newCustomer_Flag = verifyTextPresent("Customer Profile does not exist", false, OPTIONAL)

18
00:00:45,075 --> 00:00:57,652
9. verifyTextPresent("Searched By ServiceID 60121858058", false)

