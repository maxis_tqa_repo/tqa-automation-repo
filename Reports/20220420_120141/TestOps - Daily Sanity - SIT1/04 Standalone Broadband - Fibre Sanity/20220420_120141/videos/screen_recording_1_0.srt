1
00:00:01,799 --> 00:00:03,301
1. callTestCase(findTestCase("01_Retailer Portal/RP-01 New Plan, New Registration, No Device"), ["idType":idType, "idValue":idValue, "planId":planId, "customerName":customerName, "row":row, "msisdn":"", "accountNo":"", ... ], STOP_ON_FAILURE)

2
00:00:03,332 --> 00:00:04,593
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), [:], STOP_ON_FAILURE)

3
00:00:04,599 --> 00:00:16,802
1. openBrowser("")

4
00:00:16,817 --> 00:00:42,038
5. navigateToUrl(RP_URL)

5
00:00:42,055 --> 00:00:42,502
9. maximizeWindow()

6
00:00:42,512 --> 00:00:44,895
15. Username is set as local variable

7
00:00:44,902 --> 00:00:46,977
21. Password is set as local variable

8
00:00:46,987 --> 00:00:48,233
25. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

9
00:00:48,244 --> 00:00:50,233
31. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

10
00:00:50,241 --> 00:00:50,267
35. comment("ASSERTION : [ OK ] ")

11
00:00:50,274 --> 00:00:50,306
39. comment("		Successfully logged in as : " + RP_Username)

12
00:00:50,315 --> 00:00:50,322
5. runId = toString().substring(toString().lastIndexOf("\") + 1)

13
00:00:50,335 --> 00:00:51,700
9. callTestCase(findTestCase("01_Retailer Portal/RP02_Search Customer_Page/RP02_1 Search Customer"), ["idType":idType, "idValue":idValue, "accountNo":accountNo], STOP_ON_FAILURE)

14
00:00:51,711 --> 00:00:52,893
1. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/i_keyboard_arrow_down"))

15
00:00:52,900 --> 00:00:52,904
7. IF statement to check if Account No is populated in the input sheet. If TRUE idType will be forced to ACCOUNT and search done using the accountNo

16
00:00:52,910 --> 00:00:54,189
11. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/Dropdown ID Type", ["idType":idType]))

17
00:00:54,194 --> 00:00:56,301
15. setText(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), idValue)

18
00:00:56,307 --> 00:00:57,468
19. sendKeys(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), Keys.chord(ENTER))

19
00:00:57,473 --> 00:01:18,134
25. Checking for text "Customer Profile does not exist", if text exists the Global Variable newCustomer_Flag is set to TRUE and flow handling will be for New Customer 

20
00:01:18,139 --> 00:01:18,145
29. comment("WORKFLOW HANDLING : ")

21
00:01:18,149 --> 00:01:18,155
33. comment("	ID TYPE : " + idType)

22
00:01:18,159 --> 00:01:18,163
37. comment("	ID VALUE : " + idValue)

23
00:01:18,168 --> 00:01:18,172
41. "	NEW CUSTOMER : " + newCustomer_Flag

24
00:01:18,176 --> 00:01:19,321
13. callTestCase(findTestCase("01_Retailer Portal/RP03_CIC Dashboard_Page/RP03_1 New Purchase"), ["accountNo":accountNo], STOP_ON_FAILURE)

25
00:01:19,327 --> 00:01:19,332
1. comment("")

26
00:01:19,339 --> 00:01:19,344
5. comment("")

27
00:01:19,351 --> 00:01:19,358
11. Different Call To Action elements will be loaded depending on whether the customer is new or an existing one

1. New Purchase button for New Customer,
2. New Purchase button for Existing Customer Existing Account,
3. New Purchase button for Existing Customer New Account

28
00:01:19,364 --> 00:01:21,713
3. click(findTestObject("01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/button_New Purchase"))

29
00:01:21,721 --> 00:01:39,158
15. verifyTextPresent("Catalog", false)

30
00:01:39,161 --> 00:01:39,165
19. comment("ASSERTION : [ OK ] ")

31
00:01:39,169 --> 00:01:39,172
23. comment("	Product Catalogue Page loaded")

32
00:01:39,176 --> 00:01:39,807
27. comment("	Text Catalog present? " + println(verifyTextPresent("Catalog", false)))

33
00:01:39,813 --> 00:01:40,838
17. callTestCase(findTestCase("01_Retailer Portal/RP04_Catalog_Page/RP04_1 Purchase plan only, no device"), ["planId":planId], STOP_ON_FAILURE)

34
00:01:40,844 --> 00:01:42,133
3. Click mobile plan tab

35
00:01:42,139 --> 00:01:43,497
7. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.button.planProduct", ["planProduct":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3)]))

36
00:01:43,503 --> 00:02:15,261
11. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.label.planType", ["planType":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 2)]), OPTIONAL)

37
00:02:15,267 --> 00:02:15,310
15. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

38
00:02:15,315 --> 00:02:15,319
19. comment("")

39
00:02:15,326 --> 00:02:15,327
23. if (isFibreStandAlone == true)

40
00:02:15,334 --> 00:02:15,337
1. comment("	Step skipped because order flow type is for stand alone Fibre")

41
00:02:15,342 --> 00:02:15,346
27. if (isFibreStandAlone == false)

42
00:02:15,352 --> 00:02:16,379
21. callTestCase(findTestCase("01_Retailer Portal/RP05_Product Configuration_Page/RP05_1 Skip Product Configuration"), ["planId":planId], STOP_ON_FAILURE)

43
00:02:16,383 --> 00:02:16,396
1. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

44
00:02:16,401 --> 00:02:16,402
5. if (isFibreStandAlone == true)

45
00:02:16,406 --> 00:02:16,421
1. comment("	Step skipped because order flow type is for stand alone Fibre")

46
00:02:16,425 --> 00:02:16,428
9. if (isFibreStandAlone == false)

47
00:02:16,434 --> 00:02:17,524
25. callTestCase(findTestCase("01_Retailer Portal/RP06_Cart_Page/RP06_1 Cart Page"), ["msisdn":msisdn, "broadbandPlan":broadbandPlan, "planId":planId], STOP_ON_FAILURE)

48
00:02:17,531 --> 00:02:17,551
1. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

49
00:02:17,557 --> 00:02:17,559
5. if (isFibreStandAlone == true)

50
00:02:17,566 --> 00:02:18,570
1. callTestCase(findTestCase("01_Retailer Portal/RP06A_Link_to_Broadband/RP06A_2 Add Broadband - Fibre"), ["broadbandPlan":planId], STOP_ON_FAILURE)

51
00:02:18,574 --> 00:02:20,830
1. click(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_bundle-list/button_I want this plan", ["broadbandPlan":broadbandPlan]))

52
00:02:20,837 --> 00:02:22,001
5. click(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/input_search_searchAddresInput"))

53
00:02:22,006 --> 00:02:23,120
9. click(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/div_search"))

54
00:02:23,123 --> 00:02:25,243
13. setText(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/input_search_searchAddresInput"), "no 1, Jalan Medan Masria, Taman Masria, 43200 Cheras, Selangor")

55
00:02:25,248 --> 00:02:26,344
17. sendKeys(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/input_search_searchAddresInput"), Keys.chord(ENTER))

56
00:02:26,350 --> 00:02:30,669
21. click(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/i_navigate_next"))

57
00:02:30,675 --> 00:02:32,983
25. setText(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/input_access_time_providerId"), "1-12345678901")

58
00:02:32,988 --> 00:02:35,134
29. setText(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/input_Sample Format_serviceId"), "BU1234567890")

59
00:02:35,138 --> 00:02:36,318
33. click(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_check-coverage/button_Submit and Continue"))

60
00:02:36,324 --> 00:02:41,718
37. click(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_bundle-configuration/button_Next"))

61
00:02:41,725 --> 00:02:42,930
41. enhancedClick(findTestObject("01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_2 Add Fibre Component/Page_bundle-configuration/button_Continue_to_Cart"))

62
00:02:42,934 --> 00:02:44,963
45. delay(2)

63
00:02:44,966 --> 00:02:45,789
5. callTestCase(findTestCase("01_Retailer Portal/RP06A_Link_to_Broadband/RP06A_3 Assign VoIP number"), [:], STOP_ON_FAILURE)

