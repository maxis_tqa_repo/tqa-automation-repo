1
00:00:02,025 --> 00:00:03,368
1. callTestCase(findTestCase("01_Retailer Portal/RP-TMPL-01 NEW PLAN REGISTRATION NO DEVICE"), ["idType":idType, "idValue":idValue, "planId":planId, "customerName":customerName, "row":row, "msisdn":"", "accountNo":"", ... ], STOP_ON_FAILURE)

2
00:00:03,407 --> 00:00:04,719
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), [:], STOP_ON_FAILURE)

3
00:00:04,724 --> 00:00:20,914
1. openBrowser("")

4
00:00:20,923 --> 00:00:47,907
7. URL is set as a Global Variable controlled in Profiles

5
00:00:47,917 --> 00:00:50,383
13. Username is set as local variable

6
00:00:50,391 --> 00:00:52,534
19. Password is set as local variable

7
00:00:52,543 --> 00:00:53,996
23. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

8
00:00:54,004 --> 00:00:54,048
27. comment("ASSERTION")

9
00:00:54,056 --> 00:00:56,607
33. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

10
00:00:56,627 --> 00:00:56,636
5. runId = toString().substring(toString().lastIndexOf("\") + 1)

11
00:00:56,648 --> 00:00:58,526
9. callTestCase(findTestCase("01_Retailer Portal/RP02_Search Customer_Page/RP02_1 Search Customer"), ["idType":idType, "idValue":idValue, "accountNo":accountNo], STOP_ON_FAILURE)

12
00:00:58,533 --> 00:01:00,312
1. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/i_keyboard_arrow_down"))

13
00:01:00,323 --> 00:01:00,339
5. comment("IF statement to check if Account No is populated in the input sheet. If TRUE idType will be forced to ACCOUNT and search done using the accountNo")

14
00:01:00,347 --> 00:01:00,352
9. if (accountNo.isEmpty() == false)

15
00:01:00,360 --> 00:01:02,515
13. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/Dropdown ID Type", ["idType":idType]))

16
00:01:02,534 --> 00:01:04,818
17. setText(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), idValue)

17
00:01:04,827 --> 00:01:05,977
21. sendKeys(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), Keys.chord(ENTER))

18
00:01:05,985 --> 00:01:05,994
25. comment("Checking for text "Customer Profile does not exist", if text exists the Global Variable newCustomer_Flag is set to TRUE and flow handling will be for New Customer ")

19
00:01:06,003 --> 00:01:32,707
29. newCustomer_Flag = verifyTextPresent("Customer Profile does not exist", false, OPTIONAL)

20
00:01:32,717 --> 00:01:35,058
13. callTestCase(findTestCase("01_Retailer Portal/RP03_CIC Dashboard_Page/RP03_1 New Purchase"), ["accountNo":accountNo], STOP_ON_FAILURE)

21
00:01:35,072 --> 00:01:35,080
1. comment("Different Call To Action elements will be loaded depending on whether the customer is new or an existing one")

22
00:01:35,088 --> 00:01:35,096
5. comment("1. New Purchase button for New Customer, 2. New Purchase button for Existing Customer Existing Account, 3. New Purchase button for Existing Customer New Account")

23
00:01:35,104 --> 00:01:35,136
9. if (newCustomer_Flag == true)

24
00:01:35,144 --> 00:01:38,655
3. click(findTestObject("01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/button_New Purchase"))

25
00:01:38,665 --> 00:01:38,673
13. comment("ASSERTION")

26
00:01:38,682 --> 00:01:56,552
17. verifyTextPresent("Catalog", false)

27
00:01:56,560 --> 00:01:57,627
17. callTestCase(findTestCase("01_Retailer Portal/RP04_Catalog_Page/RP04_1 Purchase plan only, no device"), ["planId":planId], STOP_ON_FAILURE)

28
00:01:57,634 --> 00:01:58,843
3. Click mobile plan tab

29
00:01:58,849 --> 00:02:00,173
7. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.button.planProduct", ["planProduct":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3)]))

30
00:02:00,180 --> 00:02:01,830
11. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.label.planType", ["planType":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 2)]), OPTIONAL)

31
00:02:01,834 --> 00:02:01,882
15. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

32
00:02:01,889 --> 00:02:01,891
19. if (isFibreStandAlone == true)

33
00:02:01,896 --> 00:02:01,897
23. if (isFibreStandAlone == false)

34
00:02:01,902 --> 00:02:04,367
1. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/Button Select This Plan", ["planId":planId]))

35
00:02:04,374 --> 00:02:12,341
7. Verfiy that the Plan Name is the same as the one purchased in previous page

36
00:02:12,346 --> 00:02:13,468
11. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/button_Continue without Device"))

37
00:02:13,476 --> 00:02:15,041
21. callTestCase(findTestCase("01_Retailer Portal/RP05_Product Configuration_Page/RP05_1 Skip Product Configuration"), ["planId":planId], STOP_ON_FAILURE)

38
00:02:15,045 --> 00:02:15,073
1. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

39
00:02:15,092 --> 00:02:15,096
5. if (isFibreStandAlone == true)

40
00:02:15,115 --> 00:02:15,121
9. if (isFibreStandAlone == false)

41
00:02:15,134 --> 00:02:24,283
1. click(findTestObject("01_Retailer Portal_Objects/RP05_Product Configuration_Page_Objects/button_Next"))

42
00:02:24,291 --> 00:02:29,815
5. verifyTextPresent("Selected Plan", false)

43
00:02:29,823 --> 00:02:32,143
9. click(findTestObject("01_Retailer Portal_Objects/RP05_Product Configuration_Page_Objects/button_Continue to Cart"))

44
00:02:32,149 --> 00:02:33,344
25. callTestCase(findTestCase("01_Retailer Portal/RP06_Cart_Page/RP06_1 Cart Page"), ["msisdn":msisdn, "broadbandPlan":broadbandPlan, "planId":planId], STOP_ON_FAILURE)

45
00:02:33,351 --> 00:02:33,374
1. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

46
00:02:33,383 --> 00:02:33,385
5. if (isFibreStandAlone == true)

47
00:02:33,391 --> 00:02:33,393
9. if (isFibreStandAlone == false)

48
00:02:33,401 --> 00:02:33,403
3. If msisdn in the input files is empty, then the order will just use the assigned msisdn

49
00:02:33,411 --> 00:02:33,415
1. callTestCase(findTestCase("01_Retailer Portal/RP06_Cart_Page/RP06_5 Link Broadband and Check Out"), ["broadbandPlan":broadbandPlan], STOP_ON_FAILURE)

50
00:02:33,422 --> 00:02:34,979
5. callTestCase(findTestCase("01_Retailer Portal/RP06_Cart_Page/RP06_5 Link Broadband and Check Out - Copy"), ["broadbandPlan":broadbandPlan, "planId":planId], STOP_ON_FAILURE)

51
00:02:34,985 --> 00:02:35,002
1. isplanTypeUnlimited = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 2), "Unlimited", false, OPTIONAL)

52
00:02:35,010 --> 00:02:35,037
5. isplanTypeFamilyPlan = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 2), "Family Plan", false, OPTIONAL)

53
00:02:35,044 --> 00:02:35,066
9. isplanTypePrime = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 2), "Prime", false, OPTIONAL)

54
00:02:35,075 --> 00:02:35,077
13. if (isplanTypeUnlimited == true)

55
00:02:35,084 --> 00:02:35,086
17. if (isplanTypeFamilyPlan == true)

56
00:02:35,093 --> 00:02:35,095
21. if (isplanTypePrime == true)

57
00:02:35,102 --> 00:02:36,455
25. click(findTestObject("01_Retailer Portal_Objects/RP06_Cart_Page_Objects/button_Check Out"))

58
00:02:36,461 --> 00:02:37,957
29. callTestCase(findTestCase("01_Retailer Portal/RP07_Create Customer_Page/RP07_1 Create Customer_Page"), ["idType":idType, "customerName":customerName, "accountNo":accountNo, "idValue":idValue], STOP_ON_FAILURE)

59
00:02:37,978 --> 00:02:37,994
1. isMalaysianCitizen = verifyMatch(idType, "MALAYSIAN ID CARD", false, OPTIONAL)

60
00:02:38,003 --> 00:02:38,005
5. if (accountNo.isEmpty() == false)

61
00:02:38,015 --> 00:02:38,016
9. if (accountNo.isEmpty() == true)

62
00:02:38,025 --> 00:02:40,292
1. verifyTextPresent("Customer Details", false)

63
00:02:40,298 --> 00:02:40,299
5. if (newCustomer_Flag == true)

64
00:02:40,307 --> 00:02:40,309
9. if (newCustomer_Flag == false)

65
00:02:40,317 --> 00:02:41,507
1. callTestCase(findTestCase("01_Retailer Portal/RP07_Create Customer_Page/RP07_1_2 Use Contact Address as Billing Address"), [:], STOP_ON_FAILURE)

66
00:02:41,513 --> 00:02:42,618
1. click(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/label_Use Contact Address as Billing Address"))

67
00:02:42,625 --> 00:02:44,675
5. setText(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/input_Receive a monthly email notification _bc6444"), Email)

68
00:02:44,680 --> 00:02:45,730
9. click(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/div_Regular Paper Bill_CustomCheckbox"))

69
00:02:45,737 --> 00:02:46,917
13. click(findTestObject("01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/button_Continue"))

70
00:02:46,924 --> 00:02:48,278
33. callTestCase(findTestCase("01_Retailer Portal/RP08_Order Summary_Page/RP08_1 Document Upload and Submit Order"), ["idType":idType], STOP_ON_FAILURE)

71
00:02:48,286 --> 00:02:58,035
1. verifyTextPresent("Order Summary", false, CONTINUE_ON_FAILURE)

72
00:02:58,043 --> 00:02:59,112
5. click(findTestObject("01_Retailer Portal_Objects/RP08_Order Summary_Page_Objects/i_keyboard_arrow_down"))

73
00:02:59,122 --> 00:03:00,183
9. click(findTestObject("01_Retailer Portal_Objects/RP08_Order Summary_Page_Objects/DropDownID_Type", ["idType":idType]))

74
00:03:00,190 --> 00:03:00,193
13. dirName = getProjectDir()

75
00:03:00,198 --> 00:03:00,199
17. upload = "" + dirName + "\Data Files\Test Data and Input Files\Uploads\maxis.jpg"

76
00:03:00,205 --> 00:03:00,208
21. uploadPath = upload.replace("/", "\")

77
00:03:00,214 --> 00:03:07,446
25. com.uploadfile.UploadFile.uploadFileUsingRobot(findTestObject("01_Retailer Portal_Objects/RP08_Order Summary_Page_Objects/div_Browse"), uploadPath)

78
00:03:07,455 --> 00:03:07,499
29. delay(0)

