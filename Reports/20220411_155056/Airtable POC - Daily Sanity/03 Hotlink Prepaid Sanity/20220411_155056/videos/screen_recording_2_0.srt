1
00:00:00,003 --> 00:00:00,131
1. callTestCase(findTestCase("01_Retailer Portal/RP-TMPL-04 PREPAID STARTER PACK REGISTRATION - POC"), ["idValue":idValue, "customerName":customerName, "msisdn":msisdn, "row":row], STOP_ON_FAILURE)

2
00:00:00,138 --> 00:00:00,233
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), ["username":"mcSup", "password":"123"], STOP_ON_FAILURE)

3
00:00:00,238 --> 00:00:05,910
1. openBrowser("")

4
00:00:05,917 --> 00:00:29,594
7. URL is set as a Global Variable controlled in Profiles

5
00:00:29,599 --> 00:00:31,720
13. Username is set as local variable

6
00:00:31,723 --> 00:00:33,672
19. Password is set as local variable

7
00:00:33,677 --> 00:00:34,792
23. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

8
00:00:34,798 --> 00:00:34,802
27. comment("ASSERTION")

9
00:00:34,808 --> 00:00:36,644
33. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

10
00:00:36,648 --> 00:00:36,732
5. callTestCase(findTestCase("01_Retailer Portal/RP07_Create Customer_Page/RP07_1_6 Insert Data into AMR"), ["idValue":idValue, "customerName":customerName], STOP_ON_FAILURE)

11
00:00:36,738 --> 00:00:37,000
1. com.database.connectSql.connectDB("10.200.50.177", "AMRDB", "1433", "amr_user", "amr_user2015")

12
00:00:37,007 --> 00:00:37,077
5. com.database.connectSql.insertCustomerDetailAMR(idValue, customerName)

13
00:00:37,083 --> 00:00:37,089
9. com.database.connectSql.closeDatabaseConnection()

14
00:00:37,095 --> 00:00:37,193
9. callTestCase(findTestCase("01_Retailer Portal/RP03_CIC Dashboard_Page/RP03_4 Starter Pack Registration"), [:], STOP_ON_FAILURE)

15
00:00:37,198 --> 00:00:38,919
1. click(findTestObject("01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/div_Starter Pack Registration"))

16
00:00:38,926 --> 00:00:39,014
13. callTestCase(findTestCase("98 Webservices/WS01_Airtable REST APIs/Get Inventory and marked as used"), ["inventoryType":"inventoryMaxisPostpaidSIM", "recordId":""], STOP_ON_FAILURE)

17
00:00:39,020 --> 00:00:39,107
1. callTestCase(findTestCase("98 Webservices/WS01_Airtable REST APIs/WS01_01 GET Available Inventory"), ["inventoryType":inventoryType], STOP_ON_FAILURE)

18
00:00:39,112 --> 00:00:40,531
1. response = sendRequest(findTestObject("98 Webservices Requests/WS1_Airtable_REST/GET Available Inventory", ["inventoryType":inventoryType]))

19
00:00:40,535 --> 00:00:40,537
5. slurper = new groovy.json.JsonSlurper()

20
00:00:40,542 --> 00:00:40,544
9. parsedJson = slurper.parseText(response.getResponseText())

21
00:00:40,550 --> 00:00:40,552
13. availableSim = simSerial

22
00:00:40,557 --> 00:00:40,558
17. simSerial = replace("[", "").replace("]", "")

23
00:00:40,563 --> 00:00:40,564
21. println(simSerial)

24
00:00:40,568 --> 00:00:40,569
25. simSerial = simSerial

25
00:00:40,575 --> 00:00:40,577
29. inventoryId = id

26
00:00:40,581 --> 00:00:40,582
33. recordId = replace("[", "").replace("]", "")

27
00:00:40,587 --> 00:00:40,589
37. println(recordId)

28
00:00:40,593 --> 00:00:40,594
41. id = recordId

29
00:00:40,598 --> 00:00:40,599
45. starterPackMsisdn = msisdn

30
00:00:40,605 --> 00:00:40,606
49. msisdn = replace("[", "").replace("]", "")

31
00:00:40,611 --> 00:00:40,612
53. println(msisdn)

32
00:00:40,616 --> 00:00:40,620
57. msisdn = msisdn

33
00:00:40,624 --> 00:00:40,712
5. callTestCase(findTestCase("98 Webservices/WS01_Airtable REST APIs/WS01_02 PATCH Used Inventory"), ["recordId":recordId], STOP_ON_FAILURE)

34
00:00:40,719 --> 00:00:40,720
1. recordId = id

35
00:00:40,728 --> 00:00:42,172
5. response = sendRequest(findTestObject("98 Webservices Requests/WS1_Airtable_REST/PATCH Used Inventory", ["recordId":recordId]))

36
00:00:42,177 --> 00:00:42,186
11. Verify if the response from "REST_Status Codes/POST_201" object returns the 201 status code

37
00:00:42,190 --> 00:00:42,290
17. callTestCase(findTestCase("01_Retailer Portal/RP13_Starter Pack Registration_Page/RP13_1 Register Prepaid Starter Pack via NRIC - POC"), ["simSerial":"", "msisdn":"", "idValue":""], STOP_ON_FAILURE)

38
00:00:42,296 --> 00:00:42,298
1. simSerial = simSerial

39
00:00:42,303 --> 00:00:44,347
5. setText(findTestObject("01_Retailer Portal_Objects/RP13_Starter Pack Registration Page_Objects/input_idForSimNum"), simSerial)

40
00:00:44,352 --> 00:00:44,354
9. msisdn = msisdn

41
00:00:44,361 --> 00:00:46,237
13. setText(findTestObject("01_Retailer Portal_Objects/RP13_Starter Pack Registration Page_Objects/input_idForMsisdnNum"), msisdn)

42
00:00:46,243 --> 00:00:47,298
17. click(findTestObject("01_Retailer Portal_Objects/RP13_Starter Pack Registration Page_Objects/button_Verify"))

43
00:00:47,302 --> 00:01:19,063
21. click(findTestObject("01_Retailer Portal_Objects/RP13_Starter Pack Registration Page_Objects/button_NRIC"))

