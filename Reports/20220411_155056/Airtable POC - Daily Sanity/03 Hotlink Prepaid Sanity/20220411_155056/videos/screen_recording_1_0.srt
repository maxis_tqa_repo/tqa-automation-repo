1
00:00:01,015 --> 00:00:02,499
1. callTestCase(findTestCase("01_Retailer Portal/RP-TMPL-04 PREPAID STARTER PACK REGISTRATION - POC"), ["idValue":idValue, "customerName":customerName, "msisdn":msisdn, "row":row], STOP_ON_FAILURE)

2
00:00:02,518 --> 00:00:03,620
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), ["username":"mcSup", "password":"123"], STOP_ON_FAILURE)

3
00:00:03,624 --> 00:00:13,572
1. openBrowser("")

4
00:00:13,578 --> 00:00:38,208
7. URL is set as a Global Variable controlled in Profiles

5
00:00:38,212 --> 00:00:40,196
13. Username is set as local variable

6
00:00:40,203 --> 00:00:42,055
19. Password is set as local variable

7
00:00:42,059 --> 00:00:43,105
23. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

8
00:00:43,111 --> 00:00:43,135
27. comment("ASSERTION")

9
00:00:43,139 --> 00:00:45,071
33. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

10
00:00:45,075 --> 00:00:45,975
5. callTestCase(findTestCase("01_Retailer Portal/RP07_Create Customer_Page/RP07_1_6 Insert Data into AMR"), ["idValue":idValue, "customerName":customerName], STOP_ON_FAILURE)

11
00:00:45,979 --> 00:00:46,854
1. com.database.connectSql.connectDB("10.200.50.177", "AMRDB", "1433", "amr_user", "amr_user2015")

12
00:00:46,859 --> 00:00:47,001
5. com.database.connectSql.insertCustomerDetailAMR(idValue, customerName)

13
00:00:47,005 --> 00:00:47,011
9. com.database.connectSql.closeDatabaseConnection()

14
00:00:47,017 --> 00:00:47,791
9. callTestCase(findTestCase("01_Retailer Portal/RP03_CIC Dashboard_Page/RP03_4 Starter Pack Registration"), [:], STOP_ON_FAILURE)

15
00:00:47,795 --> 00:00:49,499
1. click(findTestObject("01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/div_Starter Pack Registration"))

16
00:00:49,510 --> 00:00:50,761
13. callTestCase(findTestCase("98 Webservices/WS01_Airtable REST APIs/Get Inventory and marked as used"), ["inventoryType":"inventoryMaxisPostpaidSIM", "recordId":""], STOP_ON_FAILURE)

17
00:00:50,768 --> 00:00:51,387
1. callTestCase(findTestCase("98 Webservices/WS01_Airtable REST APIs/WS01_01 GET Available Inventory"), ["inventoryType":inventoryType], STOP_ON_FAILURE)

18
00:00:51,392 --> 00:00:54,811
1. response = sendRequest(findTestObject("98 Webservices Requests/WS1_Airtable_REST/GET Available Inventory", ["inventoryType":inventoryType]))

19
00:00:54,818 --> 00:00:54,840
5. slurper = new groovy.json.JsonSlurper()

20
00:00:54,846 --> 00:00:54,907
9. parsedJson = slurper.parseText(response.getResponseText())

21
00:00:54,914 --> 00:00:54,944
13. availableSim = simSerial

22
00:00:54,951 --> 00:00:54,954
17. simSerial = replace("[", "").replace("]", "")

23
00:00:54,961 --> 00:00:54,971
21. println(simSerial)

24
00:00:54,980 --> 00:00:54,986
25. simSerial = simSerial

25
00:00:54,993 --> 00:00:54,995
29. inventoryId = id

26
00:00:55,000 --> 00:00:55,002
33. recordId = replace("[", "").replace("]", "")

27
00:00:55,009 --> 00:00:55,011
37. println(recordId)

28
00:00:55,016 --> 00:00:55,018
41. id = recordId

29
00:00:55,023 --> 00:00:55,024
45. starterPackMsisdn = msisdn

30
00:00:55,030 --> 00:00:55,032
49. msisdn = replace("[", "").replace("]", "")

31
00:00:55,037 --> 00:00:55,039
53. println(msisdn)

32
00:00:55,044 --> 00:00:55,050
57. msisdn = msisdn

33
00:00:55,057 --> 00:00:56,218
5. callTestCase(findTestCase("98 Webservices/WS01_Airtable REST APIs/WS01_02 PATCH Used Inventory"), ["recordId":recordId], STOP_ON_FAILURE)

34
00:00:56,222 --> 00:00:56,223
1. recordId = id

35
00:00:56,227 --> 00:00:57,710
5. response = sendRequest(findTestObject("98 Webservices Requests/WS1_Airtable_REST/PATCH Used Inventory", ["recordId":recordId]))

36
00:00:57,715 --> 00:00:57,750
11. Verify if the response from "REST_Status Codes/POST_201" object returns the 201 status code

37
00:00:57,755 --> 00:00:58,632
17. callTestCase(findTestCase("01_Retailer Portal/RP13_Starter Pack Registration_Page/RP13_1 Register Prepaid Starter Pack via NRIC - POC"), ["simSerial":"", "msisdn":"", "idValue":""], STOP_ON_FAILURE)

38
00:00:58,636 --> 00:00:58,638
1. simSerial = simSerial

39
00:00:58,643 --> 00:01:00,578
5. setText(findTestObject("01_Retailer Portal_Objects/RP13_Starter Pack Registration Page_Objects/input_idForSimNum"), simSerial)

40
00:01:00,585 --> 00:01:00,586
9. msisdn = msisdn

41
00:01:00,592 --> 00:01:02,487
13. setText(findTestObject("01_Retailer Portal_Objects/RP13_Starter Pack Registration Page_Objects/input_idForMsisdnNum"), msisdn)

42
00:01:02,491 --> 00:01:03,614
17. click(findTestObject("01_Retailer Portal_Objects/RP13_Starter Pack Registration Page_Objects/button_Verify"))

43
00:01:03,623 --> 00:01:35,425
21. click(findTestObject("01_Retailer Portal_Objects/RP13_Starter Pack Registration Page_Objects/button_NRIC"))

