1
00:00:01,288 --> 00:00:02,310
1. callTestCase(findTestCase("01_Retailer Portal/RP-01 New Plan, New Registration, No Device"), ["idType":idType, "idValue":idValue, "planId":planId, "customerName":customerName, "row":row, "msisdn":"", "accountNo":"", ... ], STOP_ON_FAILURE)

2
00:00:02,333 --> 00:00:03,240
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), [:], STOP_ON_FAILURE)

3
00:00:03,244 --> 00:00:13,005
1. openBrowser("")

4
00:00:13,032 --> 00:00:37,467
5. navigateToUrl(RP_URL)

5
00:00:37,472 --> 00:00:37,683
9. maximizeWindow()

6
00:00:37,693 --> 00:00:39,959
15. Username is set as local variable

7
00:00:39,968 --> 00:00:42,019
21. Password is set as local variable

8
00:00:42,023 --> 00:00:43,135
25. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

9
00:00:43,141 --> 00:00:46,700
31. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

10
00:00:46,705 --> 00:00:46,748
35. comment("ASSERTION : [ OK ] ")

11
00:00:46,756 --> 00:00:46,787
39. comment("		Successfully logged in as : " + RP_Username)

12
00:00:46,792 --> 00:00:46,795
5. runId = toString().substring(toString().lastIndexOf("\") + 1)

13
00:00:46,802 --> 00:00:47,632
9. callTestCase(findTestCase("01_Retailer Portal/RP02_Search Customer_Page/RP02_1 Search Customer"), ["idType":idType, "idValue":idValue, "accountNo":accountNo], STOP_ON_FAILURE)

14
00:00:47,640 --> 00:00:48,732
1. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/i_keyboard_arrow_down"))

15
00:00:48,739 --> 00:00:48,743
7. IF statement to check if Account No is populated in the input sheet. If TRUE idType will be forced to ACCOUNT and search done using the accountNo

16
00:00:48,751 --> 00:00:49,933
11. click(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/Dropdown ID Type", ["idType":idType]))

17
00:00:49,938 --> 00:00:52,008
15. setText(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), idValue)

18
00:00:52,014 --> 00:00:53,110
19. sendKeys(findTestObject("01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId"), Keys.chord(ENTER))

19
00:00:53,119 --> 00:01:34,357
25. Checking for text "Customer Profile does not exist", if text exists the Global Variable newCustomer_Flag is set to TRUE and flow handling will be for New Customer 

20
00:01:34,361 --> 00:01:34,376
29. comment("WORKFLOW HANDLING : ")

21
00:01:34,383 --> 00:01:34,387
33. comment("	ID TYPE : " + idType)

22
00:01:34,393 --> 00:01:34,396
37. comment("	ID VALUE : " + idValue)

23
00:01:34,401 --> 00:01:34,405
41. "	NEW CUSTOMER : " + newCustomer_Flag

24
00:01:34,412 --> 00:01:35,326
13. callTestCase(findTestCase("01_Retailer Portal/RP03_CIC Dashboard_Page/RP03_1 New Purchase"), ["accountNo":accountNo], STOP_ON_FAILURE)

25
00:01:35,332 --> 00:01:35,335
1. comment("")

26
00:01:35,343 --> 00:01:35,346
5. comment("")

27
00:01:35,352 --> 00:01:35,354
11. Different Call To Action elements will be loaded depending on whether the customer is new or an existing one

1. New Purchase button for New Customer,
2. New Purchase button for Existing Customer Existing Account,
3. New Purchase button for Existing Customer New Account

28
00:01:35,360 --> 00:01:40,205
3. click(findTestObject("01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/button_New Purchase"))

29
00:01:40,210 --> 00:02:10,779
15. verifyTextPresent("Catalog", false)

30
00:02:10,783 --> 00:02:10,787
19. comment("ASSERTION : [ OK ] ")

31
00:02:10,796 --> 00:02:10,800
23. comment("	Product Catalogue Page loaded")

32
00:02:10,808 --> 00:02:15,804
27. comment("	Text Catalog present? " + println(verifyTextPresent("Catalog", false)))

33
00:02:15,807 --> 00:02:16,798
17. callTestCase(findTestCase("01_Retailer Portal/RP04_Catalog_Page/RP04_1 Purchase plan only, no device"), ["planId":planId], STOP_ON_FAILURE)

34
00:02:16,802 --> 00:02:17,956
3. Click mobile plan tab

35
00:02:17,958 --> 00:02:19,265
7. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.button.planProduct", ["planProduct":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3)]))

36
00:02:19,278 --> 00:03:01,105
11. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.label.planType", ["planType":findTestData("Data Dictionary/DD_PLANS").getValue(planId, 2)]), OPTIONAL)

37
00:03:01,110 --> 00:03:01,162
15. isFibreStandAlone = verifyMatch(findTestData("Data Dictionary/DD_PLANS").getValue(planId, 3), "Fibre", false, OPTIONAL)

38
00:03:01,170 --> 00:03:01,177
19. comment("")

39
00:03:01,188 --> 00:03:01,189
23. if (isFibreStandAlone == true)

40
00:03:01,197 --> 00:03:01,199
27. if (isFibreStandAlone == false)

41
00:03:01,207 --> 00:03:33,228
1. click(findTestObject("01_Retailer Portal_Objects/RP04_Catalog Page_Objects/Button Select This Plan", ["planId":planId]))

