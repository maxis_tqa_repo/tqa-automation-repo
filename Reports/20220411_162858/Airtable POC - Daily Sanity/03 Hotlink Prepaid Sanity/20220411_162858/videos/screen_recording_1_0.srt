1
00:00:01,200 --> 00:00:02,363
1. callTestCase(findTestCase("01_Retailer Portal/RP-TMPL-04 PREPAID STARTER PACK REGISTRATION - POC"), ["idValue":idValue, "customerName":customerName, "row":row, "inventoryType":inventoryType], STOP_ON_FAILURE)

2
00:00:02,389 --> 00:00:03,339
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), ["username":"mcSup", "password":"123"], STOP_ON_FAILURE)

3
00:00:03,343 --> 00:00:13,099
1. openBrowser("")

4
00:00:13,107 --> 00:00:37,310
7. URL is set as a Global Variable controlled in Profiles

5
00:00:37,320 --> 00:00:39,457
13. Username is set as local variable

6
00:00:39,464 --> 00:00:41,291
19. Password is set as local variable

7
00:00:41,296 --> 00:00:42,354
23. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

8
00:00:42,365 --> 00:00:42,391
27. comment("ASSERTION")

9
00:00:42,400 --> 00:00:44,281
33. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

10
00:00:44,292 --> 00:00:45,177
5. callTestCase(findTestCase("01_Retailer Portal/RP07_Create Customer_Page/RP07_1_6 Insert Data into AMR"), ["idValue":idValue, "customerName":customerName], STOP_ON_FAILURE)

11
00:00:45,183 --> 00:00:46,082
1. com.database.connectSql.connectDB("10.200.50.177", "AMRDB", "1433", "amr_user", "amr_user2015")

12
00:00:46,092 --> 00:00:46,296
5. com.database.connectSql.insertCustomerDetailAMR(idValue, customerName)

13
00:00:46,305 --> 00:00:46,313
9. com.database.connectSql.closeDatabaseConnection()

14
00:00:46,321 --> 00:00:47,063
9. callTestCase(findTestCase("01_Retailer Portal/RP03_CIC Dashboard_Page/RP03_4 Starter Pack Registration"), [:], STOP_ON_FAILURE)

15
00:00:47,068 --> 00:00:48,755
1. click(findTestObject("01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/div_Starter Pack Registration"))

16
00:00:48,769 --> 00:00:49,962
13. callTestCase(findTestCase("98 Webservices/WS01_Airtable REST APIs/Get Inventory and marked as used"), ["inventoryType":inventoryType], STOP_ON_FAILURE)

17
00:00:49,967 --> 00:00:50,514
1. callTestCase(findTestCase("98 Webservices/WS01_Airtable REST APIs/WS01_01 GET Available Inventory"), ["inventoryType":inventoryType], STOP_ON_FAILURE)

18
00:00:50,519 --> 00:00:53,332
1. response = sendRequest(findTestObject("98 Webservices Requests/WS1_Airtable_REST/GET Available Inventory", ["inventoryType":inventoryType]))

19
00:00:53,340 --> 00:00:53,369
5. slurper = new groovy.json.JsonSlurper()

20
00:00:53,380 --> 00:00:53,430
9. parsedJson = slurper.parseText(response.getResponseText())

21
00:00:53,438 --> 00:00:53,463
13. availableSim = simSerial

22
00:00:53,476 --> 00:00:53,479
17. simSerial = replace("[", "").replace("]", "")

23
00:00:53,490 --> 00:00:53,498
21. println(simSerial)

24
00:00:53,510 --> 00:00:53,512
25. simSerial = simSerial

25
00:00:53,524 --> 00:00:53,526
29. inventoryId = id

26
00:00:53,535 --> 00:00:53,537
33. recordId = replace("[", "").replace("]", "")

27
00:00:53,547 --> 00:00:53,549
37. println(recordId)

28
00:00:53,556 --> 00:00:53,558
41. id = recordId

29
00:00:53,563 --> 00:00:53,565
45. starterPackMsisdn = msisdn

30
00:00:53,571 --> 00:00:53,573
49. msisdn = replace("[", "").replace("]", "")

31
00:00:53,581 --> 00:00:53,583
53. println(msisdn)

32
00:00:53,588 --> 00:00:53,593
57. msisdn = msisdn

33
00:00:53,598 --> 00:00:54,519
5. callTestCase(findTestCase("98 Webservices/WS01_Airtable REST APIs/WS01_02 PATCH Used Inventory"), ["inventoryType":inventoryType], STOP_ON_FAILURE)

34
00:00:54,524 --> 00:00:54,526
1. recordId = id

35
00:00:54,532 --> 00:00:56,026
5. response = sendRequest(findTestObject("98 Webservices Requests/WS1_Airtable_REST/PATCH Used Inventory", ["recordId":recordId, "inventoryType":"inventoryHotlinkStarterPack"]))

36
00:00:56,030 --> 00:00:56,071
11. Verify if the response from "REST_Status Codes/POST_201" object returns the 201 status code

37
00:00:56,086 --> 00:00:57,066
17. callTestCase(findTestCase("01_Retailer Portal/RP13_Starter Pack Registration_Page/RP13_1 Register Prepaid Starter Pack via NRIC - POC"), ["idValue":idValue], STOP_ON_FAILURE)

38
00:00:57,077 --> 00:00:57,282
1. setText(findTestObject("01_Retailer Portal_Objects/RP13_Starter Pack Registration Page_Objects/input_idForSimNum"), simSerial)

