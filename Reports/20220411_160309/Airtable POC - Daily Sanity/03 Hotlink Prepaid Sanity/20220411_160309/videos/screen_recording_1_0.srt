1
00:00:01,338 --> 00:00:02,941
1. callTestCase(findTestCase("01_Retailer Portal/RP-TMPL-04 PREPAID STARTER PACK REGISTRATION - POC"), ["idValue":idValue, "customerName":customerName, "msisdn":msisdn, "row":row], STOP_ON_FAILURE)

2
00:00:02,959 --> 00:00:03,775
1. callTestCase(findTestCase("01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal"), ["username":"mcSup", "password":"123"], STOP_ON_FAILURE)

3
00:00:03,779 --> 00:00:14,185
1. openBrowser("")

4
00:00:14,190 --> 00:00:38,442
7. URL is set as a Global Variable controlled in Profiles

5
00:00:38,447 --> 00:00:40,401
13. Username is set as local variable

6
00:00:40,407 --> 00:00:42,238
19. Password is set as local variable

7
00:00:42,242 --> 00:00:43,258
23. click(findTestObject("01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login"))

8
00:00:43,265 --> 00:00:43,299
27. comment("ASSERTION")

9
00:00:43,304 --> 00:00:45,122
33. Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded

10
00:00:45,127 --> 00:00:45,910
5. callTestCase(findTestCase("01_Retailer Portal/RP07_Create Customer_Page/RP07_1_6 Insert Data into AMR"), ["idValue":idValue, "customerName":customerName], STOP_ON_FAILURE)

11
00:00:45,915 --> 00:00:46,704
1. com.database.connectSql.connectDB("10.200.50.177", "AMRDB", "1433", "amr_user", "amr_user2015")

12
00:00:46,708 --> 00:00:46,855
5. com.database.connectSql.insertCustomerDetailAMR(idValue, customerName)

13
00:00:46,862 --> 00:00:46,867
9. com.database.connectSql.closeDatabaseConnection()

14
00:00:46,873 --> 00:00:47,749
9. callTestCase(findTestCase("01_Retailer Portal/RP03_CIC Dashboard_Page/RP03_4 Starter Pack Registration"), [:], STOP_ON_FAILURE)

15
00:00:47,753 --> 00:00:49,258
1. click(findTestObject("01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/div_Starter Pack Registration"))

16
00:00:49,265 --> 00:00:50,069
13. callTestCase(findTestCase("98 Webservices/WS01_Airtable REST APIs/Get Inventory and marked as used"), ["inventoryType":"inventoryHotlinkStarterPack", "recordId":""], STOP_ON_FAILURE)

17
00:00:50,074 --> 00:00:50,423
1. callTestCase(findTestCase("98 Webservices/WS01_Airtable REST APIs/WS01_01 GET Available Inventory"), ["inventoryType":inventoryType], STOP_ON_FAILURE)

18
00:00:50,427 --> 00:00:53,346
1. response = sendRequest(findTestObject("98 Webservices Requests/WS1_Airtable_REST/GET Available Inventory", ["inventoryType":inventoryType]))

19
00:00:53,352 --> 00:00:53,379
5. slurper = new groovy.json.JsonSlurper()

20
00:00:53,386 --> 00:00:53,441
9. parsedJson = slurper.parseText(response.getResponseText())

21
00:00:53,446 --> 00:00:53,467
13. availableSim = simSerial

22
00:00:53,472 --> 00:00:53,474
17. simSerial = replace("[", "").replace("]", "")

23
00:00:53,479 --> 00:00:53,486
21. println(simSerial)

24
00:00:53,490 --> 00:00:53,492
25. simSerial = simSerial

25
00:00:53,496 --> 00:00:53,499
29. inventoryId = id

26
00:00:53,505 --> 00:00:53,507
33. recordId = replace("[", "").replace("]", "")

27
00:00:53,514 --> 00:00:53,516
37. println(recordId)

28
00:00:53,523 --> 00:00:53,525
41. id = recordId

29
00:00:53,532 --> 00:00:53,534
45. starterPackMsisdn = msisdn

30
00:00:53,541 --> 00:00:53,544
49. msisdn = replace("[", "").replace("]", "")

31
00:00:53,551 --> 00:00:53,553
53. println(msisdn)

32
00:00:53,559 --> 00:00:53,565
57. msisdn = msisdn

33
00:00:53,572 --> 00:00:54,573
5. callTestCase(findTestCase("98 Webservices/WS01_Airtable REST APIs/WS01_02 PATCH Used Inventory"), ["recordId":recordId], STOP_ON_FAILURE)

34
00:00:54,581 --> 00:00:54,583
1. recordId = id

35
00:00:54,591 --> 00:00:56,037
5. response = sendRequest(findTestObject("98 Webservices Requests/WS1_Airtable_REST/PATCH Used Inventory", ["recordId":recordId]))

36
00:00:56,041 --> 00:00:56,144
11. Verify if the response from "REST_Status Codes/POST_201" object returns the 201 status code

