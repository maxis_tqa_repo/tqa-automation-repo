import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

OrderID = GlobalVariable.orderID

externalOrderID = OrderID.replaceAll('[^0-9]', '')

WebUI.navigateToUrl('http://kpodoapn04.dox.isddc.men.maxis.com.my:8050/AmdocsOSS/Portal/login.html')

WebUI.setText(findTestObject('03_ODO_Objects/input_User Name_view40'), 'FOSAgent')

WebUI.setEncryptedText(findTestObject('03_ODO_Objects/input_Password_view42'), 't/0NKqcCbdzQ5RqqUDDoug==')

WebUI.click(findTestObject('03_ODO_Objects/span_Login'))

WebUI.click(findTestObject('03_ODO_Objects/div_Tasks'))

WebUI.doubleClick(findTestObject('03_ODO_Objects/input'))

WebUI.setText(findTestObject('03_ODO_Objects/input'), externalOrderID)

WebUI.click(findTestObject('03_ODO_Objects/div_FOS Submitted'))

WebUI.setText(findTestObject('Object Repository/input_Provider Order ID_providerOrderID'), '7552525')

WebUI.setText(findTestObject('Object Repository/input_Provider Service ID_providerServiceID'), '7552525')

WebUI.setText(findTestObject('Object Repository/input_Provider Service ID_providerServiceID_1'), '7552525')

WebUI.click(findTestObject('Object Repository/button_Complete'))

WebUI.click(findTestObject('03_ODO_Objects/span_Logout'))

WebUI.closeBrowser()

