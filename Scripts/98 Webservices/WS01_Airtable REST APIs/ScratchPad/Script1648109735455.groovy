import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import groovy.json.JsonSlurper as JsonSlurper


response = WS.sendRequest(findTestObject('98 Webservices Requests/WS1_Airtable_REST/GET Available Inventory', [('inventoryType') : inventoryType]))

JsonSlurper slurper = new JsonSlurper()

Map parsedJson = slurper.parseText(response.getResponseText())

String simSerial = parsedJson.records.fields.simSerial

println(simSerial)

String recordId = parsedJson.records.id

println(recordId)

String lala = 'lala'

println(lala)

