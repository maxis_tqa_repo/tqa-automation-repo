import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/button_Manual Entry'), FailureHandling.OPTIONAL)

WebUI.setText(findTestObject('01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/input_Non-Malaysian_nameAsPerId'), 
    customerName, FailureHandling.OPTIONAL)

// WebUI.click(findTestObject('01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/button_Not Available'))

// WebUI.click(findTestObject('01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/a_Others'))

WebUI.click(findTestObject('01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/button_month'), FailureHandling.OPTIONAL)

WebUI.click(findTestObject('01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/a_Aug'), FailureHandling.OPTIONAL)

WebUI.setText(findTestObject('01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/input_Dec_day'), '31', FailureHandling.OPTIONAL)

WebUI.setText(findTestObject('01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/input_Dec_year'), '1957', FailureHandling.OPTIONAL)

WebUI.click(findTestObject('01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/label_Male'), FailureHandling.OPTIONAL)

WebUI.click(findTestObject('01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/button_Validate Passport Details'), 
    FailureHandling.OPTIONAL)

WebUI.setText(findTestObject('01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/input_Address_contactAddreslineOne'), 
    'addressLine1')

WebUI.setText(findTestObject('01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/input_Address_contactAddreslineTwo'), 
    'addressLine2')

WebUI.setText(findTestObject('01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/input_Address_contactAddreslineThree'), 
    'addressLine3')

WebUI.setText(findTestObject('01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/input_Address_contactPostalCode'), 
    '43200')

WebUI.click(findTestObject('01_Retailer Portal_Objects/RP07_Create Customer_Page_Objects/div_(function(loadWidget)        var domPla_a0cbbf_1'))

