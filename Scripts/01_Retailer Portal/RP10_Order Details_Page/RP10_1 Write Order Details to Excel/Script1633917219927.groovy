import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import java.io.FileInputStream as FileInputStream
import java.io.FileNotFoundException as FileNotFoundException
import java.io.IOException as IOException
import java.nio.file.Files as Files
import java.io.File as File
import java.util.Date as Date
import org.apache.poi.xssf.usermodel.XSSFCell as XSSFCell
import org.apache.poi.xssf.usermodel.XSSFRow as XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet as XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook as XSSFWorkbook
import java.lang.String as String
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint

accountNo = WebUI.getText(findTestObject('01_Retailer Portal_Objects/RP10_Order Details_Page_Objects/AccountNo'))

serviceMsisdn = WebUI.getText(findTestObject('01_Retailer Portal_Objects/RP10_Order Details_Page_Objects/Order Details - MSISDN'))

submissionTime = WebUI.getText(findTestObject('01_Retailer Portal_Objects/RP10_Order Details_Page_Objects/p_06 Sep 2021 125522 PM'))

orderId = WebUI.getText(findTestObject('01_Retailer Portal_Objects/RP10_Order Details_Page_Objects/p_orderId'))

customerName = WebUI.getText(findTestObject('01_Retailer Portal_Objects/RP10_Order Details_Page_Objects/p_customerName'))

servicePlan = WebUI.getText(findTestObject('01_Retailer Portal_Objects/RP10_Order Details_Page_Objects/p_MaxisServicePlan'), 
    FailureHandling.OPTIONAL)

testName = RunConfiguration.getExecutionSource().toString().substring(RunConfiguration.getExecutionSource().toString().lastIndexOf(
        '\\') + 1)

filePath = 'Data Files\\Test Data and Input Files\\Sample Test Data\\SampleTestData.xlsx'

runId = RunConfiguration.getReportFolder().toString().substring(RunConfiguration.getReportFolder().toString().lastIndexOf(
        '\\') + 1)

//resultFilename = 'Data Files\\Output\\' + timestamp + ' ' + testName + '.xlsx'
resultFilename = (((('Data Files\\Output\\' + runId) + ' ') + testName) + '.xlsx')

// Open a log file
File myLog = new File(resultFilename)

// See if the file exists
if (myLog.exists() == false) {
    println('Creating file')

    File originalWb = new File(filePath)

    File clonedWb = new File(resultFilename)

    Files.copy(originalWb.toPath(), clonedWb.toPath())
}

///
FileInputStream file = new FileInputStream(new File(resultFilename))

int i = ((row) as int)

XSSFWorkbook workbook = new XSSFWorkbook(file)

XSSFSheet sheet = workbook.getSheetAt(0)

//trim white space
String example = serviceMsisdn

// System.out.println("Without space string: "+ example.replaceAll(" ",""));
String msisdn = example.replaceAll(' ', '')

'Write data to excel'
sheet.getRow(i).createCell(3).setCellValue(orderId)
sheet.getRow(i).createCell(6).setCellValue(customerName)
sheet.getRow(i).createCell(7).setCellValue(submissionTime)
sheet.getRow(i).createCell(9).setCellValue(accountNo)

isFibreStandAlone = WebUI.verifyMatch(findTestData('Data Dictionary/DD_PLANS').getValue(planId, 3), 'Fibre', false, FailureHandling.OPTIONAL)

if (isFibreStandAlone == false) {
    sheet.getRow(i).createCell(5).setCellValue(msisdn)
}

//sheet.getRow(i).createCell(6).setCellValue(servicePlan)
isFibreStandAlone = WebUI.verifyMatch(findTestData('Data Dictionary/DD_PLANS').getValue(planId, 3), 'Fibre', false, FailureHandling.OPTIONAL)

if (isFibreStandAlone == true) {
    modemId = WebUI.getText(findTestObject('01_Retailer Portal_Objects/RP10_Order Details_Page_Objects/p_fibreStandAloneModemIP'), 
        FailureHandling.OPTIONAL)

    voipNo = WebUI.getText(findTestObject('01_Retailer Portal_Objects/RP10_Order Details_Page_Objects/p_fibreStandAloneVoIp'), 
        FailureHandling.OPTIONAL)

    sheet.getRow(i).createCell(15).setCellValue(modemId)

    sheet.getRow(i).createCell(16).setCellValue(voipNo)
}

if (broadbandPlan.isEmpty() == false) {
    isFibre = WebUI.verifyMatch(findTestData('Data Dictionary/DD_PLANS').getValue(broadbandPlan, 3), 'Fibre', false, FailureHandling.OPTIONAL)

    if (isFibre == true) {
        modemId = WebUI.getText(findTestObject('01_Retailer Portal_Objects/RP10_Order Details_Page_Objects/p_broadbandModemId'), 
            FailureHandling.OPTIONAL)

        voipNo = WebUI.getText(findTestObject('01_Retailer Portal_Objects/RP10_Order Details_Page_Objects/p_voipNo'), FailureHandling.OPTIONAL)

        sheet.getRow(i).createCell(15).setCellValue(modemId)

        sheet.getRow(i).createCell(16).setCellValue(voipNo)
    }
    
    if (isFibre == false) {
        serviceMsisdn2 = WebUI.getText(findTestObject('01_Retailer Portal_Objects/RP10_Order Details_Page_Objects/p_broadbandFWBB'), 
            FailureHandling.OPTIONAL)

        String example2 = serviceMsisdn2

        String msisdn2 = example2.replaceAll(' ', '')

        sheet.getRow(i).createCell(11).setCellValue(msisdn2)
    }
}

file.close()

FileOutputStream outFile = new FileOutputStream(new File(resultFilename))

workbook.write(outFile)

outFile.close()

WebUI.delay(5)

