import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

isFibreStandAlone = WebUI.verifyMatch(findTestData('Data Dictionary/DD_PLANS').getValue(planId, 3), 'Fibre', false, FailureHandling.OPTIONAL)

if (isFibreStandAlone == true) {
    WebUI.callTestCase(findTestCase('01_Retailer Portal/RP06A_Link_to_Broadband/RP06A_2 Add Broadband - Fibre'), [('broadbandPlan') : planId], 
        FailureHandling.STOP_ON_FAILURE)

    WebUI.callTestCase(findTestCase('01_Retailer Portal/RP06A_Link_to_Broadband/RP06A_3 Assign VoIP number'), [:], FailureHandling.STOP_ON_FAILURE)

    WebUI.click(findTestObject('01_Retailer Portal_Objects/RP06_Cart_Page_Objects/button_Check Out'))
}

if (isFibreStandAlone == false) {
    'If msisdn in the input files is empty, then the order will just use the assigned msisdn'
    if (msisdn.isEmpty() == true) {
        not_run: WebUI.callTestCase(findTestCase('01_Retailer Portal/RP06_Cart_Page/RP06_5 Link Broadband and Check Out'), 
            [('broadbandPlan') : broadbandPlan], FailureHandling.STOP_ON_FAILURE)

        WebUI.callTestCase(findTestCase('01_Retailer Portal/RP06_Cart_Page/RP06_5 Link Broadband and Check Out - Copy'), 
            [('broadbandPlan') : broadbandPlan, ('planId') : planId], FailureHandling.STOP_ON_FAILURE)
    } else {
        WebUI.callTestCase(findTestCase('01_Retailer Portal/RP06_Cart_Page/RP06_2 Change Assigned MSISDN'), [('msisdn') : msisdn], 
            FailureHandling.STOP_ON_FAILURE)
    }
}

