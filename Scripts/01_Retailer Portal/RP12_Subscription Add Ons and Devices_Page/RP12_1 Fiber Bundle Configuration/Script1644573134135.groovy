import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('01_Retailer Portal_Objects/RP12_Subscription Add Ons and Devices Page_Objects/RP12_1 Fiber Bundle Configuration/button_Modify Services  Add-ons'))

WebUI.click(findTestObject('01_Retailer Portal_Objects/RP12_Subscription Add Ons and Devices Page_Objects/RP12_1 Fiber Bundle Configuration/span_Accessories  Additional Devices'))

WebUI.click(findTestObject('01_Retailer Portal_Objects/RP12_Subscription Add Ons and Devices Page_Objects/RP12_1 Fiber Bundle Configuration/button_View Details and Select This Device'))

WebUI.click(findTestObject('01_Retailer Portal_Objects/RP12_Subscription Add Ons and Devices Page_Objects/RP12_1 Fiber Bundle Configuration/i_keyboard_arrow_down_Contract Type'))

contractType = contractType1
contractPeriod = contractPeriod1

WebUI.click(findTestObject('01_Retailer Portal_Objects/RP12_Subscription Add Ons and Devices Page_Objects/RP12_1 Fiber Bundle Configuration/a_contractType', 
        [('contractType') : contractType]))

WebUI.click(findTestObject('01_Retailer Portal_Objects/RP12_Subscription Add Ons and Devices Page_Objects/RP12_1 Fiber Bundle Configuration/i_keyboard_arrow_down_Contract Period'))

WebUI.click(findTestObject('01_Retailer Portal_Objects/RP12_Subscription Add Ons and Devices Page_Objects/RP12_1 Fiber Bundle Configuration/a_contractPeriod', 
        [('contractPeriod') : contractPeriod]))

WebUI.click(findTestObject('01_Retailer Portal_Objects/RP12_Subscription Add Ons and Devices Page_Objects/RP12_1 Fiber Bundle Configuration/button_Add to Cart'))

WebUI.click(findTestObject('01_Retailer Portal_Objects/RP12_Subscription Add Ons and Devices Page_Objects/RP12_1 Fiber Bundle Configuration/button_Continue'))

