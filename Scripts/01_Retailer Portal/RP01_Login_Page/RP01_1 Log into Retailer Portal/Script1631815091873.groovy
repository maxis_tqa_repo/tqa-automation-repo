import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.RP_URL)

WebUI.maximizeWindow()

'Username is set as local variable'
WebUI.setText(findTestObject('01_Retailer Portal_Objects/RP01_Login_Page_Objects/input_Login_rt-login-username'), GlobalVariable.RP_Username, 
    FailureHandling.STOP_ON_FAILURE)

'Password is set as local variable'
WebUI.setText(findTestObject('01_Retailer Portal_Objects/RP01_Login_Page_Objects/input_Login_rt-login-passwd'), GlobalVariable.RP_Password)

WebUI.click(findTestObject('01_Retailer Portal_Objects/RP01_Login_Page_Objects/button_Login'))

'Assertion for RP1_1 - Check if login successful and Search Customer Page is fully loaded'
WebUI.verifyTextPresent(GlobalVariable.RP_Username, false, FailureHandling.STOP_ON_FAILURE)

WebUI.comment('ASSERTION : [ OK ] ')
WebUI.comment('		Successfully logged in as : ' + GlobalVariable.RP_Username)

