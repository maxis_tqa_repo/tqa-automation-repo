import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.testobject.SelectorMethod as SelectorMethod
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.comment('')

WebUI.comment('')

'Different Call To Action elements will be loaded depending on whether the customer is new or an existing one\r\n\r\n1. New Purchase button for New Customer,\r\n2. New Purchase button for Existing Customer Existing Account,\r\n3. New Purchase button for Existing Customer New Account'
if (GlobalVariable.newCustomer_Flag == true) {
    WebUI.click(findTestObject('01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/div_New Purchase'))
} else if (accountNo.isEmpty() == false) {
    WebUI.click(findTestObject('01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/button_Existing Account New Purchase'))
} else {
    WebUI.click(findTestObject('01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/button_New Purchase'))
}

WebUI.verifyTextPresent('Catalog', false)

WebUI.comment('ASSERTION : [ OK ] ')
WebUI.comment('	Product Catalogue Page loaded')
WebUI.comment('	Text Catalog present? ' + println(WebUI.verifyTextPresent('Catalog', false)))

