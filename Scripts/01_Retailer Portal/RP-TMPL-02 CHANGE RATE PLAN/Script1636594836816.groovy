import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('01_Retailer Portal/RP02_Search Customer_Page/RP02_1 Search Customer'), [('idType') : 'MSISDN'
        , ('idValue') : idValue, ('accountNo') : ''], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('01_Retailer Portal/RP03_CIC Dashboard_Page/RP03_2 Go To Balance Services'), [('idValue') : idValue], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('01_Retailer Portal/RP11_Balance Services_Page/RP11_Services Action - Change Rate Plan'), 
    [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('01_Retailer Portal/RP04_Catalog_Page/RP04_3 Change to new rate plan'), [('planId') : planId], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('01_Retailer Portal/RP05_Product Configuration_Page/RP05_1 Skip Product Configuration'), 
    [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('01_Retailer Portal/RP06_Cart_Page/RP06_1 Cart Page'), [('msisdn') : msisdn, ('broadbandPlan') : broadbandPlan
        , ('planId') : planId], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('01_Retailer Portal/RP08_Order Summary_Page/RP08_3 Document Upload and Submit Order - CRP'), 
    [('idType') : idType], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('01_Retailer Portal/RP09_Order Confirmation_Page/RP09_1 Verify order ID is given'), [:], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('01_Retailer Portal/RP10_Order Details_Page/RP10_1 Write Order Details to Excel'), [('row') : row
        , ('broadbandPlan') : broadbandPlan], FailureHandling.STOP_ON_FAILURE)

