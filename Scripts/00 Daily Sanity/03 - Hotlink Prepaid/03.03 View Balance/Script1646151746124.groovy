import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('01_Retailer Portal/RP02_Search Customer_Page/RP02_1 Search Customer'), [('idType') : 'MSISDN'
        , ('idValue') : msisdn, ('accountNo') : ''], FailureHandling.STOP_ON_FAILURE)

WebUI.enhancedClick(findTestObject('01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/i_expand_more_PrepaidAccount'))

if (WebUI.verifyElementClickable(findTestObject('01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/a_prepaid_keyboard_arrow_right', 
        [('msisdn') : msisdn])) == false) {
    WebUI.enhancedClick(findTestObject('01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/i_expand_more_PrepaidAccount'))
}

WebUI.enhancedClick(findTestObject('01_Retailer Portal_Objects/RP03_CIC Dashboard_Page_Objects/a_prepaid_keyboard_arrow_right', 
        [('msisdn') : '60121312593']), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('01_Retailer Portal_Objects/RP11_Balance Services_Page_Objects/Prepaid/span_Balances'))

WebUI.verifyTextPresent('MYR', false)

