import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.utility.VariableCollections as VariableCollections
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint

WebUI.callTestCase(findTestCase('01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal'), [:], FailureHandling.STOP_ON_FAILURE)

runId = RunConfiguration.getReportFolder().toString().substring(RunConfiguration.getReportFolder().toString().lastIndexOf(
        '\\') + 1)

WebUI.callTestCase(findTestCase('01_Retailer Portal/RP02_Search Customer_Page/RP02_1 Search Customer'), [('idType') : idType
        , ('idValue') : idValue, ('accountNo') : accountNo], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('01_Retailer Portal/RP03_CIC Dashboard_Page/RP03_1 New Purchase'), [('accountNo') : accountNo], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('01_Retailer Portal/RP04_Catalog_Page/RP04_1 Purchase plan only, no device'), [('planId') : planId], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('01_Retailer Portal/RP05_Product Configuration_Page/RP05_1 Skip Product Configuration'), 
    [('planId') : planId], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('01_Retailer Portal/RP06_Cart_Page/RP06_1 Cart Page'), [('msisdn') : msisdn, ('broadbandPlan') : broadbandPlan
        , ('planId') : planId], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('01_Retailer Portal/RP07_Create Customer_Page/RP07_1 Create Customer_Page'), [('idType') : idType
        , ('customerName') : customerName, ('accountNo') : accountNo, ('idValue') : idValue], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('01_Retailer Portal/RP08_Order Summary_Page/RP08_1 Document Upload and Submit Order'), [
        ('idType') : idType], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('01_Retailer Portal/RP09_Order Confirmation_Page/RP09_1 Verify order ID is given'), [:], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('01_Retailer Portal/RP10_Order Details_Page/RP10_2 Write Order Details to Excel - Modular'), 
    [('row') : row, ('broadbandPlan') : broadbandPlan, ('planId') : planId], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('03_ODO/ODO-TMPL-01 COMPLETE INSTALLATION FIBRE'), [('externalOrderID') : ''], FailureHandling.STOP_ON_FAILURE)

