import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys



isMalaysianCitizen = WebUI.verifyMatch(idType, 'MALAYSIAN ID CARD', false, FailureHandling.OPTIONAL)

//If account no is provided then the flow will skip the create customer page altogether
if (accountNo.isEmpty() == false) {
    WebUI.callTestCase(findTestCase('01_Retailer Portal/RP07_Create Customer_Page/RP07_1_3 Skip Create Customer Page'), 
        [:], FailureHandling.STOP_ON_FAILURE)
}

if (accountNo.isEmpty() == true) {
    WebUI.verifyTextPresent('Customer Details', false)

    if (GlobalVariable.newCustomer_Flag == true) {
        if (isMalaysianCitizen == false) {
            WebUI.callTestCase(findTestCase('01_Retailer Portal/RP07_Create Customer_Page/RP07_1_1 Fill Contact Address'), 
                [('customerName') : customerName], FailureHandling.STOP_ON_FAILURE)

            WebUI.callTestCase(findTestCase('01_Retailer Portal/RP07_Create Customer_Page/RP07_1_4 Fill Contact Details Section'), 
                [:], FailureHandling.STOP_ON_FAILURE)

            WebUI.callTestCase(findTestCase('01_Retailer Portal/RP07_Create Customer_Page/RP07_1_2 Use Contact Address as Billing Address'), 
                [:], FailureHandling.STOP_ON_FAILURE)
        }
        
        if (isMalaysianCitizen == true) {
            WebUI.callTestCase(findTestCase('01_Retailer Portal/RP07_Create Customer_Page/RP07_1_6 Insert Data into AMR'), 
                [('idValue') : idValue, ('customerName') : customerName], FailureHandling.STOP_ON_FAILURE)

            WebUI.callTestCase(findTestCase('01_Retailer Portal/RP07_Create Customer_Page/RP07_1_5 Validate Contact Address'), 
                [:], FailureHandling.STOP_ON_FAILURE)

            WebUI.callTestCase(findTestCase('01_Retailer Portal/RP07_Create Customer_Page/RP07_1_4 Fill Contact Details Section'), 
                [:], FailureHandling.STOP_ON_FAILURE)

            WebUI.callTestCase(findTestCase('01_Retailer Portal/RP07_Create Customer_Page/RP07_1_2 Use Contact Address as Billing Address'), 
                [:], FailureHandling.STOP_ON_FAILURE)
        }
    }
    
    if (GlobalVariable.newCustomer_Flag == false) {
        WebUI.callTestCase(findTestCase('01_Retailer Portal/RP07_Create Customer_Page/RP07_1_2 Use Contact Address as Billing Address'), 
            [:], FailureHandling.STOP_ON_FAILURE)
    }
}

