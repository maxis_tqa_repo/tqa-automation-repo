import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('01_Retailer Portal_Objects/RP06A_Link_to_Broadband/a_AddLink'))

isFibre = WebUI.verifyMatch(findTestData('Data Dictionary/DD_PLANS').getValue(broadbandPlan, 3), 'Fibre', false, FailureHandling.OPTIONAL)

if (isFibre == true) {
	
	WebUI.click(findTestObject('01_Retailer Portal_Objects/RP06A_Link_to_Broadband/button_Add new Fiber'))
	
    WebUI.callTestCase(findTestCase('01_Retailer Portal/RP06A_Link_to_Broadband/RP06A_2 Add Broadband - Fibre'), [('broadbandPlan') : broadbandPlan], 
        FailureHandling.STOP_ON_FAILURE)

    WebUI.callTestCase(findTestCase('01_Retailer Portal/RP06A_Link_to_Broadband/RP06A_3 Assign VoIP number'), [:], FailureHandling.STOP_ON_FAILURE)
}

if (isFibre == false) {
	
	WebUI.click(findTestObject('01_Retailer Portal_Objects/RP06A_Link_to_Broadband/RP06A_1 Add FWBB Component/button_Add new Home 4G WiFi'))
	
    WebUI.callTestCase(findTestCase('01_Retailer Portal/RP06A_Link_to_Broadband/RP06A_1 Add Broadband - FWBB'), [('broadbandPlan') : broadbandPlan], 
        FailureHandling.STOP_ON_FAILURE)
}



