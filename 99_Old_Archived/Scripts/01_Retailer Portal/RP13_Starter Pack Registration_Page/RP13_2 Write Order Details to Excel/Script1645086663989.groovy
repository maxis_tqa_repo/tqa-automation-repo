import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.nio.file.Files

import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

orderIDtext = WebUI.getText(findTestObject('01_Retailer Portal_Objects/RP13_Starter Pack Registration Page_Objects/p_Order Id 1797980A'))

String orderId = orderIDtext.split('\\s')[2] // 0 == first, 1 == second, etc

println(orderIDtext)
println(orderId)

testName = RunConfiguration.getExecutionSource().toString().substring(RunConfiguration.getExecutionSource().toString().lastIndexOf(
        '\\') + 1)


runId = RunConfiguration.getReportFolder().toString().substring(RunConfiguration.getReportFolder().toString().lastIndexOf(
        '\\') + 1)

resultFilename = (((('Data Files\\Output\\' + runId) + ' ') + testName) + '.xlsx')

// Open a log file
File myLog = new File(resultFilename)

// See if the file exists
if (myLog.exists() == false) {
    println('Creating file')

    File originalWb = new File(filePath)

    File clonedWb = new File(resultFilename)

    Files.copy(originalWb.toPath(), clonedWb.toPath())
}

///
FileInputStream file = new FileInputStream(new File(resultFilename))

int i = ((row) as int)

XSSFWorkbook workbook = new XSSFWorkbook(file)

XSSFSheet sheet = workbook.getSheetAt(0)

sheet.getRow(i).createCell(5).setCellValue(orderId)

file.close()

FileOutputStream outFile = new FileOutputStream(new File(resultFilename))

workbook.write(outFile)

outFile.close()

WebUI.delay(5)

