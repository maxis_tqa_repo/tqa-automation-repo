import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.nio.file.Files

import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable




accountNo = WebUI.getText(findTestObject('01_Retailer Portal_Objects/RP10_Order Details_Page_Objects/AccountNo'))

serviceMsisdn = WebUI.getText(findTestObject('01_Retailer Portal_Objects/RP10_Order Details_Page_Objects/Order Details - MSISDN'))

submissionTime = WebUI.getText(findTestObject('01_Retailer Portal_Objects/RP10_Order Details_Page_Objects/p_06 Sep 2021 125522 PM'))

orderId = WebUI.getText(findTestObject('01_Retailer Portal_Objects/RP10_Order Details_Page_Objects/p_orderId'))

customerName = WebUI.getText(findTestObject('01_Retailer Portal_Objects/RP10_Order Details_Page_Objects/p_customerName'))

servicePlan = WebUI.getText(findTestObject('01_Retailer Portal_Objects/RP10_Order Details_Page_Objects/p_MaxisServicePlan'), 
    FailureHandling.OPTIONAL)

testName = RunConfiguration.getExecutionSource().toString().substring(RunConfiguration.getExecutionSource().toString().lastIndexOf(
        '\\') + 1)

// filePath = 'Data Files\\Test Data and Input Files\\Sample Test Data\\SampleTestData.xlsx'


runId = RunConfiguration.getReportFolder().toString().substring(RunConfiguration.getReportFolder().toString().lastIndexOf(
        '\\') + 1)

//resultFilename = 'Data Files\\Output\\' + timestamp + ' ' + testName + '.xlsx'
//resultFilename = (((('Data Files\\Output\\' + runId) + ' ') + testName) + '.xlsx')

// Open a log file
//File myLog = new File(resultFilename)

/*
// See if the file exists
if (myLog.exists() == false) {
    println('Creating file')

    File originalWb = new File(filePath)

    File clonedWb = new File(resultFilename)

    Files.copy(originalWb.toPath(), clonedWb.toPath())
}



FileInputStream file = new FileInputStream(new File(resultFilename))

int i = ((row) as int)

XSSFWorkbook workbook = new XSSFWorkbook(file)

XSSFSheet sheet = workbook.getSheetAt(0)

*/


//trim white space
String example = serviceMsisdn

// System.out.println("Without space string: "+ example.replaceAll(" ",""));
String msisdn = example.replaceAll(' ', '')

GlobalVariable.msisdn = msisdn
GlobalVariable.orderID = orderId

// Write data to excel row(s)

/*
sheet.getRow(i).createCell(3).setCellValue(customerName)
sheet.getRow(i).createCell(4).setCellValue(accountNo)
sheet.getRow(i).createCell(5).setCellValue(orderId)
sheet.getRow(i).createCell(6).setCellValue(submissionTime)

*/

isFibreStandAlone = WebUI.verifyMatch(findTestData('Data Dictionary/DD_PLANS').getValue(planId, 3), 'Fibre', false, FailureHandling.OPTIONAL)

//Where principal = postpaid plan, msisdn will be the service number 

if (isFibreStandAlone == false) {
   // sheet.getRow(i).createCell(9).setCellValue(msisdn)
}

//Where principal = standalone fibre, modemID and voipNo will be written

isFibreStandAlone = WebUI.verifyMatch(findTestData('Data Dictionary/DD_PLANS').getValue(planId, 3), 'Fibre', false, FailureHandling.OPTIONAL)

if (isFibreStandAlone == true) {
    modemId = WebUI.getText(findTestObject('01_Retailer Portal_Objects/RP10_Order Details_Page_Objects/p_fibreStandAloneModemIP'), 
        FailureHandling.OPTIONAL)

    voipNo = WebUI.getText(findTestObject('01_Retailer Portal_Objects/RP10_Order Details_Page_Objects/p_fibreStandAloneVoIp'), 
        FailureHandling.OPTIONAL)

    //sheet.getRow(i).createCell(16).setCellValue(modemId)

    //sheet.getRow(i).createCell(17).setCellValue(voipNo)
}


if ((broadbandPlan.isEmpty() | isFibreStandAlone ) == false) 
	
	{
   
		isFibre = WebUI.verifyMatch(findTestData('Data Dictionary/DD_PLANS').getValue(broadbandPlan, 3), 'Fibre', false, FailureHandling.OPTIONAL)
		
		    if (isFibre == true) {
		        modemId = WebUI.getText(findTestObject('01_Retailer Portal_Objects/RP10_Order Details_Page_Objects/p_broadbandModemId'), 
		            FailureHandling.OPTIONAL)
		
		        voipNo = WebUI.getText(findTestObject('01_Retailer Portal_Objects/RP10_Order Details_Page_Objects/p_voipNo'), FailureHandling.OPTIONAL)
		
				//sheet.getRow(i).createCell(16).setCellValue(modemId)
				
				//sheet.getRow(i).createCell(17).setCellValue(voipNo)
		
		    }
		    
		    if (isFibre == false) {
		        rawserviceMsisdnFWBB = WebUI.getText(findTestObject('01_Retailer Portal_Objects/RP10_Order Details_Page_Objects/p_broadbandFWBB'), 
		            FailureHandling.OPTIONAL)
		
		        String serviceMsisdnFWBB = rawserviceMsisdnFWBB
		
		        String fwbbMsisdn = serviceMsisdnFWBB.replaceAll(' ', '')
				
				GlobalVariable.fwbbMsisdn = fwbbMsisdn
		
		        //sheet.getRow(i).createCell(13).setCellValue(fwbbMsisdn)
				
				
		    }
}

/*
file.close()

FileOutputStream outFile = new FileOutputStream(new File(resultFilename))

workbook.write(outFile)

outFile.close()

*/

WebUI.delay(5)

