import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/i_keyboard_arrow_down'))

'IF statement to check if Account No is populated in the input sheet. If TRUE idType will be forced to ACCOUNT and search done using the accountNo'
if (accountNo.isEmpty() == false) {
    idType = 'ACCOUNT'
    idValue = accountNo
}


WebUI.click(findTestObject('01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/Dropdown ID Type', [('idType') : idType]))

WebUI.setText(findTestObject('01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId'), idValue)

WebUI.sendKeys(findTestObject('01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/input_search_customerId'), Keys.chord(
        Keys.ENTER))

'Checking for text "Customer Profile does not exist", if text exists the Global Variable newCustomer_Flag is set to TRUE and flow handling will be for New Customer '
GlobalVariable.newCustomer_Flag = WebUI.verifyTextPresent('Customer Profile does not exist', false, FailureHandling.OPTIONAL)

WebUI.comment('WORKFLOW HANDLING : ' )
WebUI.comment('	ID TYPE : ' + idType)
WebUI.comment('	ID VALUE : ' + idValue)
('	NEW CUSTOMER : ' + GlobalVariable.newCustomer_Flag)

