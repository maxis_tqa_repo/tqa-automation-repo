import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('01_Retailer Portal_Objects/RP02_Search Customer_Page_Objects/div_Hotlink Top Up'))

WebUI.setText(findTestObject('01_Retailer Portal_Objects/RP14_Page_Quick Topup Page_Objects/input_msisdnToTopUp'), msisdn)

WebUI.click(findTestObject('01_Retailer Portal_Objects/RP14_Page_Quick Topup Page_Objects/div_60'))

WebUI.click(findTestObject('01_Retailer Portal_Objects/RP14_Page_Quick Topup Page_Objects/button_Continue'))

WebUI.click(findTestObject('01_Retailer Portal_Objects/RP14_Page_Quick Topup Page_Objects/button_Confirm'))

WebUI.verifyTextPresent('Top up successful. Balance has been updated.', false)

