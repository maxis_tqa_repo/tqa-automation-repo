import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('01_Retailer Portal/RP01_Login_Page/RP01_1 Log into Retailer Portal'), [('username') : 'mcSup'
        , ('password') : '123'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('01_Retailer Portal/RP07_Create Customer_Page/RP07_1_6 Insert Data into AMR'), [('idValue') : idValue
        , ('customerName') : customerName], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('01_Retailer Portal/RP03_CIC Dashboard_Page/RP03_4 Starter Pack Registration'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('01_Retailer Portal/RP13_Starter Pack Registration_Page/RP13_1 Register Prepaid Starter Pack via NRIC'), 
    [('simSerial') : GlobalVariable.simSerial, ('msisdn') : GlobalVariable.msisdn, ('idValue') : idValue], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('01_Retailer Portal/RP13_Starter Pack Registration_Page/RP13_2 Write Order Details to Excel'), 
    [('row') : row, ('filePath') : 'Data Files\\Test Data and Input Files\\Daily Sanity\\Daily Sanity Test Data.xlsx'], 
    FailureHandling.STOP_ON_FAILURE)

0 == 0

assert true

toString()

