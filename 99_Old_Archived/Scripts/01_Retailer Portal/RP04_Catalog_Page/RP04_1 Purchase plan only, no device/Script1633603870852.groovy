import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'Click mobile plan tab'
WebUI.click(findTestObject('01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.button.planBrand', [('planBrand') : findTestData(
                'Data Dictionary/DD_PLANS').getValue(planId, 4)]))

WebUI.click(findTestObject('01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.button.planProduct', [('planProduct') : findTestData(
                'Data Dictionary/DD_PLANS').getValue(planId, 3)]))

WebUI.click(findTestObject('01_Retailer Portal_Objects/RP04_Catalog Page_Objects/selector.label.planType', [('planType') : findTestData(
                'Data Dictionary/DD_PLANS').getValue(planId, 2)]), FailureHandling.OPTIONAL)

isFibreStandAlone = WebUI.verifyMatch(findTestData('Data Dictionary/DD_PLANS').getValue(planId, 3), 'Fibre', false, FailureHandling.OPTIONAL)

WebUI.comment('')

if (isFibreStandAlone == true) {
    WebUI.comment('\tStep skipped because order flow type is for stand alone Fibre')
}

if (isFibreStandAlone == false) {
    WebUI.click(findTestObject('01_Retailer Portal_Objects/RP04_Catalog Page_Objects/Button Select This Plan', [('planId') : planId]))

    'Verfiy that the Plan Name is the same as the one purchased in previous page'
    WebUI.verifyTextPresent(findTestData('Data Dictionary/DD_PLANS').getValue(planId, 1), false, FailureHandling.OPTIONAL)

    WebUI.delay(10)

    WebUI.click(findTestObject('01_Retailer Portal_Objects/RP04_Catalog Page_Objects/button_Continue without Device'))

    WebUI.comment('Note that this has been identified as a common failure point. - aabangf')

    'Note that this has been identified as a common failure point. - aabangf\r\n\r\nVerify that once Continue WIthout Device button is clicked the page has moved on to the next page by checking that the title of the new page is shown'
    WebUI.verifyElementPresent(findTestObject('01_Retailer Portal_Objects/RP05_Product Configuration_Page_Objects/Page Title for Product Configuration'), 
        10)
}

