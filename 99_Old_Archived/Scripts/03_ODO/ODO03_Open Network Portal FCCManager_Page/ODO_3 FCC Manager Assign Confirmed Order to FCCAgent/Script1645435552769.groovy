import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

OrderID = GlobalVariable.orderID

externalOrderID = OrderID.replaceAll('[^0-9]', '')

WebUI.navigateToUrl('http://kpodoapn04.dox.isddc.men.maxis.com.my:8050/AmdocsOSS/Portal/login.html')

WebUI.setText(findTestObject('03_ODO_Objects/3 FCCManager/Page_Amdocs Open Network Portal Login/input_User Name_view40'), 'FCCManager')

WebUI.setEncryptedText(findTestObject('03_ODO_Objects/3 FCCManager/Page_Amdocs Open Network Portal Login/input_Password_view42'), 
    'CyyFl2Q/ySnWV66D3YDP8g==')

WebUI.click(findTestObject('03_ODO_Objects/3 FCCManager/Page_Amdocs Open Network Portal Login/span_Login'))

WebUI.click(findTestObject('03_ODO_Objects/3 FCCManager/Page_Amdocs Open Network Portal/div_Projects'))

WebUI.click(findTestObject('03_ODO_Objects/3 FCCManager/Page_Amdocs Open Network Portal/div_External Order ID'))

WebUI.setText(findTestObject('03_ODO_Objects/3 FCCManager/Page_Amdocs Open Network Portal/input'), externalOrderID)

WebUI.click(findTestObject('03_ODO_Objects/3 FCCManager/Page_Amdocs Open Network Portal/div_New_CustomerOrderManager_Maxis Fibre - _cb4fea'))

WebUI.click(findTestObject('03_ODO_Objects/3 FCCManager/Page_Amdocs Open Network Portal/div_FCC Confirmed'))

WebUI.click(findTestObject('03_ODO_Objects/3 FCCManager/Page_Amdocs Open Network Portal/div_Send_taskActionsButtonIcon'))

WebUI.click(findTestObject('03_ODO_Objects/3 FCCManager/Page_Amdocs Open Network Portal/div_Assign'))

WebUI.selectOptionByValue(findTestObject('03_ODO_Objects/3 FCCManager/Page_Amdocs Open Network Portal/select_AFFSuperAFFSuper1AFFSuper_tempECOAge_4405ba'), 
    'FCCAgent', true)

WebUI.click(findTestObject('03_ODO_Objects/3 FCCManager/Page_Amdocs Open Network Portal/span_Assign'))

WebUI.click(findTestObject('03_ODO_Objects/3 FCCManager/Page_Amdocs Open Network Portal/span_Close'))

WebUI.click(findTestObject('03_ODO_Objects/4 FCCAgent/Page_Amdocs Open Network Portal/span_Logout'))

