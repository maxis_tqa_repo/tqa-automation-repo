import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('03_ODO/ODO03_Open Network Portal FCCManager_Page/ODO_3 FCC Manager Assign Confirmed Order to FCCAgent'), [('externalOrderID') : externalOrderID], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

WebUI.callTestCase(findTestCase('03_ODO/ODO04_Open Network Portal FCCAgent_Page/ODO_4 FCC Agent Change Order to Ready To Install'), [('externalOrderID') : externalOrderID], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

WebUI.callTestCase(findTestCase('03_ODO/ODO05_Open Network Portal FCCInstallationManager_Page/ODO_5 FCC InstallerManager Assigned Installation to Installer Coordinator'), [('externalOrderID') : externalOrderID], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

WebUI.callTestCase(findTestCase('03_ODO/ODO06_Open Network Portal InstallerCoordinator_Page/ODO_6 Installer Coordinator Completes Installation'), [('externalOrderId') : externalOrderID], 
    FailureHandling.STOP_ON_FAILURE)

