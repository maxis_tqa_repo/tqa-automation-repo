import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

ErrorShown = WebUI.verifyElementPresent(findTestObject('02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/div_Error Business Date invalid'), 
    0, FailureHandling.OPTIONAL)

if (ErrorShown == true) {
    WebUI.doubleClick(findTestObject('02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/button_OK'), FailureHandling.OPTIONAL)
}

WebUI.click(findTestObject('02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/a_Tab 2'), FailureHandling.OPTIONAL)

WebUI.click(findTestObject('02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/a_Operator Declaration'), FailureHandling.OPTIONAL)

expectedValue = WebUI.getText(findTestObject('02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/span_20306.88'), FailureHandling.OPTIONAL)

WebUI.setText(findTestObject('02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/input_Cash_DeclaredAmount'), expectedValue, 
    FailureHandling.OPTIONAL)

WebUI.click(findTestObject('02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/button_Confirm Operator Declaration'))

WebUI.enhancedClick(findTestObject('02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/button_Operator Report Close'))

WebUI.click(findTestObject('02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/a_Tab 1'))

