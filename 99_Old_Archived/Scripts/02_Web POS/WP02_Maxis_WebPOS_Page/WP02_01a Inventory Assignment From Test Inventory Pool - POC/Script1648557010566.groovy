import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint

WebUI.maximizeWindow()

WebUI.click(findTestObject('02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/a_Amdocs Inventory Dashboard'))

WebUI.click(findTestObject('02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/td_Ready for Inventory_QueueNo'))

'Clicking on MSISDN for inventory assignment'
WebUI.click(findTestObject('02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/Global Variable MSISDN (Assign Inventory)', 
        [('msisdn') : msisdn]))

WebUI.click(findTestObject('02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/button_Process'))

WebUI.callTestCase(findTestCase('98 Webservices/WS01_Airtable REST APIs/Get Inventory and marked as used'), [('inventoryType') : inventoryType
        , ('recordId') : ''], FailureHandling.STOP_ON_FAILURE)

simSerial = GlobalVariable.simSerial

println(simSerial)

WebUI.setText(findTestObject('02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/input_SIM_IMEINo'), simSerial)

if (fwbbMsisdn.isEmpty() == false) {
    WebUI.setText(findTestObject('02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/input_SIM_IMEINo_2'), simSerial2)
}

WebUI.click(findTestObject('02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/button_Submit'))

WebUI.click(findTestObject('02_WebPOS_Objects/WP02_Maxis_WebPOS_Page_Objects/button_inventory_close'))

