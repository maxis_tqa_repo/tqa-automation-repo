import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS

import groovy.json.JsonSlurper
import internal.GlobalVariable

response = WS.sendRequest(findTestObject('98 Webservices Requests/WS1_Airtable_REST/GET Available Inventory', [('inventoryType') : inventoryType]))

JsonSlurper slurper = new JsonSlurper()

Map parsedJson = slurper.parseText(response.getResponseText())

//Get simSerial from response JSON

String availableSim = parsedJson.records.fields.simSerial

String simSerial = availableSim.replace("[", '').replace("]", '')

println(simSerial)

GlobalVariable.simSerial = simSerial

//Get recordId from response JSON

String inventoryId = parsedJson.records.id

String recordId = inventoryId.replace("[", '').replace("]", '')

println(recordId)

GlobalVariable.id = recordId

//Get starterPackMsisdn from response JSON

String starterPackMsisdn = parsedJson.records.fields.msisdn

String msisdn = starterPackMsisdn.replace("[", '').replace("]", '')

println(msisdn)

GlobalVariable.msisdn = msisdn


