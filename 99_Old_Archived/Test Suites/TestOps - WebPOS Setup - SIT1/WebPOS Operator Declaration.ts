<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>WebPOS Operator Declaration</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient>aabangf@maxis.com.my;bukhrin@gmail.com;</mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>8baaadfc-5cf0-478a-a9b0-edc447ffbb23</testSuiteGuid>
   <testCaseLink>
      <guid>2d8f5dae-be05-4e17-a96d-45bf00b6afc8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/02_Web POS/Refactored WebPOS/WP02 New Business Day Operator Declaration</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>8a07982c-c150-4ac0-8042-ca51dc7e728f</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
