<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS00-Scratchpad</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>dbeea538-0718-46ac-83be-91c7eefe45ab</testSuiteGuid>
   <testCaseLink>
      <guid>2f0e60b5-9483-434d-98ea-337cc78c2757</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/01_Retailer Portal/RP-TMPL-05 HOTLINK PREPAID TOPUP</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>354e25c0-a82c-402c-99e9-6744e2cb3355</id>
         <iterationEntity>
            <iterationType>RANGE</iterationType>
            <value>3-3</value>
         </iterationEntity>
         <testDataId>Data Files/Test Data and Input Files/Daily Sanity/Daily Sanity Test Data</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>354e25c0-a82c-402c-99e9-6744e2cb3355</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>msisdn</value>
         <variableId>08dca0fe-dc30-45f7-91f2-b28abb678f27</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>e20c0b02-2fe8-4aba-a9de-1de461d6ef3a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/01_Retailer Portal/RP-TMPL-06 HOTLINK PASS PURCHASE</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>24ab3b89-2dc8-400a-b05b-3ffd38d082ef</id>
         <iterationEntity>
            <iterationType>RANGE</iterationType>
            <value>3-3</value>
         </iterationEntity>
         <testDataId>Data Files/Test Data and Input Files/Daily Sanity/Daily Sanity Test Data</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>24ab3b89-2dc8-400a-b05b-3ffd38d082ef</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>msisdn</value>
         <variableId>6eab117b-f695-4b3c-aca3-a6880451cfff</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
