<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Assign inventory and payment</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>b899883d-adc4-4ec2-819b-464ba7c6da22</testSuiteGuid>
   <testCaseLink>
      <guid>c6a80784-309c-49d2-a009-52be3868f08d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/02_Web POS/WP-TMPL-01 AMDOCS DASHBOARD ASSIGN INVENTORY AND PAYMENT</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>e49307a2-f5f9-4596-91df-ed374f0b70d4</id>
         <iterationEntity>
            <iterationType>RANGE</iterationType>
            <value>2-2</value>
         </iterationEntity>
         <testDataId>Data Files/Test Data and Input Files/Sample Test Data/TD-001 NEW PLAN REGISTRATION NO DEVICE</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>e49307a2-f5f9-4596-91df-ed374f0b70d4</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>msisdn</value>
         <variableId>a6955f39-2378-4e4a-9439-a514c2e50804</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>e49307a2-f5f9-4596-91df-ed374f0b70d4</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>simSerial</value>
         <variableId>6202b219-d343-4405-8ff2-f6f6d22d2766</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
