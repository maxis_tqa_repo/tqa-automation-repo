<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h3_Top up successful. Balance has been updated</name>
   <tag></tag>
   <elementGuidId>d984ed14-eb4d-4d0b-90ab-488d8a70bb7e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h3.sub-heading</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='scrollable-component']/div/div[3]/div/div/div/div/div[3]/section/div/div[2]/h3</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Top up successful. Balance has been updated.' or . = 'Top up successful. Balance has been updated.')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h3</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>sub-heading</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Top up successful. Balance has been updated.</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;scrollable-component&quot;)/div[@class=&quot;aem-Grid aem-Grid--12 aem-Grid--default--12&quot;]/div[@class=&quot;digitalpostopupretaill9module superTypeWidget parbase aem-GridColumn aem-GridColumn--default--12&quot;]/div[@class=&quot;UXFWidget&quot;]/div[@class=&quot;pos-top-up widget-background widget-height&quot;]/div[1]/div[@class=&quot;common-order-confirmation&quot;]/div[@class=&quot;container&quot;]/section[@class=&quot;order-confirmation border rounded&quot;]/div[1]/div[@class=&quot;heading-container&quot;]/h3[@class=&quot;sub-heading&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='scrollable-component']/div/div[3]/div/div/div/div/div[3]/section/div/div[2]/h3</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='done'])[1]/following::h3[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='IMPORTANT NOTICE!'])[1]/following::h3[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Capture object:'])[1]/preceding::h3[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alt'])[1]/preceding::h3[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Top up successful. Balance has been updated.']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h3</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h3[(text() = 'Top up successful. Balance has been updated.' or . = 'Top up successful. Balance has been updated.')]</value>
   </webElementXpaths>
</WebElementEntity>
