<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Page Title for Product Configuration</name>
   <tag></tag>
   <elementGuidId>f62028a2-f4c4-4ef3-a0de-e32833a80427</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//p[(text() = 'Configure' or . = 'Configure')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>p.heading-label.mb-0.commonHeaderLineHeight</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>ba915415-c878-49da-87b9-85b1ad3551ad</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>heading-label mb-0 commonHeaderLineHeight</value>
      <webElementGuid>40c9cb43-8af1-4747-a58e-57ca555d1fcd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Configure</value>
      <webElementGuid>79bda01f-eb8f-4fcd-9382-b36091bd920b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;scrollable-component&quot;)/div[@class=&quot;aem-Grid aem-Grid--12 aem-Grid--default--12&quot;]/div[@class=&quot;digitaladdonsconfigurationretaill9module superTypeWidget parbase aem-GridColumn aem-GridColumn--default--12&quot;]/div[@class=&quot;UXFWidget&quot;]/div[@class=&quot;container widget-background addons-configuration&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;common-header col-12&quot;]/div[@class=&quot;header-without-back-btn text-center&quot;]/div[@class=&quot;react-label&quot;]/p[@class=&quot;heading-label mb-0 commonHeaderLineHeight&quot;]</value>
      <webElementGuid>f1d2582d-8390-4f21-9ed6-344eae9aeab2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='scrollable-component']/div/div[3]/div/div/div/div/div/div/p</value>
      <webElementGuid>eef9cab6-3197-43e2-9c31-69dcc4487508</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[3]/following::p[1]</value>
      <webElementGuid>4514d16c-9ce4-462a-80e9-74f221a529ff</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Confirm'])[3]/following::p[1]</value>
      <webElementGuid>f67c450e-0249-4714-b07f-d1da0161106e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add-Ons &amp; Settings'])[1]/preceding::p[3]</value>
      <webElementGuid>772360d2-9bbc-4f92-b048-0d091c4d03bf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Accessories &amp; Additional Devices'])[1]/preceding::p[3]</value>
      <webElementGuid>cbf076ae-b9fe-484c-b8ec-88fdfdd3b920</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Configure']/parent::*</value>
      <webElementGuid>02e76a88-4e27-462b-875e-b090f422448d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div/div/div/div/p</value>
      <webElementGuid>1c91211a-ef90-4e10-8962-2cfe3000ddbd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'Configure' or . = 'Configure')]</value>
      <webElementGuid>438748a9-86a1-4d89-ac02-14d707794152</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
