<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_Use Contact Address as Billing Address</name>
   <tag></tag>
   <elementGuidId>2394b031-df71-4458-be6a-1bf6946d86cf</elementGuidId>
   <imagePath></imagePath>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='scrollable-component']/div/div[2]/div/div/div/div[2]/div[2]/div[5]/div/div/label</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>IMAGE</key>
         <value></value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;scrollable-component&quot;)/div[@class=&quot;aem-Grid aem-Grid--12 aem-Grid--default--12&quot;]/div[@class=&quot;digitalcreatecustomerretaill9module superTypeWidget parbase aem-GridColumn aem-GridColumn--default--12&quot;]/div[@class=&quot;UXFWidget&quot;]/div[@class=&quot;create-customer-retail-l9-module&quot;]/div[@class=&quot;createCustomerRetail widget-background&quot;]/div[@class=&quot;container&quot;]/div[2]/div[@class=&quot;pb-0&quot;]/div[@class=&quot;mt-3&quot;]/div[@class=&quot;CustomCheckbox&quot;]/label[@class=&quot;text-black filter-type-label mt-3&quot;][count(. | //label[@class = 'text-black filter-type-label mt-3  ' and @for = 'billingCheckBox' and (text() = 'Use Contact Address as Billing Address' or . = 'Use Contact Address as Billing Address')]) = count(//label[@class = 'text-black filter-type-label mt-3  ' and @for = 'billingCheckBox' and (text() = 'Use Contact Address as Billing Address' or . = 'Use Contact Address as Billing Address')])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>label</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>text-black filter-type-label mt-3  </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>for</name>
      <type>Main</type>
      <value>billingCheckBox</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Use Contact Address as Billing Address</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;scrollable-component&quot;)/div[@class=&quot;aem-Grid aem-Grid--12 aem-Grid--default--12&quot;]/div[@class=&quot;digitalcreatecustomerretaill9module superTypeWidget parbase aem-GridColumn aem-GridColumn--default--12&quot;]/div[@class=&quot;UXFWidget&quot;]/div[@class=&quot;create-customer-retail-l9-module&quot;]/div[@class=&quot;createCustomerRetail widget-background&quot;]/div[@class=&quot;container&quot;]/div[2]/div[@class=&quot;pb-0&quot;]/div[@class=&quot;mt-3&quot;]/div[@class=&quot;CustomCheckbox&quot;]/label[@class=&quot;text-black filter-type-label mt-3&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='scrollable-component']/div/div[2]/div/div/div/div[2]/div[2]/div[5]/div/div/label</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Billing Details'])[1]/following::label[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SME Individual'])[1]/following::label[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='keyboard_arrow_down'])[8]/preceding::label[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Please Select'])[2]/preceding::label[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Use Contact Address as Billing Address']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/div/label</value>
   </webElementXpaths>
</WebElementEntity>
