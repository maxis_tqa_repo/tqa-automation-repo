<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Account accountNo</name>
   <tag></tag>
   <elementGuidId>44686146-1052-44c7-8196-fbe20310a5ad</elementGuidId>
   <imagePath></imagePath>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class = 'accountTitle' and (text() = 'Account ${GlobalVariable.accountNo}' or . = 'Account ${GlobalVariable.accountNo}')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>id(&quot;accordion_${GlobalVariable.accordionID}&quot;)/div[@class=&quot;accordion-section mb-2&quot;]/div[@class=&quot;header-section&quot;]/div[@class=&quot;title&quot;]/div[@class=&quot;accordion-title accordionHeaderTitle&quot;]/div[@class=&quot;accountTitle&quot;]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.accountTitle</value>
      </entry>
      <entry>
         <key>IMAGE</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>accountTitle</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Account ${GlobalVariable.accountNo}</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='OK'])[1]/following::div[12]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Change Ownership'])[1]/following::div[21]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='expand_less'])[1]/preceding::div[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Manage'])[1]/preceding::div[29]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Account 78367943']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div/div/div/div/div/div/div/div/div/div/div</value>
   </webElementXpaths>
</WebElementEntity>
