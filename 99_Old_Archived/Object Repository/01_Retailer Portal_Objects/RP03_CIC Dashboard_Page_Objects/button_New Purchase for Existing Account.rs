<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_New Purchase for Existing Account</name>
   <tag></tag>
   <elementGuidId>9e7217c5-f2af-4e35-a8e1-8a49c8db58c0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='${GlobalVariable.accordionId}']/div/div/div[2]/div/div/div/div/button</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;${GlobalVariable.accordionId}&quot;)/div[@class=&quot;body-section&quot;]/div[@class=&quot;col-12 px-0&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;customer-navigation container mt-2&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 my-2  preAndPostNavigation&quot;]/div[@class=&quot;react-button&quot;]/button[@id=&quot;new-purchase&quot;]</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>outline-primary large w-100 btn</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>new-purchase</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>New Purchase </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;${GlobalVariable.accordionId}&quot;)/div[@class=&quot;body-section&quot;]/div[@class=&quot;col-12 px-0&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;customer-navigation container mt-2&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-4 col-md-2 col-lg-2 my-2  preAndPostNavigation&quot;]/div[@class=&quot;react-button&quot;]/button[@id=&quot;new-purchase&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@id='new-purchase'])[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='${GlobalVariable.accordionId}']/div/div/div[2]/div/div/div/div/button</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='keyboard_arrow_right'])[3]/following::button[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Since 14 Sep 2021'])[2]/following::button[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Switch To Maxis'])[2]/preceding::button[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Case'])[1]/preceding::button[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div[2]/div/div/div/div/button</value>
   </webElementXpaths>
</WebElementEntity>
