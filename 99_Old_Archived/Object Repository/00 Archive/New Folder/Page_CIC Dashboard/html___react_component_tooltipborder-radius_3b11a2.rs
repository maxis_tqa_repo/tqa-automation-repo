<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>html___react_component_tooltipborder-radius_3b11a2</name>
   <tag></tag>
   <elementGuidId>c826f3d7-b881-419f-bfdd-045509b54978</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>html</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*/text()[normalize-space(.)='']/parent::*</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>html</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>lang</name>
      <type>Main</type>
      <value>en</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>.__react_component_tooltip{border-radius:3px;display:inline-block;font-size:13px;left:-999em;opacity:0;padding:8px 21px;position:fixed;pointer-events:none;transition:opacity 0.3s ease-out;top:-999em;visibility:hidden;z-index:999}.__react_component_tooltip.allow_hover,.__react_component_tooltip.allow_click{pointer-events:auto}.__react_component_tooltip:before,.__react_component_tooltip:after{content:&quot;&quot;;width:0;height:0;position:absolute}.__react_component_tooltip.show{opacity:0.9;margin-top:0px;margin-left:0px;visibility:visible}.__react_component_tooltip.type-dark{color:#fff;background-color:#222}.__react_component_tooltip.type-dark.place-top:after{border-top-color:#222;border-top-style:solid;border-top-width:6px}.__react_component_tooltip.type-dark.place-bottom:after{border-bottom-color:#222;border-bottom-style:solid;border-bottom-width:6px}.__react_component_tooltip.type-dark.place-left:after{border-left-color:#222;border-left-style:solid;border-left-width:6px}.__react_component_tooltip.type-dark.place-right:after{border-right-color:#222;border-right-style:solid;border-right-width:6px}.__react_component_tooltip.type-dark.border{border:1px solid #fff}.__react_component_tooltip.type-dark.border.place-top:before{border-top:8px solid #fff}.__react_component_tooltip.type-dark.border.place-bottom:before{border-bottom:8px solid #fff}.__react_component_tooltip.type-dark.border.place-left:before{border-left:8px solid #fff}.__react_component_tooltip.type-dark.border.place-right:before{border-right:8px solid #fff}.__react_component_tooltip.type-success{color:#fff;background-color:#8DC572}.__react_component_tooltip.type-success.place-top:after{border-top-color:#8DC572;border-top-style:solid;border-top-width:6px}.__react_component_tooltip.type-success.place-bottom:after{border-bottom-color:#8DC572;border-bottom-style:solid;border-bottom-width:6px}.__react_component_tooltip.type-success.place-left:after{border-left-color:#8DC572;border-left-style:solid;border-left-width:6px}.__react_component_tooltip.type-success.place-right:after{border-right-color:#8DC572;border-right-style:solid;border-right-width:6px}.__react_component_tooltip.type-success.border{border:1px solid #fff}.__react_component_tooltip.type-success.border.place-top:before{border-top:8px solid #fff}.__react_component_tooltip.type-success.border.place-bottom:before{border-bottom:8px solid #fff}.__react_component_tooltip.type-success.border.place-left:before{border-left:8px solid #fff}.__react_component_tooltip.type-success.border.place-right:before{border-right:8px solid #fff}.__react_component_tooltip.type-warning{color:#fff;background-color:#F0AD4E}.__react_component_tooltip.type-warning.place-top:after{border-top-color:#F0AD4E;border-top-style:solid;border-top-width:6px}.__react_component_tooltip.type-warning.place-bottom:after{border-bottom-color:#F0AD4E;border-bottom-style:solid;border-bottom-width:6px}.__react_component_tooltip.type-warning.place-left:after{border-left-color:#F0AD4E;border-left-style:solid;border-left-width:6px}.__react_component_tooltip.type-warning.place-right:after{border-right-color:#F0AD4E;border-right-style:solid;border-right-width:6px}.__react_component_tooltip.type-warning.border{border:1px solid #fff}.__react_component_tooltip.type-warning.border.place-top:before{border-top:8px solid #fff}.__react_component_tooltip.type-warning.border.place-bottom:before{border-bottom:8px solid #fff}.__react_component_tooltip.type-warning.border.place-left:before{border-left:8px solid #fff}.__react_component_tooltip.type-warning.border.place-right:before{border-right:8px solid #fff}.__react_component_tooltip.type-error{color:#fff;background-color:#BE6464}.__react_component_tooltip.type-error.place-top:after{border-top-color:#BE6464;border-top-style:solid;border-top-width:6px}.__react_component_tooltip.type-error.place-bottom:after{border-bottom-color:#BE6464;border-bottom-style:solid;border-bottom-width:6px}.__react_component_tooltip.type-error.place-left:after{border-left-color:#BE6464;border-left-style:solid;border-left-width:6px}.__react_component_tooltip.type-error.place-right:after{border-right-color:#BE6464;border-right-style:solid;border-right-width:6px}.__react_component_tooltip.type-error.border{border:1px solid #fff}.__react_component_tooltip.type-error.border.place-top:before{border-top:8px solid #fff}.__react_component_tooltip.type-error.border.place-bottom:before{border-bottom:8px solid #fff}.__react_component_tooltip.type-error.border.place-left:before{border-left:8px solid #fff}.__react_component_tooltip.type-error.border.place-right:before{border-right:8px solid #fff}.__react_component_tooltip.type-info{color:#fff;background-color:#337AB7}.__react_component_tooltip.type-info.place-top:after{border-top-color:#337AB7;border-top-style:solid;border-top-width:6px}.__react_component_tooltip.type-info.place-bottom:after{border-bottom-color:#337AB7;border-bottom-style:solid;border-bottom-width:6px}.__react_component_tooltip.type-info.place-left:after{border-left-color:#337AB7;border-left-style:solid;border-left-width:6px}.__react_component_tooltip.type-info.place-right:after{border-right-color:#337AB7;border-right-style:solid;border-right-width:6px}.__react_component_tooltip.type-info.border{border:1px solid #fff}.__react_component_tooltip.type-info.border.place-top:before{border-top:8px solid #fff}.__react_component_tooltip.type-info.border.place-bottom:before{border-bottom:8px solid #fff}.__react_component_tooltip.type-info.border.place-left:before{border-left:8px solid #fff}.__react_component_tooltip.type-info.border.place-right:before{border-right:8px solid #fff}.__react_component_tooltip.type-light{color:#222;background-color:#fff}.__react_component_tooltip.type-light.place-top:after{border-top-color:#fff;border-top-style:solid;border-top-width:6px}.__react_component_tooltip.type-light.place-bottom:after{border-bottom-color:#fff;border-bottom-style:solid;border-bottom-width:6px}.__react_component_tooltip.type-light.place-left:after{border-left-color:#fff;border-left-style:solid;border-left-width:6px}.__react_component_tooltip.type-light.place-right:after{border-right-color:#fff;border-right-style:solid;border-right-width:6px}.__react_component_tooltip.type-light.border{border:1px solid #222}.__react_component_tooltip.type-light.border.place-top:before{border-top:8px solid #222}.__react_component_tooltip.type-light.border.place-bottom:before{border-bottom:8px solid #222}.__react_component_tooltip.type-light.border.place-left:before{border-left:8px solid #222}.__react_component_tooltip.type-light.border.place-right:before{border-right:8px solid #222}.__react_component_tooltip.place-top{margin-top:-10px}.__react_component_tooltip.place-top:before{border-left:10px solid transparent;border-right:10px solid transparent;bottom:-8px;left:50%;margin-left:-10px}.__react_component_tooltip.place-top:after{border-left:8px solid transparent;border-right:8px solid transparent;bottom:-6px;left:50%;margin-left:-8px}.__react_component_tooltip.place-bottom{margin-top:10px}.__react_component_tooltip.place-bottom:before{border-left:10px solid transparent;border-right:10px solid transparent;top:-8px;left:50%;margin-left:-10px}.__react_component_tooltip.place-bottom:after{border-left:8px solid transparent;border-right:8px solid transparent;top:-6px;left:50%;margin-left:-8px}.__react_component_tooltip.place-left{margin-left:-10px}.__react_component_tooltip.place-left:before{border-top:6px solid transparent;border-bottom:6px solid transparent;right:-8px;top:50%;margin-top:-5px}.__react_component_tooltip.place-left:after{border-top:5px solid transparent;border-bottom:5px solid transparent;right:-6px;top:50%;margin-top:-4px}.__react_component_tooltip.place-right{margin-left:10px}.__react_component_tooltip.place-right:before{border-top:6px solid transparent;border-bottom:6px solid transparent;left:-8px;top:50%;margin-top:-5px}.__react_component_tooltip.place-right:after{border-top:5px solid transparent;border-bottom:5px solid transparent;left:-6px;top:50%;margin-top:-4px}.__react_component_tooltip .multi-line{display:block;padding:2px 0px;text-align:center}
        
CIC Dashboard




















        




	window.LANGUAGE_INFO={
    	CURRENT_LANGUAGE : 'en',
    	AUTHOR_MODE: true,
        PUBLISH_RUN_MODE: true
    };






    







    




























    #katalon{font-family:monospace;font-size:13px;background-color:rgba(0,0,0,.7);position:fixed;top:0;left:0;right:0;display:block;z-index:999999999;line-height: normal} #katalon div{padding:0;margin:0;color:#fff;} #katalon kbd{display:inline-block;padding:3px 5px;font:13px Consolas,&quot;Liberation Mono&quot;,Menlo,Courier,monospace;line-height:10px;color:#555;vertical-align:middle;background-color:#fcfcfc;border:1px solid #ccc;border-bottom-color:#bbb;border-radius:3px;box-shadow:inset 0 -1px 0 #bbb;font-weight: bold} div#katalon-spy_elementInfoDiv {color: lightblue; padding: 0px 5px 5px} div#katalon-spy_instructionDiv {padding: 5px 5px 2.5px}
    
        
        



    clearRelease contactAre you sure you want to release contact?Confirm CancelHi, SHARIDASV account_circleclearYou're about to log outAre you sure you want to log out now?Confirm Cancel
    
    
    
    
    





    
    




    
        if(window.createIntlProvider) {
            var value = {&quot;style&quot;:&quot;store-header&quot;,&quot;logo&quot;:{&quot;title&quot;:&quot;Maxis Home&quot;,&quot;logoPath&quot;:&quot;/content/dam/next1bit/images/icons/svgs/Maxis_Logo_Color.svg&quot;,&quot;targetUrl&quot;:&quot;/content/maxis/en/retailer-channel/search-load-customer.html&quot;,&quot;newTab&quot;:false,&quot;alt&quot;:&quot;Maxis Home&quot;},&quot;signInOutModel&quot;:{&quot;personIcon&quot;:&quot;account_circle&quot;,&quot;signInLabel&quot;:&quot;Sign In&quot;,&quot;menuOptions&quot;:[{&quot;label&quot;:&quot;Sign Out&quot;,&quot;newTab&quot;:false,&quot;targetUrl&quot;:&quot;/content/maxis/en/retailer-channel/login.html&quot;,&quot;title&quot;:null,&quot;id&quot;:&quot;logout&quot;}],&quot;loginPageUrl&quot;:&quot;/content/maxis/en/retailer-channel/login.html&quot;,&quot;sessionTimeOutSeconds&quot;:1200},&quot;messages&quot;:{&quot;RetailerHeader_logoutConfirmLabel&quot;:&quot;You're about to log out&quot;,&quot;RetailerHeader_confirmLabel&quot;:&quot;Confirm&quot;,&quot;RetailerHeader_confirmMessageLabel&quot;:&quot;Are you sure you want to log out now?&quot;,&quot;RetailerHeader_cancelLabel&quot;:&quot;Cancel&quot;,&quot;RetailerHeader_userName&quot;:&quot;Hi, {name}&quot;}};
            window.createIntlProvider('HeaderRetailer', 'en', {data: value}, &quot;header-container&quot;);
        }
    




	
        
			

    
    



dashboardRetailer Dashboardsupervisor_accountCIC DashboardRegister InterestEnterprise Case ChannelAstro Micrositedevices_otherBrowse CatalogstorageOrder DashboardstoreStore InventoryMAXIS CENTRE AEON KUCHING(CNTRL.00030 | 10030)clearRelease contactAre you sure you want to release contact?Confirm Cancel



    (function(loadWidget){
        var domPlaceHolder =  document.querySelector('[data-uxfwidget-id=&quot;digitalretailersideb&quot;]');
        console.log(&quot;widget props&quot;, {&quot;widgetId&quot;:&quot;digital-retailer-sidebar-retail-l9-module&quot;,&quot;messages&quot;:{&quot;releaseContactConfirmBtnLabel&quot;:&quot;Confirm&quot;,&quot;releaseContactCancelLabel&quot;:&quot;Cancel&quot;,&quot;storeInventoryLabel&quot;:&quot;Store Inventory&quot;,&quot;releaseContactLabel&quot;:&quot;Release contact&quot;,&quot;cicDashboardLabel&quot;:&quot;CIC Dashboard&quot;,&quot;enterpriseURL&quot;:&quot;https://api-digital-uat3.isddc.men.maxis.com.my/telesales/api/v4.0/telesales/redirect/telesalesCase&quot;,&quot;retailerDashboardLabel&quot;:&quot;Retailer Dashboard&quot;,&quot;leadsMicrositeURL&quot;:&quot;https://api-digital-uat3.isddc.men.maxis.com.my/leads/api/v1.0/leads/redirect/dashboard&quot;,&quot;releaseContactConfirmMessage&quot;:&quot;Are you sure you want to release contact?&quot;,&quot;browseCatalogLabel&quot;:&quot;Browse Catalog&quot;,&quot;leadsMicrositeLabel&quot;:&quot;Register Interest&quot;,&quot;astroMicrositeURL&quot;:&quot;https://api-digital-uat3.isddc.men.maxis.com.my/telesales/api/v4.0/telesales/redirect/telesalesAstro&quot;,&quot;enterpriseLabel&quot;:&quot;Enterprise Case Channel&quot;,&quot;orderDashboardLabel&quot;:&quot;Order Dashboard&quot;,&quot;astroMicrositeLabel&quot;:&quot;Astro Microsite&quot;},&quot;outcomes&quot;:{&quot;NAVIGATE_TO_STORE_INVENTORY&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/stock-inventory&quot;},&quot;NAVIGATE_TO_RETAILER_DASHBOARD&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/search-load-customer&quot;},&quot;NAVIGATE_TO_BROWSE_CATALOG&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/catalog.html?flowType=COP&quot;},&quot;NAVIGATE_TO_CIC_DASHBOARD&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/cic&quot;},&quot;NAVIGATE_TO_ORDER_DASHBOARD&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/order-dashboard&quot;}},&quot;config&quot;:{&quot;displayLeadsMicrosite&quot;:true},&quot;contextId&quot;:&quot;digitalretailersideb&quot;});
		var instanceId;
        
        loadWidget(&quot;digital-retailer-sidebar-retail-l9-module&quot;, instanceId, domPlaceHolder, {&quot;widgetId&quot;:&quot;digital-retailer-sidebar-retail-l9-module&quot;,&quot;messages&quot;:{&quot;releaseContactConfirmBtnLabel&quot;:&quot;Confirm&quot;,&quot;releaseContactCancelLabel&quot;:&quot;Cancel&quot;,&quot;storeInventoryLabel&quot;:&quot;Store Inventory&quot;,&quot;releaseContactLabel&quot;:&quot;Release contact&quot;,&quot;cicDashboardLabel&quot;:&quot;CIC Dashboard&quot;,&quot;enterpriseURL&quot;:&quot;https://api-digital-uat3.isddc.men.maxis.com.my/telesales/api/v4.0/telesales/redirect/telesalesCase&quot;,&quot;retailerDashboardLabel&quot;:&quot;Retailer Dashboard&quot;,&quot;leadsMicrositeURL&quot;:&quot;https://api-digital-uat3.isddc.men.maxis.com.my/leads/api/v1.0/leads/redirect/dashboard&quot;,&quot;releaseContactConfirmMessage&quot;:&quot;Are you sure you want to release contact?&quot;,&quot;browseCatalogLabel&quot;:&quot;Browse Catalog&quot;,&quot;leadsMicrositeLabel&quot;:&quot;Register Interest&quot;,&quot;astroMicrositeURL&quot;:&quot;https://api-digital-uat3.isddc.men.maxis.com.my/telesales/api/v4.0/telesales/redirect/telesalesAstro&quot;,&quot;enterpriseLabel&quot;:&quot;Enterprise Case Channel&quot;,&quot;orderDashboardLabel&quot;:&quot;Order Dashboard&quot;,&quot;astroMicrositeLabel&quot;:&quot;Astro Microsite&quot;},&quot;outcomes&quot;:{&quot;NAVIGATE_TO_STORE_INVENTORY&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/stock-inventory&quot;},&quot;NAVIGATE_TO_RETAILER_DASHBOARD&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/search-load-customer&quot;},&quot;NAVIGATE_TO_BROWSE_CATALOG&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/catalog.html?flowType=COP&quot;},&quot;NAVIGATE_TO_CIC_DASHBOARD&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/cic&quot;},&quot;NAVIGATE_TO_ORDER_DASHBOARD&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/order-dashboard&quot;}},&quot;config&quot;:{&quot;displayLeadsMicrosite&quot;:true},&quot;contextId&quot;:&quot;digitalretailersideb&quot;}, {&quot;baseURL&quot;:&quot;/rp-server&quot;,&quot;oldBaseURL&quot;:&quot;http://localhost:5000/&quot;,&quot;useMocks&quot;:false,&quot;singlePageApp&quot;:false,&quot;locale&quot;:&quot;en-US&quot;});
    })(window.amdocs.appBootstrap.loadWidget);



    




        
        
            

    
    







    (function(loadWidget){
        var domPlaceHolder =  document.querySelector('[data-uxfwidget-id=&quot;digitalgeneralmessag&quot;]');
        console.log(&quot;widget props&quot;, {&quot;widgetId&quot;:&quot;digital-general-message-handler-l9-module&quot;,&quot;messages&quot;:{&quot;Unauthorized&quot;:&quot;Session Expired. Please re-login&quot;,&quot;Forbidden&quot;:&quot;Error 403: This transaction is not allowed.&quot;,&quot;InternalServerError&quot;:&quot;Something went wrong. Please try again.&quot;,&quot;NotFound&quot;:&quot;Requested resource not found. Please check with system admin&quot;,&quot;BadRequest&quot;:&quot;Something wrong with the inputs. Please check.&quot;,&quot;SystemUnavailable&quot;:&quot;System unavailable. Please try after some time.&quot;},&quot;outcomes&quot;:{&quot;NAVIGATE_TO_LOGIN&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/login&quot;}},&quot;config&quot;:{&quot;loaderImagePath&quot;:&quot;/content/dam/next1bit/images/icons/Loader.gif&quot;},&quot;contextId&quot;:&quot;digitalgeneralmessag&quot;});
		var instanceId;
        
        loadWidget(&quot;digital-general-message-handler-l9-module&quot;, instanceId, domPlaceHolder, {&quot;widgetId&quot;:&quot;digital-general-message-handler-l9-module&quot;,&quot;messages&quot;:{&quot;Unauthorized&quot;:&quot;Session Expired. Please re-login&quot;,&quot;Forbidden&quot;:&quot;Error 403: This transaction is not allowed.&quot;,&quot;InternalServerError&quot;:&quot;Something went wrong. Please try again.&quot;,&quot;NotFound&quot;:&quot;Requested resource not found. Please check with system admin&quot;,&quot;BadRequest&quot;:&quot;Something wrong with the inputs. Please check.&quot;,&quot;SystemUnavailable&quot;:&quot;System unavailable. Please try after some time.&quot;},&quot;outcomes&quot;:{&quot;NAVIGATE_TO_LOGIN&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/login&quot;}},&quot;config&quot;:{&quot;loaderImagePath&quot;:&quot;/content/dam/next1bit/images/icons/Loader.gif&quot;},&quot;contextId&quot;:&quot;digitalgeneralmessag&quot;}, {&quot;baseURL&quot;:&quot;/rp-server&quot;,&quot;oldBaseURL&quot;:&quot;http://localhost:5000/&quot;,&quot;useMocks&quot;:false,&quot;singlePageApp&quot;:false,&quot;locale&quot;:&quot;en-US&quot;});
    })(window.amdocs.appBootstrap.loadWidget);






Farizam  ID: 25Feb22Searched By  ACCOUNT 78494200Maxis One Club: 0NEWperm_contact_calendarcheck_circleYour current age is 91Bio-metric Not Performed.Acc no: 78494200Write off: RCollection Status: NAcc no: 78494416Write off: RCollection Status: Nstarexit_to_appclearRelease contactAre you sure you want to release contact?Confirm Cancel



    (function(loadWidget){
        var domPlaceHolder =  document.querySelector('[data-uxfwidget-id=&quot;digitalcontactheader&quot;]');
        console.log(&quot;widget props&quot;, {&quot;widgetId&quot;:&quot;digital-contact-header-l9-module&quot;,&quot;messages&quot;:{&quot;contactHeader_releaseContactLabel&quot;:&quot;Release contact&quot;,&quot;submitedDateMsg&quot;:&quot;Submitted Date: {submitedDate}&quot;,&quot;submittedByMsg&quot;:&quot;Submitted By: {submittedBy}&quot;,&quot;contactHeader_releaseContactCancelLabel&quot;:&quot;Cancel&quot;,&quot;contactHeader_searchLabel&quot;:&quot;Searched By&quot;,&quot;contactHeader_AGENOTIFICATIONMsg&quot;:&quot;Your current age is {age}&quot;,&quot;accountIdLabel&quot;:&quot;Account Id&quot;,&quot;contactHeader_AGEMsg&quot;:&quot;Age Check done.&quot;,&quot;customerNameMsg&quot;:&quot;Customer Name: {customerName}&quot;,&quot;contactHeader_releaseContactConfirmBtnLabel&quot;:&quot;Confirm&quot;,&quot;leadsMicrositeURL&quot;:&quot;https://api-digital-uat3.isddc.men.maxis.com.my/leads/api/v1.0/leads/redirect/register&quot;,&quot;contactHeader_docTypeLabel&quot;:&quot;ID&quot;,&quot;contactHeader_FRAUDMsg&quot;:&quot;Fraudulent customer found&quot;,&quot;contactHeader_NOBIOMETRICMsg&quot;:&quot;Bio-metric Not Performed.&quot;,&quot;contactHeader_LINELIMITMsg&quot;:&quot;Line Check done.&quot;,&quot;contactHeader_releaseContactConfirmMessage&quot;:&quot;Are you sure you want to release contact?&quot;,&quot;contactHeader_BIOMETRICMsg&quot;:&quot;Your BioMetric Validation failed.&quot;,&quot;customerAccountMsg&quot;:&quot;Customer Account: {customerAccount}&quot;},&quot;outcomes&quot;:{&quot;NAVIGATE_TO_UPDATE_PROFILE&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/update-profile&quot;},&quot;NAVIGATE_TO_CART&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/cart&quot;},&quot;BACK_TO_CONTACT_DETAILS&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/cic&quot;},&quot;RELEASE_TO_DASHBOARD&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/search-load-customer&quot;},&quot;RELEASE_TO_MULTIPLE_CONTACT_LIST&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/customerandcontact&quot;}},&quot;config&quot;:{&quot;displayLeadsMicrosite&quot;:true,&quot;skipCartload&quot;:true,&quot;ddmfFeaturesFlagId&quot;:false,&quot;callWriteOff&quot;:true},&quot;contextId&quot;:&quot;digitalcontactheader&quot;});
		var instanceId;
        
        loadWidget(&quot;digital-contact-header-l9-module&quot;, instanceId, domPlaceHolder, {&quot;widgetId&quot;:&quot;digital-contact-header-l9-module&quot;,&quot;messages&quot;:{&quot;contactHeader_releaseContactLabel&quot;:&quot;Release contact&quot;,&quot;submitedDateMsg&quot;:&quot;Submitted Date: {submitedDate}&quot;,&quot;submittedByMsg&quot;:&quot;Submitted By: {submittedBy}&quot;,&quot;contactHeader_releaseContactCancelLabel&quot;:&quot;Cancel&quot;,&quot;contactHeader_searchLabel&quot;:&quot;Searched By&quot;,&quot;contactHeader_AGENOTIFICATIONMsg&quot;:&quot;Your current age is {age}&quot;,&quot;accountIdLabel&quot;:&quot;Account Id&quot;,&quot;contactHeader_AGEMsg&quot;:&quot;Age Check done.&quot;,&quot;customerNameMsg&quot;:&quot;Customer Name: {customerName}&quot;,&quot;contactHeader_releaseContactConfirmBtnLabel&quot;:&quot;Confirm&quot;,&quot;leadsMicrositeURL&quot;:&quot;https://api-digital-uat3.isddc.men.maxis.com.my/leads/api/v1.0/leads/redirect/register&quot;,&quot;contactHeader_docTypeLabel&quot;:&quot;ID&quot;,&quot;contactHeader_FRAUDMsg&quot;:&quot;Fraudulent customer found&quot;,&quot;contactHeader_NOBIOMETRICMsg&quot;:&quot;Bio-metric Not Performed.&quot;,&quot;contactHeader_LINELIMITMsg&quot;:&quot;Line Check done.&quot;,&quot;contactHeader_releaseContactConfirmMessage&quot;:&quot;Are you sure you want to release contact?&quot;,&quot;contactHeader_BIOMETRICMsg&quot;:&quot;Your BioMetric Validation failed.&quot;,&quot;customerAccountMsg&quot;:&quot;Customer Account: {customerAccount}&quot;},&quot;outcomes&quot;:{&quot;NAVIGATE_TO_UPDATE_PROFILE&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/update-profile&quot;},&quot;NAVIGATE_TO_CART&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/cart&quot;},&quot;BACK_TO_CONTACT_DETAILS&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/cic&quot;},&quot;RELEASE_TO_DASHBOARD&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/search-load-customer&quot;},&quot;RELEASE_TO_MULTIPLE_CONTACT_LIST&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/customerandcontact&quot;}},&quot;config&quot;:{&quot;displayLeadsMicrosite&quot;:true,&quot;skipCartload&quot;:true,&quot;ddmfFeaturesFlagId&quot;:false,&quot;callWriteOff&quot;:true},&quot;contextId&quot;:&quot;digitalcontactheader&quot;}, {&quot;baseURL&quot;:&quot;/rp-server&quot;,&quot;oldBaseURL&quot;:&quot;http://localhost:5000/&quot;,&quot;useMocks&quot;:false,&quot;singlePageApp&quot;:false,&quot;locale&quot;:&quot;en-US&quot;});
    })(window.amdocs.appBootstrap.loadWidget);






SPECIAL OFFERS &amp; FLASH DEALS FOR THE DAYWe are sorry! Looks like you have no campaigns available currently.



    (function(loadWidget){
        var domPlaceHolder =  document.querySelector('[data-uxfwidget-id=&quot;digitalcampaignlistr&quot;]');
        console.log(&quot;widget props&quot;, {&quot;widgetId&quot;:&quot;digital-campaign-list-retail-l9-module&quot;,&quot;messages&quot;:{&quot;View_More_Button_Label&quot;:&quot;View More&quot;,&quot;Special_Offers_Label&quot;:&quot;SPECIAL OFFERS &amp; FLASH DEALS FOR THE DAY&quot;,&quot;CMS_Offer_Id_Label&quot;:&quot;CMS_Offer ID:&quot;,&quot;Campaign_Status_Label&quot;:&quot;Campaign Status:&quot;,&quot;Fulfillment_Status_Label&quot;:&quot;Fullfilment Status:&quot;,&quot;Campaign_Code_Label&quot;:&quot;Campaign Code:&quot;,&quot;No_Campaigns_Label&quot;:&quot;We are sorry! Looks like you have no campaigns available currently.&quot;},&quot;outcomes&quot;:{&quot;NAVIGATE_TO_CAMPAIGN_SUMMARY&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/campaign-summary&quot;}},&quot;config&quot;:{&quot;campaignListConfig&quot;:5},&quot;contextId&quot;:&quot;digitalcampaignlistr&quot;});
		var instanceId;
        
        loadWidget(&quot;digital-campaign-list-retail-l9-module&quot;, instanceId, domPlaceHolder, {&quot;widgetId&quot;:&quot;digital-campaign-list-retail-l9-module&quot;,&quot;messages&quot;:{&quot;View_More_Button_Label&quot;:&quot;View More&quot;,&quot;Special_Offers_Label&quot;:&quot;SPECIAL OFFERS &amp; FLASH DEALS FOR THE DAY&quot;,&quot;CMS_Offer_Id_Label&quot;:&quot;CMS_Offer ID:&quot;,&quot;Campaign_Status_Label&quot;:&quot;Campaign Status:&quot;,&quot;Fulfillment_Status_Label&quot;:&quot;Fullfilment Status:&quot;,&quot;Campaign_Code_Label&quot;:&quot;Campaign Code:&quot;,&quot;No_Campaigns_Label&quot;:&quot;We are sorry! Looks like you have no campaigns available currently.&quot;},&quot;outcomes&quot;:{&quot;NAVIGATE_TO_CAMPAIGN_SUMMARY&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/campaign-summary&quot;}},&quot;config&quot;:{&quot;campaignListConfig&quot;:5},&quot;contextId&quot;:&quot;digitalcampaignlistr&quot;}, {&quot;baseURL&quot;:&quot;/rp-server&quot;,&quot;oldBaseURL&quot;:&quot;http://localhost:5000/&quot;,&quot;useMocks&quot;:false,&quot;singlePageApp&quot;:false,&quot;locale&quot;:&quot;en-US&quot;});
    })(window.amdocs.appBootstrap.loadWidget);










    (function(loadWidget){
        var domPlaceHolder =  document.querySelector('[data-uxfwidget-id=&quot;digitalleadmicrosite&quot;]');
        console.log(&quot;widget props&quot;, {&quot;widgetId&quot;:&quot;digital-lead-microsite-retail-l9-module&quot;,&quot;messages&quot;:{&quot;leadsMicrositeURL&quot;:&quot;https://api-digital-uat3.isddc.men.maxis.com.my/leads/api/v1.0/leads/redirect/landing&quot;,&quot;iFrameHeight&quot;:&quot;240&quot;},&quot;config&quot;:{&quot;displayLeadsMicrosite&quot;:true,&quot;inlineMessages&quot;:false},&quot;contextId&quot;:&quot;digitalleadmicrosite&quot;});
		var instanceId;
        
        loadWidget(&quot;digital-lead-microsite-retail-l9-module&quot;, instanceId, domPlaceHolder, {&quot;widgetId&quot;:&quot;digital-lead-microsite-retail-l9-module&quot;,&quot;messages&quot;:{&quot;leadsMicrositeURL&quot;:&quot;https://api-digital-uat3.isddc.men.maxis.com.my/leads/api/v1.0/leads/redirect/landing&quot;,&quot;iFrameHeight&quot;:&quot;240&quot;},&quot;config&quot;:{&quot;displayLeadsMicrosite&quot;:true,&quot;inlineMessages&quot;:false},&quot;contextId&quot;:&quot;digitalleadmicrosite&quot;}, {&quot;baseURL&quot;:&quot;/rp-server&quot;,&quot;oldBaseURL&quot;:&quot;http://localhost:5000/&quot;,&quot;useMocks&quot;:false,&quot;singlePageApp&quot;:false,&quot;locale&quot;:&quot;en-US&quot;});
    })(window.amdocs.appBootstrap.loadWidget);






Contracts Summary02Others Contract02Zerolution Fibre ContractSubscriptions Summary02VOIP02Fibre



    (function(loadWidget){
        var domPlaceHolder =  document.querySelector('[data-uxfwidget-id=&quot;digitalcontractssubs&quot;]');
        console.log(&quot;widget props&quot;, {&quot;widgetId&quot;:&quot;digital-contracts-subscriptions-summary-retail-l9-module&quot;,&quot;messages&quot;:{&quot;contractsSummaryErrorLabel&quot;:&quot;There are no active contracts&quot;,&quot;subscriptionsSummaryErrorLabel&quot;:&quot;No Subscriptions Present&quot;,&quot;subscriptionsSummaryLabel&quot;:&quot;Subscriptions Summary&quot;,&quot;contractsSummaryLabel&quot;:&quot;Contracts Summary&quot;},&quot;config&quot;:{&quot;inlineMessages&quot;:false},&quot;contextId&quot;:&quot;digitalcontractssubs&quot;});
		var instanceId;
        
        loadWidget(&quot;digital-contracts-subscriptions-summary-retail-l9-module&quot;, instanceId, domPlaceHolder, {&quot;widgetId&quot;:&quot;digital-contracts-subscriptions-summary-retail-l9-module&quot;,&quot;messages&quot;:{&quot;contractsSummaryErrorLabel&quot;:&quot;There are no active contracts&quot;,&quot;subscriptionsSummaryErrorLabel&quot;:&quot;No Subscriptions Present&quot;,&quot;subscriptionsSummaryLabel&quot;:&quot;Subscriptions Summary&quot;,&quot;contractsSummaryLabel&quot;:&quot;Contracts Summary&quot;},&quot;config&quot;:{&quot;inlineMessages&quot;:false},&quot;contextId&quot;:&quot;digitalcontractssubs&quot;}, {&quot;baseURL&quot;:&quot;/rp-server&quot;,&quot;oldBaseURL&quot;:&quot;http://localhost:5000/&quot;,&quot;useMocks&quot;:false,&quot;singlePageApp&quot;:false,&quot;locale&quot;:&quot;en-US&quot;});
    })(window.amdocs.appBootstrap.loadWidget);






New Purchase Starter Pack Registration Switch To Maxis Create Interaction View Interaction Biz Individual Registration Change Ownership It seems there is a pending orderPlease submit or cancel the order before proceeding.OK 



    (function(loadWidget){
        var domPlaceHolder =  document.querySelector('[data-uxfwidget-id=&quot;digitalcustomernavig&quot;]');
        console.log(&quot;widget props&quot;, {&quot;widgetId&quot;:&quot;digital-customer-navigation-panel-retail-l9-module&quot;,&quot;messages&quot;:{&quot;ViewInteraction&quot;:&quot;View Interaction&quot;,&quot;postToPreSimOption&quot;:&quot;How do you want to proceed ?&quot;,&quot;continueNewSim&quot;:&quot;Continue With New Sim&quot;,&quot;okButtonRetailer&quot;:&quot;OK&quot;,&quot;msgLine1ForRetailer&quot;:&quot;Eligibility popup Msg line 1 for retailer&quot;,&quot;PostpaidToPrepaid&quot;:&quot;Postpaid To Prepaid&quot;,&quot;CreateInteraction&quot;:&quot;Create Interaction&quot;,&quot;SwitchToMaxis&quot;:&quot;Switch To Maxis&quot;,&quot;continueOldSim&quot;:&quot;Continue With Old Sim&quot;,&quot;BizIndividualRegistration&quot;:&quot;Biz Individual Registration&quot;,&quot;CustomerOrders&quot;:&quot;Customers Orders&quot;,&quot;msgLine2ForRetailer&quot;:&quot;Please submit or cancel the order before proceeding.&quot;,&quot;FinancialTransaction&quot;:&quot;Financial Transaction&quot;,&quot;Cart&quot;:&quot;Cart&quot;,&quot;StarterPackRegistration&quot;:&quot;Starter Pack Registration&quot;,&quot;Case&quot;:&quot;Case&quot;,&quot;NewPurchase&quot;:&quot;New Purchase&quot;},&quot;outcomes&quot;:{&quot;BIZ_INDIVIDUAL_REGISTRATION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/bizindividual&quot;},&quot;VIEW_INTERACTION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/interaction-list&quot;},&quot;CHANGE_OWNERSHIP_TARGET_CREATE_CUSTOMER&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/target-create-customer.html?flowType=changeOwnership&quot;},&quot;NAVIGATE_TO_PLAN_LIST&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/plan-list&quot;},&quot;NEW_PURCHASE&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/catalog.html?flowType=COP&quot;},&quot;CUSTOMER_ORDERS&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/order-dashboard&quot;},&quot;STARTER_PACK_REGISTRATION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/starter-pack&quot;},&quot;VIEW_CASE&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/view-cases&quot;},&quot;CHANGE_OWNERSHIP_TARGET&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/change-ownership-target&quot;},&quot;CREATE_INTERACTION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/create-interaction&quot;},&quot;NAVIGATE_TO_FINANCIAL_TRANSACTION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/financial-transaction&quot;},&quot;CART&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/cart&quot;}},&quot;config&quot;:{&quot;customerTypeCheckbox&quot;:&quot;false&quot;},&quot;initProps&quot;:{&quot;customerType&quot;:&quot;customerContext&quot;},&quot;contextId&quot;:&quot;digitalcustomernavig&quot;});
		var instanceId;
        
        loadWidget(&quot;digital-customer-navigation-panel-retail-l9-module&quot;, instanceId, domPlaceHolder, {&quot;widgetId&quot;:&quot;digital-customer-navigation-panel-retail-l9-module&quot;,&quot;messages&quot;:{&quot;ViewInteraction&quot;:&quot;View Interaction&quot;,&quot;postToPreSimOption&quot;:&quot;How do you want to proceed ?&quot;,&quot;continueNewSim&quot;:&quot;Continue With New Sim&quot;,&quot;okButtonRetailer&quot;:&quot;OK&quot;,&quot;msgLine1ForRetailer&quot;:&quot;Eligibility popup Msg line 1 for retailer&quot;,&quot;PostpaidToPrepaid&quot;:&quot;Postpaid To Prepaid&quot;,&quot;CreateInteraction&quot;:&quot;Create Interaction&quot;,&quot;SwitchToMaxis&quot;:&quot;Switch To Maxis&quot;,&quot;continueOldSim&quot;:&quot;Continue With Old Sim&quot;,&quot;BizIndividualRegistration&quot;:&quot;Biz Individual Registration&quot;,&quot;CustomerOrders&quot;:&quot;Customers Orders&quot;,&quot;msgLine2ForRetailer&quot;:&quot;Please submit or cancel the order before proceeding.&quot;,&quot;FinancialTransaction&quot;:&quot;Financial Transaction&quot;,&quot;Cart&quot;:&quot;Cart&quot;,&quot;StarterPackRegistration&quot;:&quot;Starter Pack Registration&quot;,&quot;Case&quot;:&quot;Case&quot;,&quot;NewPurchase&quot;:&quot;New Purchase&quot;},&quot;outcomes&quot;:{&quot;BIZ_INDIVIDUAL_REGISTRATION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/bizindividual&quot;},&quot;VIEW_INTERACTION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/interaction-list&quot;},&quot;CHANGE_OWNERSHIP_TARGET_CREATE_CUSTOMER&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/target-create-customer.html?flowType=changeOwnership&quot;},&quot;NAVIGATE_TO_PLAN_LIST&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/plan-list&quot;},&quot;NEW_PURCHASE&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/catalog.html?flowType=COP&quot;},&quot;CUSTOMER_ORDERS&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/order-dashboard&quot;},&quot;STARTER_PACK_REGISTRATION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/starter-pack&quot;},&quot;VIEW_CASE&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/view-cases&quot;},&quot;CHANGE_OWNERSHIP_TARGET&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/change-ownership-target&quot;},&quot;CREATE_INTERACTION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/create-interaction&quot;},&quot;NAVIGATE_TO_FINANCIAL_TRANSACTION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/financial-transaction&quot;},&quot;CART&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/cart&quot;}},&quot;config&quot;:{&quot;customerTypeCheckbox&quot;:&quot;false&quot;},&quot;initProps&quot;:{&quot;customerType&quot;:&quot;customerContext&quot;},&quot;contextId&quot;:&quot;digitalcustomernavig&quot;}, {&quot;baseURL&quot;:&quot;/rp-server&quot;,&quot;oldBaseURL&quot;:&quot;http://localhost:5000/&quot;,&quot;useMocks&quot;:false,&quot;singlePageApp&quot;:false,&quot;locale&quot;:&quot;en-US&quot;});
    })(window.amdocs.appBootstrap.loadWidget);






Account 78494200expand_lessTypeConsumerSub-TypeMaxis IndividualSegmentNONECollection StatusNormalWrite-OffNoBilling ProfileManageDirect DebitRegister NowTotal Amount DueRM 0.00Pay Before -View Bill A57400Maxis Fibre - 300MbpsShow DetailsActiveSince 25 Feb 2022keyboard_arrow_right60395459722Maxis Fibre - VoiceShow DetailsActiveSince 25 Feb 2022keyboard_arrow_rightNew Purchase Switch To Maxis Case Customers Orders Financial Transaction Cart Change Ownership It seems there is a pending orderPlease submit or cancel the order before proceeding.OK It seems there is a pending orderPlease submit or cancel the order before proceeding.OK Account 78494416expand_moreTypeConsumerSub-TypeMaxis IndividualSegmentNONECollection StatusNormalWrite-OffNoBilling ProfileManageDirect DebitRegister NowTotal Amount DueRM 0.00Pay Before -View Bill New Purchase Switch To Maxis Case Customers Orders Financial Transaction Cart Change Ownership It seems there is a pending orderPlease submit or cancel the order before proceeding.OK It seems there is a pending orderPlease submit or cancel the order before proceeding.OK 



    (function(loadWidget){
        var domPlaceHolder =  document.querySelector('[data-uxfwidget-id=&quot;digitalaccountlistl9&quot;]');
        console.log(&quot;widget props&quot;, {&quot;widgetId&quot;:&quot;digital-account-list-l9-module&quot;,&quot;messages&quot;:{&quot;accountDetails_directDebit&quot;:&quot;Direct Debit&quot;,&quot;accountDetails_cancelledAccount&quot;:&quot;Cancelled Account&quot;,&quot;ViewInteraction&quot;:&quot;View Interaction&quot;,&quot;selectSubscription_noProductsMessage&quot;:&quot;No Products Assigned&quot;,&quot;postToPreSimOption&quot;:&quot;How do you want to proceed ?&quot;,&quot;selectSubscription_active&quot;:&quot;Active&quot;,&quot;continueNewSim&quot;:&quot;Continue With New Sim&quot;,&quot;showCancelAccountText&quot;:&quot;View Cancelled Account&quot;,&quot;okButtonRetailer&quot;:&quot;OK&quot;,&quot;msgLine1ForRetailer&quot;:&quot;Eligibility popup Msg line 1 for retailer&quot;,&quot;PostpaidToPrepaid&quot;:&quot;Postpaid To Prepaid&quot;,&quot;accountDetails_postPaidAccount&quot;:&quot;Account {billingProfileId}&quot;,&quot;CreateInteraction&quot;:&quot;Create Interaction&quot;,&quot;SwitchToMaxis&quot;:&quot;Switch To Maxis&quot;,&quot;accountDetails_writeOff&quot;:&quot;Write-Off&quot;,&quot;accountDetails_billingProfile&quot;:&quot;Billing Profile&quot;,&quot;continueOldSim&quot;:&quot;Continue With Old Sim&quot;,&quot;BizIndividualRegistration&quot;:&quot;Biz Individual Registration&quot;,&quot;accountDetails_typeText&quot;:&quot;Type&quot;,&quot;selectSubscription_suspended&quot;:&quot;Suspended&quot;,&quot;CustomerOrders&quot;:&quot;Customers Orders&quot;,&quot;msgLine2ForRetailer&quot;:&quot;Please submit or cancel the order before proceeding.&quot;,&quot;accountDetails_registerNow&quot;:&quot;Register Now&quot;,&quot;accountDetails_subType&quot;:&quot;Sub-Type&quot;,&quot;accountDetails_brn&quot;:&quot;BRN&quot;,&quot;FinancialTransaction&quot;:&quot;Financial Transaction&quot;,&quot;Cart&quot;:&quot;Cart&quot;,&quot;StarterPackRegistration&quot;:&quot;Starter Pack Registration&quot;,&quot;accountDetails_manage&quot;:&quot;Manage&quot;,&quot;Case&quot;:&quot;Case&quot;,&quot;accountDetails_collectionStatus&quot;:&quot;Collection Status&quot;,&quot;selectSubscription_since&quot;:&quot;Since {date}&quot;,&quot;accountDetails_prepaidAccount&quot;:&quot;Prepaid Account&quot;,&quot;accountDetails_primitiveAccount&quot;:&quot;Primitive Account&quot;,&quot;NewPurchase&quot;:&quot;New Purchase&quot;},&quot;outcomes&quot;:{&quot;BIZ_INDIVIDUAL_REGISTRATION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/bizindividual&quot;},&quot;VIEW_INTERACTION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/interaction-list&quot;},&quot;NAVIGATE_TO_UPDATE_BILLING_PROFILE&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/update-billing-profile&quot;},&quot;NAVIGATE_TO_PLAN_LIST&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/catalog&quot;},&quot;POSTPAID_TO_PREPAID&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/postpaid to prepaid&quot;},&quot;NAVIGATE_TO_ORDER_SUMMARY&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/order-summary&quot;},&quot;SET_SUBSCRIPTION_OPTION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/balance-and-services&quot;},&quot;NEW_PURCHASE&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/catalog.html?flowType=COP&quot;},&quot;NAVIGATE_TO_BILL_SUMMARY&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/bill-summary&quot;},&quot;CUSTOMER_ORDERS&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/order-dashboard&quot;},&quot;STARTER_PACK_REGISTRATION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/starter-pack&quot;},&quot;SET_SUBSCRIPTION_OPTION_PREPAID&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/prepaid-balance-services&quot;},&quot;CHANGE_OWNERSHIP&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/change-ownership-source&quot;},&quot;SET_SUBSCRIPTION_OPTION_POSTPAID&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/postpaid-balance-services&quot;},&quot;SET_SUBSCRIPTION_OPTION_VOIP&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/fixedVoice-subscription-overview&quot;},&quot;SET_SUBSCRIPTION_OPTION_FTTH&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/fibre-subscription-overview&quot;},&quot;NAVIGATE_TO_MANAGE_AUTOPAY&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/manage-auto-pay&quot;},&quot;VIEW_CASE&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/view-cases&quot;},&quot;SWITCH_TO_MAXIS&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/catalog&quot;},&quot;CREATE_INTERACTION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/create-interaction&quot;},&quot;NAVIGATE_TO_CAMPAIGN_SUMMARY&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/campaign-summary&quot;},&quot;NAVIGATE_TO_FINANCIAL_TRANSACTION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/financial-transaction&quot;},&quot;CART&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/cart&quot;}},&quot;contextId&quot;:&quot;digitalaccountlistl9&quot;});
		var instanceId;
        
        loadWidget(&quot;digital-account-list-l9-module&quot;, instanceId, domPlaceHolder, {&quot;widgetId&quot;:&quot;digital-account-list-l9-module&quot;,&quot;messages&quot;:{&quot;accountDetails_directDebit&quot;:&quot;Direct Debit&quot;,&quot;accountDetails_cancelledAccount&quot;:&quot;Cancelled Account&quot;,&quot;ViewInteraction&quot;:&quot;View Interaction&quot;,&quot;selectSubscription_noProductsMessage&quot;:&quot;No Products Assigned&quot;,&quot;postToPreSimOption&quot;:&quot;How do you want to proceed ?&quot;,&quot;selectSubscription_active&quot;:&quot;Active&quot;,&quot;continueNewSim&quot;:&quot;Continue With New Sim&quot;,&quot;showCancelAccountText&quot;:&quot;View Cancelled Account&quot;,&quot;okButtonRetailer&quot;:&quot;OK&quot;,&quot;msgLine1ForRetailer&quot;:&quot;Eligibility popup Msg line 1 for retailer&quot;,&quot;PostpaidToPrepaid&quot;:&quot;Postpaid To Prepaid&quot;,&quot;accountDetails_postPaidAccount&quot;:&quot;Account {billingProfileId}&quot;,&quot;CreateInteraction&quot;:&quot;Create Interaction&quot;,&quot;SwitchToMaxis&quot;:&quot;Switch To Maxis&quot;,&quot;accountDetails_writeOff&quot;:&quot;Write-Off&quot;,&quot;accountDetails_billingProfile&quot;:&quot;Billing Profile&quot;,&quot;continueOldSim&quot;:&quot;Continue With Old Sim&quot;,&quot;BizIndividualRegistration&quot;:&quot;Biz Individual Registration&quot;,&quot;accountDetails_typeText&quot;:&quot;Type&quot;,&quot;selectSubscription_suspended&quot;:&quot;Suspended&quot;,&quot;CustomerOrders&quot;:&quot;Customers Orders&quot;,&quot;msgLine2ForRetailer&quot;:&quot;Please submit or cancel the order before proceeding.&quot;,&quot;accountDetails_registerNow&quot;:&quot;Register Now&quot;,&quot;accountDetails_subType&quot;:&quot;Sub-Type&quot;,&quot;accountDetails_brn&quot;:&quot;BRN&quot;,&quot;FinancialTransaction&quot;:&quot;Financial Transaction&quot;,&quot;Cart&quot;:&quot;Cart&quot;,&quot;StarterPackRegistration&quot;:&quot;Starter Pack Registration&quot;,&quot;accountDetails_manage&quot;:&quot;Manage&quot;,&quot;Case&quot;:&quot;Case&quot;,&quot;accountDetails_collectionStatus&quot;:&quot;Collection Status&quot;,&quot;selectSubscription_since&quot;:&quot;Since {date}&quot;,&quot;accountDetails_prepaidAccount&quot;:&quot;Prepaid Account&quot;,&quot;accountDetails_primitiveAccount&quot;:&quot;Primitive Account&quot;,&quot;NewPurchase&quot;:&quot;New Purchase&quot;},&quot;outcomes&quot;:{&quot;BIZ_INDIVIDUAL_REGISTRATION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/bizindividual&quot;},&quot;VIEW_INTERACTION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/interaction-list&quot;},&quot;NAVIGATE_TO_UPDATE_BILLING_PROFILE&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/update-billing-profile&quot;},&quot;NAVIGATE_TO_PLAN_LIST&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/catalog&quot;},&quot;POSTPAID_TO_PREPAID&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/postpaid to prepaid&quot;},&quot;NAVIGATE_TO_ORDER_SUMMARY&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/order-summary&quot;},&quot;SET_SUBSCRIPTION_OPTION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/balance-and-services&quot;},&quot;NEW_PURCHASE&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/catalog.html?flowType=COP&quot;},&quot;NAVIGATE_TO_BILL_SUMMARY&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/bill-summary&quot;},&quot;CUSTOMER_ORDERS&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/order-dashboard&quot;},&quot;STARTER_PACK_REGISTRATION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/starter-pack&quot;},&quot;SET_SUBSCRIPTION_OPTION_PREPAID&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/prepaid-balance-services&quot;},&quot;CHANGE_OWNERSHIP&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/change-ownership-source&quot;},&quot;SET_SUBSCRIPTION_OPTION_POSTPAID&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/postpaid-balance-services&quot;},&quot;SET_SUBSCRIPTION_OPTION_VOIP&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/fixedVoice-subscription-overview&quot;},&quot;SET_SUBSCRIPTION_OPTION_FTTH&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/fibre-subscription-overview&quot;},&quot;NAVIGATE_TO_MANAGE_AUTOPAY&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/manage-auto-pay&quot;},&quot;VIEW_CASE&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/view-cases&quot;},&quot;SWITCH_TO_MAXIS&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/catalog&quot;},&quot;CREATE_INTERACTION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/create-interaction&quot;},&quot;NAVIGATE_TO_CAMPAIGN_SUMMARY&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/campaign-summary&quot;},&quot;NAVIGATE_TO_FINANCIAL_TRANSACTION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/financial-transaction&quot;},&quot;CART&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/cart&quot;}},&quot;contextId&quot;:&quot;digitalaccountlistl9&quot;}, {&quot;baseURL&quot;:&quot;/rp-server&quot;,&quot;oldBaseURL&quot;:&quot;http://localhost:5000/&quot;,&quot;useMocks&quot;:false,&quot;singlePageApp&quot;:false,&quot;locale&quot;:&quot;en-US&quot;});
    })(window.amdocs.appBootstrap.loadWidget);



    




        
	


        




    

/html[1]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//html</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//html[(text() = concat(&quot;.__react_component_tooltip{border-radius:3px;display:inline-block;font-size:13px;left:-999em;opacity:0;padding:8px 21px;position:fixed;pointer-events:none;transition:opacity 0.3s ease-out;top:-999em;visibility:hidden;z-index:999}.__react_component_tooltip.allow_hover,.__react_component_tooltip.allow_click{pointer-events:auto}.__react_component_tooltip:before,.__react_component_tooltip:after{content:&quot;&quot;;width:0;height:0;position:absolute}.__react_component_tooltip.show{opacity:0.9;margin-top:0px;margin-left:0px;visibility:visible}.__react_component_tooltip.type-dark{color:#fff;background-color:#222}.__react_component_tooltip.type-dark.place-top:after{border-top-color:#222;border-top-style:solid;border-top-width:6px}.__react_component_tooltip.type-dark.place-bottom:after{border-bottom-color:#222;border-bottom-style:solid;border-bottom-width:6px}.__react_component_tooltip.type-dark.place-left:after{border-left-color:#222;border-left-style:solid;border-left-width:6px}.__react_component_tooltip.type-dark.place-right:after{border-right-color:#222;border-right-style:solid;border-right-width:6px}.__react_component_tooltip.type-dark.border{border:1px solid #fff}.__react_component_tooltip.type-dark.border.place-top:before{border-top:8px solid #fff}.__react_component_tooltip.type-dark.border.place-bottom:before{border-bottom:8px solid #fff}.__react_component_tooltip.type-dark.border.place-left:before{border-left:8px solid #fff}.__react_component_tooltip.type-dark.border.place-right:before{border-right:8px solid #fff}.__react_component_tooltip.type-success{color:#fff;background-color:#8DC572}.__react_component_tooltip.type-success.place-top:after{border-top-color:#8DC572;border-top-style:solid;border-top-width:6px}.__react_component_tooltip.type-success.place-bottom:after{border-bottom-color:#8DC572;border-bottom-style:solid;border-bottom-width:6px}.__react_component_tooltip.type-success.place-left:after{border-left-color:#8DC572;border-left-style:solid;border-left-width:6px}.__react_component_tooltip.type-success.place-right:after{border-right-color:#8DC572;border-right-style:solid;border-right-width:6px}.__react_component_tooltip.type-success.border{border:1px solid #fff}.__react_component_tooltip.type-success.border.place-top:before{border-top:8px solid #fff}.__react_component_tooltip.type-success.border.place-bottom:before{border-bottom:8px solid #fff}.__react_component_tooltip.type-success.border.place-left:before{border-left:8px solid #fff}.__react_component_tooltip.type-success.border.place-right:before{border-right:8px solid #fff}.__react_component_tooltip.type-warning{color:#fff;background-color:#F0AD4E}.__react_component_tooltip.type-warning.place-top:after{border-top-color:#F0AD4E;border-top-style:solid;border-top-width:6px}.__react_component_tooltip.type-warning.place-bottom:after{border-bottom-color:#F0AD4E;border-bottom-style:solid;border-bottom-width:6px}.__react_component_tooltip.type-warning.place-left:after{border-left-color:#F0AD4E;border-left-style:solid;border-left-width:6px}.__react_component_tooltip.type-warning.place-right:after{border-right-color:#F0AD4E;border-right-style:solid;border-right-width:6px}.__react_component_tooltip.type-warning.border{border:1px solid #fff}.__react_component_tooltip.type-warning.border.place-top:before{border-top:8px solid #fff}.__react_component_tooltip.type-warning.border.place-bottom:before{border-bottom:8px solid #fff}.__react_component_tooltip.type-warning.border.place-left:before{border-left:8px solid #fff}.__react_component_tooltip.type-warning.border.place-right:before{border-right:8px solid #fff}.__react_component_tooltip.type-error{color:#fff;background-color:#BE6464}.__react_component_tooltip.type-error.place-top:after{border-top-color:#BE6464;border-top-style:solid;border-top-width:6px}.__react_component_tooltip.type-error.place-bottom:after{border-bottom-color:#BE6464;border-bottom-style:solid;border-bottom-width:6px}.__react_component_tooltip.type-error.place-left:after{border-left-color:#BE6464;border-left-style:solid;border-left-width:6px}.__react_component_tooltip.type-error.place-right:after{border-right-color:#BE6464;border-right-style:solid;border-right-width:6px}.__react_component_tooltip.type-error.border{border:1px solid #fff}.__react_component_tooltip.type-error.border.place-top:before{border-top:8px solid #fff}.__react_component_tooltip.type-error.border.place-bottom:before{border-bottom:8px solid #fff}.__react_component_tooltip.type-error.border.place-left:before{border-left:8px solid #fff}.__react_component_tooltip.type-error.border.place-right:before{border-right:8px solid #fff}.__react_component_tooltip.type-info{color:#fff;background-color:#337AB7}.__react_component_tooltip.type-info.place-top:after{border-top-color:#337AB7;border-top-style:solid;border-top-width:6px}.__react_component_tooltip.type-info.place-bottom:after{border-bottom-color:#337AB7;border-bottom-style:solid;border-bottom-width:6px}.__react_component_tooltip.type-info.place-left:after{border-left-color:#337AB7;border-left-style:solid;border-left-width:6px}.__react_component_tooltip.type-info.place-right:after{border-right-color:#337AB7;border-right-style:solid;border-right-width:6px}.__react_component_tooltip.type-info.border{border:1px solid #fff}.__react_component_tooltip.type-info.border.place-top:before{border-top:8px solid #fff}.__react_component_tooltip.type-info.border.place-bottom:before{border-bottom:8px solid #fff}.__react_component_tooltip.type-info.border.place-left:before{border-left:8px solid #fff}.__react_component_tooltip.type-info.border.place-right:before{border-right:8px solid #fff}.__react_component_tooltip.type-light{color:#222;background-color:#fff}.__react_component_tooltip.type-light.place-top:after{border-top-color:#fff;border-top-style:solid;border-top-width:6px}.__react_component_tooltip.type-light.place-bottom:after{border-bottom-color:#fff;border-bottom-style:solid;border-bottom-width:6px}.__react_component_tooltip.type-light.place-left:after{border-left-color:#fff;border-left-style:solid;border-left-width:6px}.__react_component_tooltip.type-light.place-right:after{border-right-color:#fff;border-right-style:solid;border-right-width:6px}.__react_component_tooltip.type-light.border{border:1px solid #222}.__react_component_tooltip.type-light.border.place-top:before{border-top:8px solid #222}.__react_component_tooltip.type-light.border.place-bottom:before{border-bottom:8px solid #222}.__react_component_tooltip.type-light.border.place-left:before{border-left:8px solid #222}.__react_component_tooltip.type-light.border.place-right:before{border-right:8px solid #222}.__react_component_tooltip.place-top{margin-top:-10px}.__react_component_tooltip.place-top:before{border-left:10px solid transparent;border-right:10px solid transparent;bottom:-8px;left:50%;margin-left:-10px}.__react_component_tooltip.place-top:after{border-left:8px solid transparent;border-right:8px solid transparent;bottom:-6px;left:50%;margin-left:-8px}.__react_component_tooltip.place-bottom{margin-top:10px}.__react_component_tooltip.place-bottom:before{border-left:10px solid transparent;border-right:10px solid transparent;top:-8px;left:50%;margin-left:-10px}.__react_component_tooltip.place-bottom:after{border-left:8px solid transparent;border-right:8px solid transparent;top:-6px;left:50%;margin-left:-8px}.__react_component_tooltip.place-left{margin-left:-10px}.__react_component_tooltip.place-left:before{border-top:6px solid transparent;border-bottom:6px solid transparent;right:-8px;top:50%;margin-top:-5px}.__react_component_tooltip.place-left:after{border-top:5px solid transparent;border-bottom:5px solid transparent;right:-6px;top:50%;margin-top:-4px}.__react_component_tooltip.place-right{margin-left:10px}.__react_component_tooltip.place-right:before{border-top:6px solid transparent;border-bottom:6px solid transparent;left:-8px;top:50%;margin-top:-5px}.__react_component_tooltip.place-right:after{border-top:5px solid transparent;border-bottom:5px solid transparent;left:-6px;top:50%;margin-top:-4px}.__react_component_tooltip .multi-line{display:block;padding:2px 0px;text-align:center}
        
CIC Dashboard




















        




	window.LANGUAGE_INFO={
    	CURRENT_LANGUAGE : &quot; , &quot;'&quot; , &quot;en&quot; , &quot;'&quot; , &quot;,
    	AUTHOR_MODE: true,
        PUBLISH_RUN_MODE: true
    };






    







    




























    #katalon{font-family:monospace;font-size:13px;background-color:rgba(0,0,0,.7);position:fixed;top:0;left:0;right:0;display:block;z-index:999999999;line-height: normal} #katalon div{padding:0;margin:0;color:#fff;} #katalon kbd{display:inline-block;padding:3px 5px;font:13px Consolas,&quot;Liberation Mono&quot;,Menlo,Courier,monospace;line-height:10px;color:#555;vertical-align:middle;background-color:#fcfcfc;border:1px solid #ccc;border-bottom-color:#bbb;border-radius:3px;box-shadow:inset 0 -1px 0 #bbb;font-weight: bold} div#katalon-spy_elementInfoDiv {color: lightblue; padding: 0px 5px 5px} div#katalon-spy_instructionDiv {padding: 5px 5px 2.5px}
    
        
        



    clearRelease contactAre you sure you want to release contact?Confirm CancelHi, SHARIDASV account_circleclearYou&quot; , &quot;'&quot; , &quot;re about to log outAre you sure you want to log out now?Confirm Cancel
    
    
    
    
    





    
    




    
        if(window.createIntlProvider) {
            var value = {&quot;style&quot;:&quot;store-header&quot;,&quot;logo&quot;:{&quot;title&quot;:&quot;Maxis Home&quot;,&quot;logoPath&quot;:&quot;/content/dam/next1bit/images/icons/svgs/Maxis_Logo_Color.svg&quot;,&quot;targetUrl&quot;:&quot;/content/maxis/en/retailer-channel/search-load-customer.html&quot;,&quot;newTab&quot;:false,&quot;alt&quot;:&quot;Maxis Home&quot;},&quot;signInOutModel&quot;:{&quot;personIcon&quot;:&quot;account_circle&quot;,&quot;signInLabel&quot;:&quot;Sign In&quot;,&quot;menuOptions&quot;:[{&quot;label&quot;:&quot;Sign Out&quot;,&quot;newTab&quot;:false,&quot;targetUrl&quot;:&quot;/content/maxis/en/retailer-channel/login.html&quot;,&quot;title&quot;:null,&quot;id&quot;:&quot;logout&quot;}],&quot;loginPageUrl&quot;:&quot;/content/maxis/en/retailer-channel/login.html&quot;,&quot;sessionTimeOutSeconds&quot;:1200},&quot;messages&quot;:{&quot;RetailerHeader_logoutConfirmLabel&quot;:&quot;You&quot; , &quot;'&quot; , &quot;re about to log out&quot;,&quot;RetailerHeader_confirmLabel&quot;:&quot;Confirm&quot;,&quot;RetailerHeader_confirmMessageLabel&quot;:&quot;Are you sure you want to log out now?&quot;,&quot;RetailerHeader_cancelLabel&quot;:&quot;Cancel&quot;,&quot;RetailerHeader_userName&quot;:&quot;Hi, {name}&quot;}};
            window.createIntlProvider(&quot; , &quot;'&quot; , &quot;HeaderRetailer&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;en&quot; , &quot;'&quot; , &quot;, {data: value}, &quot;header-container&quot;);
        }
    




	
        
			

    
    



dashboardRetailer Dashboardsupervisor_accountCIC DashboardRegister InterestEnterprise Case ChannelAstro Micrositedevices_otherBrowse CatalogstorageOrder DashboardstoreStore InventoryMAXIS CENTRE AEON KUCHING(CNTRL.00030 | 10030)clearRelease contactAre you sure you want to release contact?Confirm Cancel



    (function(loadWidget){
        var domPlaceHolder =  document.querySelector(&quot; , &quot;'&quot; , &quot;[data-uxfwidget-id=&quot;digitalretailersideb&quot;]&quot; , &quot;'&quot; , &quot;);
        console.log(&quot;widget props&quot;, {&quot;widgetId&quot;:&quot;digital-retailer-sidebar-retail-l9-module&quot;,&quot;messages&quot;:{&quot;releaseContactConfirmBtnLabel&quot;:&quot;Confirm&quot;,&quot;releaseContactCancelLabel&quot;:&quot;Cancel&quot;,&quot;storeInventoryLabel&quot;:&quot;Store Inventory&quot;,&quot;releaseContactLabel&quot;:&quot;Release contact&quot;,&quot;cicDashboardLabel&quot;:&quot;CIC Dashboard&quot;,&quot;enterpriseURL&quot;:&quot;https://api-digital-uat3.isddc.men.maxis.com.my/telesales/api/v4.0/telesales/redirect/telesalesCase&quot;,&quot;retailerDashboardLabel&quot;:&quot;Retailer Dashboard&quot;,&quot;leadsMicrositeURL&quot;:&quot;https://api-digital-uat3.isddc.men.maxis.com.my/leads/api/v1.0/leads/redirect/dashboard&quot;,&quot;releaseContactConfirmMessage&quot;:&quot;Are you sure you want to release contact?&quot;,&quot;browseCatalogLabel&quot;:&quot;Browse Catalog&quot;,&quot;leadsMicrositeLabel&quot;:&quot;Register Interest&quot;,&quot;astroMicrositeURL&quot;:&quot;https://api-digital-uat3.isddc.men.maxis.com.my/telesales/api/v4.0/telesales/redirect/telesalesAstro&quot;,&quot;enterpriseLabel&quot;:&quot;Enterprise Case Channel&quot;,&quot;orderDashboardLabel&quot;:&quot;Order Dashboard&quot;,&quot;astroMicrositeLabel&quot;:&quot;Astro Microsite&quot;},&quot;outcomes&quot;:{&quot;NAVIGATE_TO_STORE_INVENTORY&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/stock-inventory&quot;},&quot;NAVIGATE_TO_RETAILER_DASHBOARD&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/search-load-customer&quot;},&quot;NAVIGATE_TO_BROWSE_CATALOG&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/catalog.html?flowType=COP&quot;},&quot;NAVIGATE_TO_CIC_DASHBOARD&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/cic&quot;},&quot;NAVIGATE_TO_ORDER_DASHBOARD&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/order-dashboard&quot;}},&quot;config&quot;:{&quot;displayLeadsMicrosite&quot;:true},&quot;contextId&quot;:&quot;digitalretailersideb&quot;});
		var instanceId;
        
        loadWidget(&quot;digital-retailer-sidebar-retail-l9-module&quot;, instanceId, domPlaceHolder, {&quot;widgetId&quot;:&quot;digital-retailer-sidebar-retail-l9-module&quot;,&quot;messages&quot;:{&quot;releaseContactConfirmBtnLabel&quot;:&quot;Confirm&quot;,&quot;releaseContactCancelLabel&quot;:&quot;Cancel&quot;,&quot;storeInventoryLabel&quot;:&quot;Store Inventory&quot;,&quot;releaseContactLabel&quot;:&quot;Release contact&quot;,&quot;cicDashboardLabel&quot;:&quot;CIC Dashboard&quot;,&quot;enterpriseURL&quot;:&quot;https://api-digital-uat3.isddc.men.maxis.com.my/telesales/api/v4.0/telesales/redirect/telesalesCase&quot;,&quot;retailerDashboardLabel&quot;:&quot;Retailer Dashboard&quot;,&quot;leadsMicrositeURL&quot;:&quot;https://api-digital-uat3.isddc.men.maxis.com.my/leads/api/v1.0/leads/redirect/dashboard&quot;,&quot;releaseContactConfirmMessage&quot;:&quot;Are you sure you want to release contact?&quot;,&quot;browseCatalogLabel&quot;:&quot;Browse Catalog&quot;,&quot;leadsMicrositeLabel&quot;:&quot;Register Interest&quot;,&quot;astroMicrositeURL&quot;:&quot;https://api-digital-uat3.isddc.men.maxis.com.my/telesales/api/v4.0/telesales/redirect/telesalesAstro&quot;,&quot;enterpriseLabel&quot;:&quot;Enterprise Case Channel&quot;,&quot;orderDashboardLabel&quot;:&quot;Order Dashboard&quot;,&quot;astroMicrositeLabel&quot;:&quot;Astro Microsite&quot;},&quot;outcomes&quot;:{&quot;NAVIGATE_TO_STORE_INVENTORY&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/stock-inventory&quot;},&quot;NAVIGATE_TO_RETAILER_DASHBOARD&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/search-load-customer&quot;},&quot;NAVIGATE_TO_BROWSE_CATALOG&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/catalog.html?flowType=COP&quot;},&quot;NAVIGATE_TO_CIC_DASHBOARD&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/cic&quot;},&quot;NAVIGATE_TO_ORDER_DASHBOARD&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/order-dashboard&quot;}},&quot;config&quot;:{&quot;displayLeadsMicrosite&quot;:true},&quot;contextId&quot;:&quot;digitalretailersideb&quot;}, {&quot;baseURL&quot;:&quot;/rp-server&quot;,&quot;oldBaseURL&quot;:&quot;http://localhost:5000/&quot;,&quot;useMocks&quot;:false,&quot;singlePageApp&quot;:false,&quot;locale&quot;:&quot;en-US&quot;});
    })(window.amdocs.appBootstrap.loadWidget);



    




        
        
            

    
    







    (function(loadWidget){
        var domPlaceHolder =  document.querySelector(&quot; , &quot;'&quot; , &quot;[data-uxfwidget-id=&quot;digitalgeneralmessag&quot;]&quot; , &quot;'&quot; , &quot;);
        console.log(&quot;widget props&quot;, {&quot;widgetId&quot;:&quot;digital-general-message-handler-l9-module&quot;,&quot;messages&quot;:{&quot;Unauthorized&quot;:&quot;Session Expired. Please re-login&quot;,&quot;Forbidden&quot;:&quot;Error 403: This transaction is not allowed.&quot;,&quot;InternalServerError&quot;:&quot;Something went wrong. Please try again.&quot;,&quot;NotFound&quot;:&quot;Requested resource not found. Please check with system admin&quot;,&quot;BadRequest&quot;:&quot;Something wrong with the inputs. Please check.&quot;,&quot;SystemUnavailable&quot;:&quot;System unavailable. Please try after some time.&quot;},&quot;outcomes&quot;:{&quot;NAVIGATE_TO_LOGIN&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/login&quot;}},&quot;config&quot;:{&quot;loaderImagePath&quot;:&quot;/content/dam/next1bit/images/icons/Loader.gif&quot;},&quot;contextId&quot;:&quot;digitalgeneralmessag&quot;});
		var instanceId;
        
        loadWidget(&quot;digital-general-message-handler-l9-module&quot;, instanceId, domPlaceHolder, {&quot;widgetId&quot;:&quot;digital-general-message-handler-l9-module&quot;,&quot;messages&quot;:{&quot;Unauthorized&quot;:&quot;Session Expired. Please re-login&quot;,&quot;Forbidden&quot;:&quot;Error 403: This transaction is not allowed.&quot;,&quot;InternalServerError&quot;:&quot;Something went wrong. Please try again.&quot;,&quot;NotFound&quot;:&quot;Requested resource not found. Please check with system admin&quot;,&quot;BadRequest&quot;:&quot;Something wrong with the inputs. Please check.&quot;,&quot;SystemUnavailable&quot;:&quot;System unavailable. Please try after some time.&quot;},&quot;outcomes&quot;:{&quot;NAVIGATE_TO_LOGIN&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/login&quot;}},&quot;config&quot;:{&quot;loaderImagePath&quot;:&quot;/content/dam/next1bit/images/icons/Loader.gif&quot;},&quot;contextId&quot;:&quot;digitalgeneralmessag&quot;}, {&quot;baseURL&quot;:&quot;/rp-server&quot;,&quot;oldBaseURL&quot;:&quot;http://localhost:5000/&quot;,&quot;useMocks&quot;:false,&quot;singlePageApp&quot;:false,&quot;locale&quot;:&quot;en-US&quot;});
    })(window.amdocs.appBootstrap.loadWidget);






Farizam  ID: 25Feb22Searched By  ACCOUNT 78494200Maxis One Club: 0NEWperm_contact_calendarcheck_circleYour current age is 91Bio-metric Not Performed.Acc no: 78494200Write off: RCollection Status: NAcc no: 78494416Write off: RCollection Status: Nstarexit_to_appclearRelease contactAre you sure you want to release contact?Confirm Cancel



    (function(loadWidget){
        var domPlaceHolder =  document.querySelector(&quot; , &quot;'&quot; , &quot;[data-uxfwidget-id=&quot;digitalcontactheader&quot;]&quot; , &quot;'&quot; , &quot;);
        console.log(&quot;widget props&quot;, {&quot;widgetId&quot;:&quot;digital-contact-header-l9-module&quot;,&quot;messages&quot;:{&quot;contactHeader_releaseContactLabel&quot;:&quot;Release contact&quot;,&quot;submitedDateMsg&quot;:&quot;Submitted Date: {submitedDate}&quot;,&quot;submittedByMsg&quot;:&quot;Submitted By: {submittedBy}&quot;,&quot;contactHeader_releaseContactCancelLabel&quot;:&quot;Cancel&quot;,&quot;contactHeader_searchLabel&quot;:&quot;Searched By&quot;,&quot;contactHeader_AGENOTIFICATIONMsg&quot;:&quot;Your current age is {age}&quot;,&quot;accountIdLabel&quot;:&quot;Account Id&quot;,&quot;contactHeader_AGEMsg&quot;:&quot;Age Check done.&quot;,&quot;customerNameMsg&quot;:&quot;Customer Name: {customerName}&quot;,&quot;contactHeader_releaseContactConfirmBtnLabel&quot;:&quot;Confirm&quot;,&quot;leadsMicrositeURL&quot;:&quot;https://api-digital-uat3.isddc.men.maxis.com.my/leads/api/v1.0/leads/redirect/register&quot;,&quot;contactHeader_docTypeLabel&quot;:&quot;ID&quot;,&quot;contactHeader_FRAUDMsg&quot;:&quot;Fraudulent customer found&quot;,&quot;contactHeader_NOBIOMETRICMsg&quot;:&quot;Bio-metric Not Performed.&quot;,&quot;contactHeader_LINELIMITMsg&quot;:&quot;Line Check done.&quot;,&quot;contactHeader_releaseContactConfirmMessage&quot;:&quot;Are you sure you want to release contact?&quot;,&quot;contactHeader_BIOMETRICMsg&quot;:&quot;Your BioMetric Validation failed.&quot;,&quot;customerAccountMsg&quot;:&quot;Customer Account: {customerAccount}&quot;},&quot;outcomes&quot;:{&quot;NAVIGATE_TO_UPDATE_PROFILE&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/update-profile&quot;},&quot;NAVIGATE_TO_CART&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/cart&quot;},&quot;BACK_TO_CONTACT_DETAILS&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/cic&quot;},&quot;RELEASE_TO_DASHBOARD&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/search-load-customer&quot;},&quot;RELEASE_TO_MULTIPLE_CONTACT_LIST&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/customerandcontact&quot;}},&quot;config&quot;:{&quot;displayLeadsMicrosite&quot;:true,&quot;skipCartload&quot;:true,&quot;ddmfFeaturesFlagId&quot;:false,&quot;callWriteOff&quot;:true},&quot;contextId&quot;:&quot;digitalcontactheader&quot;});
		var instanceId;
        
        loadWidget(&quot;digital-contact-header-l9-module&quot;, instanceId, domPlaceHolder, {&quot;widgetId&quot;:&quot;digital-contact-header-l9-module&quot;,&quot;messages&quot;:{&quot;contactHeader_releaseContactLabel&quot;:&quot;Release contact&quot;,&quot;submitedDateMsg&quot;:&quot;Submitted Date: {submitedDate}&quot;,&quot;submittedByMsg&quot;:&quot;Submitted By: {submittedBy}&quot;,&quot;contactHeader_releaseContactCancelLabel&quot;:&quot;Cancel&quot;,&quot;contactHeader_searchLabel&quot;:&quot;Searched By&quot;,&quot;contactHeader_AGENOTIFICATIONMsg&quot;:&quot;Your current age is {age}&quot;,&quot;accountIdLabel&quot;:&quot;Account Id&quot;,&quot;contactHeader_AGEMsg&quot;:&quot;Age Check done.&quot;,&quot;customerNameMsg&quot;:&quot;Customer Name: {customerName}&quot;,&quot;contactHeader_releaseContactConfirmBtnLabel&quot;:&quot;Confirm&quot;,&quot;leadsMicrositeURL&quot;:&quot;https://api-digital-uat3.isddc.men.maxis.com.my/leads/api/v1.0/leads/redirect/register&quot;,&quot;contactHeader_docTypeLabel&quot;:&quot;ID&quot;,&quot;contactHeader_FRAUDMsg&quot;:&quot;Fraudulent customer found&quot;,&quot;contactHeader_NOBIOMETRICMsg&quot;:&quot;Bio-metric Not Performed.&quot;,&quot;contactHeader_LINELIMITMsg&quot;:&quot;Line Check done.&quot;,&quot;contactHeader_releaseContactConfirmMessage&quot;:&quot;Are you sure you want to release contact?&quot;,&quot;contactHeader_BIOMETRICMsg&quot;:&quot;Your BioMetric Validation failed.&quot;,&quot;customerAccountMsg&quot;:&quot;Customer Account: {customerAccount}&quot;},&quot;outcomes&quot;:{&quot;NAVIGATE_TO_UPDATE_PROFILE&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/update-profile&quot;},&quot;NAVIGATE_TO_CART&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/cart&quot;},&quot;BACK_TO_CONTACT_DETAILS&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/cic&quot;},&quot;RELEASE_TO_DASHBOARD&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/search-load-customer&quot;},&quot;RELEASE_TO_MULTIPLE_CONTACT_LIST&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/customerandcontact&quot;}},&quot;config&quot;:{&quot;displayLeadsMicrosite&quot;:true,&quot;skipCartload&quot;:true,&quot;ddmfFeaturesFlagId&quot;:false,&quot;callWriteOff&quot;:true},&quot;contextId&quot;:&quot;digitalcontactheader&quot;}, {&quot;baseURL&quot;:&quot;/rp-server&quot;,&quot;oldBaseURL&quot;:&quot;http://localhost:5000/&quot;,&quot;useMocks&quot;:false,&quot;singlePageApp&quot;:false,&quot;locale&quot;:&quot;en-US&quot;});
    })(window.amdocs.appBootstrap.loadWidget);






SPECIAL OFFERS &amp; FLASH DEALS FOR THE DAYWe are sorry! Looks like you have no campaigns available currently.



    (function(loadWidget){
        var domPlaceHolder =  document.querySelector(&quot; , &quot;'&quot; , &quot;[data-uxfwidget-id=&quot;digitalcampaignlistr&quot;]&quot; , &quot;'&quot; , &quot;);
        console.log(&quot;widget props&quot;, {&quot;widgetId&quot;:&quot;digital-campaign-list-retail-l9-module&quot;,&quot;messages&quot;:{&quot;View_More_Button_Label&quot;:&quot;View More&quot;,&quot;Special_Offers_Label&quot;:&quot;SPECIAL OFFERS &amp; FLASH DEALS FOR THE DAY&quot;,&quot;CMS_Offer_Id_Label&quot;:&quot;CMS_Offer ID:&quot;,&quot;Campaign_Status_Label&quot;:&quot;Campaign Status:&quot;,&quot;Fulfillment_Status_Label&quot;:&quot;Fullfilment Status:&quot;,&quot;Campaign_Code_Label&quot;:&quot;Campaign Code:&quot;,&quot;No_Campaigns_Label&quot;:&quot;We are sorry! Looks like you have no campaigns available currently.&quot;},&quot;outcomes&quot;:{&quot;NAVIGATE_TO_CAMPAIGN_SUMMARY&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/campaign-summary&quot;}},&quot;config&quot;:{&quot;campaignListConfig&quot;:5},&quot;contextId&quot;:&quot;digitalcampaignlistr&quot;});
		var instanceId;
        
        loadWidget(&quot;digital-campaign-list-retail-l9-module&quot;, instanceId, domPlaceHolder, {&quot;widgetId&quot;:&quot;digital-campaign-list-retail-l9-module&quot;,&quot;messages&quot;:{&quot;View_More_Button_Label&quot;:&quot;View More&quot;,&quot;Special_Offers_Label&quot;:&quot;SPECIAL OFFERS &amp; FLASH DEALS FOR THE DAY&quot;,&quot;CMS_Offer_Id_Label&quot;:&quot;CMS_Offer ID:&quot;,&quot;Campaign_Status_Label&quot;:&quot;Campaign Status:&quot;,&quot;Fulfillment_Status_Label&quot;:&quot;Fullfilment Status:&quot;,&quot;Campaign_Code_Label&quot;:&quot;Campaign Code:&quot;,&quot;No_Campaigns_Label&quot;:&quot;We are sorry! Looks like you have no campaigns available currently.&quot;},&quot;outcomes&quot;:{&quot;NAVIGATE_TO_CAMPAIGN_SUMMARY&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/campaign-summary&quot;}},&quot;config&quot;:{&quot;campaignListConfig&quot;:5},&quot;contextId&quot;:&quot;digitalcampaignlistr&quot;}, {&quot;baseURL&quot;:&quot;/rp-server&quot;,&quot;oldBaseURL&quot;:&quot;http://localhost:5000/&quot;,&quot;useMocks&quot;:false,&quot;singlePageApp&quot;:false,&quot;locale&quot;:&quot;en-US&quot;});
    })(window.amdocs.appBootstrap.loadWidget);










    (function(loadWidget){
        var domPlaceHolder =  document.querySelector(&quot; , &quot;'&quot; , &quot;[data-uxfwidget-id=&quot;digitalleadmicrosite&quot;]&quot; , &quot;'&quot; , &quot;);
        console.log(&quot;widget props&quot;, {&quot;widgetId&quot;:&quot;digital-lead-microsite-retail-l9-module&quot;,&quot;messages&quot;:{&quot;leadsMicrositeURL&quot;:&quot;https://api-digital-uat3.isddc.men.maxis.com.my/leads/api/v1.0/leads/redirect/landing&quot;,&quot;iFrameHeight&quot;:&quot;240&quot;},&quot;config&quot;:{&quot;displayLeadsMicrosite&quot;:true,&quot;inlineMessages&quot;:false},&quot;contextId&quot;:&quot;digitalleadmicrosite&quot;});
		var instanceId;
        
        loadWidget(&quot;digital-lead-microsite-retail-l9-module&quot;, instanceId, domPlaceHolder, {&quot;widgetId&quot;:&quot;digital-lead-microsite-retail-l9-module&quot;,&quot;messages&quot;:{&quot;leadsMicrositeURL&quot;:&quot;https://api-digital-uat3.isddc.men.maxis.com.my/leads/api/v1.0/leads/redirect/landing&quot;,&quot;iFrameHeight&quot;:&quot;240&quot;},&quot;config&quot;:{&quot;displayLeadsMicrosite&quot;:true,&quot;inlineMessages&quot;:false},&quot;contextId&quot;:&quot;digitalleadmicrosite&quot;}, {&quot;baseURL&quot;:&quot;/rp-server&quot;,&quot;oldBaseURL&quot;:&quot;http://localhost:5000/&quot;,&quot;useMocks&quot;:false,&quot;singlePageApp&quot;:false,&quot;locale&quot;:&quot;en-US&quot;});
    })(window.amdocs.appBootstrap.loadWidget);






Contracts Summary02Others Contract02Zerolution Fibre ContractSubscriptions Summary02VOIP02Fibre



    (function(loadWidget){
        var domPlaceHolder =  document.querySelector(&quot; , &quot;'&quot; , &quot;[data-uxfwidget-id=&quot;digitalcontractssubs&quot;]&quot; , &quot;'&quot; , &quot;);
        console.log(&quot;widget props&quot;, {&quot;widgetId&quot;:&quot;digital-contracts-subscriptions-summary-retail-l9-module&quot;,&quot;messages&quot;:{&quot;contractsSummaryErrorLabel&quot;:&quot;There are no active contracts&quot;,&quot;subscriptionsSummaryErrorLabel&quot;:&quot;No Subscriptions Present&quot;,&quot;subscriptionsSummaryLabel&quot;:&quot;Subscriptions Summary&quot;,&quot;contractsSummaryLabel&quot;:&quot;Contracts Summary&quot;},&quot;config&quot;:{&quot;inlineMessages&quot;:false},&quot;contextId&quot;:&quot;digitalcontractssubs&quot;});
		var instanceId;
        
        loadWidget(&quot;digital-contracts-subscriptions-summary-retail-l9-module&quot;, instanceId, domPlaceHolder, {&quot;widgetId&quot;:&quot;digital-contracts-subscriptions-summary-retail-l9-module&quot;,&quot;messages&quot;:{&quot;contractsSummaryErrorLabel&quot;:&quot;There are no active contracts&quot;,&quot;subscriptionsSummaryErrorLabel&quot;:&quot;No Subscriptions Present&quot;,&quot;subscriptionsSummaryLabel&quot;:&quot;Subscriptions Summary&quot;,&quot;contractsSummaryLabel&quot;:&quot;Contracts Summary&quot;},&quot;config&quot;:{&quot;inlineMessages&quot;:false},&quot;contextId&quot;:&quot;digitalcontractssubs&quot;}, {&quot;baseURL&quot;:&quot;/rp-server&quot;,&quot;oldBaseURL&quot;:&quot;http://localhost:5000/&quot;,&quot;useMocks&quot;:false,&quot;singlePageApp&quot;:false,&quot;locale&quot;:&quot;en-US&quot;});
    })(window.amdocs.appBootstrap.loadWidget);






New Purchase Starter Pack Registration Switch To Maxis Create Interaction View Interaction Biz Individual Registration Change Ownership It seems there is a pending orderPlease submit or cancel the order before proceeding.OK 



    (function(loadWidget){
        var domPlaceHolder =  document.querySelector(&quot; , &quot;'&quot; , &quot;[data-uxfwidget-id=&quot;digitalcustomernavig&quot;]&quot; , &quot;'&quot; , &quot;);
        console.log(&quot;widget props&quot;, {&quot;widgetId&quot;:&quot;digital-customer-navigation-panel-retail-l9-module&quot;,&quot;messages&quot;:{&quot;ViewInteraction&quot;:&quot;View Interaction&quot;,&quot;postToPreSimOption&quot;:&quot;How do you want to proceed ?&quot;,&quot;continueNewSim&quot;:&quot;Continue With New Sim&quot;,&quot;okButtonRetailer&quot;:&quot;OK&quot;,&quot;msgLine1ForRetailer&quot;:&quot;Eligibility popup Msg line 1 for retailer&quot;,&quot;PostpaidToPrepaid&quot;:&quot;Postpaid To Prepaid&quot;,&quot;CreateInteraction&quot;:&quot;Create Interaction&quot;,&quot;SwitchToMaxis&quot;:&quot;Switch To Maxis&quot;,&quot;continueOldSim&quot;:&quot;Continue With Old Sim&quot;,&quot;BizIndividualRegistration&quot;:&quot;Biz Individual Registration&quot;,&quot;CustomerOrders&quot;:&quot;Customers Orders&quot;,&quot;msgLine2ForRetailer&quot;:&quot;Please submit or cancel the order before proceeding.&quot;,&quot;FinancialTransaction&quot;:&quot;Financial Transaction&quot;,&quot;Cart&quot;:&quot;Cart&quot;,&quot;StarterPackRegistration&quot;:&quot;Starter Pack Registration&quot;,&quot;Case&quot;:&quot;Case&quot;,&quot;NewPurchase&quot;:&quot;New Purchase&quot;},&quot;outcomes&quot;:{&quot;BIZ_INDIVIDUAL_REGISTRATION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/bizindividual&quot;},&quot;VIEW_INTERACTION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/interaction-list&quot;},&quot;CHANGE_OWNERSHIP_TARGET_CREATE_CUSTOMER&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/target-create-customer.html?flowType=changeOwnership&quot;},&quot;NAVIGATE_TO_PLAN_LIST&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/plan-list&quot;},&quot;NEW_PURCHASE&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/catalog.html?flowType=COP&quot;},&quot;CUSTOMER_ORDERS&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/order-dashboard&quot;},&quot;STARTER_PACK_REGISTRATION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/starter-pack&quot;},&quot;VIEW_CASE&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/view-cases&quot;},&quot;CHANGE_OWNERSHIP_TARGET&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/change-ownership-target&quot;},&quot;CREATE_INTERACTION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/create-interaction&quot;},&quot;NAVIGATE_TO_FINANCIAL_TRANSACTION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/financial-transaction&quot;},&quot;CART&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/cart&quot;}},&quot;config&quot;:{&quot;customerTypeCheckbox&quot;:&quot;false&quot;},&quot;initProps&quot;:{&quot;customerType&quot;:&quot;customerContext&quot;},&quot;contextId&quot;:&quot;digitalcustomernavig&quot;});
		var instanceId;
        
        loadWidget(&quot;digital-customer-navigation-panel-retail-l9-module&quot;, instanceId, domPlaceHolder, {&quot;widgetId&quot;:&quot;digital-customer-navigation-panel-retail-l9-module&quot;,&quot;messages&quot;:{&quot;ViewInteraction&quot;:&quot;View Interaction&quot;,&quot;postToPreSimOption&quot;:&quot;How do you want to proceed ?&quot;,&quot;continueNewSim&quot;:&quot;Continue With New Sim&quot;,&quot;okButtonRetailer&quot;:&quot;OK&quot;,&quot;msgLine1ForRetailer&quot;:&quot;Eligibility popup Msg line 1 for retailer&quot;,&quot;PostpaidToPrepaid&quot;:&quot;Postpaid To Prepaid&quot;,&quot;CreateInteraction&quot;:&quot;Create Interaction&quot;,&quot;SwitchToMaxis&quot;:&quot;Switch To Maxis&quot;,&quot;continueOldSim&quot;:&quot;Continue With Old Sim&quot;,&quot;BizIndividualRegistration&quot;:&quot;Biz Individual Registration&quot;,&quot;CustomerOrders&quot;:&quot;Customers Orders&quot;,&quot;msgLine2ForRetailer&quot;:&quot;Please submit or cancel the order before proceeding.&quot;,&quot;FinancialTransaction&quot;:&quot;Financial Transaction&quot;,&quot;Cart&quot;:&quot;Cart&quot;,&quot;StarterPackRegistration&quot;:&quot;Starter Pack Registration&quot;,&quot;Case&quot;:&quot;Case&quot;,&quot;NewPurchase&quot;:&quot;New Purchase&quot;},&quot;outcomes&quot;:{&quot;BIZ_INDIVIDUAL_REGISTRATION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/bizindividual&quot;},&quot;VIEW_INTERACTION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/interaction-list&quot;},&quot;CHANGE_OWNERSHIP_TARGET_CREATE_CUSTOMER&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/target-create-customer.html?flowType=changeOwnership&quot;},&quot;NAVIGATE_TO_PLAN_LIST&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/plan-list&quot;},&quot;NEW_PURCHASE&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/catalog.html?flowType=COP&quot;},&quot;CUSTOMER_ORDERS&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/order-dashboard&quot;},&quot;STARTER_PACK_REGISTRATION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/starter-pack&quot;},&quot;VIEW_CASE&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/view-cases&quot;},&quot;CHANGE_OWNERSHIP_TARGET&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/change-ownership-target&quot;},&quot;CREATE_INTERACTION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/create-interaction&quot;},&quot;NAVIGATE_TO_FINANCIAL_TRANSACTION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/financial-transaction&quot;},&quot;CART&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/cart&quot;}},&quot;config&quot;:{&quot;customerTypeCheckbox&quot;:&quot;false&quot;},&quot;initProps&quot;:{&quot;customerType&quot;:&quot;customerContext&quot;},&quot;contextId&quot;:&quot;digitalcustomernavig&quot;}, {&quot;baseURL&quot;:&quot;/rp-server&quot;,&quot;oldBaseURL&quot;:&quot;http://localhost:5000/&quot;,&quot;useMocks&quot;:false,&quot;singlePageApp&quot;:false,&quot;locale&quot;:&quot;en-US&quot;});
    })(window.amdocs.appBootstrap.loadWidget);






Account 78494200expand_lessTypeConsumerSub-TypeMaxis IndividualSegmentNONECollection StatusNormalWrite-OffNoBilling ProfileManageDirect DebitRegister NowTotal Amount DueRM 0.00Pay Before -View Bill A57400Maxis Fibre - 300MbpsShow DetailsActiveSince 25 Feb 2022keyboard_arrow_right60395459722Maxis Fibre - VoiceShow DetailsActiveSince 25 Feb 2022keyboard_arrow_rightNew Purchase Switch To Maxis Case Customers Orders Financial Transaction Cart Change Ownership It seems there is a pending orderPlease submit or cancel the order before proceeding.OK It seems there is a pending orderPlease submit or cancel the order before proceeding.OK Account 78494416expand_moreTypeConsumerSub-TypeMaxis IndividualSegmentNONECollection StatusNormalWrite-OffNoBilling ProfileManageDirect DebitRegister NowTotal Amount DueRM 0.00Pay Before -View Bill New Purchase Switch To Maxis Case Customers Orders Financial Transaction Cart Change Ownership It seems there is a pending orderPlease submit or cancel the order before proceeding.OK It seems there is a pending orderPlease submit or cancel the order before proceeding.OK 



    (function(loadWidget){
        var domPlaceHolder =  document.querySelector(&quot; , &quot;'&quot; , &quot;[data-uxfwidget-id=&quot;digitalaccountlistl9&quot;]&quot; , &quot;'&quot; , &quot;);
        console.log(&quot;widget props&quot;, {&quot;widgetId&quot;:&quot;digital-account-list-l9-module&quot;,&quot;messages&quot;:{&quot;accountDetails_directDebit&quot;:&quot;Direct Debit&quot;,&quot;accountDetails_cancelledAccount&quot;:&quot;Cancelled Account&quot;,&quot;ViewInteraction&quot;:&quot;View Interaction&quot;,&quot;selectSubscription_noProductsMessage&quot;:&quot;No Products Assigned&quot;,&quot;postToPreSimOption&quot;:&quot;How do you want to proceed ?&quot;,&quot;selectSubscription_active&quot;:&quot;Active&quot;,&quot;continueNewSim&quot;:&quot;Continue With New Sim&quot;,&quot;showCancelAccountText&quot;:&quot;View Cancelled Account&quot;,&quot;okButtonRetailer&quot;:&quot;OK&quot;,&quot;msgLine1ForRetailer&quot;:&quot;Eligibility popup Msg line 1 for retailer&quot;,&quot;PostpaidToPrepaid&quot;:&quot;Postpaid To Prepaid&quot;,&quot;accountDetails_postPaidAccount&quot;:&quot;Account {billingProfileId}&quot;,&quot;CreateInteraction&quot;:&quot;Create Interaction&quot;,&quot;SwitchToMaxis&quot;:&quot;Switch To Maxis&quot;,&quot;accountDetails_writeOff&quot;:&quot;Write-Off&quot;,&quot;accountDetails_billingProfile&quot;:&quot;Billing Profile&quot;,&quot;continueOldSim&quot;:&quot;Continue With Old Sim&quot;,&quot;BizIndividualRegistration&quot;:&quot;Biz Individual Registration&quot;,&quot;accountDetails_typeText&quot;:&quot;Type&quot;,&quot;selectSubscription_suspended&quot;:&quot;Suspended&quot;,&quot;CustomerOrders&quot;:&quot;Customers Orders&quot;,&quot;msgLine2ForRetailer&quot;:&quot;Please submit or cancel the order before proceeding.&quot;,&quot;accountDetails_registerNow&quot;:&quot;Register Now&quot;,&quot;accountDetails_subType&quot;:&quot;Sub-Type&quot;,&quot;accountDetails_brn&quot;:&quot;BRN&quot;,&quot;FinancialTransaction&quot;:&quot;Financial Transaction&quot;,&quot;Cart&quot;:&quot;Cart&quot;,&quot;StarterPackRegistration&quot;:&quot;Starter Pack Registration&quot;,&quot;accountDetails_manage&quot;:&quot;Manage&quot;,&quot;Case&quot;:&quot;Case&quot;,&quot;accountDetails_collectionStatus&quot;:&quot;Collection Status&quot;,&quot;selectSubscription_since&quot;:&quot;Since {date}&quot;,&quot;accountDetails_prepaidAccount&quot;:&quot;Prepaid Account&quot;,&quot;accountDetails_primitiveAccount&quot;:&quot;Primitive Account&quot;,&quot;NewPurchase&quot;:&quot;New Purchase&quot;},&quot;outcomes&quot;:{&quot;BIZ_INDIVIDUAL_REGISTRATION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/bizindividual&quot;},&quot;VIEW_INTERACTION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/interaction-list&quot;},&quot;NAVIGATE_TO_UPDATE_BILLING_PROFILE&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/update-billing-profile&quot;},&quot;NAVIGATE_TO_PLAN_LIST&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/catalog&quot;},&quot;POSTPAID_TO_PREPAID&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/postpaid to prepaid&quot;},&quot;NAVIGATE_TO_ORDER_SUMMARY&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/order-summary&quot;},&quot;SET_SUBSCRIPTION_OPTION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/balance-and-services&quot;},&quot;NEW_PURCHASE&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/catalog.html?flowType=COP&quot;},&quot;NAVIGATE_TO_BILL_SUMMARY&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/bill-summary&quot;},&quot;CUSTOMER_ORDERS&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/order-dashboard&quot;},&quot;STARTER_PACK_REGISTRATION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/starter-pack&quot;},&quot;SET_SUBSCRIPTION_OPTION_PREPAID&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/prepaid-balance-services&quot;},&quot;CHANGE_OWNERSHIP&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/change-ownership-source&quot;},&quot;SET_SUBSCRIPTION_OPTION_POSTPAID&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/postpaid-balance-services&quot;},&quot;SET_SUBSCRIPTION_OPTION_VOIP&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/fixedVoice-subscription-overview&quot;},&quot;SET_SUBSCRIPTION_OPTION_FTTH&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/fibre-subscription-overview&quot;},&quot;NAVIGATE_TO_MANAGE_AUTOPAY&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/manage-auto-pay&quot;},&quot;VIEW_CASE&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/view-cases&quot;},&quot;SWITCH_TO_MAXIS&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/catalog&quot;},&quot;CREATE_INTERACTION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/create-interaction&quot;},&quot;NAVIGATE_TO_CAMPAIGN_SUMMARY&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/campaign-summary&quot;},&quot;NAVIGATE_TO_FINANCIAL_TRANSACTION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/financial-transaction&quot;},&quot;CART&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/cart&quot;}},&quot;contextId&quot;:&quot;digitalaccountlistl9&quot;});
		var instanceId;
        
        loadWidget(&quot;digital-account-list-l9-module&quot;, instanceId, domPlaceHolder, {&quot;widgetId&quot;:&quot;digital-account-list-l9-module&quot;,&quot;messages&quot;:{&quot;accountDetails_directDebit&quot;:&quot;Direct Debit&quot;,&quot;accountDetails_cancelledAccount&quot;:&quot;Cancelled Account&quot;,&quot;ViewInteraction&quot;:&quot;View Interaction&quot;,&quot;selectSubscription_noProductsMessage&quot;:&quot;No Products Assigned&quot;,&quot;postToPreSimOption&quot;:&quot;How do you want to proceed ?&quot;,&quot;selectSubscription_active&quot;:&quot;Active&quot;,&quot;continueNewSim&quot;:&quot;Continue With New Sim&quot;,&quot;showCancelAccountText&quot;:&quot;View Cancelled Account&quot;,&quot;okButtonRetailer&quot;:&quot;OK&quot;,&quot;msgLine1ForRetailer&quot;:&quot;Eligibility popup Msg line 1 for retailer&quot;,&quot;PostpaidToPrepaid&quot;:&quot;Postpaid To Prepaid&quot;,&quot;accountDetails_postPaidAccount&quot;:&quot;Account {billingProfileId}&quot;,&quot;CreateInteraction&quot;:&quot;Create Interaction&quot;,&quot;SwitchToMaxis&quot;:&quot;Switch To Maxis&quot;,&quot;accountDetails_writeOff&quot;:&quot;Write-Off&quot;,&quot;accountDetails_billingProfile&quot;:&quot;Billing Profile&quot;,&quot;continueOldSim&quot;:&quot;Continue With Old Sim&quot;,&quot;BizIndividualRegistration&quot;:&quot;Biz Individual Registration&quot;,&quot;accountDetails_typeText&quot;:&quot;Type&quot;,&quot;selectSubscription_suspended&quot;:&quot;Suspended&quot;,&quot;CustomerOrders&quot;:&quot;Customers Orders&quot;,&quot;msgLine2ForRetailer&quot;:&quot;Please submit or cancel the order before proceeding.&quot;,&quot;accountDetails_registerNow&quot;:&quot;Register Now&quot;,&quot;accountDetails_subType&quot;:&quot;Sub-Type&quot;,&quot;accountDetails_brn&quot;:&quot;BRN&quot;,&quot;FinancialTransaction&quot;:&quot;Financial Transaction&quot;,&quot;Cart&quot;:&quot;Cart&quot;,&quot;StarterPackRegistration&quot;:&quot;Starter Pack Registration&quot;,&quot;accountDetails_manage&quot;:&quot;Manage&quot;,&quot;Case&quot;:&quot;Case&quot;,&quot;accountDetails_collectionStatus&quot;:&quot;Collection Status&quot;,&quot;selectSubscription_since&quot;:&quot;Since {date}&quot;,&quot;accountDetails_prepaidAccount&quot;:&quot;Prepaid Account&quot;,&quot;accountDetails_primitiveAccount&quot;:&quot;Primitive Account&quot;,&quot;NewPurchase&quot;:&quot;New Purchase&quot;},&quot;outcomes&quot;:{&quot;BIZ_INDIVIDUAL_REGISTRATION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/bizindividual&quot;},&quot;VIEW_INTERACTION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/interaction-list&quot;},&quot;NAVIGATE_TO_UPDATE_BILLING_PROFILE&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/update-billing-profile&quot;},&quot;NAVIGATE_TO_PLAN_LIST&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/catalog&quot;},&quot;POSTPAID_TO_PREPAID&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/postpaid to prepaid&quot;},&quot;NAVIGATE_TO_ORDER_SUMMARY&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/order-summary&quot;},&quot;SET_SUBSCRIPTION_OPTION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/balance-and-services&quot;},&quot;NEW_PURCHASE&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/catalog.html?flowType=COP&quot;},&quot;NAVIGATE_TO_BILL_SUMMARY&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/bill-summary&quot;},&quot;CUSTOMER_ORDERS&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/order-dashboard&quot;},&quot;STARTER_PACK_REGISTRATION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/starter-pack&quot;},&quot;SET_SUBSCRIPTION_OPTION_PREPAID&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/prepaid-balance-services&quot;},&quot;CHANGE_OWNERSHIP&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/change-ownership-source&quot;},&quot;SET_SUBSCRIPTION_OPTION_POSTPAID&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/postpaid-balance-services&quot;},&quot;SET_SUBSCRIPTION_OPTION_VOIP&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/fixedVoice-subscription-overview&quot;},&quot;SET_SUBSCRIPTION_OPTION_FTTH&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/fibre-subscription-overview&quot;},&quot;NAVIGATE_TO_MANAGE_AUTOPAY&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/manage-auto-pay&quot;},&quot;VIEW_CASE&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/view-cases&quot;},&quot;SWITCH_TO_MAXIS&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/catalog&quot;},&quot;CREATE_INTERACTION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/create-interaction&quot;},&quot;NAVIGATE_TO_CAMPAIGN_SUMMARY&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/campaign-summary&quot;},&quot;NAVIGATE_TO_FINANCIAL_TRANSACTION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/financial-transaction&quot;},&quot;CART&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/cart&quot;}},&quot;contextId&quot;:&quot;digitalaccountlistl9&quot;}, {&quot;baseURL&quot;:&quot;/rp-server&quot;,&quot;oldBaseURL&quot;:&quot;http://localhost:5000/&quot;,&quot;useMocks&quot;:false,&quot;singlePageApp&quot;:false,&quot;locale&quot;:&quot;en-US&quot;});
    })(window.amdocs.appBootstrap.loadWidget);



    




        
	


        




    

/html[1]&quot;) or . = concat(&quot;.__react_component_tooltip{border-radius:3px;display:inline-block;font-size:13px;left:-999em;opacity:0;padding:8px 21px;position:fixed;pointer-events:none;transition:opacity 0.3s ease-out;top:-999em;visibility:hidden;z-index:999}.__react_component_tooltip.allow_hover,.__react_component_tooltip.allow_click{pointer-events:auto}.__react_component_tooltip:before,.__react_component_tooltip:after{content:&quot;&quot;;width:0;height:0;position:absolute}.__react_component_tooltip.show{opacity:0.9;margin-top:0px;margin-left:0px;visibility:visible}.__react_component_tooltip.type-dark{color:#fff;background-color:#222}.__react_component_tooltip.type-dark.place-top:after{border-top-color:#222;border-top-style:solid;border-top-width:6px}.__react_component_tooltip.type-dark.place-bottom:after{border-bottom-color:#222;border-bottom-style:solid;border-bottom-width:6px}.__react_component_tooltip.type-dark.place-left:after{border-left-color:#222;border-left-style:solid;border-left-width:6px}.__react_component_tooltip.type-dark.place-right:after{border-right-color:#222;border-right-style:solid;border-right-width:6px}.__react_component_tooltip.type-dark.border{border:1px solid #fff}.__react_component_tooltip.type-dark.border.place-top:before{border-top:8px solid #fff}.__react_component_tooltip.type-dark.border.place-bottom:before{border-bottom:8px solid #fff}.__react_component_tooltip.type-dark.border.place-left:before{border-left:8px solid #fff}.__react_component_tooltip.type-dark.border.place-right:before{border-right:8px solid #fff}.__react_component_tooltip.type-success{color:#fff;background-color:#8DC572}.__react_component_tooltip.type-success.place-top:after{border-top-color:#8DC572;border-top-style:solid;border-top-width:6px}.__react_component_tooltip.type-success.place-bottom:after{border-bottom-color:#8DC572;border-bottom-style:solid;border-bottom-width:6px}.__react_component_tooltip.type-success.place-left:after{border-left-color:#8DC572;border-left-style:solid;border-left-width:6px}.__react_component_tooltip.type-success.place-right:after{border-right-color:#8DC572;border-right-style:solid;border-right-width:6px}.__react_component_tooltip.type-success.border{border:1px solid #fff}.__react_component_tooltip.type-success.border.place-top:before{border-top:8px solid #fff}.__react_component_tooltip.type-success.border.place-bottom:before{border-bottom:8px solid #fff}.__react_component_tooltip.type-success.border.place-left:before{border-left:8px solid #fff}.__react_component_tooltip.type-success.border.place-right:before{border-right:8px solid #fff}.__react_component_tooltip.type-warning{color:#fff;background-color:#F0AD4E}.__react_component_tooltip.type-warning.place-top:after{border-top-color:#F0AD4E;border-top-style:solid;border-top-width:6px}.__react_component_tooltip.type-warning.place-bottom:after{border-bottom-color:#F0AD4E;border-bottom-style:solid;border-bottom-width:6px}.__react_component_tooltip.type-warning.place-left:after{border-left-color:#F0AD4E;border-left-style:solid;border-left-width:6px}.__react_component_tooltip.type-warning.place-right:after{border-right-color:#F0AD4E;border-right-style:solid;border-right-width:6px}.__react_component_tooltip.type-warning.border{border:1px solid #fff}.__react_component_tooltip.type-warning.border.place-top:before{border-top:8px solid #fff}.__react_component_tooltip.type-warning.border.place-bottom:before{border-bottom:8px solid #fff}.__react_component_tooltip.type-warning.border.place-left:before{border-left:8px solid #fff}.__react_component_tooltip.type-warning.border.place-right:before{border-right:8px solid #fff}.__react_component_tooltip.type-error{color:#fff;background-color:#BE6464}.__react_component_tooltip.type-error.place-top:after{border-top-color:#BE6464;border-top-style:solid;border-top-width:6px}.__react_component_tooltip.type-error.place-bottom:after{border-bottom-color:#BE6464;border-bottom-style:solid;border-bottom-width:6px}.__react_component_tooltip.type-error.place-left:after{border-left-color:#BE6464;border-left-style:solid;border-left-width:6px}.__react_component_tooltip.type-error.place-right:after{border-right-color:#BE6464;border-right-style:solid;border-right-width:6px}.__react_component_tooltip.type-error.border{border:1px solid #fff}.__react_component_tooltip.type-error.border.place-top:before{border-top:8px solid #fff}.__react_component_tooltip.type-error.border.place-bottom:before{border-bottom:8px solid #fff}.__react_component_tooltip.type-error.border.place-left:before{border-left:8px solid #fff}.__react_component_tooltip.type-error.border.place-right:before{border-right:8px solid #fff}.__react_component_tooltip.type-info{color:#fff;background-color:#337AB7}.__react_component_tooltip.type-info.place-top:after{border-top-color:#337AB7;border-top-style:solid;border-top-width:6px}.__react_component_tooltip.type-info.place-bottom:after{border-bottom-color:#337AB7;border-bottom-style:solid;border-bottom-width:6px}.__react_component_tooltip.type-info.place-left:after{border-left-color:#337AB7;border-left-style:solid;border-left-width:6px}.__react_component_tooltip.type-info.place-right:after{border-right-color:#337AB7;border-right-style:solid;border-right-width:6px}.__react_component_tooltip.type-info.border{border:1px solid #fff}.__react_component_tooltip.type-info.border.place-top:before{border-top:8px solid #fff}.__react_component_tooltip.type-info.border.place-bottom:before{border-bottom:8px solid #fff}.__react_component_tooltip.type-info.border.place-left:before{border-left:8px solid #fff}.__react_component_tooltip.type-info.border.place-right:before{border-right:8px solid #fff}.__react_component_tooltip.type-light{color:#222;background-color:#fff}.__react_component_tooltip.type-light.place-top:after{border-top-color:#fff;border-top-style:solid;border-top-width:6px}.__react_component_tooltip.type-light.place-bottom:after{border-bottom-color:#fff;border-bottom-style:solid;border-bottom-width:6px}.__react_component_tooltip.type-light.place-left:after{border-left-color:#fff;border-left-style:solid;border-left-width:6px}.__react_component_tooltip.type-light.place-right:after{border-right-color:#fff;border-right-style:solid;border-right-width:6px}.__react_component_tooltip.type-light.border{border:1px solid #222}.__react_component_tooltip.type-light.border.place-top:before{border-top:8px solid #222}.__react_component_tooltip.type-light.border.place-bottom:before{border-bottom:8px solid #222}.__react_component_tooltip.type-light.border.place-left:before{border-left:8px solid #222}.__react_component_tooltip.type-light.border.place-right:before{border-right:8px solid #222}.__react_component_tooltip.place-top{margin-top:-10px}.__react_component_tooltip.place-top:before{border-left:10px solid transparent;border-right:10px solid transparent;bottom:-8px;left:50%;margin-left:-10px}.__react_component_tooltip.place-top:after{border-left:8px solid transparent;border-right:8px solid transparent;bottom:-6px;left:50%;margin-left:-8px}.__react_component_tooltip.place-bottom{margin-top:10px}.__react_component_tooltip.place-bottom:before{border-left:10px solid transparent;border-right:10px solid transparent;top:-8px;left:50%;margin-left:-10px}.__react_component_tooltip.place-bottom:after{border-left:8px solid transparent;border-right:8px solid transparent;top:-6px;left:50%;margin-left:-8px}.__react_component_tooltip.place-left{margin-left:-10px}.__react_component_tooltip.place-left:before{border-top:6px solid transparent;border-bottom:6px solid transparent;right:-8px;top:50%;margin-top:-5px}.__react_component_tooltip.place-left:after{border-top:5px solid transparent;border-bottom:5px solid transparent;right:-6px;top:50%;margin-top:-4px}.__react_component_tooltip.place-right{margin-left:10px}.__react_component_tooltip.place-right:before{border-top:6px solid transparent;border-bottom:6px solid transparent;left:-8px;top:50%;margin-top:-5px}.__react_component_tooltip.place-right:after{border-top:5px solid transparent;border-bottom:5px solid transparent;left:-6px;top:50%;margin-top:-4px}.__react_component_tooltip .multi-line{display:block;padding:2px 0px;text-align:center}
        
CIC Dashboard




















        




	window.LANGUAGE_INFO={
    	CURRENT_LANGUAGE : &quot; , &quot;'&quot; , &quot;en&quot; , &quot;'&quot; , &quot;,
    	AUTHOR_MODE: true,
        PUBLISH_RUN_MODE: true
    };






    







    




























    #katalon{font-family:monospace;font-size:13px;background-color:rgba(0,0,0,.7);position:fixed;top:0;left:0;right:0;display:block;z-index:999999999;line-height: normal} #katalon div{padding:0;margin:0;color:#fff;} #katalon kbd{display:inline-block;padding:3px 5px;font:13px Consolas,&quot;Liberation Mono&quot;,Menlo,Courier,monospace;line-height:10px;color:#555;vertical-align:middle;background-color:#fcfcfc;border:1px solid #ccc;border-bottom-color:#bbb;border-radius:3px;box-shadow:inset 0 -1px 0 #bbb;font-weight: bold} div#katalon-spy_elementInfoDiv {color: lightblue; padding: 0px 5px 5px} div#katalon-spy_instructionDiv {padding: 5px 5px 2.5px}
    
        
        



    clearRelease contactAre you sure you want to release contact?Confirm CancelHi, SHARIDASV account_circleclearYou&quot; , &quot;'&quot; , &quot;re about to log outAre you sure you want to log out now?Confirm Cancel
    
    
    
    
    





    
    




    
        if(window.createIntlProvider) {
            var value = {&quot;style&quot;:&quot;store-header&quot;,&quot;logo&quot;:{&quot;title&quot;:&quot;Maxis Home&quot;,&quot;logoPath&quot;:&quot;/content/dam/next1bit/images/icons/svgs/Maxis_Logo_Color.svg&quot;,&quot;targetUrl&quot;:&quot;/content/maxis/en/retailer-channel/search-load-customer.html&quot;,&quot;newTab&quot;:false,&quot;alt&quot;:&quot;Maxis Home&quot;},&quot;signInOutModel&quot;:{&quot;personIcon&quot;:&quot;account_circle&quot;,&quot;signInLabel&quot;:&quot;Sign In&quot;,&quot;menuOptions&quot;:[{&quot;label&quot;:&quot;Sign Out&quot;,&quot;newTab&quot;:false,&quot;targetUrl&quot;:&quot;/content/maxis/en/retailer-channel/login.html&quot;,&quot;title&quot;:null,&quot;id&quot;:&quot;logout&quot;}],&quot;loginPageUrl&quot;:&quot;/content/maxis/en/retailer-channel/login.html&quot;,&quot;sessionTimeOutSeconds&quot;:1200},&quot;messages&quot;:{&quot;RetailerHeader_logoutConfirmLabel&quot;:&quot;You&quot; , &quot;'&quot; , &quot;re about to log out&quot;,&quot;RetailerHeader_confirmLabel&quot;:&quot;Confirm&quot;,&quot;RetailerHeader_confirmMessageLabel&quot;:&quot;Are you sure you want to log out now?&quot;,&quot;RetailerHeader_cancelLabel&quot;:&quot;Cancel&quot;,&quot;RetailerHeader_userName&quot;:&quot;Hi, {name}&quot;}};
            window.createIntlProvider(&quot; , &quot;'&quot; , &quot;HeaderRetailer&quot; , &quot;'&quot; , &quot;, &quot; , &quot;'&quot; , &quot;en&quot; , &quot;'&quot; , &quot;, {data: value}, &quot;header-container&quot;);
        }
    




	
        
			

    
    



dashboardRetailer Dashboardsupervisor_accountCIC DashboardRegister InterestEnterprise Case ChannelAstro Micrositedevices_otherBrowse CatalogstorageOrder DashboardstoreStore InventoryMAXIS CENTRE AEON KUCHING(CNTRL.00030 | 10030)clearRelease contactAre you sure you want to release contact?Confirm Cancel



    (function(loadWidget){
        var domPlaceHolder =  document.querySelector(&quot; , &quot;'&quot; , &quot;[data-uxfwidget-id=&quot;digitalretailersideb&quot;]&quot; , &quot;'&quot; , &quot;);
        console.log(&quot;widget props&quot;, {&quot;widgetId&quot;:&quot;digital-retailer-sidebar-retail-l9-module&quot;,&quot;messages&quot;:{&quot;releaseContactConfirmBtnLabel&quot;:&quot;Confirm&quot;,&quot;releaseContactCancelLabel&quot;:&quot;Cancel&quot;,&quot;storeInventoryLabel&quot;:&quot;Store Inventory&quot;,&quot;releaseContactLabel&quot;:&quot;Release contact&quot;,&quot;cicDashboardLabel&quot;:&quot;CIC Dashboard&quot;,&quot;enterpriseURL&quot;:&quot;https://api-digital-uat3.isddc.men.maxis.com.my/telesales/api/v4.0/telesales/redirect/telesalesCase&quot;,&quot;retailerDashboardLabel&quot;:&quot;Retailer Dashboard&quot;,&quot;leadsMicrositeURL&quot;:&quot;https://api-digital-uat3.isddc.men.maxis.com.my/leads/api/v1.0/leads/redirect/dashboard&quot;,&quot;releaseContactConfirmMessage&quot;:&quot;Are you sure you want to release contact?&quot;,&quot;browseCatalogLabel&quot;:&quot;Browse Catalog&quot;,&quot;leadsMicrositeLabel&quot;:&quot;Register Interest&quot;,&quot;astroMicrositeURL&quot;:&quot;https://api-digital-uat3.isddc.men.maxis.com.my/telesales/api/v4.0/telesales/redirect/telesalesAstro&quot;,&quot;enterpriseLabel&quot;:&quot;Enterprise Case Channel&quot;,&quot;orderDashboardLabel&quot;:&quot;Order Dashboard&quot;,&quot;astroMicrositeLabel&quot;:&quot;Astro Microsite&quot;},&quot;outcomes&quot;:{&quot;NAVIGATE_TO_STORE_INVENTORY&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/stock-inventory&quot;},&quot;NAVIGATE_TO_RETAILER_DASHBOARD&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/search-load-customer&quot;},&quot;NAVIGATE_TO_BROWSE_CATALOG&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/catalog.html?flowType=COP&quot;},&quot;NAVIGATE_TO_CIC_DASHBOARD&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/cic&quot;},&quot;NAVIGATE_TO_ORDER_DASHBOARD&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/order-dashboard&quot;}},&quot;config&quot;:{&quot;displayLeadsMicrosite&quot;:true},&quot;contextId&quot;:&quot;digitalretailersideb&quot;});
		var instanceId;
        
        loadWidget(&quot;digital-retailer-sidebar-retail-l9-module&quot;, instanceId, domPlaceHolder, {&quot;widgetId&quot;:&quot;digital-retailer-sidebar-retail-l9-module&quot;,&quot;messages&quot;:{&quot;releaseContactConfirmBtnLabel&quot;:&quot;Confirm&quot;,&quot;releaseContactCancelLabel&quot;:&quot;Cancel&quot;,&quot;storeInventoryLabel&quot;:&quot;Store Inventory&quot;,&quot;releaseContactLabel&quot;:&quot;Release contact&quot;,&quot;cicDashboardLabel&quot;:&quot;CIC Dashboard&quot;,&quot;enterpriseURL&quot;:&quot;https://api-digital-uat3.isddc.men.maxis.com.my/telesales/api/v4.0/telesales/redirect/telesalesCase&quot;,&quot;retailerDashboardLabel&quot;:&quot;Retailer Dashboard&quot;,&quot;leadsMicrositeURL&quot;:&quot;https://api-digital-uat3.isddc.men.maxis.com.my/leads/api/v1.0/leads/redirect/dashboard&quot;,&quot;releaseContactConfirmMessage&quot;:&quot;Are you sure you want to release contact?&quot;,&quot;browseCatalogLabel&quot;:&quot;Browse Catalog&quot;,&quot;leadsMicrositeLabel&quot;:&quot;Register Interest&quot;,&quot;astroMicrositeURL&quot;:&quot;https://api-digital-uat3.isddc.men.maxis.com.my/telesales/api/v4.0/telesales/redirect/telesalesAstro&quot;,&quot;enterpriseLabel&quot;:&quot;Enterprise Case Channel&quot;,&quot;orderDashboardLabel&quot;:&quot;Order Dashboard&quot;,&quot;astroMicrositeLabel&quot;:&quot;Astro Microsite&quot;},&quot;outcomes&quot;:{&quot;NAVIGATE_TO_STORE_INVENTORY&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/stock-inventory&quot;},&quot;NAVIGATE_TO_RETAILER_DASHBOARD&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/search-load-customer&quot;},&quot;NAVIGATE_TO_BROWSE_CATALOG&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/catalog.html?flowType=COP&quot;},&quot;NAVIGATE_TO_CIC_DASHBOARD&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/cic&quot;},&quot;NAVIGATE_TO_ORDER_DASHBOARD&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/order-dashboard&quot;}},&quot;config&quot;:{&quot;displayLeadsMicrosite&quot;:true},&quot;contextId&quot;:&quot;digitalretailersideb&quot;}, {&quot;baseURL&quot;:&quot;/rp-server&quot;,&quot;oldBaseURL&quot;:&quot;http://localhost:5000/&quot;,&quot;useMocks&quot;:false,&quot;singlePageApp&quot;:false,&quot;locale&quot;:&quot;en-US&quot;});
    })(window.amdocs.appBootstrap.loadWidget);



    




        
        
            

    
    







    (function(loadWidget){
        var domPlaceHolder =  document.querySelector(&quot; , &quot;'&quot; , &quot;[data-uxfwidget-id=&quot;digitalgeneralmessag&quot;]&quot; , &quot;'&quot; , &quot;);
        console.log(&quot;widget props&quot;, {&quot;widgetId&quot;:&quot;digital-general-message-handler-l9-module&quot;,&quot;messages&quot;:{&quot;Unauthorized&quot;:&quot;Session Expired. Please re-login&quot;,&quot;Forbidden&quot;:&quot;Error 403: This transaction is not allowed.&quot;,&quot;InternalServerError&quot;:&quot;Something went wrong. Please try again.&quot;,&quot;NotFound&quot;:&quot;Requested resource not found. Please check with system admin&quot;,&quot;BadRequest&quot;:&quot;Something wrong with the inputs. Please check.&quot;,&quot;SystemUnavailable&quot;:&quot;System unavailable. Please try after some time.&quot;},&quot;outcomes&quot;:{&quot;NAVIGATE_TO_LOGIN&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/login&quot;}},&quot;config&quot;:{&quot;loaderImagePath&quot;:&quot;/content/dam/next1bit/images/icons/Loader.gif&quot;},&quot;contextId&quot;:&quot;digitalgeneralmessag&quot;});
		var instanceId;
        
        loadWidget(&quot;digital-general-message-handler-l9-module&quot;, instanceId, domPlaceHolder, {&quot;widgetId&quot;:&quot;digital-general-message-handler-l9-module&quot;,&quot;messages&quot;:{&quot;Unauthorized&quot;:&quot;Session Expired. Please re-login&quot;,&quot;Forbidden&quot;:&quot;Error 403: This transaction is not allowed.&quot;,&quot;InternalServerError&quot;:&quot;Something went wrong. Please try again.&quot;,&quot;NotFound&quot;:&quot;Requested resource not found. Please check with system admin&quot;,&quot;BadRequest&quot;:&quot;Something wrong with the inputs. Please check.&quot;,&quot;SystemUnavailable&quot;:&quot;System unavailable. Please try after some time.&quot;},&quot;outcomes&quot;:{&quot;NAVIGATE_TO_LOGIN&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/login&quot;}},&quot;config&quot;:{&quot;loaderImagePath&quot;:&quot;/content/dam/next1bit/images/icons/Loader.gif&quot;},&quot;contextId&quot;:&quot;digitalgeneralmessag&quot;}, {&quot;baseURL&quot;:&quot;/rp-server&quot;,&quot;oldBaseURL&quot;:&quot;http://localhost:5000/&quot;,&quot;useMocks&quot;:false,&quot;singlePageApp&quot;:false,&quot;locale&quot;:&quot;en-US&quot;});
    })(window.amdocs.appBootstrap.loadWidget);






Farizam  ID: 25Feb22Searched By  ACCOUNT 78494200Maxis One Club: 0NEWperm_contact_calendarcheck_circleYour current age is 91Bio-metric Not Performed.Acc no: 78494200Write off: RCollection Status: NAcc no: 78494416Write off: RCollection Status: Nstarexit_to_appclearRelease contactAre you sure you want to release contact?Confirm Cancel



    (function(loadWidget){
        var domPlaceHolder =  document.querySelector(&quot; , &quot;'&quot; , &quot;[data-uxfwidget-id=&quot;digitalcontactheader&quot;]&quot; , &quot;'&quot; , &quot;);
        console.log(&quot;widget props&quot;, {&quot;widgetId&quot;:&quot;digital-contact-header-l9-module&quot;,&quot;messages&quot;:{&quot;contactHeader_releaseContactLabel&quot;:&quot;Release contact&quot;,&quot;submitedDateMsg&quot;:&quot;Submitted Date: {submitedDate}&quot;,&quot;submittedByMsg&quot;:&quot;Submitted By: {submittedBy}&quot;,&quot;contactHeader_releaseContactCancelLabel&quot;:&quot;Cancel&quot;,&quot;contactHeader_searchLabel&quot;:&quot;Searched By&quot;,&quot;contactHeader_AGENOTIFICATIONMsg&quot;:&quot;Your current age is {age}&quot;,&quot;accountIdLabel&quot;:&quot;Account Id&quot;,&quot;contactHeader_AGEMsg&quot;:&quot;Age Check done.&quot;,&quot;customerNameMsg&quot;:&quot;Customer Name: {customerName}&quot;,&quot;contactHeader_releaseContactConfirmBtnLabel&quot;:&quot;Confirm&quot;,&quot;leadsMicrositeURL&quot;:&quot;https://api-digital-uat3.isddc.men.maxis.com.my/leads/api/v1.0/leads/redirect/register&quot;,&quot;contactHeader_docTypeLabel&quot;:&quot;ID&quot;,&quot;contactHeader_FRAUDMsg&quot;:&quot;Fraudulent customer found&quot;,&quot;contactHeader_NOBIOMETRICMsg&quot;:&quot;Bio-metric Not Performed.&quot;,&quot;contactHeader_LINELIMITMsg&quot;:&quot;Line Check done.&quot;,&quot;contactHeader_releaseContactConfirmMessage&quot;:&quot;Are you sure you want to release contact?&quot;,&quot;contactHeader_BIOMETRICMsg&quot;:&quot;Your BioMetric Validation failed.&quot;,&quot;customerAccountMsg&quot;:&quot;Customer Account: {customerAccount}&quot;},&quot;outcomes&quot;:{&quot;NAVIGATE_TO_UPDATE_PROFILE&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/update-profile&quot;},&quot;NAVIGATE_TO_CART&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/cart&quot;},&quot;BACK_TO_CONTACT_DETAILS&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/cic&quot;},&quot;RELEASE_TO_DASHBOARD&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/search-load-customer&quot;},&quot;RELEASE_TO_MULTIPLE_CONTACT_LIST&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/customerandcontact&quot;}},&quot;config&quot;:{&quot;displayLeadsMicrosite&quot;:true,&quot;skipCartload&quot;:true,&quot;ddmfFeaturesFlagId&quot;:false,&quot;callWriteOff&quot;:true},&quot;contextId&quot;:&quot;digitalcontactheader&quot;});
		var instanceId;
        
        loadWidget(&quot;digital-contact-header-l9-module&quot;, instanceId, domPlaceHolder, {&quot;widgetId&quot;:&quot;digital-contact-header-l9-module&quot;,&quot;messages&quot;:{&quot;contactHeader_releaseContactLabel&quot;:&quot;Release contact&quot;,&quot;submitedDateMsg&quot;:&quot;Submitted Date: {submitedDate}&quot;,&quot;submittedByMsg&quot;:&quot;Submitted By: {submittedBy}&quot;,&quot;contactHeader_releaseContactCancelLabel&quot;:&quot;Cancel&quot;,&quot;contactHeader_searchLabel&quot;:&quot;Searched By&quot;,&quot;contactHeader_AGENOTIFICATIONMsg&quot;:&quot;Your current age is {age}&quot;,&quot;accountIdLabel&quot;:&quot;Account Id&quot;,&quot;contactHeader_AGEMsg&quot;:&quot;Age Check done.&quot;,&quot;customerNameMsg&quot;:&quot;Customer Name: {customerName}&quot;,&quot;contactHeader_releaseContactConfirmBtnLabel&quot;:&quot;Confirm&quot;,&quot;leadsMicrositeURL&quot;:&quot;https://api-digital-uat3.isddc.men.maxis.com.my/leads/api/v1.0/leads/redirect/register&quot;,&quot;contactHeader_docTypeLabel&quot;:&quot;ID&quot;,&quot;contactHeader_FRAUDMsg&quot;:&quot;Fraudulent customer found&quot;,&quot;contactHeader_NOBIOMETRICMsg&quot;:&quot;Bio-metric Not Performed.&quot;,&quot;contactHeader_LINELIMITMsg&quot;:&quot;Line Check done.&quot;,&quot;contactHeader_releaseContactConfirmMessage&quot;:&quot;Are you sure you want to release contact?&quot;,&quot;contactHeader_BIOMETRICMsg&quot;:&quot;Your BioMetric Validation failed.&quot;,&quot;customerAccountMsg&quot;:&quot;Customer Account: {customerAccount}&quot;},&quot;outcomes&quot;:{&quot;NAVIGATE_TO_UPDATE_PROFILE&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/update-profile&quot;},&quot;NAVIGATE_TO_CART&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/cart&quot;},&quot;BACK_TO_CONTACT_DETAILS&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/cic&quot;},&quot;RELEASE_TO_DASHBOARD&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/search-load-customer&quot;},&quot;RELEASE_TO_MULTIPLE_CONTACT_LIST&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/customerandcontact&quot;}},&quot;config&quot;:{&quot;displayLeadsMicrosite&quot;:true,&quot;skipCartload&quot;:true,&quot;ddmfFeaturesFlagId&quot;:false,&quot;callWriteOff&quot;:true},&quot;contextId&quot;:&quot;digitalcontactheader&quot;}, {&quot;baseURL&quot;:&quot;/rp-server&quot;,&quot;oldBaseURL&quot;:&quot;http://localhost:5000/&quot;,&quot;useMocks&quot;:false,&quot;singlePageApp&quot;:false,&quot;locale&quot;:&quot;en-US&quot;});
    })(window.amdocs.appBootstrap.loadWidget);






SPECIAL OFFERS &amp; FLASH DEALS FOR THE DAYWe are sorry! Looks like you have no campaigns available currently.



    (function(loadWidget){
        var domPlaceHolder =  document.querySelector(&quot; , &quot;'&quot; , &quot;[data-uxfwidget-id=&quot;digitalcampaignlistr&quot;]&quot; , &quot;'&quot; , &quot;);
        console.log(&quot;widget props&quot;, {&quot;widgetId&quot;:&quot;digital-campaign-list-retail-l9-module&quot;,&quot;messages&quot;:{&quot;View_More_Button_Label&quot;:&quot;View More&quot;,&quot;Special_Offers_Label&quot;:&quot;SPECIAL OFFERS &amp; FLASH DEALS FOR THE DAY&quot;,&quot;CMS_Offer_Id_Label&quot;:&quot;CMS_Offer ID:&quot;,&quot;Campaign_Status_Label&quot;:&quot;Campaign Status:&quot;,&quot;Fulfillment_Status_Label&quot;:&quot;Fullfilment Status:&quot;,&quot;Campaign_Code_Label&quot;:&quot;Campaign Code:&quot;,&quot;No_Campaigns_Label&quot;:&quot;We are sorry! Looks like you have no campaigns available currently.&quot;},&quot;outcomes&quot;:{&quot;NAVIGATE_TO_CAMPAIGN_SUMMARY&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/campaign-summary&quot;}},&quot;config&quot;:{&quot;campaignListConfig&quot;:5},&quot;contextId&quot;:&quot;digitalcampaignlistr&quot;});
		var instanceId;
        
        loadWidget(&quot;digital-campaign-list-retail-l9-module&quot;, instanceId, domPlaceHolder, {&quot;widgetId&quot;:&quot;digital-campaign-list-retail-l9-module&quot;,&quot;messages&quot;:{&quot;View_More_Button_Label&quot;:&quot;View More&quot;,&quot;Special_Offers_Label&quot;:&quot;SPECIAL OFFERS &amp; FLASH DEALS FOR THE DAY&quot;,&quot;CMS_Offer_Id_Label&quot;:&quot;CMS_Offer ID:&quot;,&quot;Campaign_Status_Label&quot;:&quot;Campaign Status:&quot;,&quot;Fulfillment_Status_Label&quot;:&quot;Fullfilment Status:&quot;,&quot;Campaign_Code_Label&quot;:&quot;Campaign Code:&quot;,&quot;No_Campaigns_Label&quot;:&quot;We are sorry! Looks like you have no campaigns available currently.&quot;},&quot;outcomes&quot;:{&quot;NAVIGATE_TO_CAMPAIGN_SUMMARY&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/campaign-summary&quot;}},&quot;config&quot;:{&quot;campaignListConfig&quot;:5},&quot;contextId&quot;:&quot;digitalcampaignlistr&quot;}, {&quot;baseURL&quot;:&quot;/rp-server&quot;,&quot;oldBaseURL&quot;:&quot;http://localhost:5000/&quot;,&quot;useMocks&quot;:false,&quot;singlePageApp&quot;:false,&quot;locale&quot;:&quot;en-US&quot;});
    })(window.amdocs.appBootstrap.loadWidget);










    (function(loadWidget){
        var domPlaceHolder =  document.querySelector(&quot; , &quot;'&quot; , &quot;[data-uxfwidget-id=&quot;digitalleadmicrosite&quot;]&quot; , &quot;'&quot; , &quot;);
        console.log(&quot;widget props&quot;, {&quot;widgetId&quot;:&quot;digital-lead-microsite-retail-l9-module&quot;,&quot;messages&quot;:{&quot;leadsMicrositeURL&quot;:&quot;https://api-digital-uat3.isddc.men.maxis.com.my/leads/api/v1.0/leads/redirect/landing&quot;,&quot;iFrameHeight&quot;:&quot;240&quot;},&quot;config&quot;:{&quot;displayLeadsMicrosite&quot;:true,&quot;inlineMessages&quot;:false},&quot;contextId&quot;:&quot;digitalleadmicrosite&quot;});
		var instanceId;
        
        loadWidget(&quot;digital-lead-microsite-retail-l9-module&quot;, instanceId, domPlaceHolder, {&quot;widgetId&quot;:&quot;digital-lead-microsite-retail-l9-module&quot;,&quot;messages&quot;:{&quot;leadsMicrositeURL&quot;:&quot;https://api-digital-uat3.isddc.men.maxis.com.my/leads/api/v1.0/leads/redirect/landing&quot;,&quot;iFrameHeight&quot;:&quot;240&quot;},&quot;config&quot;:{&quot;displayLeadsMicrosite&quot;:true,&quot;inlineMessages&quot;:false},&quot;contextId&quot;:&quot;digitalleadmicrosite&quot;}, {&quot;baseURL&quot;:&quot;/rp-server&quot;,&quot;oldBaseURL&quot;:&quot;http://localhost:5000/&quot;,&quot;useMocks&quot;:false,&quot;singlePageApp&quot;:false,&quot;locale&quot;:&quot;en-US&quot;});
    })(window.amdocs.appBootstrap.loadWidget);






Contracts Summary02Others Contract02Zerolution Fibre ContractSubscriptions Summary02VOIP02Fibre



    (function(loadWidget){
        var domPlaceHolder =  document.querySelector(&quot; , &quot;'&quot; , &quot;[data-uxfwidget-id=&quot;digitalcontractssubs&quot;]&quot; , &quot;'&quot; , &quot;);
        console.log(&quot;widget props&quot;, {&quot;widgetId&quot;:&quot;digital-contracts-subscriptions-summary-retail-l9-module&quot;,&quot;messages&quot;:{&quot;contractsSummaryErrorLabel&quot;:&quot;There are no active contracts&quot;,&quot;subscriptionsSummaryErrorLabel&quot;:&quot;No Subscriptions Present&quot;,&quot;subscriptionsSummaryLabel&quot;:&quot;Subscriptions Summary&quot;,&quot;contractsSummaryLabel&quot;:&quot;Contracts Summary&quot;},&quot;config&quot;:{&quot;inlineMessages&quot;:false},&quot;contextId&quot;:&quot;digitalcontractssubs&quot;});
		var instanceId;
        
        loadWidget(&quot;digital-contracts-subscriptions-summary-retail-l9-module&quot;, instanceId, domPlaceHolder, {&quot;widgetId&quot;:&quot;digital-contracts-subscriptions-summary-retail-l9-module&quot;,&quot;messages&quot;:{&quot;contractsSummaryErrorLabel&quot;:&quot;There are no active contracts&quot;,&quot;subscriptionsSummaryErrorLabel&quot;:&quot;No Subscriptions Present&quot;,&quot;subscriptionsSummaryLabel&quot;:&quot;Subscriptions Summary&quot;,&quot;contractsSummaryLabel&quot;:&quot;Contracts Summary&quot;},&quot;config&quot;:{&quot;inlineMessages&quot;:false},&quot;contextId&quot;:&quot;digitalcontractssubs&quot;}, {&quot;baseURL&quot;:&quot;/rp-server&quot;,&quot;oldBaseURL&quot;:&quot;http://localhost:5000/&quot;,&quot;useMocks&quot;:false,&quot;singlePageApp&quot;:false,&quot;locale&quot;:&quot;en-US&quot;});
    })(window.amdocs.appBootstrap.loadWidget);






New Purchase Starter Pack Registration Switch To Maxis Create Interaction View Interaction Biz Individual Registration Change Ownership It seems there is a pending orderPlease submit or cancel the order before proceeding.OK 



    (function(loadWidget){
        var domPlaceHolder =  document.querySelector(&quot; , &quot;'&quot; , &quot;[data-uxfwidget-id=&quot;digitalcustomernavig&quot;]&quot; , &quot;'&quot; , &quot;);
        console.log(&quot;widget props&quot;, {&quot;widgetId&quot;:&quot;digital-customer-navigation-panel-retail-l9-module&quot;,&quot;messages&quot;:{&quot;ViewInteraction&quot;:&quot;View Interaction&quot;,&quot;postToPreSimOption&quot;:&quot;How do you want to proceed ?&quot;,&quot;continueNewSim&quot;:&quot;Continue With New Sim&quot;,&quot;okButtonRetailer&quot;:&quot;OK&quot;,&quot;msgLine1ForRetailer&quot;:&quot;Eligibility popup Msg line 1 for retailer&quot;,&quot;PostpaidToPrepaid&quot;:&quot;Postpaid To Prepaid&quot;,&quot;CreateInteraction&quot;:&quot;Create Interaction&quot;,&quot;SwitchToMaxis&quot;:&quot;Switch To Maxis&quot;,&quot;continueOldSim&quot;:&quot;Continue With Old Sim&quot;,&quot;BizIndividualRegistration&quot;:&quot;Biz Individual Registration&quot;,&quot;CustomerOrders&quot;:&quot;Customers Orders&quot;,&quot;msgLine2ForRetailer&quot;:&quot;Please submit or cancel the order before proceeding.&quot;,&quot;FinancialTransaction&quot;:&quot;Financial Transaction&quot;,&quot;Cart&quot;:&quot;Cart&quot;,&quot;StarterPackRegistration&quot;:&quot;Starter Pack Registration&quot;,&quot;Case&quot;:&quot;Case&quot;,&quot;NewPurchase&quot;:&quot;New Purchase&quot;},&quot;outcomes&quot;:{&quot;BIZ_INDIVIDUAL_REGISTRATION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/bizindividual&quot;},&quot;VIEW_INTERACTION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/interaction-list&quot;},&quot;CHANGE_OWNERSHIP_TARGET_CREATE_CUSTOMER&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/target-create-customer.html?flowType=changeOwnership&quot;},&quot;NAVIGATE_TO_PLAN_LIST&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/plan-list&quot;},&quot;NEW_PURCHASE&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/catalog.html?flowType=COP&quot;},&quot;CUSTOMER_ORDERS&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/order-dashboard&quot;},&quot;STARTER_PACK_REGISTRATION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/starter-pack&quot;},&quot;VIEW_CASE&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/view-cases&quot;},&quot;CHANGE_OWNERSHIP_TARGET&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/change-ownership-target&quot;},&quot;CREATE_INTERACTION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/create-interaction&quot;},&quot;NAVIGATE_TO_FINANCIAL_TRANSACTION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/financial-transaction&quot;},&quot;CART&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/cart&quot;}},&quot;config&quot;:{&quot;customerTypeCheckbox&quot;:&quot;false&quot;},&quot;initProps&quot;:{&quot;customerType&quot;:&quot;customerContext&quot;},&quot;contextId&quot;:&quot;digitalcustomernavig&quot;});
		var instanceId;
        
        loadWidget(&quot;digital-customer-navigation-panel-retail-l9-module&quot;, instanceId, domPlaceHolder, {&quot;widgetId&quot;:&quot;digital-customer-navigation-panel-retail-l9-module&quot;,&quot;messages&quot;:{&quot;ViewInteraction&quot;:&quot;View Interaction&quot;,&quot;postToPreSimOption&quot;:&quot;How do you want to proceed ?&quot;,&quot;continueNewSim&quot;:&quot;Continue With New Sim&quot;,&quot;okButtonRetailer&quot;:&quot;OK&quot;,&quot;msgLine1ForRetailer&quot;:&quot;Eligibility popup Msg line 1 for retailer&quot;,&quot;PostpaidToPrepaid&quot;:&quot;Postpaid To Prepaid&quot;,&quot;CreateInteraction&quot;:&quot;Create Interaction&quot;,&quot;SwitchToMaxis&quot;:&quot;Switch To Maxis&quot;,&quot;continueOldSim&quot;:&quot;Continue With Old Sim&quot;,&quot;BizIndividualRegistration&quot;:&quot;Biz Individual Registration&quot;,&quot;CustomerOrders&quot;:&quot;Customers Orders&quot;,&quot;msgLine2ForRetailer&quot;:&quot;Please submit or cancel the order before proceeding.&quot;,&quot;FinancialTransaction&quot;:&quot;Financial Transaction&quot;,&quot;Cart&quot;:&quot;Cart&quot;,&quot;StarterPackRegistration&quot;:&quot;Starter Pack Registration&quot;,&quot;Case&quot;:&quot;Case&quot;,&quot;NewPurchase&quot;:&quot;New Purchase&quot;},&quot;outcomes&quot;:{&quot;BIZ_INDIVIDUAL_REGISTRATION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/bizindividual&quot;},&quot;VIEW_INTERACTION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/interaction-list&quot;},&quot;CHANGE_OWNERSHIP_TARGET_CREATE_CUSTOMER&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/target-create-customer.html?flowType=changeOwnership&quot;},&quot;NAVIGATE_TO_PLAN_LIST&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/plan-list&quot;},&quot;NEW_PURCHASE&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/catalog.html?flowType=COP&quot;},&quot;CUSTOMER_ORDERS&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/order-dashboard&quot;},&quot;STARTER_PACK_REGISTRATION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/starter-pack&quot;},&quot;VIEW_CASE&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/view-cases&quot;},&quot;CHANGE_OWNERSHIP_TARGET&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/change-ownership-target&quot;},&quot;CREATE_INTERACTION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/create-interaction&quot;},&quot;NAVIGATE_TO_FINANCIAL_TRANSACTION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/financial-transaction&quot;},&quot;CART&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/cart&quot;}},&quot;config&quot;:{&quot;customerTypeCheckbox&quot;:&quot;false&quot;},&quot;initProps&quot;:{&quot;customerType&quot;:&quot;customerContext&quot;},&quot;contextId&quot;:&quot;digitalcustomernavig&quot;}, {&quot;baseURL&quot;:&quot;/rp-server&quot;,&quot;oldBaseURL&quot;:&quot;http://localhost:5000/&quot;,&quot;useMocks&quot;:false,&quot;singlePageApp&quot;:false,&quot;locale&quot;:&quot;en-US&quot;});
    })(window.amdocs.appBootstrap.loadWidget);






Account 78494200expand_lessTypeConsumerSub-TypeMaxis IndividualSegmentNONECollection StatusNormalWrite-OffNoBilling ProfileManageDirect DebitRegister NowTotal Amount DueRM 0.00Pay Before -View Bill A57400Maxis Fibre - 300MbpsShow DetailsActiveSince 25 Feb 2022keyboard_arrow_right60395459722Maxis Fibre - VoiceShow DetailsActiveSince 25 Feb 2022keyboard_arrow_rightNew Purchase Switch To Maxis Case Customers Orders Financial Transaction Cart Change Ownership It seems there is a pending orderPlease submit or cancel the order before proceeding.OK It seems there is a pending orderPlease submit or cancel the order before proceeding.OK Account 78494416expand_moreTypeConsumerSub-TypeMaxis IndividualSegmentNONECollection StatusNormalWrite-OffNoBilling ProfileManageDirect DebitRegister NowTotal Amount DueRM 0.00Pay Before -View Bill New Purchase Switch To Maxis Case Customers Orders Financial Transaction Cart Change Ownership It seems there is a pending orderPlease submit or cancel the order before proceeding.OK It seems there is a pending orderPlease submit or cancel the order before proceeding.OK 



    (function(loadWidget){
        var domPlaceHolder =  document.querySelector(&quot; , &quot;'&quot; , &quot;[data-uxfwidget-id=&quot;digitalaccountlistl9&quot;]&quot; , &quot;'&quot; , &quot;);
        console.log(&quot;widget props&quot;, {&quot;widgetId&quot;:&quot;digital-account-list-l9-module&quot;,&quot;messages&quot;:{&quot;accountDetails_directDebit&quot;:&quot;Direct Debit&quot;,&quot;accountDetails_cancelledAccount&quot;:&quot;Cancelled Account&quot;,&quot;ViewInteraction&quot;:&quot;View Interaction&quot;,&quot;selectSubscription_noProductsMessage&quot;:&quot;No Products Assigned&quot;,&quot;postToPreSimOption&quot;:&quot;How do you want to proceed ?&quot;,&quot;selectSubscription_active&quot;:&quot;Active&quot;,&quot;continueNewSim&quot;:&quot;Continue With New Sim&quot;,&quot;showCancelAccountText&quot;:&quot;View Cancelled Account&quot;,&quot;okButtonRetailer&quot;:&quot;OK&quot;,&quot;msgLine1ForRetailer&quot;:&quot;Eligibility popup Msg line 1 for retailer&quot;,&quot;PostpaidToPrepaid&quot;:&quot;Postpaid To Prepaid&quot;,&quot;accountDetails_postPaidAccount&quot;:&quot;Account {billingProfileId}&quot;,&quot;CreateInteraction&quot;:&quot;Create Interaction&quot;,&quot;SwitchToMaxis&quot;:&quot;Switch To Maxis&quot;,&quot;accountDetails_writeOff&quot;:&quot;Write-Off&quot;,&quot;accountDetails_billingProfile&quot;:&quot;Billing Profile&quot;,&quot;continueOldSim&quot;:&quot;Continue With Old Sim&quot;,&quot;BizIndividualRegistration&quot;:&quot;Biz Individual Registration&quot;,&quot;accountDetails_typeText&quot;:&quot;Type&quot;,&quot;selectSubscription_suspended&quot;:&quot;Suspended&quot;,&quot;CustomerOrders&quot;:&quot;Customers Orders&quot;,&quot;msgLine2ForRetailer&quot;:&quot;Please submit or cancel the order before proceeding.&quot;,&quot;accountDetails_registerNow&quot;:&quot;Register Now&quot;,&quot;accountDetails_subType&quot;:&quot;Sub-Type&quot;,&quot;accountDetails_brn&quot;:&quot;BRN&quot;,&quot;FinancialTransaction&quot;:&quot;Financial Transaction&quot;,&quot;Cart&quot;:&quot;Cart&quot;,&quot;StarterPackRegistration&quot;:&quot;Starter Pack Registration&quot;,&quot;accountDetails_manage&quot;:&quot;Manage&quot;,&quot;Case&quot;:&quot;Case&quot;,&quot;accountDetails_collectionStatus&quot;:&quot;Collection Status&quot;,&quot;selectSubscription_since&quot;:&quot;Since {date}&quot;,&quot;accountDetails_prepaidAccount&quot;:&quot;Prepaid Account&quot;,&quot;accountDetails_primitiveAccount&quot;:&quot;Primitive Account&quot;,&quot;NewPurchase&quot;:&quot;New Purchase&quot;},&quot;outcomes&quot;:{&quot;BIZ_INDIVIDUAL_REGISTRATION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/bizindividual&quot;},&quot;VIEW_INTERACTION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/interaction-list&quot;},&quot;NAVIGATE_TO_UPDATE_BILLING_PROFILE&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/update-billing-profile&quot;},&quot;NAVIGATE_TO_PLAN_LIST&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/catalog&quot;},&quot;POSTPAID_TO_PREPAID&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/postpaid to prepaid&quot;},&quot;NAVIGATE_TO_ORDER_SUMMARY&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/order-summary&quot;},&quot;SET_SUBSCRIPTION_OPTION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/balance-and-services&quot;},&quot;NEW_PURCHASE&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/catalog.html?flowType=COP&quot;},&quot;NAVIGATE_TO_BILL_SUMMARY&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/bill-summary&quot;},&quot;CUSTOMER_ORDERS&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/order-dashboard&quot;},&quot;STARTER_PACK_REGISTRATION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/starter-pack&quot;},&quot;SET_SUBSCRIPTION_OPTION_PREPAID&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/prepaid-balance-services&quot;},&quot;CHANGE_OWNERSHIP&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/change-ownership-source&quot;},&quot;SET_SUBSCRIPTION_OPTION_POSTPAID&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/postpaid-balance-services&quot;},&quot;SET_SUBSCRIPTION_OPTION_VOIP&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/fixedVoice-subscription-overview&quot;},&quot;SET_SUBSCRIPTION_OPTION_FTTH&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/fibre-subscription-overview&quot;},&quot;NAVIGATE_TO_MANAGE_AUTOPAY&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/manage-auto-pay&quot;},&quot;VIEW_CASE&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/view-cases&quot;},&quot;SWITCH_TO_MAXIS&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/catalog&quot;},&quot;CREATE_INTERACTION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/create-interaction&quot;},&quot;NAVIGATE_TO_CAMPAIGN_SUMMARY&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/campaign-summary&quot;},&quot;NAVIGATE_TO_FINANCIAL_TRANSACTION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/financial-transaction&quot;},&quot;CART&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/cart&quot;}},&quot;contextId&quot;:&quot;digitalaccountlistl9&quot;});
		var instanceId;
        
        loadWidget(&quot;digital-account-list-l9-module&quot;, instanceId, domPlaceHolder, {&quot;widgetId&quot;:&quot;digital-account-list-l9-module&quot;,&quot;messages&quot;:{&quot;accountDetails_directDebit&quot;:&quot;Direct Debit&quot;,&quot;accountDetails_cancelledAccount&quot;:&quot;Cancelled Account&quot;,&quot;ViewInteraction&quot;:&quot;View Interaction&quot;,&quot;selectSubscription_noProductsMessage&quot;:&quot;No Products Assigned&quot;,&quot;postToPreSimOption&quot;:&quot;How do you want to proceed ?&quot;,&quot;selectSubscription_active&quot;:&quot;Active&quot;,&quot;continueNewSim&quot;:&quot;Continue With New Sim&quot;,&quot;showCancelAccountText&quot;:&quot;View Cancelled Account&quot;,&quot;okButtonRetailer&quot;:&quot;OK&quot;,&quot;msgLine1ForRetailer&quot;:&quot;Eligibility popup Msg line 1 for retailer&quot;,&quot;PostpaidToPrepaid&quot;:&quot;Postpaid To Prepaid&quot;,&quot;accountDetails_postPaidAccount&quot;:&quot;Account {billingProfileId}&quot;,&quot;CreateInteraction&quot;:&quot;Create Interaction&quot;,&quot;SwitchToMaxis&quot;:&quot;Switch To Maxis&quot;,&quot;accountDetails_writeOff&quot;:&quot;Write-Off&quot;,&quot;accountDetails_billingProfile&quot;:&quot;Billing Profile&quot;,&quot;continueOldSim&quot;:&quot;Continue With Old Sim&quot;,&quot;BizIndividualRegistration&quot;:&quot;Biz Individual Registration&quot;,&quot;accountDetails_typeText&quot;:&quot;Type&quot;,&quot;selectSubscription_suspended&quot;:&quot;Suspended&quot;,&quot;CustomerOrders&quot;:&quot;Customers Orders&quot;,&quot;msgLine2ForRetailer&quot;:&quot;Please submit or cancel the order before proceeding.&quot;,&quot;accountDetails_registerNow&quot;:&quot;Register Now&quot;,&quot;accountDetails_subType&quot;:&quot;Sub-Type&quot;,&quot;accountDetails_brn&quot;:&quot;BRN&quot;,&quot;FinancialTransaction&quot;:&quot;Financial Transaction&quot;,&quot;Cart&quot;:&quot;Cart&quot;,&quot;StarterPackRegistration&quot;:&quot;Starter Pack Registration&quot;,&quot;accountDetails_manage&quot;:&quot;Manage&quot;,&quot;Case&quot;:&quot;Case&quot;,&quot;accountDetails_collectionStatus&quot;:&quot;Collection Status&quot;,&quot;selectSubscription_since&quot;:&quot;Since {date}&quot;,&quot;accountDetails_prepaidAccount&quot;:&quot;Prepaid Account&quot;,&quot;accountDetails_primitiveAccount&quot;:&quot;Primitive Account&quot;,&quot;NewPurchase&quot;:&quot;New Purchase&quot;},&quot;outcomes&quot;:{&quot;BIZ_INDIVIDUAL_REGISTRATION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/bizindividual&quot;},&quot;VIEW_INTERACTION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/interaction-list&quot;},&quot;NAVIGATE_TO_UPDATE_BILLING_PROFILE&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/update-billing-profile&quot;},&quot;NAVIGATE_TO_PLAN_LIST&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/catalog&quot;},&quot;POSTPAID_TO_PREPAID&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/postpaid to prepaid&quot;},&quot;NAVIGATE_TO_ORDER_SUMMARY&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/order-summary&quot;},&quot;SET_SUBSCRIPTION_OPTION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/balance-and-services&quot;},&quot;NEW_PURCHASE&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/catalog.html?flowType=COP&quot;},&quot;NAVIGATE_TO_BILL_SUMMARY&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/bill-summary&quot;},&quot;CUSTOMER_ORDERS&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/order-dashboard&quot;},&quot;STARTER_PACK_REGISTRATION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/starter-pack&quot;},&quot;SET_SUBSCRIPTION_OPTION_PREPAID&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/prepaid-balance-services&quot;},&quot;CHANGE_OWNERSHIP&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/change-ownership-source&quot;},&quot;SET_SUBSCRIPTION_OPTION_POSTPAID&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/postpaid-balance-services&quot;},&quot;SET_SUBSCRIPTION_OPTION_VOIP&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/fixedVoice-subscription-overview&quot;},&quot;SET_SUBSCRIPTION_OPTION_FTTH&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/fibre-subscription-overview&quot;},&quot;NAVIGATE_TO_MANAGE_AUTOPAY&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/manage-auto-pay&quot;},&quot;VIEW_CASE&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/view-cases&quot;},&quot;SWITCH_TO_MAXIS&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/catalog&quot;},&quot;CREATE_INTERACTION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/create-interaction&quot;},&quot;NAVIGATE_TO_CAMPAIGN_SUMMARY&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/campaign-summary&quot;},&quot;NAVIGATE_TO_FINANCIAL_TRANSACTION&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/financial-transaction&quot;},&quot;CART&quot;:{&quot;type&quot;:&quot;navigate&quot;,&quot;value&quot;:&quot;/content/maxis/en/retailer-channel/cart&quot;}},&quot;contextId&quot;:&quot;digitalaccountlistl9&quot;}, {&quot;baseURL&quot;:&quot;/rp-server&quot;,&quot;oldBaseURL&quot;:&quot;http://localhost:5000/&quot;,&quot;useMocks&quot;:false,&quot;singlePageApp&quot;:false,&quot;locale&quot;:&quot;en-US&quot;});
    })(window.amdocs.appBootstrap.loadWidget);



    




        
	


        




    

/html[1]&quot;))]</value>
   </webElementXpaths>
</WebElementEntity>
