<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_clearRelease contactAre you sure you wa_1c63b8</name>
   <tag></tag>
   <elementGuidId>e8b2df50-90d2-4e60-a2a4-ad9dcc496951</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.header-top.pt-1.pb-3</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='header-container']/header/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>header-top pt-1 pb-3</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>clearRelease contactAre you sure you want to release contact?Confirm CancelHi, SHARIDASV account_circleclearYou're about to log outAre you sure you want to log out now?Confirm Cancel</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;header-container&quot;)/header[@class=&quot;header retailer store-header&quot;]/div[@class=&quot;header-top pt-1 pb-3&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='header-container']/header/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//header/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = concat(&quot;clearRelease contactAre you sure you want to release contact?Confirm CancelHi, SHARIDASV account_circleclearYou&quot; , &quot;'&quot; , &quot;re about to log outAre you sure you want to log out now?Confirm Cancel&quot;) or . = concat(&quot;clearRelease contactAre you sure you want to release contact?Confirm CancelHi, SHARIDASV account_circleclearYou&quot; , &quot;'&quot; , &quot;re about to log outAre you sure you want to log out now?Confirm Cancel&quot;))]</value>
   </webElementXpaths>
</WebElementEntity>
