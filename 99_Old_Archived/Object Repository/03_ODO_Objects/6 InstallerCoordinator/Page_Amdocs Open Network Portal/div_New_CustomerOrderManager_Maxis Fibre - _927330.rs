<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_New_CustomerOrderManager_Maxis Fibre - _927330</name>
   <tag></tag>
   <elementGuidId>e5aa5dbb-25c1-46fe-acb4-279310c2698f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.project-name</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='grid']/div/div[2]/div[4]/div/div[2]/table/tbody/tr/td[4]/div/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>project-name</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>New_CustomerOrderManager_Maxis Fibre - 300Mbps_25/02/22_1804983</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>New_CustomerOrderManager_Maxis Fibre - 300Mbps_25/02/22_1804983</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;grid&quot;)/div[@class=&quot;project-list&quot;]/div[@class=&quot;dop-grid&quot;]/div[@class=&quot;dop-grid-table&quot;]/div[@class=&quot;dop-table dop-table-id-1645757009714&quot;]/div[@class=&quot;dop-table-rows&quot;]/table[1]/tbody[1]/tr[@class=&quot;dop-table-row&quot;]/td[@class=&quot;nameAndLocationColumn&quot;]/div[@class=&quot;nameAndLocation-cell&quot;]/div[@class=&quot;project-name&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/03_ODO_Objects/6 InstallerCoordinator/Page_Amdocs Open Network Portal/iframe_empty frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='grid']/div/div[2]/div[4]/div/div[2]/table/tbody/tr/td[4]/div/div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add to Bookmarks'])[1]/following::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Export'])[1]/following::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Farizam'])[1]/preceding::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='In progress'])[2]/preceding::div[4]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='New_CustomerOrderManager_Maxis Fibre - 300Mbps_25/02/22_1804983']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[4]/div/div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@title = 'New_CustomerOrderManager_Maxis Fibre - 300Mbps_25/02/22_1804983' and (text() = 'New_CustomerOrderManager_Maxis Fibre - 300Mbps_25/02/22_1804983' or . = 'New_CustomerOrderManager_Maxis Fibre - 300Mbps_25/02/22_1804983')]</value>
   </webElementXpaths>
</WebElementEntity>
