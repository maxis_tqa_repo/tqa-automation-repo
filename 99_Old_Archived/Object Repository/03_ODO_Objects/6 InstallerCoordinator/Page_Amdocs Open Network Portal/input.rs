<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input</name>
   <tag></tag>
   <elementGuidId>92d0343a-818a-407e-aceb-1b667675488d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>input[type=&quot;text&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//input[@value=''])[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;grid&quot;)/div[@class=&quot;project-list&quot;]/div[@class=&quot;dop-grid&quot;]/div[@class=&quot;dop-grid-filter&quot;]/div[@class=&quot;dop-filters&quot;]/div[@class=&quot;filters&quot;]/div[@class=&quot;dop-filter externalIdColumn expand&quot;]/div[2]/div[@class=&quot;controls&quot;]/div[@class=&quot;text-filter-control externalIdControl&quot;]/input[1]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/03_ODO_Objects/6 InstallerCoordinator/Page_Amdocs Open Network Portal/iframe_empty frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@value=''])[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='grid']/div/div[2]/div[2]/div/div[2]/div[3]/div[2]/div/div/input</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/input</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text']</value>
   </webElementXpaths>
</WebElementEntity>
