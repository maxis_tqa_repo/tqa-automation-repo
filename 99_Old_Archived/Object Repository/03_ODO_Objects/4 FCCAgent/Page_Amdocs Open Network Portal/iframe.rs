<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>iframe</name>
   <tag></tag>
   <elementGuidId>e2840671-b53d-4b02-9393-058e3ff1adcc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>iframe</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//iframe</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>/html[@class=&quot;no-js&quot;]/body[1]/div[@class=&quot;smallcell&quot;]/div[@class=&quot;contentArea standAlonecontentArea&quot;]/div[1]/div[@class=&quot;contentAreaContent guidedTaskContent&quot;]/iframe[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>iframe</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-uxf-point</name>
      <type>Main</type>
      <value>gtIframe</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-isdirty</name>
      <type>Main</type>
      <value>false</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>/smallcell-gt/index.html?gtMode=inbox&amp;gtTaskId=6DD8C1EAFB5C4BEAB5CC7C14FDE35B19</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>cd_frame_id_</name>
      <type>Main</type>
      <value>0b11ae893cd1465ab83e9d7b648b92ba</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;no-js&quot;]/body[1]/div[@class=&quot;smallcell&quot;]/div[@class=&quot;contentArea standAlonecontentArea&quot;]/div[1]/div[@class=&quot;contentAreaContent guidedTaskContent&quot;]/iframe[1]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/03_ODO_Objects/4 FCCAgent/Page_Amdocs Open Network Portal/iframe_empty frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//iframe</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//iframe[@src = '/smallcell-gt/index.html?gtMode=inbox&amp;gtTaskId=6DD8C1EAFB5C4BEAB5CC7C14FDE35B19']</value>
   </webElementXpaths>
</WebElementEntity>
