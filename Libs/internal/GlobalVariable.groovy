package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.main.TestCaseMain


/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p></p>
     */
    public static Object Email
     
    /**
     * <p>Profile default : Retailer Portal URL for this profile
Profile ITV1 : Retailer Portal URL for this profile
Profile SIT1 : Retailer Portal URL for this profile</p>
     */
    public static Object RP_URL
     
    /**
     * <p>Profile default : RP Username
Profile ITV1 : RP Username
Profile SIT1 : RP Username</p>
     */
    public static Object RP_Username
     
    /**
     * <p>Profile default : RP Password
Profile ITV1 : RP Password
Profile SIT1 : RP Password</p>
     */
    public static Object RP_Password
     
    /**
     * <p>Profile default : WebPOS URL
Profile ITV1 : WebPOS URL
Profile SIT1 : WebPOS URL</p>
     */
    public static Object WebPOS_URL
     
    /**
     * <p></p>
     */
    public static Object WebPOS_Username
     
    /**
     * <p></p>
     */
    public static Object WebPOS_Password
     
    /**
     * <p></p>
     */
    public static Object newCustomer_Flag
     
    /**
     * <p></p>
     */
    public static Object msisdn
     
    /**
     * <p></p>
     */
    public static Object fwbbMsisdn
     
    /**
     * <p></p>
     */
    public static Object filePath
     
    /**
     * <p></p>
     */
    public static Object orderID
     
    /**
     * <p></p>
     */
    public static Object id
     
    /**
     * <p></p>
     */
    public static Object inventoryType
     
    /**
     * <p></p>
     */
    public static Object simSerial
     

    static {
        try {
            def selectedVariables = TestCaseMain.getGlobalVariables("default")
			selectedVariables += TestCaseMain.getGlobalVariables(RunConfiguration.getExecutionProfile())
            selectedVariables += TestCaseMain.getParsedValues(RunConfiguration.getOverridingParameters())
    
            Email = selectedVariables['Email']
            RP_URL = selectedVariables['RP_URL']
            RP_Username = selectedVariables['RP_Username']
            RP_Password = selectedVariables['RP_Password']
            WebPOS_URL = selectedVariables['WebPOS_URL']
            WebPOS_Username = selectedVariables['WebPOS_Username']
            WebPOS_Password = selectedVariables['WebPOS_Password']
            newCustomer_Flag = selectedVariables['newCustomer_Flag']
            msisdn = selectedVariables['msisdn']
            fwbbMsisdn = selectedVariables['fwbbMsisdn']
            filePath = selectedVariables['filePath']
            orderID = selectedVariables['orderID']
            id = selectedVariables['id']
            inventoryType = selectedVariables['inventoryType']
            simSerial = selectedVariables['simSerial']
            
        } catch (Exception e) {
            TestCaseMain.logGlobalVariableError(e)
        }
    }
}
