<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_FCC Ready To Install</name>
   <tag></tag>
   <elementGuidId>f5ba5bf8-14b6-40cb-b5ee-b76782355626</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.activity-name-cell.ellipsis-cell.linkable-tile-header</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='configProjectOverviewModule']/div/div[2]/div/div[3]/div/div[2]/div/div/div[2]/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>activity-name-cell ellipsis-cell linkable-tile-header</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>FCC Ready To Install</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>FCC Ready To Install</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;configProjectOverviewModule&quot;)/div[@class=&quot;project-overview&quot;]/div[2]/div[@class=&quot;react-grid-layout cards-layout-container&quot;]/div[@class=&quot;react-grid-item cssTransforms&quot;]/div[@class=&quot;card with-title&quot;]/div[@class=&quot;card-content&quot;]/div[@class=&quot;activity-card&quot;]/div[@class=&quot;dop-tile clickable-tile&quot;]/div[@class=&quot;main-tile&quot;]/div[1]/div[@class=&quot;activity-name-cell ellipsis-cell linkable-tile-header&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/03_ODO_Objects/5 FCCInstallerManager/Page_Amdocs Open Network Portal/iframe_empty frame</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='configProjectOverviewModule']/div/div[2]/div/div[3]/div/div[2]/div/div/div[2]/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Activities'])[2]/following::div[9]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='oss_status_fcc_readyforinstall'])[1]/following::div[25]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Installation'])[2]/preceding::div[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='In Progress'])[1]/preceding::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='FCC Ready To Install']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div[2]/div/div/div[2]/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@title = 'FCC Ready To Install' and (text() = 'FCC Ready To Install' or . = 'FCC Ready To Install')]</value>
   </webElementXpaths>
</WebElementEntity>
