<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>div_Installation Completed</description>
   <name>div_Installation Completed</name>
   <tag></tag>
   <elementGuidId>9b4b4f2f-f165-493f-87a4-87fa544ef62d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@title = 'Installation Completed']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Installation Completed</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            Installation Completed</value>
   </webElementProperties>
</WebElementEntity>
