<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Regular Paper Bill_CustomCheckbox</name>
   <tag></tag>
   <elementGuidId>d876bf40-74cd-4373-84f8-6356524993a6</elementGuidId>
   <imagePath></imagePath>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='scrollable-component']/div/div[2]/div/div/div/div[2]/div[2]/div[7]/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>IMAGE</key>
         <value></value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;scrollable-component&quot;)/div[@class=&quot;aem-Grid aem-Grid--12 aem-Grid--default--12&quot;]/div[@class=&quot;digitalcreatecustomerretaill9module superTypeWidget parbase aem-GridColumn aem-GridColumn--default--12&quot;]/div[@class=&quot;UXFWidget&quot;]/div[@class=&quot;create-customer-retail-l9-module&quot;]/div[@class=&quot;createCustomerRetail widget-background&quot;]/div[@class=&quot;container&quot;]/div[2]/div[@class=&quot;col-12 pt-4 pb-5 border-bottom&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;CustomCheckbox&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>CustomCheckbox  </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;scrollable-component&quot;)/div[@class=&quot;aem-Grid aem-Grid--12 aem-Grid--default--12&quot;]/div[@class=&quot;digitalcreatecustomerretaill9module superTypeWidget parbase aem-GridColumn aem-GridColumn--default--12&quot;]/div[@class=&quot;UXFWidget&quot;]/div[@class=&quot;create-customer-retail-l9-module&quot;]/div[@class=&quot;createCustomerRetail widget-background&quot;]/div[@class=&quot;container&quot;]/div[2]/div[@class=&quot;col-12 pt-4 pb-5 border-bottom&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;CustomCheckbox&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='scrollable-component']/div/div[2]/div/div/div/div[2]/div[2]/div[7]/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Regular Paper Bill'])[1]/following::div[5]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Receive a monthly email notification when your online bill is ready'])[1]/following::div[10]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='terms of notice'])[1]/preceding::div[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div/div</value>
   </webElementXpaths>
</WebElementEntity>
