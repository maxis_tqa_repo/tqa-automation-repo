<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Assign Number</name>
   <tag></tag>
   <elementGuidId>471be5c9-cf3d-40b3-96a9-6a9ce1c27f62</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'pl-2 edit-change-color openSans fs-12' and (text() = 'Assign Number' or . = 'Assign Number')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.custom-border-bottom > div.cart-items > div.col-12.pb-4 > div.item.row.mt-3 > div.col-10 > div.item-head.pb-2 > div.d-flex > div.hyperlink > a.pl-2.edit-change-color.openSans.fs-12</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='scrollable-component']/div/div[3]/div/div/div[2]/div/div[2]/div/div/div[2]/div/div/div/div[2]/div/div[3]/div[2]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>pl-2 edit-change-color openSans fs-12</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Assign Number</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;scrollable-component&quot;)/div[@class=&quot;aem-Grid aem-Grid--12 aem-Grid--default--12&quot;]/div[@class=&quot;digitalshoppingcartretaill9module superTypeWidget parbase aem-GridColumn aem-GridColumn--default--12&quot;]/div[@class=&quot;UXFWidget&quot;]/div[@class=&quot;container shopping-cart-rt widget-background pb-3&quot;]/div[2]/div[1]/div[2]/div[@class=&quot;cart-item-box mt-3&quot;]/div[@class=&quot;bundle-item&quot;]/div[@class=&quot;custom-border-bottom&quot;]/div[@class=&quot;cart-items&quot;]/div[@class=&quot;col-12 pb-4&quot;]/div[@class=&quot;item row mt-3&quot;]/div[@class=&quot;col-10&quot;]/div[@class=&quot;item-head pb-2&quot;]/div[@class=&quot;d-flex&quot;]/div[@class=&quot;hyperlink&quot;]/a[@class=&quot;pl-2 edit-change-color openSans fs-12&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='scrollable-component']/div/div[3]/div/div/div[2]/div/div[2]/div/div/div[2]/div/div/div/div[2]/div/div[3]/div[2]/a</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Assign Number')]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Edit'])[2]/following::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Remove'])[2]/preceding::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Empty Cart'])[1]/preceding::a[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Assign Number']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div[2]/div/div[3]/div[2]/a</value>
   </webElementXpaths>
</WebElementEntity>
