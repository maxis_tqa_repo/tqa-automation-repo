<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_ADD NEW LINE</name>
   <tag></tag>
   <elementGuidId>328d4b75-3046-49d7-8834-4c10e4de288a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='account-overlay']/div[3]/div/div[2]/div/div/div/div[5]/div/div[3]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.new-line.row.mx-0.px-15px.maxis-extraBold.text-blue-green.flex-row.justify-content-between.cursor-pointer.account-to-select-selected.cursor-pointer</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>new-line row mx-0 px-15px maxis-extraBold text-blue-green flex-row
                            justify-content-between cursor-pointer account-to-select-selected cursor-pointer</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>+ADD NEW LINE</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;account-overlay&quot;)/div[@class=&quot;content d-flex col-12 col-md-8 justify-content-center px-0&quot;]/div[@class=&quot;col-12 px-0 mt-3 content-enter-animated&quot;]/div[2]/div[@class=&quot;account-selection&quot;]/div[1]/div[@class=&quot;container mb-5 mb-md-0&quot;]/div[@class=&quot;account-details ml-md-2 false&quot;]/div[@class=&quot;account-details-col col-12 p-0 col-lg-7 m-auto&quot;]/div[@class=&quot;new-line row mx-0 px-15px maxis-extraBold text-blue-green flex-row
                            justify-content-between cursor-pointer account-to-select-selected cursor-pointer&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='account-overlay']/div[3]/div/div[2]/div/div/div/div[5]/div/div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='add new plan'])[1]/following::div[28]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Looking for something else? For help, click to chat'])[2]/preceding::div[7]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CONTINUE TO CART'])[1]/preceding::div[8]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '+ADD NEW LINE' or . = '+ADD NEW LINE')]</value>
   </webElementXpaths>
</WebElementEntity>
