<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Error Business Date invalid</name>
   <tag></tag>
   <elementGuidId>cbe32a86-fe05-4237-b620-dee4b7090f9b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.modal-content.bs-callout.bs-callout-danger</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = '   You got an error! Business Date is invalid. Please do declaration before perform any other transactionsOK' or . = '   You got an error! Business Date is invalid. Please do declaration before perform any other transactionsOK')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='errorBox']/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>e56fbb91-9aab-47c9-beb4-0c8468539100</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>modal-content bs-callout bs-callout-danger</value>
      <webElementGuid>0614e901-969a-44c3-8063-c6f3bf63dd92</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>   You got an error! Business Date is invalid. Please do declaration before perform any other transactionsOK</value>
      <webElementGuid>ed76b70e-fb14-46ad-9b8c-d39bd9044610</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;errorBox&quot;)/div[@class=&quot;modal-dialog&quot;]/div[@class=&quot;modal-content bs-callout bs-callout-danger&quot;]</value>
      <webElementGuid>4d0ffa41-917f-40b5-8508-47248234bb25</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='errorBox']/div/div</value>
      <webElementGuid>7045e3e8-f507-4e3f-8917-447a1ac6e929</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Retailstore'])[1]/following::div[3]</value>
      <webElementGuid>63e35cb6-f6a7-4ae1-8ceb-022bdcc85eae</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pos No.'])[1]/following::div[3]</value>
      <webElementGuid>0ff291cc-b5d4-4012-b4fc-52ed29948313</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//body/div/div/div</value>
      <webElementGuid>0abc3a44-89fc-4b3d-b6fd-e46aea42c06d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
